<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTreatmentTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treatment_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('sub_type_id')->nullable();
            $table->text('desc')->nullable();
            $table->integer('piority')->nullable();
            $table->decimal('price',$precision = 8, $scale = 2)->nullable();
            $table->integer('status')->default(1);
            $table->integer('deleted')->default(1);
            $table->integer('user_id')->nullable();
            $table->integer('clinic_id')->nullable();
            $table->timestamps();

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('treatment_types');
    }
}
