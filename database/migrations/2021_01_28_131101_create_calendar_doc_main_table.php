<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarDocMainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_doc_main', function (Blueprint $table) {
            $table->id();
            $table->date('duty_date')->format('d/m/Y');
            $table->integer('doc_id');
            $table->integer('room_id');
            $table->integer('user_id');
            $table->string('start_time');
            $table->string('end_time');
            $table->integer('patient_num')->default('0');
            $table->integer('status_id')->default('1');
            $table->integer('deleted')->default('1');
            $table->date('create_date')->format('Y/m/d');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar_doc_main');
    }
}
