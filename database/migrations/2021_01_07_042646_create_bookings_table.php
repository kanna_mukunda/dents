<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('treatment_id');
            $table->integer('hn');
            $table->integer('docter_id');
            $table->dateTime('booking_date');
            $table->integer('room_id');
            $table->integer('user_id');
            $table->text('u_line_id');
            $table->integer('deleted')->default(1);
            $table->integer('status_id')->default(1);
            $table->timestamps();

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
    }
}
