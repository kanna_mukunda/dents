<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->integer('patient_id');
            $table->integer('dent_id');
            $table->integer('treatment_id');
            $table->integer('slot_start');
            $table->integer('slot_end');
            $table->enum('period_type',['am', 'pm']);
            $table->enum('status', ['standby', 'reserved', 'processing', 'cancel', 'complete']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job');
    }
}
