<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColunmPhoneUserStatusAndDescInBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->text('desc')->after('room_id');
            $table->enum('user_status',[0,1])->after('room_id');
            $table->string('user_phone')->after('room_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->removeColumn('desc');
            $table->removeColumn('user_status');
            $table->removeColumn('user_phone');
        });
    }
}
