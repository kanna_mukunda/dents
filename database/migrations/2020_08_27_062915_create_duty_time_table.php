<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDutyTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('duty_time', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->integer('dent_id');
            $table->integer('am_slot_start');
            $table->integer('am_slot_end');
            $table->integer('pm_slot_start');
            $table->integer('pm_slot_end');
            $table->integer('last_slot');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('duty_time');
    }
}
