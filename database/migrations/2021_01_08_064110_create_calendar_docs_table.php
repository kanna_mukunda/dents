<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCalendarDocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_doc', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('date_book')->nullable();
            $table->integer('type_id')->nullable();
            $table->integer('doc_id')->nullable();
            $table->integer('deleted')->default(1);
            $table->integer('user_id')->nullable();
            $table->integer('status_id')->default(1);
            $table->integer('clinic_id')->nullable();
            $table->timestamps();

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('calendar_docs');
    }
}
