<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRoomIdColumnAtJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job', function (Blueprint $table){
            $table->integer('room_id')->after('dent_id');
            $table->string('start_time')->default('00:00')->after('dent_id');
            $table->string('end_time')->default('00:00')->after('dent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job', function (Blueprint $table){
            $table->removeColumn('room_id');
            $table->removeColumn('start_time');
            $table->removeColumn('end_time');
        });
    }
}
