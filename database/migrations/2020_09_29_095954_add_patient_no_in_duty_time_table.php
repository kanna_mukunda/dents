<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPatientNoInDutyTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('duty_time', function (Blueprint $table) {
            $table->integer('patient_no_am')->after('am_slot_end');
            $table->integer('patient_no_ev')->after('ev_slot_end');
            $table->integer('patient_no_pm')->after('am_slot_end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('duty_time', function (Blueprint $table) {
            $table->dropColumn('patient_no_am');
            $table->dropColumn('patient_no_ev');
            $table->dropColumn('patient_no_pm');
        });
    }
}
