<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TreatmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            DB::table('treatment')->insert([
                'id' =>1 ,'treatment_name' => 'ปรับ'     , 'treatment_mean_time' => 5  , 'treatment_price' => 100, 'piority' => 3]);
            DB::table('treatment')->insert([
                'id' =>2 ,'treatment_name' => 'ขูดหินปูน' , 'treatment_mean_time' => 10 , 'treatment_price' => 100, 'piority' => 2]);
            DB::table('treatment')->insert([
                'id' =>3 ,'treatment_name' => 'ถอนฟัน'  , 'treatment_mean_time' => 20 , 'treatment_price' => 100, 'piority' => 1]);
            DB::table('treatment')->insert([
                'id' =>4 ,'treatment_name' => 'อุดฟัน'  , 'treatment_mean_time' => 20 , 'treatment_price' => 100, 'piority' => 1]);
            DB::table('treatment')->insert([
                'id' =>5 ,'treatment_name' => 'จัดฟัน'  , 'treatment_mean_time' => 20 , 'treatment_price' => 100, 'piority' => 1]);
            DB::table('treatment')->insert([
                'id' =>6 ,'treatment_name' => 'รักษารากฟัน'  , 'treatment_mean_time' => 20 , 'treatment_price' => 100, 'piority' => 1]);
            DB::table('treatment')->insert([
                'id' =>7 ,'treatment_name' => 'ฟันปลอม'  , 'treatment_mean_time' => 20 , 'treatment_price' => 100, 'piority' => 1]);
            DB::table('treatment')->insert([
                'id' =>8,'treatment_name' => 'ทำครอบฟัน'  , 'treatment_mean_time' => 20 , 'treatment_price' => 100, 'piority' => 1]);
    }
}

