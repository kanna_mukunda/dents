<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('job')->insert(
            ['id'=> 1,'date' => '2020-08-26', 'patient_id' => 1, 'dent_id' => 1, 'treatment_id' => 1, 
                'slot_start' => 100, 'slot_end' => 100, 'period_type' => 'am', 'status' => 'reserved']
        );

        DB::table('job')->insert(
            ['id'=> 2,'date' => '2020-08-26', 'patient_id' => 1, 'dent_id' => 2, 'treatment_id' => 1, 
                'slot_start' => 120, 'slot_end' => 120, 'period_type' => 'am', 'status' => 'reserved']
        );
        DB::table('job')->insert(
            ['id'=> 3,'date' => '2020-08-26', 'patient_id' => 2, 'dent_id' => 3, 'treatment_id' => 2, 
                'slot_start' => 5, 'slot_end' => 10, 'period_type' => 'am', 'status' => 'reserved']
        );
        DB::table('job')->insert(
            ['id'=> 4,'date' => '2020-08-26', 'patient_id' => 1, 'dent_id' => 1, 'treatment_id' => 1, 
                'slot_start' => 5, 'slot_end' => 5, 'period_type' => 'am', 'status' => 'reserved']
        );
        DB::table('job')->insert(
            ['id'=> 5,'date' => '2020-08-26', 'patient_id' => 3, 'dent_id' => 1, 'treatment_id' => 3, 
                'slot_start' => 15, 'slot_end' => 30, 'period_type' => 'am', 'status' => 'reserved']
        );
    }
}
