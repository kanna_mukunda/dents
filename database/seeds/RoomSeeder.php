<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('room')->insert([
            'id' => 1,
            'room_name' => 'D1-01',
            'floor' => '1',
            'status' => 'active',
        ]);
        DB::table('room')->insert([
            'id' => 2,
            'room_name' => 'D1-02',
            'floor' => '1',
            'status' => 'active',
        ]);
        DB::table('room')->insert([
            'id' => 3,
            'room_name' => 'D1-03',
            'floor' => '1',
            'status' => 'active',
        ]);
        DB::table('room')->insert([
            'id' => 4,
            'room_name' => 'D2-01',
            'floor' => '1',
            'status' => 'active',
        ]);
        DB::table('room')->insert([
            'id' => 5,
            'room_name' => 'D2-02',
            'floor' => '1',
            'status' => 'active',
        ]);
    }
}
