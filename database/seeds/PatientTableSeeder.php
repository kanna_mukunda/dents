<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
class PatientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('patient')->insert([
            'id' => 1,
            'name' => Str::random(10),
            'phone' => '0993332323',
            'hn' => '12'. Str::random(1),
        ]);
        DB::table('patient')->insert([
            'id' => 2,
            'name' => Str::random(10),
            'phone' => '0993332323',
            'hn' => '12'. Str::random(1),
        ]);
        DB::table('patient')->insert([
            'id' => 3,
            'name' => Str::random(10),
            'phone' => '0993332323',
            'hn' => '12'. Str::random(1),
        ]);
      
    }
}
