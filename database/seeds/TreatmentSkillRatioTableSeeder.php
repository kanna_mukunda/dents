<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TreatmentSkillRatioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('treatment_skill_ratio')->insert(['id' => 1, 'dent_id'=> 1, 'treatment_id'=> 1, 'ratio'=> 1]);
        DB::table('treatment_skill_ratio')->insert(['id' => 2, 'dent_id'=> 1, 'treatment_id'=> 2, 'ratio'=> 2]);
        DB::table('treatment_skill_ratio')->insert(['id' => 3, 'dent_id'=> 1, 'treatment_id'=> 3, 'ratio'=> 3]);
        DB::table('treatment_skill_ratio')->insert(['id' => 4, 'dent_id'=> 2, 'treatment_id'=> 1, 'ratio'=> 1]);
        DB::table('treatment_skill_ratio')->insert(['id' => 5, 'dent_id'=> 2, 'treatment_id'=> 2, 'ratio'=> 2]);
        DB::table('treatment_skill_ratio')->insert(['id' => 6, 'dent_id'=> 3, 'treatment_id'=> 1, 'ratio'=> 1]);
      
    }
}
