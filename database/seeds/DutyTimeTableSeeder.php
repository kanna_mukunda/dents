<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DutyTimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

          DB::table('duty_time')->insert(
              ['id'=> 1,'date'=> '2020-08-26', 'dent_id' => 1, 'am_slot_start' =>  5, 'am_slot_end' => 120, 
                    'pm_slot_start' => 0, 'pm_slot_end' => 0, 'room_id' => 1, 'last_slot'=>  5]);
          DB::table('duty_time')->insert(
              ['id'=> 2,'date'=> '2020-08-26', 'dent_id' => 2, 'am_slot_start' =>  5, 'am_slot_end' => 120, 
                    'pm_slot_start' => 0, 'pm_slot_end' => 0, 'room_id' => 1, 'last_slot'=>  5]);
          DB::table('duty_time')->insert(
              ['id'=> 3,'date'=> '2020-08-26', 'dent_id' => 3, 'am_slot_start' => 5, 'am_slot_end' => 120, 
                    'pm_slot_start' => 0, 'pm_slot_end' => 0, 'room_id' => 2,'last_slot'=> 5]);
          DB::table('duty_time')->insert(
              ['id'=> 4,'date'=> '2020-08-27', 'dent_id' => 1, 'am_slot_start' =>  5, 'am_slot_end' =>  90, 
                    'pm_slot_start' => 0, 'pm_slot_end' => 0,'room_id' => 2, 'last_slot'=>  5]);
       
      
    }
}
