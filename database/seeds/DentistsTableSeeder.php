<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DentistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dentist')->insert([
            'id'=> 1,
            'dent_name' => 'หมอม่อน',
            'phone' => '0993332323',
        ]);

        DB::table('dentist')->insert([
            'id'=> 2,
            'dent_name' => 'หมอโย',
            'phone' => '0993332323',
        ]);

        DB::table('dentist')->insert([
            'id'=> 3,
            'dent_name' => 'หมอเอ',
            'phone' => '0993332323',
        ]);
        DB::table('dentist')->insert([
            'id'=> 4,
            'dent_name' => 'หมอแทน',
            'phone' => '0993332323',
        ]);
    }
}
