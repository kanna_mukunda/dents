<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $table = 'calendar';


    public function dentist(){
        return $this->belongsTo('App\models\Dentist', 'dent_id', 'id');
    }
}
