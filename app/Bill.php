<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $table = 'bill';

    public function patient(){
        return $this->belongsTo('App\models\Patient', 'hn', 'hn');
    }

    public function treatment(){
        return $this->belongsTo('App\Treatment', 'treat', 'id');
    }

    public function dentist(){
        return $this->belongsTo('App\models\Dentist', 'dent', 'id');
    }
}
