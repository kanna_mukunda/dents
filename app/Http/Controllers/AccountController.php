<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bill;

class AccountController extends Controller
{
    public function invoice($bill_no){
        $invoices = Bill::where('bill', $bill_no)
                ->with('patient', 'dentist', 'treatment')
                ->get();
        return view('account.invoice', compact('invoices'));
    }

    public function invoice_print($bill_no){
        $invoices = Bill::where('bill', $bill_no)
                ->with('patient', 'dentist', 'treatment')
                ->get();
        return view('account.invoice_print', compact('invoices'));
    }

    public function store(REQUEST $request){
        //หา bill id ว่าสุด
        $bill_last = Bill::all()->last();
        
        $bill_no = 0;
        if(collect($bill_last)->count() == 0){
            $bill_no = 1;
        }else{
            $bill_no = $bill_last->bill + 1;
        }
    
       
        foreach($request->get('detail_arr') as $details){
            $bill = new Bill();
            $bill->bill = $bill_no;
            $bill->hn   = $request->get('hn');
            $bill->date = date('Y-m-d');
            $bill->treat = $details['treatment_id'];
            $bill->detail = $details['detail'];
            $bill->t_amount = $details['amount'];
            $bill->cost = $details['price'] * $details['amount'];
            $bill->dent = $request->get('dent_id');
            $bill->pay_dent = 0;
            $bill->save();
        }
        
        return \response()->json($bill_no);
    }
}
