<?php

namespace App\Http\Controllers\settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\settings\PatientPerDay;
use App\Http\Controllers\DateTimeFormatController;
class PatientPerDayController extends Controller
{
    public function index(){
        $dateFormatCtl = new DateTimeFormatController();
        $patient_per_day = PatientPerDay::where('date', date('Y-m-d'))->get();
        foreach ($patient_per_day as $patient){
            $patient->date = $dateFormatCtl->thaiDateFormat($patient->date);
            $patient->start_period = $patient->start_period < 10 ? '0'.$patient->start_period." : 00" : $patient->start_period." : 00";
            $patient->end_period = $patient->end_period < 10 ? '0'.$patient->end_period." : 00" : $patient->end_period." : 00";

        }
        return view('settings.patient_per_day.index', compact('patient_per_day'));
    }

    public function create(){
        return view('settings.patient_per_day.create');
    }

    public function store(Request $request){
        $patient_per_day = new PatientPerDay;
        $patient_per_day->date = $request->get('y')."-".$request->get('m')."-".$request->get('d');
        $patient_per_day->start_period = $request->get('from') == "all" ? "9" : $request->get('from');
        $patient_per_day->end_period = $request->get('to') == "all" ? "20" : $request->get('to');
        $patient_per_day->patient_no = $request->get('patient_no');
        $patient_per_day->created_at = date('Y-m-d H:i:s');
        $patient_per_day->updated_at = date('Y-m-d H:i:s');
        $patient_per_day->save();

        // return redirect('settings/patient_per_day/index');
        return response()->json('ok');
    }
}
