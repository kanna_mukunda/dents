<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Dentist;
use App\Models\Patient;
use App\Treatment;
use Illuminate\Http\Request;
use App\Http\Controllers\DateTimeFormatController;

class JobController extends Controller
{
    public function index($date = null)
    {
        $dateTimeFormatCtl          = new DateTimeFormatController();

        $date = $date == null ? date('Y-m-d') : $date;
        $jobs = Job::where('date', $date)
            ->where('status', '!=', 'cancel')
            ->with('dentist.dutyTime.room', 'treatment')
            ->get();
        foreach ($jobs as $job) {
            if($job->status == 'reserved'){
                $job->thStatus = 'นัดหมอแล้ว';
                $job->btn_style = 'btn-warning';
            }else if($job->status == 'cancel'){
                $job->thStatus = 'ยกเลิก';
                $job->btn_style = 'btn-default';
            }else if($job->status == 'complete'){
                $job->thStatus = 'รักษาเสร็จแล้ว';
                $job->btn_style = 'btn-success';
            }
            else if($job->status == 'processing'){
                $job->thStatus = 'กำลังทำการรักษา';
                $job->btn_style = 'btn-primary';
            }
            else if($job->status == 'standby'){
                $job->thStatus = 'มาตามนัด';
                $job->btn_style = 'btn-info';
            }
            else if($job->status == 'pospone'){
                $job->thStatus = 'เลืื่อนนัด';
                $job->btn_style = 'btn-fuchsia';
            }
            $job->thDate = $dateTimeFormatCtl->thaiDateFormat($job->date);
            $job->treatment_time = $this->convertSlotToTime($job->slot_start, $job->slot_end, $job->period_type);
        }
        // return $jobs;
        return view('job.index', compact('jobs'));
    }

    public function convertSlotToTime($slot_start, $slot_end, $period_type){
        $hourStart = 0;
        $hourEnd = 0;
        $nextHour = (($slot_start * 5)/60)%10;
        $lastHour = (($slot_end * 5)/60)%10;
        if($period_type == 'am'){
            $hourStart  = 9 + $nextHour;
            $hourEnd    = 9 + $lastHour;
        }else if($period_type == 'ev'){
            $hourStart  = 13 + $nextHour;
            $hourEnd    = 13 + $lastHour;
        }else if($period_type == 'pm'){
            $hourStart  = 17 + $nextHour;
            $hourEnd    = 17 + $lastHour;
        }
        $minStart = ($slot_start*5)%60;
        $minEnd = (($slot_start*5)%60) + 5;

        $hourStart = $hourStart < 10 ? "0".$hourStart : $hourStart;
        $hourEnd = $hourEnd < 10 ? "0".$hourEnd : $hourEnd;
        $minStart = $minStart < 10 ? "0".$minStart : $minStart;
        $minEnd = $minEnd < 10 ? "0".$minEnd : $minEnd;
        return $hourStart.":".$minStart." - ".$hourEnd.":".$minEnd;

    }

    public function postpone_appointment(){
        $dateTimeFormatCtl          = new DateTimeFormatController();

        $postpone_appointment = Job::where('date', 'Like', '%-'.date('m').'-%')
                ->with('dentist.dutyTime.room', 'treatment')
                ->orderBy('date', 'asc')
                ->get();

        foreach ($postpone_appointment as $job) {
            if($job->status == 'reserved'){
                $job->thStatus = 'นัดหมอแล้ว';
            }
            $job->thDate = $dateTimeFormatCtl->thaiDateFormat($job->date);
            $job->treatment_time = $this->convertSlotToTime($job->slot_start, $job->slot_end, $job->period_type);
        }
        return view('job.postpone_appointment', compact('postpone_appointment'));
    }

    public function create()
    {
        $dentists = Dentist::all();
        $treatments = Treatment::all();
        $patients =    Patient::all();
        return view('job.create', compact('dentists', 'treatments', 'patients'));
    }

    public function store(Request $request)
    {
        $job = new Job();
        $job->dent_id        = $request->get('dent_id');
        $job->date   = $request->get('date');
        $job->patient_id = $request->get('patient_id');
        $job->treatment_id = $request->get('treatment_id');
        $job->slot_start = 5;
        $job->slot_end = 10;
        $job->period_type = "am";
        $job->status  = 'reserved';
        $job->save();

        return redirect('job/index');
    }

    public function edit($jobId)
    {
        $job = Job::find($jobId);
        $dentists = Dentist::all();
        $treatments = Treatment::all();
        $patients =    Patient::all();
        return view('job.edit', compact('job','dentists', 'treatments', 'patients'));

    }

    public function invoice_print($invoice_id){
        echo 'sdfdsf';
        return view('job.invoice_print');
    }

    public function update(Request $request, $jobId)
    {
        $job = Job::find($jobId);
        $job->dent_id        = $request->get('dent_id');
        $job->date   = $request->get('date');
        $job->patient_id = $request->get('patient_id');
        $job->treatment_id = $request->get('treatment_id');
        $job->slot_start = 5;
        $job->slot_end = 10;
        $job->period_type = "am";
        $job->status  = 'reserved';
        $job->save();
        return redirect('job/index');
    }

    public function update_status(Request $request){
        $job = Job::find($request->get('job_id'));
        $job->status = $request->get('status');
        $job->comment = $request->get('comment');
        $job->save();
        return response()->json($job->status);
    }

    public function dent_job($dent_id){
        $dent = Dentist::find($dent_id);
        return view('job.dent_job', \compact('dent'));
    }

    public function treatment_calculate_table($date = null){
        $date = $date == null ? date('Y-m-d') : $date;

        $jobs = Job::where('date', $date)
                    ->with('dentist', 'treatment', 'patient')
                    ->get();
                    // where('status', 'processing')
                    // ->orWhere('status', 'complete')
        return view('job.cal.index', compact('jobs'));
    }

    public function treatment_calulate($jobId){

        return view('job.cal.create');
    }


    public function destroy($jobId)
    {
        $job = Job::find($jobId);
        $job->delete();
        return redirect('job/index');
    }



}
