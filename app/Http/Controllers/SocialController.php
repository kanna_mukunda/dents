<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Socialite;

class SocialController extends Controller
{
    public function redirect()
    {
     return Socialite::driver("line")->redirect();
    }

    public function Callback()
    {
        $userSocial =   Socialite::driver("line")->stateless()->user();
        dd($userSocial);
        $users       =   User::where(['email' => $userSocial->getEmail()])->first();
        if($users){
            Auth::login($users);
            return redirect('/');
        }else{
            $user = User::create([
                    'name'          => $userSocial->getName(),
                    'email'         => $userSocial->getEmail(),
                    'image'         => $userSocial->getAvatar(),
                    'provider_id'   => $userSocial->getId(),
                    'provider'      => $provider,
                ]);
            return redirect()->route('home');
        }
    }
}