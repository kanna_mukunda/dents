<?php

namespace App\Http\Controllers\Booking;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Treat;
use App\Models\Patient;
use App\Models\Booking;
use App\Models\CalendarDoc;
use App\Models\Dentist;
use App\Models\TreatmentSkillRatio;
use Illuminate\Http\Request;

class BookingController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $treatment_id = Treat::where('treatment_name', 'LIKE', "%$keyword%")->get('id')->first();
            $docter_id = Dentist::where("dent_name", 'LIKE', "%$keyword%")->get('id');

            $hn = Patient::where('f_name', 'LIKE', "%$keyword%")->get('hn')->first();
            $booking = Booking::where('treatment_id', collect($treatment_id)->isEmpty()   ? 0 : $treatment_id->id)

                ->orWhere('hn', collect($hn)->isEmpty()   ? 0 : $hn->hn)
                ->orWhere('docter_id', '=', collect($docter_id)->isNotEmpty() ? $docter_id[0]->id : '')
                ->orWhere('booking_date', 'LIKE', "%$keyword%")
                ->orWhere('room_id', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->orWhere('u_line_id', 'LIKE', "%$keyword%")
                ->orWhere('deleted', 'LIKE', "%$keyword%")
                ->orWhere('status_id', 'LIKE', "%$keyword%")
                ->orWhere('user_phone', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $booking = Booking::where('status_id', 2)
                ->with('patient')
                ->orWhere('status_id', 3)
                ->orWhere('status_id', 4)->latest()->paginate($perPage);
        }

        return view('booking.booking.index', compact('booking'));
    }

    public function create()
    {
        $treats = Treat::all();

        return view('booking.booking.create', \compact('treats'));
    }


    public function store(Request $request)
    {
        $validated = $request->validate(
            [
                'fullname' => 'required',
                'user_phone' => 'required',
                'booking_date' => 'required',
                'treatment_id' => 'required',
                'user_status' => 'required',
            ],
            [
                'fullname.required' => 'กรุณาใส่ ชื่อ-นามสกุล ของท่าน',
                'user_phone.required' => 'กรุณาใส่ เบอร์โทรศัพท์ ของท่าน',
                'booking_date.required' => 'กรุณาเลือกวัน และ วัด ที่ต้องการนัด',
                'treatment_id.required' => 'กรุณาเลือกประเภทการรักษา',
                'user_status.required' => 'กรุณาเลือกว่าเคยรักษาที่คลินิคหรือไม่',
            ]
        );
        $request['u_line_id'] = $request->get('lineid');
        $request['status']  = 2;
        $requestData = $request->all();
        $bookingSave = Booking::create($requestData);


        return redirect('booking/booking')->with('flash_message', 'Booking added!');
    }

    public function show($id)
    {
        $booking = Booking::findOrFail($id);

        return view('booking.booking.show', compact('booking'));
    }

    public function edit($id)
    {
        $booking = Booking::findOrFail($id);

        $treats = Treat::all();

        return view('booking.booking.edit', compact('booking', 'treats'));
    }


    public function update(Request $request, $id)
    {
        $request['status_id'] = 1;
        $requestData = $request->all();
        $booking = Booking::findOrFail($id);
        $booking->update($requestData);

        return redirect('booking/booking/' . $booking->id)->with('flash_message', 'Booking updated!');
    }

    public function destroy($id)
    {
        Booking::destroy($id);

        return redirect('booking/booking')->with('flash_message', 'Booking deleted!');
    }

    public function appointmentconfirm($id)
    {
    }

    public function complete($id)
    {

        $booking = Booking::find($id);
        if (!$booking) {
            return redirect('booking/booking')->with('flash_message', 'Booking updated!');
        }
        $booking->update(['status_id' => 2]);
        return view('booking.booking.bookingcomplete');
    }


    public function emp_create()
    {
        $treats = Treat::all();
        $patients = Patient::get(['hn', 'f_name', 'l_name']);

        return view('booking.booking.emp_create', \compact('treats', 'patients'));
    }

    public function emp_store(Request $request)
    {
        $validated = $request->validate([
                'user_phone' => 'required',
                'appointment_date' => 'required|not_in:0',
                'appointment_time' => 'required|not_in:0',
                'doc_id' => 'required|not_in:0',
                'treatment_id' => 'required|integer',
                'hn' => 'required'
            ],
            [
                'user_phone.required' => 'กรุณาใส่ เบอร์โทรศัพท์ ของท่าน',
                'appointment_date.required' => 'กรุณาเลือกวันที่ต้องการนัด',
                'appointment_time.required' => 'กรุณาเลือกเวลาที่ต้องการนัด',
                'treatment_id.required' => 'กรุณาเลือกประเภทการรักษา',
                'doc_id.required' => 'กรุณาเลือกหมอ',
                'hn.required' => 'กรุณาเลือกลูกค้า',
            ]
        );
        //หาเวลาว่างของหมอ
        $calendar_docs =  CalendarDoc::where('doc_id', $request->get('doc_id'))
            ->where('type_id', $request->get('treatment_id'))
            ->where('status_id', 1)
            ->get()->first();
        // return $request->get('appointment_date');
        //save  ใน booking
        $dates = explode('/', $request->get('appointment_date'));
        $request['booking_date'] = $dates[2] . '-' . $dates[1] . '-' . $dates[0] . ' ' . $calendar_docs->a_time;
        $request['fullname'] = 'xxx';
        $request['docter_id'] = $request->get('doc_id');
        $request['calendar_doc_id'] = $calendar_docs->id;
        $request['u_line_id'] = $request->get('lineid');
        $request['status_id']  = 3;
        $requestData = $request->all();
        $bookingSave = Booking::create($requestData);

        //ทำการ update calendar doc row ที่จองให้ status = 2
        $calendar_doc_selected = CalendarDoc::find($calendar_docs->id);//
        $calendar_doc_selected->status_id = 2;
        $calendar_doc_selected->save();

        //หาเวลา a_time ของตัวที่เลือก + ratio ของ type ที่ถูกเลือก
        $current_type = TreatmentSkillRatio::where('dent_id', $calendar_doc_selected->doc_id)
                        ->where('treatment_id', $calendar_doc_selected->type_id)
                        ->get()->first();

        //บวกนาทีกับ ratio time
        $next_time = $this->findNextAtime($calendar_doc_selected->a_time, $current_type->ratio);

        //หา type อื้น ของหมอคนเดียวกัน ที่อยู่ในวันเดียวกันกับ $currentDate ที่เวลาอยู่ในช่วง  <= $next_time   >= $a_time_select
        $this->updateInterValTimeOfOtherTypeId($calendar_doc_selected, 
                                                $request->get('doc_id'), 
                                                $request->get('treatment_id'), 
                                                $next_time );

        $other_type_of_docs = TreatmentSkillRatio::where('dent_id', $calendar_doc_selected->doc_id)
                ->where('treatment_id', '!=',$request->get('treatment_id'))
                ->get();  
        
        foreach($other_type_of_docs as $other){
           $this->checkNextActiveAtime($other->treatment_id, $calendar_doc_selected, $current_type->ratio);
        }
                                
    //    dd('sss');
        return redirect('booking/booking/emp_show/'.$bookingSave->id)->with('flash_message', 'Booking added!');
    }

    private function checkNextActiveAtime($type_id , $calendar_doc_selected, $ratio){
        $book_date = explode('|', $calendar_doc_selected->book_date);
        
        $other = CalendarDoc::where('doc_id', $calendar_doc_selected->doc_id)
            ->with('treat.treatmentSkillRatio')
            ->where('date_book', 'LIKE', $book_date[0].'%')
            ->where('type_id', $type_id)
            ->where('status_id', 1)->get()->first();
            $other_time = $this->explodeTime($other->a_time );
            
        $otherPlusRatio = $this->findNextAtime($other->a_time, $other->treat->treatment_mean_time);

        // return$calendar_doc_selected_time = $this->explodeTime($calendar_doc_selected->a_time );
        if($otherPlusRatio > $calendar_doc_selected->a_time && $other->a_time <= $calendar_doc_selected->a_time){
            $update = calendarDoc::where('id', $other->id)
                ->update(['status_id'=> 2 ]);
            $this->checkNextActiveAtime($type_id , $calendar_doc_selected, $ratio);
        }else{
            return;
            // exit();
        }
    }

    private function explodeTime($time){
        $time = explode(":", $time);
        $min_temp = $time[1][0]  == 0 ? $time[1][1] : $time[1];
        $hr_temp = $time[0]  == '09' ? $time[0][1] : $time[0];
        return ['hr' => $time[0], 'hr_temp'=> $hr_temp, 'min' => $time[1], 'min_temp' => $min_temp];
    }

    public function emp_show($id)
    {
        $booking = Booking::where('id', $id)
            ->with('treat', 'patient', 'calendar_doc', 'booking_status')
            ->get()->first();

        return view('booking.booking.emp_show', compact('booking'));
    }

    public function emp_edit($id)
    {
        $booking = Booking::where('id', $id)
            ->with('calendar_doc')
            ->get()->first();

        $calendar_docs = $this->get_calendar_doc($booking->calendar_doc->type_id);
        $treats = Treat::all();
        $patients = Patient::get(['hn', 'f_name', 'l_name']);

        return view('booking.booking.emp_edit', compact('booking', 'treats', 'patients', 'calendar_docs'));
    }


    public function emp_update(Request $request, $id)
    {
        $booking= Booking::where('id', $id);
        $oldBooking = $booking->first();

         // //หาเวลาที่ว่างที่เวลา >= ที่ request มา
         return$calendar_doc =  CalendarDoc::where('doc_id', $request->get('doc_id'))
            ->where('date_book', 'LIKE', $request->get('appointment_date').'%')
            ->where('type_id', $request->get('treatment_id'))
            ->where('a_time', '>=', $request->get('appointment_time'))
            ->where('status_id' , 1)->get();

            return$calendar_doc_update = $calendar_doc->update(['status_id' => 2]);

         
        //-หา ratio
        $ratio_old_booking = TreatmentSkillRatio::where('dent_id', $oldBooking->docter_id)
                        ->where('treatment_id', $oldBooking->treatment_id)
                        ->get('ratio')->first();
        //-update  calendarDoc เวลาเดิม ที่ ถูกบวกเข้าไปกับ ratio time ให้ status_id = 1
        $a_time_from_booking_date = explode(" ", $oldBooking->booking_date)[1];
        $old_a_time =  explode(':', $a_time_from_booking_date);
        $old_init_atime = $old_a_time[0].":".$old_a_time[1];
        $old_next_atime =  $this->findNextAtime($old_init_atime, $ratio_old_booking->ratio);

        $oldCalendarDocTemp =  CalendarDoc::where('a_time', $old_init_atime)
                ->where('a_time', '<', $old_next_atime)
                ->update(['status_id'=> 7]);
                
        //update เวลาว่างถัดไปที่สามารถจองได้ status_id= 2 และ update เวลาของ typ_id อื่น ที่ถูก Type_id ที่เลือกกินเวลาไป
        $oldCalendarDocOtherTypeID = CalendarDoc::where('doc_id', $request->get('doc_id'))
                ->where('date_book', 'LIKE', $request->get('appointment_date').'%')
                ->where('type_id', $request->get('treatment_id'))
                ->where('a_time', $old_init_atime)
                ->where('a_time', '<', $old_next_atime)
                ->update(['status_id'=> 7]);
                
        $test = CalendarDoc::where('doc_zid', $request->get('doc_id'))
                ->where('date_book', 'LIKE', $request->get('appointment_date').'%')
                ->where('type_id', $request->get('treatment_id'))
                ->where('status_id', 1)->get()->first();
       
                    
        $calendar_doc_get_id = $calendar_doc->first();
        // $calendar_doc_update = $calendar_doc->update(['status_id' => 2, 'booking_date' => $calendar_doc-]);

        //update Booking ->calendar_doc_id และ booking_date
        $bookin_calendar_doc_id = $booking->update([
            'calendar_doc_id' => $calendar_doc_get_id->id,
            'booking_date' => date('d-m-Y', strtotime($request->get('appointment_date'))) . " " . $calendar_doc_get_id->a_time . ":00",

        ]);

        // check appointment_time กับ a_time ก่อนว่า เหมือนกันไหม
        // ถ้าไม่เหมือน ให้หา row ที่ว่าง ใน   appointment_time  แล้ว update status_id =2

        $booking = Booking::where('id', $id)
            ->with('calendar_doc')
            ->get()->first();
        $appointment_time = explode(':', $request->get('appointment_time'));
        $a_time = explode(':', $booking->calendar_doc->a_time);
        $change = false;
        if ($appointment_time[0] != $a_time[0]) {
            //หา เวลาแรกที่ว่า ณ ชั่วโมงนั้น
            // $newBooking = Booking::where() 
        } else {
            $a_time_min_temp = $a_time[1][0] == 0 ? $a_time[1][1] : $a_time[1];
            $appointment_time_min_temp = $appointment_time[1][0] == 0 ? $appointment_time[1][1] : $appointment_time[1];
            $minAbs = abs($a_time_min_temp - $appointment_time_min_temp);

            if ($minAbs >= 30) {
                $change = true;
            } //
        }


        //a_time update status_id =1
        $request['status_id'] = 1;
        $requestData = $request->all();
        $booking = Booking::findOrFail($id);
        $booking->update($requestData);

        return redirect('booking/booking/emp_show' . $booking->id)->with('flash_message', 'Booking updated!');
    }

    public function emp_destroy($id)
    {
        $destroy = Booking::find($id);
        $destroy->status_id = 4;
        $destroy->save();
        return redirect('booking/booking')->with('flash_message', 'Booking deleted!');
    }


    public function get_calendar_doc($typ_id)
    {
        $calendar_docs =  CalendarDoc::where('type_id', $typ_id)->get();
        //get เฉพาะ date book ที่ มากกว่า เท่ากับ วันที่กำลังทำการเลือก
        // $avilableDate = [];
        $docsAvailable = [];
        $avilableDate = collect($calendar_docs)->filter(function ($val) {
            $dateTimeArr = explode('|', $val->date_book);
            if ($dateTimeArr[0] >= date('d/m/Y')) {

                return $val;
            }
        });
        $doc_grouped = collect($avilableDate)->groupBy('doc_id');

        foreach ($doc_grouped  as $key => $val) {
            //ดึงข้อมูลหมอมาแสดง
            $doc = Dentist::find($key);

            array_push($docsAvailable, ['id' => $doc->id, 'dent_name' => $doc->dent_name]);
        }
        return $docsAvailable;
    }

    public function get_date_of_doc($doc_id)
    {
        $calendar_docs =  CalendarDoc::where('doc_id', $doc_id)
            ->where('status_id', 1)
            ->get();
        //groupBy date
        $date_book_grouped = collect($calendar_docs)->groupBy('date_book');
        $availableDate = [];
        foreach ($date_book_grouped as $key => $val) {
            // return$val;
            $dateTimeArr = explode('|', $key);

            if ($dateTimeArr[0] >= date('d/m/Y') && !\in_array($dateTimeArr[0], $availableDate)) {
                array_push($availableDate, $dateTimeArr[0]);
            }
        }
        return $availableDate;
    }

    public function get_time_by_date_and_doc($date, $month, $year, $doc_id, $treatment_id, $id){
        $calendar_docs =CalendarDoc::where('doc_id', $doc_id)
                        ->where('type_id', $treatment_id)
                        ->where('date_book', 'LIKE', $date.'/'.$month.'/'.$year.'%')
                        ->where('status_id' , 1)
                        ->get();

        $test = $id == 0 ? CalendarDoc::find($calendar_docs[0]->id) : CalendarDoc::find($id);
        
        $date_book_grouped = collect($calendar_docs)->groupBy('date_book');
        $timeArray = [];

        $text = '<option>เลือก...</option>';
        foreach ($date_book_grouped as $key => $vals) {
            //เอา a_time มาแสดง
            foreach ($vals as $val) {

                $a_time_selected_array =  explode(":", $val->a_time);
                //set ชั่วโมง หลังจากบวกนาที
                $next_min_temp = $a_time_selected_array[1][0] == 0 ? $a_time_selected_array[1][1] : $a_time_selected_array[1];
                $next_hr = "";
                if ($a_time_selected_array[0] == '09') {
                    $next_hr = $next_min_temp >= 60 ? $a_time_selected_array[0][1] + 1 : $a_time_selected_array[0];
                } else {
                    $next_hr = $next_min_temp >= 60 ? $a_time_selected_array[0] + 1 : $a_time_selected_array[0];
                }

                $next_min = $next_min_temp < 10 ? "0" . $next_min_temp : $next_min_temp;
                //เวลา ที่จองได้ถัดไป
                $next_time = $next_hr . ":" . $next_min;
                if ($next_min_temp >= 0 && $next_min_temp < 30) {
                    array_push($timeArray, $next_hr . ":00");
                } elseif ($next_min_temp >= 30 && $next_min_temp < 60) {
                    array_push($timeArray, $next_hr . ":30");
                } else {
                    $text .= "<option value='" . $next_time . "'>" . $next_time . "</option>";
                }
            }
        }

        return collect(array_unique($timeArray))->flatten();
    }


    public function get_docs_by_date($month = null, $date = null, $year = null)
    {

        $calendar_docs =  CalendarDoc::where('date_book', 'LIKE', $month . '/' . $date . '/' . $year . '%')
            ->where('status_id', '!=', 2)
            ->with('dentist')
            ->get()
            ->groupBy('doc_id');

        $text = '';
        foreach ($calendar_docs as $docs) {
            $text .= "<div class='card'>";
            $text .= "<div class='card-header doc_name'>";

            // $text.= "<h4>".$docs[0]['dentist']['dent_name']."</h4>";
            $text .= "</div>";

            $text .= "<div class='card-body row'>";
            $text2 = "";

            foreach ($docs as $doc) {
                $dateTimeArr = explode('|', $doc->date_book);
                $int  = explode(':', $dateTimeArr[1])[0];
                $end  = explode(':', $dateTimeArr[2])[0];
                for ($i = $int; $i <= $end; $i++) {
                    // $text.="<div class='btn btn-success btn-sm mr-1 col-md-3 col-xs-3'>";
                    $text2 .= "<option value='" . $i . ":00'>";
                    $text2 .= $i . ":00";
                    $text2 .= "</option>";
                    if ($i != $end) {
                        $text2 .= "<option value='" . $i . ":00'>";
                        $text2 .= $i . ":30";
                        $text2 .= "</option>";
                    }

                    // $text.="</div>";

                }
            }
            $text .= "<select class='form-control doc_time' disabled>";
            $text .= $text2;
            $text .= '</select>';
            $text .= "</div>";
            $text .= "</div>";
        }

        return $text;
    }

    public function get_user_info($hn)
    {
        return $user = Patient::where('hn', $hn)->get()->first();
    }

    private function findNextAtime($a_time, $ratio){
        $a_time_selected_array =  explode(":", $a_time);
        //set ชั่วโมง หลังจากบวกนาที
        $next_min_temp1 = $a_time_selected_array[1][0] == 0 ? $a_time_selected_array[1][1] : $a_time_selected_array[1];
        $next_min_temp = $next_min_temp1 + $ratio;
        $next_hr = "";
        if($a_time_selected_array[0] == '09'){
            $next_hr = $next_min_temp >= 60 ? $a_time_selected_array[0][1] + 1 : $a_time_selected_array[0];
        }else{
            $next_hr = $next_min_temp >= 60 ? $a_time_selected_array[0] +1 : $a_time_selected_array[0];
        }
        $next_min_temp = $next_min_temp >= 60 ? $next_min_temp - 60 : $next_min_temp; 
        $next_min = $next_min_temp  < 10 ? "0".$next_min_temp : $next_min_temp;
        //เวลา ที่จองได้ถัดไป
        return $next_hr.":".$next_min;
    }

    private function updateInterValTimeOfOtherTypeId($calendar_doc_selected, $doc_id, $selected_type_id, $next_time){
        $currentDate = explode('|', $calendar_doc_selected->date_book)[0]; //เอาวันที่
        $otherTypes = CalendarDoc::where('date_book', 'LIKE', $currentDate.'%')
                        ->where('doc_id', $doc_id)
                        ->where('type_id', '!=', $selected_type_id)
                        ->where('a_time',  '<', $next_time)
                        ->where('a_time',  '>=', $calendar_doc_selected->a_time)
                        ->where('status_id' , 1);
        $otherTypes->update(['status_id'=> 2]);  
    }
}
