<?php

namespace App\Http\Controllers;

use App\Treatment;
use Illuminate\Http\Request;

class TreatmentController extends Controller
{

    public function index()
    {
        $treatments = Treatment::all();
        return view('treatment.index', compact('treatments'));
    }

    public function create()
    {
        return view('treatment.create');
    }

    public function store(Request $request)
    {
        $treatment = new Treatment();
        $treatment->treatment_name                  = $request->get('treatment_name');
        $treatment->treatment_mean_time  = $request->get('treatment_mean_time');
        $treatment->treatment_price                 = $request->get('treatment_price');
        $treatment->piority                         = $request->get('piority');
        $treatment->created_at                      = date('Y-m-d H:i:s');
        $treatment->updated_at                      = date('Y-m-d H:i:s');
        $treatment->save();
        return redirect('treatment/index');
    }

    public function show(Treatment $treatment)
    {
        //
    }

    public function edit($treatment_id)
    {
        $treatment = Treatment::find($treatment_id);
        return view('treatment.edit', compact('treatment'));
    }

    public function update(Request $request,  $treatment_id)
    {
        $treatment = Treatment::find($treatment_id);
        $treatment->treatment_name                  = $request->get('treatment_name');
        $treatment->treatment_mean_time  = $request->get('treatment_mean_time');
        $treatment->treatment_price                 = $request->get('treatment_price');
        $treatment->piority                         = $request->get('piority');
        $treatment->created_at                      = date('Y-m-d H:i:s');
        $treatment->updated_at                      = date('Y-m-d H:i:s');
        $treatment->save();
        return redirect('treatment/index');

    }

    public function destroy($treatment_id)
    {
        $treatment = Treatment::find($treatment_id);
        $treatment->delete();
        return redirect('treatment/index');
    }
}



