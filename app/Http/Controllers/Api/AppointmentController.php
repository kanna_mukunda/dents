<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Treatment;
use App\Models\Job;
use App\Models\DutyTime;
class AppointmentController extends Controller
{
    public function index(){
        $treatments = Treatment::all();
        return view('api.appointment.index', compact('treatments'));
    }

    public function create(){

        //หาหมอที่เข้าเวร ถ้าไม่มีใช้ $rooms
        $dutyTimeModel = new DutyTime();
        $date = date('yyyy-mm-dd');
        $treatment_id = 2;
        $where          = ['date'=> $date];
        $dutyTimeofDentists = $dutyTimeModel->dutyTime($where);
        foreach ($dutyTimeofDentists as $dent){
            //สร้าง slot ตารางงานของหมอ แต่ละคน
            $where = ['treatment_id' => $treatment_id, 'dent_id' => $dent->dent_id];
            $dent['job_slot'] = $this->findSlotByDateAndTreatment($date, $where);
        }
        return $dutyTimeofDentists;



    return view('api.appointment.create', compact('rooms'));

    }
    public function search(Request $request){
        $date           = $request->get('date');//2020-08-26';
        $treatment_id   = $request->get('treatment_id');//2; //ถอนฟัน
        $patient_id     = $request->get('patient');
        $dutyTimeModel  = new DutyTime();
        $where          = ['date'=>$date];

        $dutyTimeofDentists = $dutyTimeModel->dutyTime($where);
        foreach ($dutyTimeofDentists as $dent){
            //สร้าง slot ตารางงานของหมอ แต่ละคน
            $where = ['treatment_id' => $treatment_id, 'dent_id' => $dent->dent_id];
            $dent['job_slot'] = $this->findSlotByDateAndTreatment($date, $where);
        }
        return $dutyTimeofDentists;
        dd($request);
    }

    public function genNumberHN($name)
    {
        $array = substr("",$name);
        $hn = "";
        $count = 0;
        for ($i = 0; $i < count($array)-1; $i=$i+2)
        {
            if ($array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?')
            {
                $hn = $hn + "1";
                $count++;
            }
            else if ($array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?')
            {
                $hn = $hn + "2";
                $count++;
            }
            else if ($array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?'
                || $array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?')
            {
                $hn = $hn + "3";
                $count++;
            }
            else if ($array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?' )
            {
                $hn = $hn + "4";
                $count++;
            }
            else if ($array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?')
            {
                $hn = $hn + "5";
                $count++;
            }
            else if ($array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?'
                || $array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?')
            {
                $hn = $hn + "6";
                $count++;
            }
            else if ($array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?')
            {
                $hn = $hn + "7";
                $count++;
            }
            else if ($array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?')
            {
                $hn = $hn + "8";
                $count++;
            }
            else if ($array[$i] == '?' || $array[$i] == '?' || $array[$i] == '?')
            {
                $hn = $hn + "9";
                $count++;
            }
            //english
            if ($array[$i] == 'a' || $array[$i] == 'b' || $array[$i] == 'c' )
            {
                $hn = $hn + "1";
                $count++;
            }
            else if ($array[$i] == 'e' || $array[$i] == 'f'  || $array[$i] == 'd')
            {
                $hn = $hn + "2";
                $count++;
            }
            else if ($array[$i] == 'h' || $array[$i] == 'i'  || $array[$i] == 'g')
            {
                $hn = $hn + "3";
                $count++;
            }
            else if ( $array[$i] == 'j' || $array[$i] == 'k'||$array[$i] == 'l' )
            {
                $hn = $hn + "4";
                $count++;
            }
            else if ($array[$i] == 'o' || $array[$i] == 'm' || $array[$i] == 'n')
            {
                $hn = $hn + "5";
                $count++;
            }
            else if ($array[$i] == 'p' || $array[$i] == 'q'||$array[$i] == 'r' )
            {
                $hn = $hn + "6";
                $count++;
            }
            else if ($array[$i] == 's' || $array[$i] == 't'||$array[$i] == 'u' )
            {
                $hn = $hn + "7";
                $count++;
            }
            else if ( $array[$i] == 'v' || $array[$i] == 'w'||$array[$i] == 'x' )
            {
                $hn = $hn + "8";
                $count++;
            }
            else if ($array[$i] == 'y' || $array[$i] == 'z')
            {
                $hn = $hn + "9";
                $count++;
            }

            if ($count == 3)
            {
                break;
            }
        }
        if (count($hn) == 1)
        {
             $hn = $hn + "00";
        }else if (count($hn) == 2)
        {
             $hn = $hn + "0";
        }

        return $hn;
    }

    public function appointment_result($job_id){
        $appointmentResult = Job::where('id', $job_id)
            ->where('status', 'reserved')
            ->with('dentist', 'patient', 'treatment', 'room')
            ->get()->first();

        return $appointmentResult;
    }
}
