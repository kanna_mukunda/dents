<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Api\AppointmentController as apiAppontmentCtrl;
use App\Models\Job;
use App\Models\Patient;
use App\Treatment;
use App\Http\Controllers\AppointmentController;

class LineController extends Controller
{
    public function index(){
        return view('line.appointment.index');
    }

    public function postpone(REQUEST $request){

        return view('line.postpone.index');
    }

    public function login(){
        return view('line.postpone.login');
    }

    public function dashboard(REQUEST $request){
        $patient = $request->session()->get('patient');
        $job  =  Job::where('patient_id',$patient->hn)
                    ->where('status', 'reserved')
                    ->with('treatment', 'dentist', 'patient', 'room')
                    ->get();
        $job_id = collect($job)->isNotEmpty() ? $job[0]->id : 0;
        session(['job_id' => $job[0]->id]);

        return view('patient.appointment.dashboard', compact('job', 'job_id'));
    }
    public function authen(REQUEST $request){
        // หา user โดยจากเบอโทร
        $user = Patient::where('id' , $request->get('id'))->first();
        if(collect($user)->isNotEmpty()){
            session(['patient' => $user]);
        }

        $treatments = Treatment::all();
        $appointmentCtrl             = new AppointmentController();
        $apiAppontmentCtrl = new apiAppontmentCtrl();
        $appointmentInfos =  Job::where('patient_id', $user->hn)
                    ->where('status', 'reserved')
                    ->orderBy('date', 'desc')
                    ->get()->first();
        $timeArray = explode('-',$appointmentCtrl->convertSlotToTime($appointmentInfos->slot_start, $appointmentInfos->slot_end, $appointmentInfos->period_type));
        $appointmentInfos->start_time = $timeArray[0];
        $appointmentInfos->end_time = $timeArray[1];
        session('job_id', $appointmentInfos->id);
        $job_id = $request->session()->get('job_id');
        return view('line.postpone.appointment_summary', compact('appointmentInfos'));

    }

    public function postpone_store(REQUEST $request){
        Job::where('id', $request->get('job_id'))
            ->update(['status' => 'postpone']);

        $new_appointment = new Job();
        $dateArr = explode('-', $request->get('date'));
        $new_appointment->date = $dateArr[2].'-'.$dateArr['1'].'-'.$dateArr[0];
        $new_appointment->dent_id = $request->get('dent_id');
        $new_appointment->treatment_id = $request->get('treatment_id');

        $new_appointment->patient_id = $request->get('hn');
        $new_appointment->start_time = $request->get('time');
        $new_appointment->end_time = '00:00:00';
        $new_appointment->room_id = 1;
        $new_appointment->slot_start = 0;
        $new_appointment->slot_end = 0;
        $new_appointment->period_type= 'am';
        $new_appointment->status = 'reserved';
        $new_appointment->created_at = date('Y-m-d H:i:s');
        $new_appointment->updated_at = date('Y-m-d H:i:s');
        // $new_appointment->save();

        $treatments = Treatment::all();
        $appointmentCtrl             = new AppointmentController();
        $apiAppontmentCtrl = new apiAppontmentCtrl();
        $appointmentInfos =  Job::where('id', '228')//$new_appointment->id;
                    ->where('status', 'reserved')
                    ->orderBy('date', 'desc')
                    ->get()->first();
        $timeArray = explode('-',$appointmentCtrl->convertSlotToTime($appointmentInfos->slot_start, $appointmentInfos->slot_end, $appointmentInfos->period_type));
        // $appointmentInfos->start_time = $timeArray[0];
        $appointmentInfos->end_time = $timeArray[1];

        return view('line.postpone.postpone_summary', compact('appointmentInfos'));

    }

    public function redirectToProvider()
    {

    }
    public function getFormatTextMessage($text)
	{
		$datas = [];
		$datas['type'] = 'text';
		$datas['text'] = $text;

		return $datas;
	}

	public function sentMessage($encodeJson,$datas)
	{
		// $datasReturn = [];
		// $curl = curl_init();
		// curl_setopt_array($curl, array(
		//   CURLOPT_URL => $datas['url'],
		//   CURLOPT_RETURNTRANSFER => true,
		//   CURLOPT_ENCODING => "",
		//   CURLOPT_MAXREDIRS => 10,
		//   CURLOPT_TIMEOUT => 30,
		//   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		//   CURLOPT_CUSTOMREQUEST => "POST",
		//   CURLOPT_POSTFIELDS => $encodeJson,
		//   CURLOPT_HTTPHEADER => array(
		//     "authorization: Bearer ".$datas['token'],
		//     "cache-control: no-cache",
		//     "content-type: application/json; charset=UTF-8",
		//   ),
		// ));

		// $response = curl_exec($curl);
		// $err = curl_error($curl);

		// curl_close($curl);

		// if ($err) {
		//     $datasReturn['result'] = 'E';
		//     $datasReturn['message'] = $err;
		// } else {
		//     if($response == "{}"){
		// 	$datasReturn['result'] = 'S';
		// 	$datasReturn['message'] = 'Success';
		//     }else{
		// 	$datasReturn['result'] = 'E';
		// 	$datasReturn['message'] = $response;
		//     }
		// }

		// return $datasReturn;
	}
    public function callback()
    {
        return Socialite::driver('line')->redirect();
        $user =  Socialite::driver('line')->user();

        dd($user);
        $accessTokenResponseBody = $user->accessTokenResponseBody;
        $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient('<channel access token>');
        $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => '0ff3595a31db556ea0977ec80feb0978']);

        $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder('hello');
        $response = $bot->replyMessage('<replyToken>', $textMessageBuilder);

        echo $response->getHTTPStatus() . ' ' . $response->getRawBody();
#
        // dd($user->token);
        // $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($user->token);
        // $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => '1aeafbb93222486d76bddc74bb2afc05']);
        // $LINEData = file_get_contents('php://input');
        // $jsonData = json_decode($LINEData,true);
        // dd($jsonData)
        // $replyToken = $jsonData["events"][0]["replyToken"];
        // $text = $jsonData["events"][0]["message"]["text"];
        
        // function sendMessage($replyJson, $token){
        //         $ch = curl_init($token["URL"]);
        //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //         curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        //         curl_setopt($ch, CURLOPT_POST, true);
        //         curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        //             'Content-Type: application/json',
        //             'Authorization: Bearer ' . $token["AccessToken"])
        //             );
        //         curl_setopt($ch, CURLOPT_POSTFIELDS, $replyJson);
        //         $result = curl_exec($ch);
        //         curl_close($ch);
        // return $result;
        // }
        
        // if ($text == "s"){
        //     $message = '{
        //     "type" : "sticker",
        //     "packageId" : 11537,
        //     "stickerId" : 52002744
        //     }';
        //     $replymessage = json_decode($message);
        // }
        // else if ($text == "image"){
        // $message = '{
        //     "type": "image",
        //     "originalContentUrl": "https://linefriends.com/img/bangolufsen/img_og.jpg",
        //     "previewImageUrl": "https://linefriends.com/img/bangolufsen/img_og.jpg"
        //     }';
        //     $replymessage = json_decode($message);
        // }
        // else if ($text == "l"){
        // $message = '{
        //     "type": "location",
        //     "title": "my location",
        //     "address": "〒150-0002 東京都渋谷区渋谷２丁目２１−１",
        //     "latitude": 35.65910807942215,
        //     "longitude": 139.70372892916203
        //     }';
        //     $replymessage = json_decode($message);
        // }
        // else if ($text == "a"){
        // $message = '{
        //     "type" : "audio",
        //     "originalContentUrl": "https://mokmoon.com/audios/line.mp3",
        //     "duration" : 1000
        //     }';
        //     $replymessage = json_decode($message);
        // }
        // else if ($text == "v"){
        // $message = '{
        //     "type" : "video",
        //     "originalContentUrl" : "https://mokmoon.com/videos/Brown.mp4",
        //     "previewImageUrl" : "https://linefriends.com/img/bangolufsen/img_og.jpg"
        //     }';
        //     $replymessage = json_decode($message);
        // }
        // else if ($text == "q"){
        // $message = '{
        //     "type": "text",
        //     "text": "Hello Quick Reply!",
        //     "quickReply": {
        //     "items": [
        //     {
        //         "type": "action",
        //         "action": {
        //         "type":"location",
        //         "label":"Location"
        //         }
        //     }
        //     ]
        //     } 
        //     }';
        //     $replymessage = json_decode($message);
        // }
        // else{
        // $message = '{
        //     "type" : "text",
        //     "text" : "ไม่มีข้อมูลที่ต้องการ"
        //     }';
        //     $replymessage = json_decode($message);
        // }
        
        // $lineData['URL'] = "https://api.line.me/v2/bot/message/reply";
        // $lineData['AccessToken'] = "###";
        // $replyJson["replyToken"] = $replyToken;
        // $replyJson["messages"][0] = $replymessage;
        
        // $encodeJson = json_encode($replyJson);
        
        // $results = sendMessage($encodeJson,$lineData);
        // echo $results;
        // http_response_code(200);

        
    }
}
