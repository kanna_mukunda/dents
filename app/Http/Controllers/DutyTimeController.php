<?php

namespace App\Http\Controllers;

use App\Models\DutyTime;
use App\Models\Dentist;
use App\Models\room;
use Illuminate\Http\Request;
use App\Http\Controllers\DateTimeFormatController;

class DutyTimeController extends Controller
{

    public function index()
    {
        $dutytimes = DutyTime::with('room', 'dentist')->get();

        $dateTimeFormatCtl          = new DateTimeFormatController();
        foreach ($dutytimes as $dutytime){
            $dutytime->date = $dateTimeFormatCtl->thaiDateFormat($dutytime->date);
            $dutytime->am_slot_start = $dutytime->am_slot_start != -1 ? $this->convertSlotToTime($dutytime->am_slot_start, 9) : [];
            $dutytime->am_slot_end  =  $dutytime->am_slot_end != -1 ? $this->convertSlotToTime($dutytime->am_slot_end, 9) : [];
            $dutytime->ev_slot_start = $dutytime->ev_slot_start != -1 ? $this->convertSlotToTime($dutytime->ev_slot_start, 13) : [];
            $dutytime->ev_slot_end = $dutytime->ev_slot_end != -1 ? $this->convertSlotToTime($dutytime->ev_slot_end, 13) : [];
            $dutytime->pm_slot_start = $dutytime->pm_slot_start != -1 ? $this->convertSlotToTime($dutytime->pm_slot_start, 16) : [];
            $dutytime->pm_slot_end = $dutytime->pm_slot_end != -1 ? $this->convertSlotToTime($dutytime->pm_slot_end, 16) : [];
        }
        // return $dutytimes;
        return view('dutytime.index', compact('dutytimes'));
    }

    public function convertSlotToTime($slotnum, $hourPeriod){
        $slotToMin  = $slotnum * 5;
        $hourStep   = floor($slotToMin/60);
        $hour       = $hourStep + $hourPeriod; //  ชั่วโมงเริ่มต้น
        $minAbs     = abs(($hourStep*60) - ($slotToMin));
        $min        = $minAbs == 0 ? $minAbs."0" : $minAbs;
        return $hour.":".$min;
        $hour = "";
        $min = "";
        if($slotnum >= 0 && $slotnum < 12){
            $hour = "09";
            $min = $slotnum * 5;
        }
        elseif($slotnum >= 12 && $slotnum < 24){
            $hour = "10";
            $min = $slotnum * 5;
        }

        return $hour.":".$min;
    }

    public function create()
    {
        $dentists   = Dentist::all();
        $rooms      = room::all();
        return view('dutytime.create', compact('dentists', 'rooms'));
    }

    public function store(Request $request)
    {
// return($request);
        $dateTimeFormatCtl          = new DateTimeFormatController();

        $dutytime                   = new DutyTime();
        $dutytime->date             = $request->get('date');//$dateTimeFormatCtl->convertThaidateToEngDateFormat($request->get('date'));
        $dutytime->dent_id          = $request->get('dent_id');
        $dutytime->am_slot_start    = $request->get('am_start_time')    == null ? '-1' :$request->get('am_start_time');
        $dutytime->am_slot_end      = $request->get('am_end_time')      == null ? '-1' :$request->get('am_end_time');
        $dutytime->am_start_time    = '00:00';//$request->get('am_start_time');
        $dutytime->am_end_time      = '00:00';//$request->get('am_end_time');

        $dutytime->ev_slot_end      = $request->get('ev_end_time')      == null ? '-1' :$request->get('ev_end_time');
        $dutytime->ev_slot_start    = $request->get('ev_start_time')    == null ? '-1' :$request->get('ev_start_time');
        $dutytime->ev_start_time    = '00:00';//$request->get('ev_start_time');
        $dutytime->ev_end_time      = '00:00';//$request->get('ev_end_time');

        $dutytime->pm_slot_start    = $request->get('pm_start_time')    == null ? '-1' :$request->get('pm_start_time');
        $dutytime->pm_slot_end      = $request->get('pm_end_time')      == null ? '-1' :$request->get('pm_end_time');
        $dutytime->pm_start_time    = '00:00';//$request->get('pm_start_time');
        $dutytime->pm_end_time      = '00:00';//$request->get('pm_end_time');

        $dutytime->patient_no_am    = $request->get('am_start_time')    == null ? 0   : $request->get('patient_no_am');
        $dutytime->patient_no_ev    = $request->get('ev_start_time')    == null ? 0   : $request->get('patient_no_ev');
        $dutytime->patient_no_pm    = $request->get('pm_start_time')    == null ? 0   : $request->get('patient_no_pm');
        $dutytime->room_id          = $request->get('room_id');
        $dutytime->last_slot        =  $dutytime->am_slot_end;
        $dutytime->created_at       = date('Y-m-d H:i:s');
        $dutytime->updated_at       = date('Y-m-d H:i:s');
        $dutytime->save();

        return redirect('dutytime/index');
    }

    public function edit($dutytimeId)
    {
        $dutytime = DutyTime::find($dutytimeId);
        $dentists = Dentist::all();
        $rooms      = room::all();
        return view('dutytime.edit', compact('dentists', 'dutytime', 'rooms'));
    }

  
    public function update(Request $request, $dutytimeId)
    {
        // return $request;

        $dutytime = DutyTime::find($dutytimeId);
        $dateTimeFormatCtl          = new DateTimeFormatController();

        $dutytime->date             = $dateTimeFormatCtl->convertThaidateToEngDateFormat($request->get('date'));
        $dutytime->dent_id         = $request->get('dent_id');
        $dutytime->am_slot_start    = $request->get('am_start_time')    == null ? '-1' :$request->get('am_start_time');
        $dutytime->am_slot_end      = $request->get('am_end_time')      == null ? '-1' :$request->get('am_end_time');

        $dutytime->ev_slot_end      = $request->get('ev_end_time')      == null ? '-1' :$request->get('ev_end_time');
        $dutytime->ev_slot_start    = $request->get('ev_start_time')    == null ? '-1' :$request->get('ev_start_time');

        $dutytime->pm_slot_start    = $request->get('pm_start_time')    == null ? '-1' :$request->get('pm_start_time');
        $dutytime->pm_slot_end      = $request->get('pm_end_time')      == null ? '-1' :$request->get('pm_end_time');

        $dutytime->patient_no_am    = $request->get('am_start_time')    == null ? 0   : $request->get('patient_no_am');
        $dutytime->patient_no_ev    = $request->get('ev_start_time')    == null ? 0   : $request->get('patient_no_ev');
        $dutytime->patient_no_pm    = $request->get('pm_start_time')    == null ? 0   : $request->get('patient_no_pm');
        $dutytime->room_id          = $request->get('room_id');
        $dutytime->last_slot        =  $dutytime->am_slot_end;
        $dutytime->created_at       = date('Y-m-d H:i:s');
        $dutytime->updated_at       = date('Y-m-d H:i:s');
        $dutytime->save();
        // $dutytime                   = new DutyTime();
        // $dutytime->date             = $request->get('date');//$dateTimeFormatCtl->convertThaidateToEngDateFormat($request->get('date'));
        // $dutytime->dent_id          = $request->get('dent_id');
        // $dutytime->am_slot_start    = 0;//$request->get('am_start_time')    == null ? '-1' :$request->get('am_start_time');
        // $dutytime->am_slot_end      = 0;//$request->get('am_end_time')      == null ? '-1' :$request->get('am_end_time');
        // $dutytime->am_start_time    = $request->get('am_start_time');
        // $dutytime->am_end_time    = $request->get('am_end_time');

        // $dutytime->ev_slot_end      = 0;//$request->get('ev_end_time')      == null ? '-1' :$request->get('ev_end_time');
        // $dutytime->ev_slot_start    = 0;//$request->get('ev_start_time')    == null ? '-1' :$request->get('ev_start_time');
        // $dutytime->ev_start_time    = $request->get('ev_start_time');
        // $dutytime->ev_end_time    = $request->get('ev_end_time');

        // $dutytime->pm_slot_start    = 0;//$request->get('pm_start_time')    == null ? '-1' :$request->get('pm_start_time');
        // $dutytime->pm_slot_end      = 0;//$request->get('pm_end_time')      == null ? '-1' :$request->get('pm_end_time');
        // $dutytime->pm_start_time    = $request->get('pm_start_time');
        // $dutytime->pm_end_time    = $request->get('pm_end_time');

        // $dutytime->patient_no_am    = $request->get('am_start_time')    == null ? 0   : $request->get('patient_no_am');
        // $dutytime->patient_no_ev    = $request->get('ev_start_time')    == null ? 0   : $request->get('patient_no_ev');
        // $dutytime->patient_no_pm    = $request->get('pm_start_time')    == null ? 0   : $request->get('patient_no_pm');
        // $dutytime->room_id          = $request->get('room_id');
        // $dutytime->last_slot        =  $dutytime->am_slot_end;
        // // $dutytime->created_at       = date('Y-m-d H:i:s');
        // $dutytime->updated_at       = date('Y-m-d H:i:s');
        // $dutytime->save();
        return redirect('dutytime/index');

    }


    public function destroy($dutytimeId)
    {
        $dutytime = DutyTime::find($dutytimeId);
        $dutytime->delete();
        return redirect('dutytime/index');
    }
}
