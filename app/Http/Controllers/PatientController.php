<?php

namespace App\Http\Controllers;

use App\Models\Patient;
use Illuminate\Http\Request;
use App\Models\Job;
use App\Http\Controllers\Api\AppointmentController;
use App\Http\Controllers\DateTimeFormatController;

class PatientController extends Controller
{
    public function index()
    {
        $patients = Patient::orderBy('hn', 'asc')
                ->get(['hn','f_name','l_name', 'phone', 'id']);
        return view('patient.index', compact('patients'));
    }

    public function create()
    {
        $lastHn = Patient::get('hn')->last();
        return view('patient.create', compact('lastHn'));
    
    }

    public function store(Request $request)
    {
        $patient = new Patient();
        $patient->id            = $request->get('idcard'); 
        $patient->hn            = $request->get('hn'); 
        $patient->f_name        = $request->get('name'); 
        $patient->l_name        = $request->get('name'); 
        $patient->age           = $request->get('age');
        $patient->weight        = $request->get('weight');
        $patient->tall          = $request->get('tall');

            $b_dateArr = explode('/', $request->get('birthday'));//10/05/2543"
        $patient->b_date        = ($b_dateArr[2]-543).'-'.$b_dateArr[1].'-'.$b_dateArr[0];
        $patient->sex           = $request->get('gender');
        $patient->phone         = $request->get('phone'); 
        $patient->address       = $request->get('address');
        $patient->r_name        = $request->get('relative_person');
        $patient->r_phone       = $request->get('relative_person_phone');

        $patient->cd            = $request->get('congenital_disease');
        $patient->ah            = $request->get('drug_allergy_history');
        $patient->ca            = $request->get('chemical_allergy_history');

        $patient->heart_t       = $request->get('heart_trouble')        == 'on' ? 'y' : 'n';
        $patient->rheumatic_f   = $request->get('rhrheumatic_fever')    == 'on' ? 'y' : 'n';
        $patient->hypotension   = $request->get('hypotension')          == 'on' ? 'y' : 'n';
        $patient->hypertension  = $request->get('hypertension')         == 'on' ? 'y' : 'n';
        $patient->blood_d       = $request->get('blood_disease')        == 'on' ? 'y' : 'n';
        $patient->tuberculosis  = $request->get('tuberculasis')         == 'on' ?'y'  : 'n';
        $patient->liver_d       = $request->get('liver_disease')        == 'on' ? 'y' : 'n';
        $patient->diabetes_m    = $request->get('dusdusbetes_melitus')  == 'on' ? 'y' : 'n';
        $patient->allergy       = $request->get('allergy')              == 'on' ? 'y' : 'n';
        $patient->std           = $request->get('std')                  == 'on' ? 'y' : 'n';
        $patient->other         = $request->get('other')                == 'on' ? $request->get('other_value') : 'n';
 
        $patient->created_at    = date('Y-m-d H:i:s'); 
        $patient->updated_at    = date('Y-m-d H:i:s');
        $patient->save();


        return redirect('patient/index');
    }

    public function edit($patientId)
    {
        $patient = Patient::find($patientId);
        return view('patient.edit', compact('patient'));
    }

    public function view($patientHn){
        $patientHn = '6517870';
        $patient = Patient::where('hn', $patientHn)
            ->with('bill', 'bill.treatment')
            ->first();
        $datTimeCtrl = new DateTimeFormatController();
        $patient->b_dateTh = $datTimeCtrl->thaiDateFormat($patient->b_date);
        foreach($patient->bill as $bill){
            $bill->dateTh = $datTimeCtrl->thaiDateFormat($bill->date);
        }
        // $patient->nextAppointment = Job::where('patient_id', $patientHn)
        //                     ->where('status', 'reserved')
        //                     ->with('dentist', 'treatment')
        //                     ->get();
        return view('patient.view', compact('patient'));
    }
   
    public function update(Request $request, $patientId)
    {
        $patient = Patient::where('id', $patientId)->
        update([
            'name'           => $request->get('name'), 
            'hn'             => $request->get('hn'), 
            'phone'          => $request->get('phone'), 
            'created_at'     => date('Y-m-d H:i:s'), 
            'updated_at'     => date('Y-m-d H:i:s'),
        ]);
        return redirect('patient/index');

    }

    public function destroy($patientId)
    {
        $patient = Patient::find($patientId);
        $patient->delete();
        return redirect('patient/index');


    }

    public function findPatientByName($name){
        $patientModel = new Patient();
        $patient = $patientModel->find_patient_by_name($name);

        return \response()->json($patient);
    }
}
