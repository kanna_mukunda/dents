<?php

namespace App\Http\Controllers;
use App\Calendar;
use App\Models\Appointment;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Models\Dentist;
use App\Models\DutyTime;
use App\Treatment;
use App\Models\Treat;
use App\Models\Job;
use App\Models\Patient;
use App\Models\TreatmentSkillRatio;
use App\Http\Controllers\DateTimeFormatController;
use App\Http\Controllers\patient\AppointController;
use App\Http\Controllers\DutyTimeController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\Api\AppointmentController as apiAppontmentCtrl;

class AppointmentController extends Controller
{

    public function index($date = null)
    {
        if($date == null){
            $date = date('Y-m-d');
        }else{
            $dateArray = explode(" ", $date);
            $date = $dateArray[1]." ".$dateArray[2]." ".$dateArray[3];
            $date =  date('Y-m-d', strtotime($date));
        }
        $dateTimeFormatCtl          = new DateTimeFormatController();


        //หางานจาก ตาราง Job วันที่่เลือก
        $jobs = Job::where('date', $date)
                    ->orderBy('slot_start' )
                    ->orderBy('slot_end')
                    ->get();
        $jobsGrouped = collect($jobs)->groupBy('treatment_id')->values();
        $treatments = Treat::all();
        foreach ($treatments as $treatment){
            $jobCount= Job::where('date', $date)
                    ->where('treatment_id', $treatment->id)
                    ->count();
            $treatment->count = $jobCount;
        }

        // dd($treatments);
        $patients =    Patient::orderBy('hn', 'desc')->get();
        //หาหมอที่เข้าเวร
        $dutyTimeModel = new DutyTime();
        $where = ['date'=>$date];
        $dutyTimeofDentists = $dutyTimeModel->dutyTime($where);
        $patientAppointController = new AppointController();

        foreach ($dutyTimeofDentists as $dent){
            $dent->thaiDate = $dateTimeFormatCtl->thaiDateFormat($date);

            $infoArray = [];
            $dent->job_grouped = $dent->dentist->job->groupBy('treatment_id')->values();
            foreach ($dent->job_grouped as $group){
                $group['count'] = collect($group)->count();
            }
            //สร้าง slot ตารางงานของหมอ แต่ละคน
            $where = ['dent_id' => $dent->dent_id ];

            //สร้าง slot ตารางงานของหมอ แต่ละคน
            // slot($dent , $date, $init_time_hr_temp, $last_time_hr_temp, $slot_num_temp, $hour_num_temp)
            $dent['morningSlot'] = $this->slot($dent, $date, 9, 12, 36, 3, 'am');
            $dent['eveningSlot'] = $this->slot($dent, $date, 13, 16, 36, 3, 'ev');
            $dent['nightSlot'] = $this->slot($dent, $date, 16, 20, 48, 4, 'pm');


            $filtered = collect($dent['morningSlot'])->filter(function($val){
                return $val['slot_type'] != 'empty';
            });
            $infoByRoom = $filtered->groupBy('job_id')->values();
            foreach($infoByRoom as $a){
                $first = collect($a)->first();
                $last  = collect($a)->last();
                // dd($first);
                $firstHour  = $first['hour']< 10 ? '0'.$first['hour'] : $first['hour'];
                $lastHour   = $last['hour'] < 10 ? '0'.$last['hour'] : $last['hour'];
                $firstMin   = $first['min'] < 10 ? '0'.$first['min'] : $first['min'];
                $lastMin    = $last['min'] < 10 ? '0'.$last['min'] : $last['min'];
                array_push($infoArray , [
                    'job_id'        => $first['job_id'],
                    'firstHourMin'  => $firstHour.":".$firstMin,
                    'lastHourMin'   => $lastHour.":".$lastMin,
                    'patient_name'  => $first['jobInfo']['patient']['name'],
                    'patient_phone' => $first['jobInfo']['patient']['phone'],
                    'dent_name'     =>  $dent->dentist->dent_name,
                    'treatment'     => $first['attribute']['treatment_name'],
                    'status'        => $first['jobInfo']['status'],
                    'created_at'    => $first['jobInfo']['created_at'],
                ]);
            }

            $dent->infoByRoom = $infoArray;

             $dent->morningSlotChunk_3 = $this->chunk3($dent['morningSlot']);
             $dent->eveningSlotChunk_3 = $this->chunk3($dent['eveningSlot']);
             $dent->nightSlotChunk_3 = $this->chunk3($dent['nightSlot']);
        }

    //    return $dutyTimeofDentists;
       return view('appointment.index', compact('dutyTimeofDentists', 'treatments', 'patients', 'jobs'));
    }

    public function appoint_by_staff()
    {
        $dutyTimeCtl                = new DutyTimeController();
        $dateTimeFormatCtl          = new DateTimeFormatController();

        $number = cal_days_in_month(CAL_GREGORIAN, date('n'), date('Y')); // 31

        $day_number_arr = [];
        for($i = 0; $i < $number; $i++){
            $d = $i+ 1;
            array_push($day_number_arr, [
                'index' => $i,
                'day_number' =>  $d,
                'day_type' => '',
                'dayInfo' => [],
                'day_name' => '',
                'dents' => [
                    'work' => [],
                    'vacation' => [ ]
                ]
            ]);
        }

        // หาวันทยุด
        $day_offs = Calendar::where('start_date', 'LIKE', date('Y-m-%'))
            ->with('dentist')
            ->orderBy('start_date', 'asc')
            ->get();

        foreach($day_offs as $day_off){

            $day_off_start_date = str_split(\explode('-',$day_off['start_date'])[2]);
            $start_date = $day_off_start_date[0]  == 0 ? $day_off_start_date[1] : $day_off_start_date[0].$day_off_start_date[1];
            $day_off_end_date = str_split(\explode('-',$day_off['end_date'])[2]);
            $end_date = $day_off_end_date[0]  == 0 ? $day_off_end_date[1] : $day_off_end_date[0].$day_off_end_date[1];


            for($j = $start_date; $j <= $end_date; $j++){
                $dayOffIndex = $j - 1;

                //ถ้าเป็นวันลา ให้ add ข้อมูลหมอที่ลา เข้าไป day_number_arr->dents->vaction []
                $day_number_arr[$dayOffIndex]['day_type'] = 'dayOff';

                if($day_off['calen_day_type'] == 'personal_leave'){
                    array_push($day_number_arr[$dayOffIndex]['dents']['vacation'], [

                        'title'=> $day_off['title'],
                        'start_date'=> $day_off['start_date'],
                        'start_time' => $day_off['start_time'],
                        'end_date'=> $day_off['end_date'],
                        'end_time' => $day_off['end_time'],
                        'dentInfo' =>  $day_off['dentist']
                    ]);
                    $day_number_arr[$dayOffIndex]['day_type'] = 'dayWork';

                }else{
                    $day_number_arr[$dayOffIndex]['day_name'] = $day_off['title'];
                    $day_number_arr[$dayOffIndex]['dayInfo'] = [
                        'start_date'=> $day_off['start_date'],
                        'start_time' => $day_off['start_time'],
                        'end_date'=> $day_off['end_date'],
                        'end_time' => $day_off['end_time'],
                        'dentInfo' =>  $day_off['dentist']
                    ];
                }

            }
        }


        //หาตารางเวรหมอ เอาไปใส่ $day_number_arr
        $dutytimes = DutyTime::where('date', 'LIKE', date('Y-m-%'))->orderBy('date', 'asc')
            ->with('Dentist')
            ->get(
                ['am_slot_start',
                'am_slot_end',
                'ev_slot_start',
                'ev_slot_end',
                'pm_slot_start',
                'pm_slot_end',
                'dent_id',
                'date']);


        //แปลงวันที่
        foreach($dutytimes as $dutytime){
            $dutytime->date = $dateTimeFormatCtl->thaiDateFormat($dutytime->date);
            $dutytime->am_slot_start = $dutytime->am_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->am_slot_start, 9) : [];
            $dutytime->am_slot_end  =  $dutytime->am_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->am_slot_end, 9) : [];
            $dutytime->ev_slot_start = $dutytime->ev_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->ev_slot_start, 13) : [];
            $dutytime->ev_slot_end = $dutytime->ev_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->ev_slot_end, 13) : [];
            $dutytime->pm_slot_start = $dutytime->pm_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->pm_slot_start, 16) : [];
            $dutytime->pm_slot_end = $dutytime->pm_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->pm_slot_end, 16) : [];

            $dutytime->am_hour_start = explode(':' ,$dutytime->am_slot_start)[0];
            $dutytime->am_hour_end = explode(':' ,$dutytime->am_slot_end)[0];
            $dutytime->ev_hour_start = explode(':' ,$dutytime->ev_slot_start)[0];
            $dutytime->ev_hour_end = explode(':' ,$dutytime->ev_slot_end)[0];
            $dutytime->pm_hour_start = explode(':' ,$dutytime->pm_slot_start)[0];
            $dutytime->pm_hour_end = explode(':' ,$dutytime->pm_slot_end)[0];
            $dateOnly              = str_split(\explode(' ', $dutytime->date)[0]);
            $dutytime->dateOnly    = $dateOnly[0] == '0' ? $dateOnly[1] : $dateOnly[0].$dateOnly[1];

            //เพิ้ม ตารางเวรของหมอ
            $dayOffIndex = $dutytime->dateOnly - 1;

            array_push($day_number_arr[$dayOffIndex]['dents']['work'], [
                'dentInfo' =>  $dutytime
            ]);
        }
        $dents = Dentist::all();
        $calendarController = new CalendarController();
        $dent_dutytimes = $calendarController->dentDutyTime();
        return view('appointment.appointment_by_staff.index', \compact('dents', 'day_number_arr', 'dent_dutytimes', 'day_offs'));

    }

    public function appoint_by_staff_by_date($date){
        // หาตารางเวรของหมอทุกคนที่เข้าเวรในวัน $date
        $dateTimeFormatCtl          = new DateTimeFormatController();

        //หางานจาก ตาราง Job วันที่่เลือก
        $jobs = Job::where('date', $date)
                    ->orderBy('slot_start' )
                    ->orderBy('slot_end')
                    ->get();

        $jobsGrouped = collect($jobs)->groupBy('treatment_id')->values();

        $treatments = Treatment::all();

        foreach ($treatments as $treatment){
            $jobCount= Job::where('date', $date)
                    ->where('treatment_id', $treatment->id)
                    ->count();
            $treatment->count = $jobCount;
        }

        $patients =    Patient::get(['hn', 'f_name', 'l_name']);
        //หาหมอที่เข้าเวร
        $dutyTimeModel = new DutyTime();
        $where = ['date'=>$date];
        $dutyTimeofDentists = $dutyTimeModel->dutyTime($where);

        //ถ้าไม่มีหมอเข้าเวรให้ return กลับไป appointment/appoint_by_staff
        if(collect($dutyTimeofDentists)->isEmpty()){
            return redirect('appointment/appoint_by_staff');
        }

        $patientAppointController = new AppointController();

        foreach ($dutyTimeofDentists as $dent){
            $dent->thaiDate = $dateTimeFormatCtl->thaiDateFormat($date);

            $infoArray = [];



            $dent->job_grouped = $dent->dentist->job->groupBy('treatment_id')->values();
            foreach ($dent->job_grouped as $group){
                $group['count'] = collect($group)->count();
            }
            //สร้าง slot ตารางงานของหมอ แต่ละคน
            $where = ['dent_id' => $dent->dent_id ];

            //สร้าง slot ตารางงานของหมอ แต่ละคน
            // slot($dent , $date, $init_time_hr_temp, $last_time_hr_temp, $slot_num_temp, $hour_num_temp)
            $dent['morningSlot'] = $this->slot($dent, $date, 9, 12, 36, 3, 'am');
            $dent['eveningSlot'] = $this->slot($dent, $date, 13, 16, 36, 3, 'ev');
            $dent['nightSlot'] = $this->slot($dent, $date, 16, 20, 48, 4, 'pm');


            $filtered = collect($dent['morningSlot'])->filter(function($val){
                return $val['slot_type'] != 'empty';
            });
            $infoByRoom = $filtered->groupBy('job_id')->values();
            foreach($infoByRoom as $a){
                $first = collect($a)->first();
                $last  = collect($a)->last();
                // dd($first);
                $firstHour  = $first['hour']< 10 ? '0'.$first['hour'] : $first['hour'];
                $lastHour   = $last['hour'] < 10 ? '0'.$last['hour'] : $last['hour'];
                $firstMin   = $first['min'] < 10 ? '0'.$first['min'] : $first['min'];
                $lastMin    = $last['min'] < 10 ? '0'.$last['min'] : $last['min'];
                array_push($infoArray , [
                    'job_id'        => $first['job_id'],
                    'firstHourMin'  => $firstHour.":".$firstMin,
                    'lastHourMin'   => $lastHour.":".$lastMin,
                    'patient_name'  => $first['jobInfo']['patient']['name'],
                    'patient_phone' => $first['jobInfo']['patient']['phone'],
                    'dent_name'     =>  $dent->dentist->dent_name,
                    'treatment'     => $first['attribute']['treatment_name'],
                    'status'        => $first['jobInfo']['status'],
                    'created_at'    => $first['jobInfo']['created_at'],
                ]);
            }

            $dent->infoByRoom = $infoArray;

             $dent->morningSlotChunk_3 = $this->chunk3($dent['morningSlot']);
             $dent->eveningSlotChunk_3 = $this->chunk3($dent['eveningSlot']);
             $dent->nightSlotChunk_3 = $this->chunk3($dent['nightSlot']);
        }

    //    return $dutyTimeofDentists;
       return view('appointment.appointment_by_staff.findbydate', compact('dutyTimeofDentists', 'treatments', 'patients', 'jobs'));
    }

    public function appoint_by_staff_store(REQUEST $request){
        $job = new Job();
        // $dateTimeFormatCtl  = new DateTimeFormatController();
        // $processing_slot = $request->get('processing_time');
        $patientAppointCtl = new AppointController();
        $hour_slot = 0;
        $slot_start = 0;
        $hour_slot = $patientAppointCtl->findHourSlot($request->get('hour'));
        $min_slot = $request->get('min')/5;
        $slot_start = $hour_slot + $min_slot;

        $treatment_ratio = TreatmentSkillRatio::where('dent_id', $request->get('dent_id'))
                            ->where('treatment_id', $request->get('treatment_id'))
                            ->first();

        $job->date              = $request->get('date');//$dateTimeFormatCtl->convertThaidateStringToEngDateFormat($request->get('date'));
        $job->patient_id        = $request->get('patient_hn');
        $job->dent_id           = $request->get('dent_id');
        $job->treatment_id      = $request->get('treatment_id');
        $job->slot_start        = $slot_start;
        $job->slot_end          = ($slot_start + $treatment_ratio->ratio)-1;
        $job->period_type       = $request->get('period_type');
        $job->room_id           = $request->get('room');
        $job->status            = 'reserved';
        $job->save();
        return response()->json($job);
    }

    public function find_dutytime_dents($date){
        $dateTimeFormatCtl          = new DateTimeFormatController();

        //หางานจาก ตาราง Job วันที่่เลือก
        $jobs = Job::where('date', $date)
                    ->orderBy('slot_start' )
                    ->orderBy('slot_end')
                    ->get();
        $jobsGrouped = collect($jobs)->groupBy('treatment_id')->values();
        $treatments = Treatment::all();
        foreach ($treatments as $treatment){
            $jobCount= Job::where('date', $date)
                    ->where('treatment_id', $treatment->id)
                    ->count();
            $treatment->count = $jobCount;
        }

        // dd($treatments);
        $patients =    Patient::all();
        //หาหมอที่เข้าเวร
        $dutyTimeModel = new DutyTime();
        $where = ['date'=>$date];
        $dutyTimeofDentists = $dutyTimeModel->dutyTime($where);
        $patientAppointController = new AppointController();

        foreach ($dutyTimeofDentists as $dent){
            $dent->thaiDate = $dateTimeFormatCtl->thaiDateFormat($date);

            $infoArray = [];
            $dent->job_grouped = $dent->dentist->job->groupBy('treatment_id')->values();
            foreach ($dent->job_grouped as $group){
                $group['count'] = collect($group)->count();
            }
            //สร้าง slot ตารางงานของหมอ แต่ละคน
            $where = ['dent_id' => $dent->dent_id ];

            //สร้าง slot ตารางงานของหมอ แต่ละคน
            // slot($dent , $date, $init_time_hr_temp, $last_time_hr_temp, $slot_num_temp, $hour_num_temp)
            $dent['morningSlot'] = $this->slot($dent, $date, 9, 12, 36, 3, 'am');
            $dent['eveningSlot'] = $this->slot($dent, $date, 13, 16, 36, 3, 'ev');
            $dent['nightSlot'] = $this->slot($dent, $date, 16, 20, 48, 4, 'pm');


            $filtered = collect($dent['morningSlot'])->filter(function($val){
                return $val['slot_type'] != 'empty';
            });
            $infoByRoom = $filtered->groupBy('job_id')->values();
            foreach($infoByRoom as $a){
                $first = collect($a)->first();
                $last  = collect($a)->last();
                // dd($first);
                $firstHour  = $first['hour']< 10 ? '0'.$first['hour'] : $first['hour'];
                $lastHour   = $last['hour'] < 10 ? '0'.$last['hour'] : $last['hour'];
                $firstMin   = $first['min'] < 10 ? '0'.$first['min'] : $first['min'];
                $lastMin    = $last['min'] < 10 ? '0'.$last['min'] : $last['min'];
                array_push($infoArray , [
                    'job_id'        => $first['job_id'],
                    'firstHourMin'  => $firstHour.":".$firstMin,
                    'lastHourMin'   => $lastHour.":".$lastMin,
                    'patient_name'  => $first['jobInfo']['patient']['name'],
                    'patient_phone' => $first['jobInfo']['patient']['phone'],
                    'dent_name'     =>  $dent->dentist->dent_name,
                    'treatment'     => $first['attribute']['treatment_name'],
                    'status'        => $first['jobInfo']['status'],
                    'created_at'    => $first['jobInfo']['created_at'],
                ]);
            }

            $dent->infoByRoom = $infoArray;

             $dent->morningSlotChunk_3 = $this->chunk3($dent['morningSlot']);
             $dent->eveningSlotChunk_3 = $this->chunk3($dent['eveningSlot']);
             $dent->nightSlotChunk_3 = $this->chunk3($dent['nightSlot']);
        }

       return view('appointment.index', compact('dutyTimeofDentists', 'treatments', 'patients', 'jobs'));
        return $date;
    }

    public function appointment_result($job_id){
        $apiAppontmentCtrl = new apiAppontmentCtrl();
        $appointmentInfos =  $apiAppontmentCtrl->appointment_result($job_id);
        $timeArray = explode('-',$this->convertSlotToTime($appointmentInfos->slot_start, $appointmentInfos->slot_end, $appointmentInfos->period_type));
        $appointmentInfos->start_time = $timeArray[0];
        $appointmentInfos->end_time = $timeArray[1];
        return view('appointment.appoint_summary', compact('appointmentInfos'));
    }


    public function chunk3($slot){
        $slotChunk_3 = collect($slot)->chunk(3);
        foreach ($slotChunk_3 as $key => $item){
            $slotChunk_3[$key] = $item->values();
        }
        foreach($slotChunk_3 as $item){
            foreach ($item as $it){
                // dd($it['slot_type']);
                if($it['slot_type'] == 'start-end' || $it['slot_type'] == 'start' || $it['slot_type'] == 'end'){
                    // $item['chunk'] = $it['attribute'];
                    break;
                }else{
                    // $item['4'] = [];
                }
            }
        }
        return $slotChunk_3;
    }

    public function slot($dent, $date, $init_time_hr_temp, $last_time_hr_temp, $slot_num_temp, $hour_num_temp, $period_temp){
        // หาslot ที่ไมว่างของหมอ
        $jobs = Job::where('dent_id', $dent->dent_id)
                ->where('date' , $date)
                ->where('period_type', $period_temp)
                ->with('patient', 'treatment', 'dentist')
                ->orderBy('slot_start', 'asc')
                ->get();

        // dd($jobs['treatment']);

        $count = 0;
        $init_time_hr = $init_time_hr_temp; //9;
        $last_time_hr = $last_time_hr_temp;//12;
        $init_time_min = 0;
        $slot_num = $slot_num_temp;//36;
        $hour_num = $hour_num_temp;
        $slotArray = [];
        for($i = 0; $i < $hour_num; $i++){
            //ช่วงเช้ามี 3   ชั่วโมง
            for($j = 0; $j < 12; $j++){
                $min = ($init_time_min + $j) * 5;
                $morningSlot = [
                    "index" => $count,
                    "job_id" => 0,
                    'dent_id' =>  $dent->dent_id,
                    'slot_type' => 'empty',
                    'status' => 'active',
                    'hour' => $init_time_hr,
                    'min' => $min,
                    'min_f_slot'=>$min,
                    'min_l_slot'=>$min + 5,
                    'attribute' =>
                                [   'btn_type'=> 'btn-outline-info',
                                    'treatment_name' => "",
                                    'patient_name' => '-'
                                ]
                ];
                array_push($slotArray, $morningSlot);
                $count++;
            }
            $init_time_hr = $init_time_hr+1;
        }

        foreach($jobs as $job){
            $job_ratio = TreatmentSkillRatio::where('dent_id', $dent->dent_id)
                    ->where('treatment_id' , $job['treatment']['id'])
                    ->get('ratio')->first();


            for($i = 0; $i <$job_ratio['ratio']; $i++){
                // dd($job);
                $slot_start = $job->slot_start;
                $slot_end = $job->slot_end;
                $btn_type = 'btn-outline-info';
                if($job->treatment_id == 1){
                    $btn_type = 'btn-primary';
                }elseif($job->treatment_id == 2){
                    $btn_type = 'btn-warning';
                }
                if($i == 0){
                    // dd($slot_num_temp);
                    //่slot แรก
                    $slotArray[$slot_start]['job_id'] = $job->id;
                    $slotArray[$slot_start]['status'] = 'inacative';
                    $slotArray[$slot_start]['slot_type'] = $slot_start == $slot_end ? 'start-end' : 'start';
                    $slotArray[$slot_start]['jobInfo'] = $job;

                    $slotArray[$slot_start]['attribute'] = [
                            'btn_type'      => $btn_type,
                            'treatment_name' => $job['treatment']['treatment_name'],
                            'hour_start'    => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                            'hour_end'      => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                            'min_start'     => $slotArray[$slot_start]['min'] < 10 ? '0'.$slotArray[$slot_start]['min'] : $slotArray[$slot_start]['min'],
                            'min_end'       => $slotArray[$slot_start]['min'] < 10 ? '0'.$slotArray[$slot_start]['min'] : $slotArray[$slot_start]['min'],
                            'patient_name'      => $job['patient']['f_name'].' '.$job['patient']['l_name']
                    ];

                }else if($i < $job_ratio['ratio'] && $i > 0){
                    //job ระหว่าง first กับ last
                    $slotArray[$slot_start]['job_id'] = $job->id;
                    $slotArray[$slot_start]['slot_type'] = $slot_start == $slot_end ? 'start-end' : 'start';
                    $slotArray[$slot_start]['status'] = 'inacative';
                    $slotArray[$slot_start]['jobInfo'] = $job;
                    $slotArray[$slot_start]['attribute'] = [
                        'btn_type'=> $btn_type,
                        'treatment_name' => $job['treatment']['treatment_name'],
                        'hour_start'    => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                        'hour_end'      => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                        'min_start'     => $slotArray[$slot_start]['min'] < 10 ? '0'.$slotArray[$slot_start]['min'] : $slotArray[$slot_start]['min'],
                        'min_end'       => $slotArray[$slot_start]['min'] < 10 ? '0'.$slotArray[$slot_start]['min'] : $slotArray[$slot_start]['min'],
                        'patient_name'      => $job['patient']['f_name'].' '.$job['patient']['l_name']
                    ];

                    $slotArray[$slot_end]['job_id'] = $job->id;
                    $slotArray[$slot_end]['slot_type']  =  $slotArray[$slot_start]['slot_type'] != 'start-end' ? 'end' : 'start-end';
                    $slotArray[$slot_end]['status']     =  'inacative';
                    $slotArray[$slot_end]['jobInfo'] = $job;
                    $slotArray[$slot_end]['attribute'] = [
                        'btn_type'=> $btn_type,
                        'treatment_name' => $job['treatment']['treatment_name'],
                        'hour_start'    => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                        'hour_end'      => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                        'min_start'     => $slotArray[$slot_start]['min'] < 10 ? '0'.$slotArray[$slot_start]['min'] : $slotArray[$slot_start]['min'],
                        'min_end'       => $slotArray[$slot_start]['min'] < 10 ? '0'.$slotArray[$slot_start]['min'] : $slotArray[$slot_start]['min'],
                        'patient_name'      => $job['patient']['f_name'].' '.$job['patient']['l_name']
                    ];
                    if($slotArray[$slot_start]['slot_type'] != 'start-end'){
                        //หา child
                        $range = $slot_end - $slot_start;
                        for($i = 1; $i < $range; $i++){
                            $a = $slot_start + $i;
                            $slotArray[$a]['job_id'] = $job->id;
                            $slotArray[$a]['slot_type'] = 'child';
                            $slotArray[$a]['status']     =  'inacative';
                            $slotArray[$a]['jobInfo'] = $job;
                            $slotArray[$a]['attribute'] = [
                                'btn_type'=> $btn_type,
                                'treatment_name' => $job['treatment']['treatment_name'],
                                'hour_start'    => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                                'min_start'     => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                                'hour_end'      => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                                'min_end'       => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                                'patient_name'      => $job['patient']['f_name'].' '.$job['patient']['l_name']
                            ];
                        }
                    }

                }else if($i == $job_ratio['ratio'] ){
                    //job สุดท้าย
                    $slotArray[$slot_start]['job_id'] = $job->id;
                    $slotArray[$slot_start]['slot_type'] = $slot_start == $slot_end ? 'start-end' : 'start';
                    $slotArray[$slot_start]['status'] = 'inacative';
                    $slotArray[$slot_start]['jobInfo'] = $job;
                    $slotArray[$slot_start]['attribute'] = [
                        'btn_type'=> $btn_type,
                        'treatment_name' => $job['treatment']['treatment_name'],
                        'hour_start'    => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                        'hour_end'      => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                        'min_start'     => $slotArray[$slot_start]['min'] < 10 ? '0'.$slotArray[$slot_start]['min'] : $slotArray[$slot_start]['min'],
                        'min_end'       => $slotArray[$slot_start]['min'] < 10 ? '0'.$slotArray[$slot_start]['min'] : $slotArray[$slot_start]['min'],
                        'patient_name'      => $job['patient']['f_name'].' '.$job['patient']['l_name']
                    ];

                    $slotArray[$slot_end]['job_id'] = $job->id;
                    $slotArray[$slot_end]['slot_type']  =  $slotArray[$slot_start]['slot_type'] != 'start-end' ? 'end' : 'start-end';
                    $slotArray[$slot_end]['status']     =  'inacative';
                    $slotArray[$slot_end]['jobInfo'] = $job;
                    $slotArray[$slot_end]['attribute'] = [
                        'btn_type'=> $btn_type,
                        'treatment_name' => $job['treatment']['treatment_name'],
                        'hour_start'    => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                        'hour_end'      => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                        'min_start'     => $slotArray[$slot_start]['min'] < 10 ? '0'.$slotArray[$slot_start]['min'] : $slotArray[$slot_start]['min'],
                        'min_end'       => $slotArray[$slot_start]['min'] < 10 ? '0'.$slotArray[$slot_start]['min'] : $slotArray[$slot_start]['min'],
                        'patient_name'      => $job['patient']['f_name'].' '.$job['patient']['l_name']
                    ];
                    if($slotArray[$slot_start]['slot_type'] != 'start-end'){
                        //หา child
                        $range = $slot_end - $slot_start;
                        for($i = 1; $i < $range; $i++){
                            $a = $slot_start + $i;
                            $slotArray[$a]['job_id'] = $job->id;
                            $slotArray[$a]['slot_type'] = 'child';
                            $slotArray[$a]['status']     =  'inacative';
                            $slotArray[$a]['jobInfo'] = $job;
                            $slotArray[$a]['attribute'] = [
                                'btn_type'=> $btn_type,
                                'treatment_name' => $job['treatment']['treatment_name'],
                                'hour_start'      => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                                'hour_end'      => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                                'min_start'     => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                                'min_end'       => $slotArray[$slot_start]['hour'] < 10 ? '0'.$slotArray[$slot_start]['hour'] : $slotArray[$slot_start]['hour'],
                                'patient_name'      => $job['patient']['f_name'].' '.$job['patient']['l_name']
                            ];
                        }

                    }
                }
            }
        }
        return($slotArray);
    }

    public function create(REQUEST $request)
    {
        // dd($request);
        $date           = $request->get('reserve_date');//2020-08-26';
        $treatment_id   = $request->get('treatment_id');//2; //ถอนฟัน
        $patient_id     = $request->get('patient');
        $dutyTimeModel  = new DutyTime();
        $where          = ['date'=>$date];

        $dutyTimeofDentists = $dutyTimeModel->dutyTime($where);
        foreach ($dutyTimeofDentists as $dent){
            //สร้าง slot ตารางงานของหมอ แต่ละคน
            $where = ['treatment_id' => $treatment_id, 'dent_id' => $dent->dent_id];
            $dent['job_slot'] = $this->findSlotByDateAndTreatment($date, $where);
        }

        $treatmetns = Treatment::all();
        $patients   =    Patient::all();
        return view('appointment.create', compact('dutyTimeofDentists', 'treatmetns', 'patients'));
    }

    public function store()
    {
        $reserve_date= "2020-08-26";
        $slot_start= "20";
        $slot_end= "10";
        $dent_id= "2";
        $period_type= "am";
        $dent_id= "2";
        $ratio= "1";
        $reserve_date= "2020-08-26";
        $slot_start= "20";
        $treatment_mean_time= "5";
        $slot_end= $slot_start + ($ratio * $treatment_mean_time);

        $job = new Job();
        $job->date          =$reserve_date;
        $job->patient_id    =1;
        $job->dent_id       =$dent_id;
        $job->treatment_id  =1;
        $job->slot_start    =$slot_start;
        $job->slot_end      =$slot_end;
        $job->period_type   =$period_type;
        $job->status        = 'reserved';
        $job->save();
        return response()->json($job->id);
    }


    public function patient_apointment()
    {
        $treatments = Treatment::all();
        return view('appointment.patient_apointment', compact('treatments'));
    }

    public function patient_select_time(REQUEST $request){
        $date                   = '2020-08-26';//$request->get('date'); //"";
        $treatment_id           = '1';//$request->get('treatment_id');//"3";
        $dutyTimeModel          = new DutyTime();
        $where                  = ['date'=>$date];
        $dutyTimeNextDateArray  = [];

        // ค้นหาหมอที่เข้าเวร วันที่คนไข้เลือก
        $dutyTimeofDentists = $dutyTimeModel->dutyTime($where);
        if($dutyTimeofDentists->isEmpty()){
            //ถ้าไม่มีหมอเข้าเวร ตามการรักษาที่เลือก

        }else{
            //ถ้ามีหมอเข้าเวร ตามรายการรักษาที่เลือก
            foreach ($dutyTimeofDentists as $dent){
                //สร้าง slot ตารางงานของหมอ แต่ละคน
                $where = ['treatment_id' => $treatment_id, 'dent_id' => $dent->dent_id];

                $dent['job_slot']           = $this->findSlotByDateAndTreatment($date, $where);
                $treatmentModel             = new Treatment();
                $dent['selected_treatment'] = $treatmentModel->getTreatment($treatment_id);
                $selected_treatment_id      = $dent['selected_treatment']->id;

                for($i = 0; $i < count($dent->dentist->treatmentSkillRatio); $i++){
                    if($dent->dentist->treatmentSkillRatio[$i]['id'] == $selected_treatment_id){
                        $dent['processing_time'] = $dent->dentist->treatmentSkillRatio[$i]['ratio'] * $dent['selected_treatment']->treatment_mean_time;
                    }
                }
            }
        }
        // return $dutyTimeofDentists;
        //ค้นหาหมอที่มี skill การรักษาอาการที่ patient เลือก
        $DentistsHaveSelectedSkill = $dutyTimeofDentists->filter(function($val) use ($treatment_id){
            $where = ['dent_id' => $val->dentist->id, 'treatment_id'=> $treatment_id];
            $skillRatioOfDent = TreatmentSkillRatio::where($where)->first();
            return $skillRatioOfDent != null;
        });
        // return $DentistsHaveSelectedSkill;
        // // ถ้าไม่มีหมอเฉพาะทางเข้าเวรวันที่ patient เลือก ให้หาวันที่มีหมอเฉพาะทางเข้าเวร
        // $where = ['treatment_id'=> $treatment_id];
        // $resultText = "ไม่พบข้อมูล";
        // if($findDentFromSkillRatioByTreatmentId->isNotEmpty()){
        //     //หาวันที่หมอเฉพาะทางเข้าเวรที่มากกว่า วันที่ patient เลือก
        //     $aa =[];
        //     foreach($findDentFromSkillRatioByTreatmentId as $dent){
        //             $findDutyTimeOfDentHaveSkill = DutyTime::where('dent_id', $dent->dent_id)
        //                                             ->where('date', '>', $date)->orderBy('date', 'asc')->get(['date', 'dent_id']);
        //         array_push($aa, $findDutyTimeOfDentHaveSkill);
        //     }

        //     return $dutyTimeNextDateArray = collect($aa)->collapse()->groupBy('date')->sortKeys();
        //     return view('appointment.patient_select_time', \compact('dutyTimeofDentists', 'dutyTimeNextDateArray', 'treatment_id'));
        // }

        foreach ($dutyTimeofDentists as $dent){
            //สร้าง slot ตารางงานของหมอ แต่ละคน
            $where = ['treatment_id' => $treatment_id, 'dent_id' => $dent->dent_id];
            $dent['job_slot'] = $this->findSlotByDateAndTreatment($date, $where);
            $treatmentModel = new Treatment();
            $dent['selected_treatment'] = $treatmentModel->getTreatment($treatment_id);
            $selected_treatment_id = $dent['selected_treatment']->id;

            for($i = 0; $i < count($dent->dentist->treatmentSkillRatio); $i++){
                if($dent->dentist->treatmentSkillRatio[$i]['id'] == $selected_treatment_id){
                    $dent['processing_time'] = $dent->dentist->treatmentSkillRatio[$i]['ratio'] * $dent['selected_treatment']->treatment_mean_time;
                }
            }

        }
        //ทำการคัดกรองหมอที่ยังไม่มีงาน
        $dentJobEmpty = $dutyTimeofDentists->filter(function($val){
            return ($val->dentist->job)->isEmpty();
        });


        // if($dentJobEmpty->count() > 0){
        //     $dutyTimeofDentists = $dutyTimeofDentists[0];
        //     return view('appointment.patient_select_time', compact('dutyTimeNextDateArray', 'dutyTimeofDentists'));
        // }

        $dutyTimeofDentistsJobSlotIsFalse = $dutyTimeofDentists->filter(function($val){
            return $val['job_slot'] == false;
        });
        if(count($dutyTimeofDentistsJobSlotIsFalse) > 0){
            //หาวันที่หมอเฉพาะทางลงเวลา
            $dentsWhoHavetreatmentSkill = TreatmentSkillRatio::
                            where('treatment_id', $treatment_id)->get();

            foreach($dentsWhoHavetreatmentSkill as $dent){
                $dutyTimeNextDate = DutyTime::where('date', '>', $date)
                                ->where('dent_id', $dent->dent_id)->get();
                if($dutyTimeNextDate->isNotEmpty())
                    array_push($dutyTimeNextDateArray, $dutyTimeNextDate);
            }
            return view('appointment.patient_select_time', compact('dutyTimeNextDateArray', 'dutyTimeofDentists'));            // $dutyTimeNextDate = DutyTime::where()
        }
        $dutyTimeofDentists = $dutyTimeofDentists[0];

        $dentTistCount = $dutyTimeofDentists->count();

        return view('appointment.patient_select_time', compact('dutyTimeofDentists', 'dentTistCount', 'dutyTimeNextDateArray'));

    }

    public function patient_store(REQUEST $request){
        $job = new Job();

        $processing_slot = $request->get('processing_time');
        // $job->hour = $request->get('hour');
        // $job->min = $request->get();

        $job->date = $request->get('date');
        $job->patient_id  = 2;
        $job->dent_id = $request->get('dent_id');
        $job->treatment_id = $request->get('treatment_id');
        $job->slot_start = $request->get('slot');
        $job->slot_end = $request->get('slot') + $processing_slot;
        $job->period_type = 'am';
        $job->status = 'reserved';
        $job->save();
        return \redirect('appointment/patient_appoint_summary/'.$job->patient_id);
        return \response()->json($request);
    }

    public function patient_appoint_summary($patient_id)
    {
        dd($patient_id);
    }

    public function update(Request $request, Appointment $appointment)
    {
        //
    }

    public function view($patientHn){
        $patient = Patient::where('hn', $patientHn)
            ->with('bill', 'bill.dentist', 'bill.treatment', 'bill.patient')
            ->first();

            $dateTimeFormatCtl    = new DateTimeFormatController();

        $patient->b_date_th = $dateTimeFormatCtl->thaiDateFormat($patient->b_date);
        $treatments = Treatment::all();


        $postpone_appointment = Job::where('patient_id', $patientHn)
                            // ->where(function($query){
                            //     return $query->where('status', '=', 'reserved')
                            //             ->orWhere('status', '=', 'processing')
                            //             ->orWhere('status', '=', 'standby');
                            // })

                            ->with('dentist', 'treatment')
                            ->get();

        foreach ($postpone_appointment as $job) {
            if($job->status == 'reserved'){
                $job->thStatus = 'นัดหมอแล้ว';
                $job->btn_style = 'btn-block btn-sm btn-warning';
            }else if($job->status == 'cancel'){
                $job->thStatus = 'ยกเลิก';
                $job->btn_style = 'btn-block btn-sm btn-default';
            }else if($job->status == 'complete'){
                $job->thStatus = 'รักษาเสร็จแล้ว';
                $job->btn_style = 'btn-block btn-sm btn-success';
            }
            else if($job->status == 'processing'){
                $job->thStatus = 'กำลังทำการรักษา';
                $job->btn_style = 'btn-block btn-sm btn-primary';
            }
            else if($job->status == 'standby'){
                $job->thStatus = 'มาตามนัด';
                $job->btn_style = 'btn-block btn-sm btn-info';
            }
            else if($job->status == 'postpone'){
                $job->thStatus = 'เลื่อนนัด';
                $job->btn_style = 'btn-block btn-sm btn-secondary';
            }
            $job->thDate = $dateTimeFormatCtl->thaiDateFormat($job->date);
            $job->treatment_time = $this->convertSlotToTime($job->slot_start, $job->slot_end, $job->period_type);
        }
        return view('appointment.view', compact('postpone_appointment', 'patient', 'treatments'));

    }

    public function destroy(Appointment $appointment)
    {
        //
    }

    public function convertSlotToTime($slot_start, $slot_end, $period_type){
        $hourStart = 0;
        $hourEnd = 0;
        $nextHour = (($slot_start * 5)/60)%10;
        $lastHour = (($slot_end * 5)/60)%10;
        if($period_type == 'am'){
            $hourStart  = 9 + $nextHour;
            $hourEnd    = 9 + $lastHour;
        }else if($period_type == 'ev'){
            $hourStart  = 13 + $nextHour;
            $hourEnd    = 13 + $lastHour;
        }else if($period_type == 'pm'){
            $hourStart  = 17 + $nextHour;
            $hourEnd    = 17 + $lastHour;
        }
        $minStart = ($slot_start*5)%60;
        $minEnd = (($slot_start*5)%60) + 5;

        $hourStart = $hourStart < 10 ? "0".$hourStart : $hourStart;
        $hourEnd = $hourEnd < 10 ? "0".$hourEnd : $hourEnd;
        $minStart = $minStart < 10 ? "0".$minStart : $minStart;
        $minEnd = $minEnd < 10 ? "0".$minEnd : $minEnd;
        return $hourStart.":".$minStart." - ".$hourEnd.":".$minEnd;

    }

    public function dashboard(){
        return view('dashboard');
    }

    public function byroom($room){
        $where = ['date'=>'2020-11-11'];
        $dutyTimeModel = new DutyTime();

        $dutyTimeofDentists = $dutyTimeModel->dutyTime($where);
        $patients =    Patient::all();
        $treatments = Treatment::all();
        $jobs = Job::where('date', '2020-11-11')
        ->orderBy('slot_start' )
        ->orderBy('slot_end')
        ->get();
    
        return view('appointment.byroom', compact('dutyTimeofDentists', 'treatments', 'patients', 'jobs'));
    }
    public function appoint_mobile(){
        $patients   =    Patient::all();
        return view('appointment.appoint_mobile', compact('patients'));
    }
    public function postpone(){
        return view('appointment.postpone');
    }
}
