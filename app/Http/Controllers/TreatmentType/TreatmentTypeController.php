<?php

namespace App\Http\Controllers\TreatmentType;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\TreatmentType;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class TreatmentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {  
            $treatmenttype = TreatmentType::where('name', 'LIKE', "%$keyword%")
                ->orwhere('price', 'LIKE', "%$keyword%")
                // ->orwhere('phone', 'LIKE', "%$keyword%")
               ->latest()->paginate($perPage);
         } else {
            $treatmenttype = TreatmentType::latest()->paginate($perPage);
        }
        
        $treatmenttypeModel = new TreatmentType();

        return view('treament_type.treatment-type.index', compact('treatmenttype'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('treament_type.treatment-type.create');
    }

    
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'piority' => 'required',
            'price' => 'required',
            'sub_type_id' => ['required',Rule::notIn(0)]
        ],
        [
            'name.required' => 'กรุณาใส่ข้อมูล',
            'piority.required' => 'กรุณาใส่ข้อมูล',
            'price.required' => 'กรุณาใส่ข้อมูล',
            'sub_type_id.required' => 'กรุณาเลือก'
        ]);
        $request['user_id'] = 2;
        $request['clinic_id'] = 2;
        $requestData = $request->all();
        
        $TreatmentType = TreatmentType::create($requestData);

        return redirect('treament_type/treatment-type/'.$TreatmentType->id)->with('flash_message', 'TreatmentType added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $treatmenttype = TreatmentType::findOrFail($id);

        return view('treament_type.treatment-type.show', compact('treatmenttype'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $treatmenttype = TreatmentType::findOrFail($id);

        return view('treament_type.treatment-type.edit', compact('treatmenttype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required',
            'piority' => 'required',
            'price' => 'required',
            'sub_type_id' => 'required'
        ],
        [
            'name.required' => 'กรุณาใส่ข้อมูล',
            'piority.required' => 'กรุณาใส่ข้อมูล',
            'price.required' => 'กรุณาใส่ข้อมูล',
            'sub_type_id.required' => 'กรุณาเลือก'
        ]);
        
        $requestData = $request->all();
        
        $treatmenttype = TreatmentType::findOrFail($id);
        $treatmenttype->update($requestData);

        return redirect('treament_type/treatment-type')->with('flash_message', 'TreatmentType updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TreatmentType::destroy($id);

        return redirect('treament_type/treatment-type')->with('flash_message', 'TreatmentType deleted!');
    }
}
