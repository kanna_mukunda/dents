<?php

namespace App\Http\Controllers;

use App\Calendar;
use Illuminate\Http\Request;
use App\Models\DutyTime;
use App\Models\Dentist;
use App\Http\Controllers\DateTimeFormatController;
use App\Http\Controllers\DutyTimeController;
use App\Models\Patient;
use App\Models\room;
class CalendarController extends Controller
{
    public function index()
    {
        $dents = Dentist::all();
        $rooms = room::all();
        return view('calendar.index', compact('dents', 'rooms'));
    }


    public function dentDutyTime(){
        $dutytimes = DutyTime::with('room', 'dentist')->get();

        $dateTimeFormatCtl          = new DateTimeFormatController();
        $dutyTimeCtl                = new DutyTimeController();
        foreach ($dutytimes as $dutytime){
            $dutytime->date = $dateTimeFormatCtl->thaiDateFormat($dutytime->date);
            $dutytime->am_slot_start = $dutytime->am_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->am_slot_start, 9) : [];
            $dutytime->am_slot_end  =  $dutytime->am_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->am_slot_end, 9) : [];
            $dutytime->ev_slot_start = $dutytime->ev_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->ev_slot_start, 13) : [];
            $dutytime->ev_slot_end = $dutytime->ev_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->ev_slot_end, 13) : [];
            $dutytime->pm_slot_start = $dutytime->pm_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->pm_slot_start, 16) : [];
            $dutytime->pm_slot_end = $dutytime->pm_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->pm_slot_end, 16) : [];
        }
        return $dutytimes;

    }

    public function store(Request $request)
    {
        // return $request;
        foreach($request->get('day_off') as $day_off){
            $calendar = new Calendar();

            $calendar->title        = $day_off["'name'"];
            $calendar->dent_id      = $day_off["'dent_id'"];
            $calendar->start_date   = $this->reArangeDateFormat($day_off["'from_date'"]);
            $calendar->start_time   = $day_off["'from_time'"].":00";
            $calendar->end_date     = $this->reArangeDateFormat($day_off["'to_date'"]);
            $calendar->end_time     = $day_off["'to_time'"].":00";
            $calendar->comment      = 'zz';
            $calendar->status       = 'active';
            $calendar->created_at   = date('Y-m-d H:i:s');
            $calendar->updated_at   = date('Y-m-d H:i:s');
            $calendar->save();
        }


        return redirect('calendar/index');
    }

    public function reArangeDateFormat($date){
        $dateArray  = explode('-', $date);
        return $dateArray[2]."-".$dateArray[1]."-".$dateArray[0];
    }

    public function add_events(){
        $dutyTimeCtl                = new DutyTimeController();

        $events = [
            ['id' => 1,
            'title' => 'test',
            'start' => '2020-09-13T10:00:00',
            'end'=> '2020-09-13T11:00:00'],
            ['id' => 2,
            'title' => 'test',
            'start' => '2020-09-14T10:00:00',
            'end'=> '2020-09-14T11:00:00',
            'clickable' => false
        ],

        ];
        $calendars = Calendar::where('start_date', 'like', '%-'.date('m').'-%')->get();
        $arr = [];
        foreach ($calendars as $calendar){
            // 2020-09-13T10:00:00
            array_push($arr, [
                'id' => $calendar->id,
                'title' => $calendar->title,
                'start' => $calendar->start_date."T".$calendar->start_time,
                'end' => $calendar->end_date."T".$calendar->end_time,
                // 'type' => 'calendar'
            ]);
        }
        $dutytimes = DutyTime::where('date', 'LIKE', date('Y-m-%'))->orderBy('date', 'asc')
            ->with('Dentist')
            ->get(
                ['id', 'am_slot_start',
                'am_slot_end',
                'ev_slot_start',
                'ev_slot_end',
                'pm_slot_start',
                'pm_slot_end',
                'dent_id',
                'date']);

        foreach($dutytimes as $dutytime){
            // $dutytime->date = $dateTimeFormatCtl->thaiDateFormat($dutytime->date);
            $dutytime->am_slot_start = $dutytime->am_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->am_slot_start, 9) : [];
            $dutytime->am_slot_end  =  $dutytime->am_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->am_slot_end, 9) : [];
            $dutytime->ev_slot_start = $dutytime->ev_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->ev_slot_start, 13) : [];
            $dutytime->ev_slot_end = $dutytime->ev_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->ev_slot_end, 13) : [];
            $dutytime->pm_slot_start = $dutytime->pm_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->pm_slot_start, 16) : [];
            $dutytime->pm_slot_end = $dutytime->pm_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->pm_slot_end, 16) : [];

            $dutytime->am_hour_start = explode(':' ,$dutytime->am_slot_start)[0];
            $dutytime->am_hour_end = explode(':' ,$dutytime->am_slot_end)[0];
            $dutytime->ev_hour_start = explode(':' ,$dutytime->ev_slot_start)[0];
            $dutytime->ev_hour_end = explode(':' ,$dutytime->ev_slot_end)[0];
            $dutytime->pm_hour_start = explode(':' ,$dutytime->pm_slot_start)[0];
            $dutytime->pm_hour_end = explode(':' ,$dutytime->pm_slot_end)[0];
            // $dateOnly              = str_split(\explode(' ', $dutytime->date)[0]);
            // $dutytime->dateOnly    = $dateOnly[0] == '0' ? $dateOnly[1] : $dateOnly[0].$dateOnly[1];
            $hr_start = $dutytime->am_hour_start < 10 ? "0".$dutytime->am_hour_start : $dutytime->am_hour_start;
            $hr_end = $dutytime->pm_hour_end < 10 ? "0".$dutytime->pm_hour_end : $dutytime->pm_hour_end;
            array_push($arr, [
                'title' =>$dutytime->Dentist->dent_name."[".$hr_start." - ".$hr_end."]",
                'start' => $dutytime->date."T10:00:00",
                'end' => $dutytime->date."T00:00:00",
                'extendedProps'=> [
                    'department'=> 'BioChemistry',
                    'id' => $dutytime->id,
                    'status' => $dutytime->status

                ],
            ]);
        }


        return response()->json($arr);
    }

    public function indexbackup()
    {
        $dutyTimeCtl                = new DutyTimeController();
        $dateTimeFormatCtl          = new DateTimeFormatController();

        $number = cal_days_in_month(CAL_GREGORIAN, date('n'), date('Y')); // 31

        $day_number_arr = [];
        for($i = 0; $i < $number; $i++){
            $d = $i+ 1;
            array_push($day_number_arr, [
                'index' => $i,
                'day_number' =>  $d,
                'day_type' => '',
                'dayInfo' => [],
                'day_name' => '',
                'dents' => [
                    'work' => [
                    ],
                    'vacation' => []
                ]

            ]);
        }

        // หาวันทยุด
        $day_offs = Calendar::where('start_date', 'LIKE', date('Y-m-%'))
            ->with('dentist')
            ->orderBy('start_date', 'asc')
            ->get();
// return $day_offs;
        foreach($day_offs as $day_off){

            $day_off_start_date = str_split(\explode('-',$day_off['start_date'])[2]);
            $start_date = $day_off_start_date[0]  == 0 ? $day_off_start_date[1] : $day_off_start_date[0].$day_off_start_date[1];
            $day_off_end_date = str_split(\explode('-',$day_off['end_date'])[2]);
            $end_date = $day_off_end_date[0]  == 0 ? $day_off_end_date[1] : $day_off_end_date[0].$day_off_end_date[1];


            for($j = $start_date; $j <= $end_date; $j++){
                $dayOffIndex = $j - 1;

                //ถ้าเป็นวันลา ให้ add ข้อมูลหมอที่ลา เข้าไป day_number_arr->dents->vaction []
                $day_number_arr[$dayOffIndex]['day_type'] = 'dayOff';

                if($day_off['calen_day_type'] == 'personal_leave'){
                    array_push($day_number_arr[$dayOffIndex]['dents']['vacation'], [

                        'title'=> $day_off['title'],
                        'start_date'=> $day_off['start_date'],
                        'start_time' => $day_off['start_time'],
                        'end_date'=> $day_off['end_date'],
                        'end_time' => $day_off['end_time'],
                        'dentInfo' =>  $day_off['dentist']
                    ]);
                    $day_number_arr[$dayOffIndex]['day_type'] = 'dayWork';

                }else{
                    $day_number_arr[$dayOffIndex]['day_name'] = $day_off['title'];
                    $day_number_arr[$dayOffIndex]['dayInfo'] = [
                        'start_date'=> $day_off['start_date'],
                        'start_time' => $day_off['start_time'],
                        'end_date'=> $day_off['end_date'],
                        'end_time' => $day_off['end_time'],
                        'dentInfo' =>  $day_off['dentist']
                    ];
                }

            }
        }


        //หาตารางเวรหมอ เอาไปใส่ $day_number_arr
        $dutytimes = DutyTime::where('date', 'LIKE', date('Y-m-%'))->orderBy('date', 'asc')
            ->with('Dentist')
            ->get(
                ['am_slot_start',
                'am_slot_end',
                'ev_slot_start',
                'ev_slot_end',
                'pm_slot_start',
                'pm_slot_end',
                'dent_id',
                'date']);


        //แปลงวันที่
        foreach($dutytimes as $dutytime){
            $dutytime->date = $dateTimeFormatCtl->thaiDateFormat($dutytime->date);
            $dutytime->am_slot_start = $dutytime->am_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->am_slot_start, 9) : [];
            $dutytime->am_slot_end  =  $dutytime->am_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->am_slot_end, 9) : [];
            $dutytime->ev_slot_start = $dutytime->ev_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->ev_slot_start, 13) : [];
            $dutytime->ev_slot_end = $dutytime->ev_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->ev_slot_end, 13) : [];
            $dutytime->pm_slot_start = $dutytime->pm_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->pm_slot_start, 16) : [];
            $dutytime->pm_slot_end = $dutytime->pm_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->pm_slot_end, 16) : [];

            $dutytime->am_hour_start = explode(':' ,$dutytime->am_slot_start)[0];
            $dutytime->am_hour_end = explode(':' ,$dutytime->am_slot_end)[0];
            $dutytime->ev_hour_start = explode(':' ,$dutytime->ev_slot_start)[0];
            $dutytime->ev_hour_end = explode(':' ,$dutytime->ev_slot_end)[0];
            $dutytime->pm_hour_start = explode(':' ,$dutytime->pm_slot_start)[0];
            $dutytime->pm_hour_end = explode(':' ,$dutytime->pm_slot_end)[0];
            $dateOnly              = str_split(\explode(' ', $dutytime->date)[0]);
            $dutytime->dateOnly    = $dateOnly[0] == '0' ? $dateOnly[1] : $dateOnly[0].$dateOnly[1];

            //เพิ้ม ตารางเวรของหมอ
            $dayOffIndex = $dutytime->dateOnly - 1;

            array_push($day_number_arr[$dayOffIndex]['dents']['work'], [
                // 'title'=> $day_off['title'],
                // 'start_date'=> $day_off['start_date'],
                // 'start_time' => $day_off['start_time'],
                // 'end_date'=> $day_off['end_date'],
                // 'end_time' => $day_off['end_time'],
                'dentInfo' =>  $dutytime
            ]);
        }

// return $day_number_arr;

        //หาตารางเวรหมอ เอาไปใส่ $day_number_arr
//         $dutytimes = DutyTime::where('date', 'LIKE', date('Y-m-%'))->orderBy('date', 'asc')
//             ->with('Dentist')
//             ->get(
//                 ['am_slot_start',
//                 'am_slot_end',
//                 'ev_slot_start',
//                 'ev_slot_end',
//                 'pm_slot_start',
//                 'pm_slot_end',
//                 'dent_id',
//                 'date']);
//         //แปลงวันที่
//         foreach($dutytimes as $dutytime){
//             $dutytime->date = $dateTimeFormatCtl->thaiDateFormat($dutytime->date);
//             $dutytime->am_slot_start = $dutytime->am_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->am_slot_start, 9) : [];
//             $dutytime->am_slot_end  =  $dutytime->am_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->am_slot_end, 9) : [];
//             $dutytime->ev_slot_start = $dutytime->ev_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->ev_slot_start, 13) : [];
//             $dutytime->ev_slot_end = $dutytime->ev_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->ev_slot_end, 13) : [];
//             $dutytime->pm_slot_start = $dutytime->pm_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->pm_slot_start, 16) : [];
//             $dutytime->pm_slot_end = $dutytime->pm_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->pm_slot_end, 16) : [];

//             $dutytime->am_hour_start = explode(':' ,$dutytime->am_slot_start)[0];
//             $dutytime->am_hour_end = explode(':' ,$dutytime->am_slot_end)[0];
//             $dutytime->ev_hour_start = explode(':' ,$dutytime->ev_slot_start)[0];
//             $dutytime->ev_hour_end = explode(':' ,$dutytime->ev_slot_end)[0];
//             $dutytime->pm_hour_start = explode(':' ,$dutytime->pm_slot_start)[0];
//             $dutytime->pm_hour_end = explode(':' ,$dutytime->pm_slot_end)[0];
//             $dateOnly              = str_split(\explode(' ', $dutytime->date)[0]);
//             $dutytime->dateOnly    = $dateOnly[0] == '0' ? $dateOnly[1] : $dateOnly[0].$dateOnly[1];

//             //เพิ้ม ตารางเวรของหมอ
//             $dayOffIndex = $dutytime->dateOnly - 1;
//             $day_number_arr[$dayOffIndex]['day_type'] = 'dutytime';
//             array_push($day_number_arr[$dayOffIndex]['dent_dutytime'],[$dutytime->Dentist]);
//             // $day_number_arr[$dayOffIndex]['dent_dutytime'] = ['dentsInfo' => [$day_off['dentist']]];

//         }

//         // return $day_number_arr;
//         for($j= 9; $j <=20; $j++){
//             // หนึ่งวันทำงาน 12 ชม.
//             //แต่ละ ชม มีหมอเข้าเวรกี่คน
//             //1 ชม มี 3 ช่วง เช้า บ่าย ค่ำ
//             foreach($dutytimes as $findDents) {
//                 if($findDents->dateOnly == $d){
//                     if($j <= 12){
//                         //ช่วงเช้า 9-12
//                         if($j >= $findDents->am_hour_start && $j <= $findDents->am_hour_end){
//                             array_push($dents, $findDents);
//                         }
//                     }else if($j > 12 && $j <= 16 ){
//                         // ช่วงบ่าย 13 -16
//                         if($j >= $findDents->ev_hour_start && $j <= $findDents->ev_hour_end){
//                             array_push($dents, $findDents);
//                         }
//                     }else if($j > 16 && $j <= 20){
//                         // ช่วงค่ำ 17 -20
//                         if($j >= $findDents->pm_hour_start && $j <= $findDents->pm_hour_end){
//                             array_push($dents, $findDents);
//                         }
//                     }
//                 }//OUTE IF
//             }//foreach
//             array_push($dentsEachHour, ['hr'=> $j,
//                 'dents'=> $date_type == "day_off"? $dents_day_off : $dents,
//                 'date_type'=> $date_type
//             ]);
//             $dents = [];
//         }


//         return $day_number_arr;
//         $events = [];
//         //สร้าง array เวลา 09:00 -20:00
//         $time_arr = [];
//         for($i= 9; $i <= 20; $i++){
//             // $d = $i< 10 ?'0'.$i : $i;
//             array_push($time_arr, $i);
//         }
//         $dutytimeEachDate = [];
//         $dutytimes = DutyTime::where('date', 'LIKE', date('Y-m-%'))->orderBy('date', 'asc')
//                                         ->with('Dentist')
//                                         ->get(
//                                             ['am_slot_start',
//                                             'am_slot_end',
//                                             'ev_slot_start',
//                                             'ev_slot_end',
//                                             'pm_slot_start',
//                                             'pm_slot_end',
//                                             'dent_id',
//                                             'date']);
//         $day_offs = Calendar::where('start_date', 'LIKE', date('Y-m-%'))
//                 ->with('dentist')
//                 ->orderBy('start_date', 'asc')
//                 ->get();
//         foreach($dutytimes as $dutytime){
//             $dutytime->date = $dateTimeFormatCtl->thaiDateFormat($dutytime->date);
//             $dutytime->am_slot_start = $dutytime->am_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->am_slot_start, 9) : [];
//             $dutytime->am_slot_end  =  $dutytime->am_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->am_slot_end, 9) : [];
//             $dutytime->ev_slot_start = $dutytime->ev_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->ev_slot_start, 13) : [];
//             $dutytime->ev_slot_end = $dutytime->ev_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->ev_slot_end, 13) : [];
//             $dutytime->pm_slot_start = $dutytime->pm_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->pm_slot_start, 16) : [];
//             $dutytime->pm_slot_end = $dutytime->pm_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->pm_slot_end, 16) : [];

//             $dutytime->am_hour_start = explode(':' ,$dutytime->am_slot_start)[0];
//             $dutytime->am_hour_end = explode(':' ,$dutytime->am_slot_end)[0];
//             $dutytime->ev_hour_start = explode(':' ,$dutytime->ev_slot_start)[0];
//             $dutytime->ev_hour_end = explode(':' ,$dutytime->ev_slot_end)[0];
//             $dutytime->pm_hour_start = explode(':' ,$dutytime->pm_slot_start)[0];
//             $dutytime->pm_hour_end = explode(':' ,$dutytime->pm_slot_end)[0];
//             $dutytime->dateOnly        = \explode(' ', $dutytime->date)[0];


//         }
//         // 1. ใน 1 เดือนมี $number วัน
//         for($i = 1; $i <=$number; $i++){
//             //  2.ในแต่ละวัน จะมี ต้อง check ว่าเป็นวันหยุดไหม
//              $day_off_bool = false;
//              $hour_period_info = [];
//             foreach($day_offs as $day_off){
//                 $day_off_start_date = str_split(\explode('-',$day_off['start_date'])[2]);
//                 $start_date = $day_off_start_date[0]  == 0 ? $day_off_start_date[1] : $day_off_start_date[0].$day_off_start_date[1];
//                 $day_off_end_date = str_split(\explode('-',$day_off['end_date'])[2]);
//                 $end_date = $day_off_end_date[0]  == 0 ? $day_off_end_date[1] : $day_off_end_date[0].$day_off_end_date[1];
//                 $date_type = 'dutytime';
//                 if($i >= $start_date && $i <= $end_date){
//                    $day_off_bool = true;
//                 }

//                 //ถ้าเป็นวันหยุด
//                 if($day_off_bool == true){
//                     //ให้สร้าง array
//                     $hour_period_info = [
//                         'hour' => $i,
//                         'day_type' => 'dayOff',
//                         'dents' => $day_off->dentist == null ? ['dentist'=>['dent_name'=> 'หมอทุกคน']] : $day_off->dentist
//                     ];
//                 }
//             }


//         }

// return 'xx';
//         for($i = 1; $i <=$number; $i++){
//             // $d = $i< 10 ?'0'.$i : $i;
//             //วันของเดือน
//             $dentsEachHour = [];
//             $dents = [];
//             $aa = [];
//             foreach($day_offs as $day_off){
//                 //หาวันหยุด วันลาของหมอ
//                 $day_off_start_date = str_split(\explode('-',$day_off['start_date'])[2]);
//                 $start_date = $day_off_start_date[0]  == 0 ? $day_off_start_date[1] : $day_off_start_date[0].$day_off_start_date[1];
//                 $day_off_end_date = str_split(\explode('-',$day_off['end_date'])[2]);
//                 $end_date = $day_off_end_date[0]  == 0 ? $day_off_end_date[1] : $day_off_end_date[0].$day_off_end_date[1];
//                 $date_type = 'dutytime';
//                 if($i >= $start_date && $i <= $end_date){
//                     // for($j= 9; $j <=20; $j++){
//                         // array_push($dentsEachHour, ['hr'=> $j,
//                         //             'dents'=> $day_off->dentist == null ? ['dentist'=>['dent_name'=> 'หมอทุกคน']] : $day_off->dentist ,
//                         //             'date_type'=> 'day_off'
//                         //  ]);
//                         $date_type="day_off";
//                         $dents_day_off= $day_off->dentist == null ? ['dentist'=>['dent_name'=> 'หมอทุกคน']] : $day_off->dentist ;

//                     // }

//                 }
//                     //วันทำงานเวรหมอ
//                     for($j= 9; $j <=20; $j++){
//                         // หนึ่งวันทำงาน 12 ชม.
//                         //แต่ละ ชม มีหมอเข้าเวรกี่คน
//                         //1 ชม มี 3 ช่วง เช้า บ่าย ค่ำ
//                         foreach($dutytimes as $findDents) {
//                             if($findDents->dateOnly == $d){
//                                 if($j <= 12){
//                                     //ช่วงเช้า 9-12
//                                     if($j >= $findDents->am_hour_start && $j <= $findDents->am_hour_end){
//                                         array_push($dents, $findDents);
//                                     }
//                                 }else if($j > 12 && $j <= 16 ){
//                                     // ช่วงบ่าย 13 -16
//                                     if($j >= $findDents->ev_hour_start && $j <= $findDents->ev_hour_end){
//                                         array_push($dents, $findDents);
//                                     }
//                                 }else if($j > 16 && $j <= 20){
//                                     // ช่วงค่ำ 17 -20
//                                     if($j >= $findDents->pm_hour_start && $j <= $findDents->pm_hour_end){
//                                         array_push($dents, $findDents);
//                                     }
//                                 }
//                             }//OUTE IF
//                         }//foreach
//                         array_push($dentsEachHour, ['hr'=> $j,
//                             'dents'=> $date_type == "day_off"? $dents_day_off : $dents,
//                             'date_type'=> $date_type
//                         ]);
//                         $dents = [];
//                     }
//                 }
//             // }


//             array_push($dutytimeEachDate,['date'=> $i, 'dutytimeOfDent'=> $dentsEachHour]);
//         }

// return $day_number_arr;
        $dents = Dentist::all();
        $dent_dutytimes = $this->dentDutyTime();
    return view('calendar.index_patial', \compact('dents', 'day_number_arr', 'dent_dutytimes'));

        // return view('calendar.index', \compact('events', 'dutytimeEachDate', 'dents', 'day_offs', 'dent_dutytimes'));
    }

    public function dutytime_table(){
        return view('calendar.dutytime_table');
    }

    
    
    public function appointment_by_date(){
        return view('calendar.appointment_by_date');
    }
    public function date_click(){
        $patients = Patient::all();
        return view('calendar.date_click', compact('patients'));
    }
}



 // return$dutytimeEachDate;
        // for($i = 1; $i <=$number; $i++){
        //     $d = $i< 10 ?'0'.$i : $i;
        //     $duty_times = DutyTime::where('date', date('Y-m-'.$d))
        //         ->with('Dentist')
        //         ->get();

        //     $dateTimeFormatCtl          = new DateTimeFormatController();
        //     $dutyTimeCtl                = new DutyTimeController();
        //     foreach($duty_times as $dutytime){
        //         $dutytime->date = $dateTimeFormatCtl->thaiDateFormat($dutytime->date);
        //         $dutytime->am_slot_start = $dutytime->am_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->am_slot_start, 9) : [];
        //         $dutytime->am_slot_end  =  $dutytime->am_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->am_slot_end, 9) : [];
        //         $dutytime->ev_slot_start = $dutytime->ev_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->ev_slot_start, 13) : [];
        //         $dutytime->ev_slot_end = $dutytime->ev_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->ev_slot_end, 13) : [];
        //         $dutytime->pm_slot_start = $dutytime->pm_slot_start != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->pm_slot_start, 16) : [];
        //         $dutytime->pm_slot_end = $dutytime->pm_slot_end != -1 ? $dutyTimeCtl->convertSlotToTime($dutytime->pm_slot_end, 16) : [];

        //     }
        //     \array_push($events, [
        //         'date' => $i,
        //         'duty_times' => $duty_times
        //     ]);

        // }

    //   foreach($events as $key => $event){
    //     if(collect($event['duty_times'])->count() > 0){
    //         foreach($event['duty_times'] as $ev){
    //             $start = explode(':',$ev['am_slot_start'])[0];
    //             $end = explode(':',$ev['am_slot_end'])[0];
    //             $index = 0;
    //             for($a = 9; $a <= 12; $a++){
    //                 if($a >= $start && $a <= $end){
    //                     $time_arr[$index] = ['time' => $a, 'dent' => $event['duty_times'][0]['Dentist']];
    //                 }else{
    //                     $time_arr[$index] =0;
    //                 }
    //                 $index++;
    //             }

    //             $ev_start = explode(':',$ev['ev_slot_start'])[0];
    //             $ev_end = explode(':',$ev['ev_slot_end'])[0];
    //             for($aa = 13; $aa <= 16; $aa++){
    //                 if($aa >= $ev_start && $aa <= $ev_end){
    //                     $time_arr[$index] = ['time' => $aa, 'dent' => $event['duty_times'][0]['Dentist']];
    //                 }else{
    //                     $time_arr[$index] =0;
    //                 }
    //                 $index++;
    //             }
    //             $pm_start = explode(':',$ev['pm_slot_start'])[0];
    //             $pm_end = explode(':',$ev['pm_slot_end'])[0];
    //             for($aaa = 17; $aaa <= 20; $aaa++){
    //                 if($aaa >= $pm_start && $aaa <= $pm_end){
    //                 //    dd($event['duty_times'][0]['Dentist']);
    //                     $time_arr[$index] = ['time' => $aaa, 'dent' => $event['duty_times'][0]['Dentist']];
    //                 }
    //                 else{
    //                     $time_arr[$index] =0;
    //                 }
    //                 $index++;
    //             }
    //             $events[$key]['duty_times']['time'] = $time_arr;
    //         }
    //     }
    //   }
    //    return($events);

