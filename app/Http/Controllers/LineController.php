<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class LineController extends Controller
{
    public function redirectToProvider()
    {
            // URL API LINE
            $API_URL = 'https://api.line.me/v2/bot/message';
            // ใส่ Channel access token (long-lived)
            $ACCESS_TOKEN = 'hPqXmF5MJxxmzOBlvY43ULEq/gv/H0zMks3Oq6tIW+HpwO84LZ/P20e/Qq2iyPJ2rsgMBUMgzbm/wcU17nbIivlcP29aQ7DZBVpnkAwxmlU27c5LG9pCYas5+fc+u2O6KTGww76RwSXgnJWgkOCJcgdB04t89/1O/w1cDnyilFU=';
            // ใส่ Channel Secret
            $CHANNEL_SECRET = '83e3da3c14bd4898608ca2a3beb6b295';
            // Set HEADER
            $POST_HEADER = array('Content-Type: application/json', 'Authorization: Bearer ' . $ACCESS_TOKEN);
            // Get request content
            $request = file_get_contents('php://input');
            // Decode JSON to Array
            $request_array = json_decode($request, true);

            if ( sizeof($request_array['events']) > 0 ) {
                foreach ($request_array['events'] as $event) {

                $reply_message = '';
                $reply_token = $event['replyToken'];
                $data = [
                   'replyToken' => $reply_token,
                   'messages' => [
                      ['type' => 'text',
                       'text' => json_encode($request_array)]
                   ]
                ];
                $post_body = json_encode($data, JSON_UNESCAPED_UNICODE);
                $send_result = $this->send_reply_message($API_URL.'/reply', $POST_HEADER, $post_body);
                echo "Result: ".$send_result."\r\n";
             }
          }
          echo "OK";

    }

    function send_reply_message($url, $post_header, $post_body)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $post_header);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_body);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }


    /**
     * Obtain the user information from line.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
       $user = Socialite::driver('line')->user();
	dd($user);




    }
}
