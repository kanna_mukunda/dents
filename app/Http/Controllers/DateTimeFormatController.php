<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DateTimeFormatController extends Controller
{
    public function thaiDateFormat($date){
        $dateArray  = explode('-', $date);
        $month      = $this->thaiMonthFormat($dateArray[1]);
        $year       = $dateArray[0] + 543;
        return $dateArray[2]." ".$month." ".$year;
    }

    public function thaiMonthFormat($monthDigit){
         $thMonth = [
                "01"=> "มกราคม",
                "02"=> "กุมภาพันธ์",
                "03"=> "มีนาคม",
                "04"=> "เมษายน",
                "05"=> "พฤษภาคม",
                "06"=> "มิถุนายน",
                "07"=> "กรกฏาคม",
                "08"=> "สิงหาคม",
                "09"=> "กันยายน",
                "10"=> "ตุลาคม",
                "11"=> "พฤศจิกายน",
                "12"=> "ธันวาคม"
        ];
        return $thMonth[$monthDigit];
    }

    public function getthaiMonthStringToNumber($monthThString){
        $thMonth = [
             "มกราคม" => "01",
             "กุมภาพันธ์" => "02",
             "มีนาคม" => "03",
             "เมษายน" => "04",
             "พฤษภาคม" => "05",
             "มิถุนายน" => "06",
             "กรกฏาคม" => "07",
             "สิงหาคม" => "08",
             "กันยายน" => "09",
             "ตุลาคม" => "10",
             "พฤศจิกายน" => "11",
             "ธันวาคม" => "12",
       ];
       return $thMonth[$monthThString];
   }

    public function convertThaidateToEngDateFormat($thaidate){
        //วว/ดด/ปปปป
        $dateArray  = explode('-', $thaidate);
        $month = $dateArray[1];
        if(strlen($month) > 2){
            //เป็นเดือนภาษาไทย
            $month = $this->getthaiMonthStringToNumber($month);
        }
        $year       = $dateArray[2] - 543;
        return $year."-".$month."-".$dateArray[0];
    }

    public function convertThaidateStringToEngDateFormat($thaidate){
        //วว/ดด/ปปปป
        $dateArray  = explode(' ', $thaidate);
        $month = $this->getthaiMonthStringToNumber($dateArray[1]);
        $year       = $dateArray[2] - 543;
        return $year."-".$month."-".$dateArray[0];
    }

    // public function convertThaidateToEngDateFormatFromDatabae($thaidate){
    //     //วว/ดด/ปปปป
    //     $dateArray  = explode('-', $thaidate);
    //     $year       = $dateArray[2] - 543;
    //     return $year."-".$dateArray[1]."-".$dateArray[0];
    // }

}


