<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Patient;
use App\Models\TreatmentImage;

class TreatmentHistoryController extends Controller
{
    public function create($hn){
        $patient = Patient::where('hn', $hn)
            ->with('bill', 'bill.dentist', 'bill.treatment', 'bill.patient')
            ->first();
        // return $patient;
        return view('treatment_history.create', compact('patient'));
    }

    public function store(REQUEST $request){
        $target_dir = "uploads/";
        $uploadOk = 1;

        for($i = 0; $i < count($_FILES["file"]['name']); $i++){
            $target_file = $target_dir . basename($_FILES["file"]["name"][$i]);
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

            // // Check if image file is a actual image or fake image
            //     $check = getimagesize($_FILES["file"]["tmp_name"][$i]);
            //     if($check !== false) {
            //     echo "File is an image - " . $check["mime"] . ".";
            //     $uploadOk = 1;
            //     } else {
            //     echo "File is not an image.";
            //     $uploadOk = 0;
            //     }

            // Check if file already exists
            if (file_exists($target_file)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }

            // Check file size
            if ($_FILES["file"]["size"][$i] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }

            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["file"]["tmp_name"][$i], $target_file)) {
                    $upload             = new TreatmentImage();
                    $upload->bill_id    = $request->get('bill_id');
                    $upload->image_path = basename( $_FILES["file"]["name"][$i]);
                    $upload->status     = 'active';
                    $upload->created_at = date('Y-m-d H:i:s');
                    $upload->updated_at = date('Y-m-d H:i:s');
                    $upload->save();
                } else {
                echo "Sorry, there was an error uploading your file.";
                }
            }
        }

    }
}
