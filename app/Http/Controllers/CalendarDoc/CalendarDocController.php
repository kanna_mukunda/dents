<?php

namespace App\Http\Controllers\CalendarDoc;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\CalendarDoc;
use App\Models\TreatmentSkillRatio;
use App\Models\Dentist;
use App\Models\Treat;
use Illuminate\Http\Request;

use function PHPSTORM_META\type;

class CalendarDocController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $checkDate =  explode(" ", $keyword);
        $perPage = 25;

        if (!empty($keyword)) {
            $type_id = Treat::where("treatment_name", 'LIKE', "%$keyword%")->get(['id']);
            $doc_id = Dentist::where("dent_name", 'LIKE', "%$keyword%")->get(['id']);
            // return $doc_id;
            $a_time = CalendarDoc::where("a_time", 'LIKE', "%$keyword%")->get(['a_time']);

            $calendardoc = CalendarDoc::where('name', 'LIKE', "%$keyword%")
                ->orWhere('date_book','LIKE', count($checkDate) > 1 ? "%$checkDate[0]%" : "%$keyword%")
                ->orWhere('a_time', 'LIKE', count($checkDate) > 1 ? "%$checkDate[1]%" : "%$keyword%")
                ->orWhere('type_id','=', collect($type_id)->isNotEmpty() ? $type_id[0]->id : '')
                ->orWhere('doc_id', '=', collect($doc_id)->isNotEmpty() ? $doc_id[0]->id : '')
                ->orWhere('deleted', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->orWhere('clinic_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $calendardoc = CalendarDoc::latest()->paginate($perPage);
        }
        
        $calendardocModel = new CalendarDoc();

        foreach($calendardoc as $calen){
            $calen->datetime = $calendardocModel->explodeDateTimeFormat($calen['date_book']);
        }
        return view('calendar_doc.calendar-doc.index', compact('calendardoc'));
    }

    public function create()
    {
        $dentists = Dentist::all();
        $treatments = Treat::all();
        return view('calendar_doc.calendar-doc.create', compact('dentists','treatments'));
    }

 
    public function store(Request $request)
    {
        $dentist_skills = TreatmentSkillRatio::where('dent_id', $request->get('doc_id'))->get();
        $start =  intval(explode(':', $request->get('start_time'))[0]);
        $end   =  intval(explode(':', $request->get('end_time'))[0]);
        foreach($dentist_skills as $skill){
            $numPerPeriods = 60/$skill->ratio;
            for($i = $start; $i < $end; $i++){ 
                for($j= 0; $j < $numPerPeriods; $j++){
                    $a_time = $skill->ratio *$j;
                    $calendardoc = new CalendarDoc();
                    $calendardoc->name          = $request->get('name');
                    
                    $date = date('d/m/Y', strtotime($request->get('date')));
                    $calendardoc->date_book	    = $date.'|'.$request->get('start_time').'|'.$request->get('end_time');
                    $calendardoc->create_date   = date('d/m/Y');
                    $calendardoc->type_id       = $skill->treatment_id;
                    $hr     = $i < 10 ? '0'.$i : $i;
                    $min    = $a_time < 10 ? '0'.$a_time : $a_time;
                    $calendardoc->a_time        = $hr.':'.$min;
                    $calendardoc->doc_id        = $request->get('doc_id');
                    $calendardoc->created_at    = date('Y-m-d H:i:s');
                    $calendardoc->updated_at	= date('Y-m-d H:i:s');
                    $calendardoc->save();
                    
                }
            }
        }
        return redirect('calendar-doc')->with('flash_message', 'CalendarDoc added!');
        // return redirect('calendar-doc/'.$calendardoc->doc_id)->with('flash_message', 'CalendarDoc added!');
    }

   
    public function show($doc_id)
    {   
        $calendardocModel = new CalendarDoc();
        $calendardocs = CalendarDoc::where('doc_id', $doc_id)->get();
        foreach($calendardocs as $calendardoc){
             $calendardoc->datetime = $calendardocModel->explodeDateTimeFormat($calendardoc->date_book);

        }
        return view('calendar_doc.calendar-doc.show', compact('calendardocs'));
    }

  
    public function edit($id)
    {
        $calendardoc = CalendarDoc::findOrFail($id);

        return view('calendar_doc.calendar-doc.edit', compact('calendardoc'));
    }

   
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $calendardoc = CalendarDoc::findOrFail($id);
        $calendardoc->update($requestData);

        return redirect('calendar-doc')->with('flash_message', 'CalendarDoc updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        CalendarDoc::destroy($id);

        return redirect('calendar-doc')->with('flash_message', 'CalendarDoc deleted!');
    }
}
