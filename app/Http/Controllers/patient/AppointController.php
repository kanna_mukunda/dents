<?php

namespace App\Http\Controllers\patient;

use App\Http\Controllers\Controller;
use App\Http\Controllers\DateTimeFormatController;
use App\Http\Controllers\AppointmentController;
use Illuminate\Http\Request;
use App\Treatment;
use App\Models\Patient;
use App\Models\DutyTime;
use Illuminate\Support\Facades\DB;
use App\Models\TreatmentSkillRatio;
use App\Models\Job;
use Illuminate\Support\MessageBag;
use App\Http\Controllers\Api\AppointmentController as apiAppontmentCtrl;


class AppointController extends Controller
{
    public function index(REQUEST $request){
        $job_id = $request->session()->get('job_id');
        $treatments = Treatment::all();
        return view('patient.appointment.index', compact('treatments', 'job_id'));
    }

    public function dashboard(REQUEST $request){
        $patient = $request->session()->get('patient');
        $job  =  Job::where('patient_id',$patient->hn)
                    ->where('status', 'reserved')
                    ->with('treatment', 'dentist', 'patient', 'room')
                    ->get();
        $job_id = collect($job)->isNotEmpty() ? $job[0]->id : 0;
        session(['job_id' => $job[0]->id]);

        return view('patient.appointment.dashboard', compact('job', 'job_id'));
    }

    public function authen(REQUEST $request){
        // หา user โดยจากเบอโทร
        $user = Patient::where('id' , $request->get('id'))->first();
        if(collect($user)->isNotEmpty()){
            session(['patient' => $user]);
        }
        $treatments = Treatment::all();
        return redirect('patient/appointment/dashboard');

    }

    public function about_us(Request $request){
        $job_id = $request->session()->get('job_id');
        return view('patient.about_us', compact('job_id'));
    }

    public function select_date_treate(Request $request){
        $job_id = $request->session()->get('job_id');
        $treatments = Treatment::all();
        return view('patient.appointment.select_day_treat', compact('treatments', 'job_id'));
    }


    public function index2($date = null)
    {
        if($date == null){
            $date = date('Y-m-d');
        }
        // else{
        //     $dateArray = explode(" ", $date);
        //     $date = $dateArray[1]." ".$dateArray[2]." ".$dateArray[3];
        //     $date =  date('Y-m-d', strtotime($date));
        // }
        $dateTimeFormatCtl          = new DateTimeFormatController();
        $appointmentCtl             = new AppointmentController();

        //หางานจาก ตาราง Job วันที่่เลือก
        $jobs = Job::where('date', $date)
                    ->orderBy('slot_start' )
                    ->orderBy('slot_end')
                    ->get();
        $jobsGrouped = collect($jobs)->groupBy('treatment_id')->values();
        $treatments = Treatment::all();
        foreach ($treatments as $treatment){
            $jobCount= Job::where('date', $date)
                    ->where('treatment_id', $treatment->id)
                    ->count();
            $treatment->count = $jobCount;
        }

        $patients =    Patient::all();
        //หาหมอที่เข้าเวร
        $dutyTimeModel = new DutyTime();
        $where = ['date'=>$date];
        $dutyTimeofDentists = $dutyTimeModel->dutyTime($where);
        $patientAppointController = new AppointController();

        foreach ($dutyTimeofDentists as $dent){
            $dent->thaiDate = $dateTimeFormatCtl->thaiDateFormat($date);

            $infoArray = [];

            //สร้าง slot ตารางงานของหมอ แต่ละคน
            // slot($dent , $date, $init_time_hr_temp, $last_time_hr_temp, $slot_num_temp, $hour_num_temp)
            $morningSlot = $appointmentCtl->slot($dent, $date, 9, 12, 36, 3, 'am');
            $eveningSlot = $appointmentCtl->slot($dent, $date, 13, 16, 36, 3, 'ev');
            $nightSlot = $appointmentCtl->slot($dent, $date, 16, 20, 48, 4, 'pm');


            $filtered = collect($morningSlot)->filter(function($val){
                return $val['slot_type'] != 'empty';
            });
            $infoByRoom = $filtered->groupBy('job_id')->values();

            // foreach($infoByRoom as $a){
            //     $first = collect($a)->first();
            //     $last  = collect($a)->last();
            //     // dd($first);
            //     $firstHour  = $first['hour']< 10 ? '0'.$first['hour'] : $first['hour'];
            //     $lastHour   = $last['hour'] < 10 ? '0'.$last['hour'] : $last['hour'];
            //     $firstMin   = $first['min'] < 10 ? '0'.$first['min'] : $first['min'];
            //     $lastMin    = $last['min'] < 10 ? '0'.$last['min'] : $last['min'];
            //     array_push($infoArray , [
            //         'job_id'        => $first['job_id'],
            //         'firstHourMin'  => $firstHour.":".$firstMin,
            //         'lastHourMin'   => $lastHour.":".$lastMin,
            //         'patient_name'  => $first['jobInfo']['patient']['name'],
            //         'patient_phone' => $first['jobInfo']['patient']['phone'],
            //         'dent_name'     =>  $dent->dentist->dent_name,
            //         'treatment'     => $first['attribute']['treatment_name'],
            //         'status'        => $first['jobInfo']['status'],
            //         'created_at'    => $first['jobInfo']['created_at'],
            //     ]);
            // }

            // $dent->infoByRoom = $infoArray;

             $dent->morningSlotChunk_3 = $appointmentCtl->chunk3($morningSlot);
             $dent->eveningSlotChunk_3 = $appointmentCtl->chunk3($eveningSlot);
             $dent->nightSlotChunk_3 = $appointmentCtl->chunk3($nightSlot);
        }
        //หาช่่วงเวลาที่ไม่ว่างของหมอหลักก่อน
               //หาช่่วงเวลาที่ไม่ว่างของหมอหลักก่อน
               $empMorningPeriodTimeDents = $this->findEmpMorningPeriodTimeDents($dutyTimeofDentists);
               $empEveningPeriodTimeDents = $this->findEmpEvenningPeriodTimeDents($dutyTimeofDentists);
               $empNightPeriodTimeDents = $this->findEmpNightPeriodTimeDents($dutyTimeofDentists);

        return
        [
            'dutyTimeofDentists'        => $dutyTimeofDentists,
            // 'treatments'                => $treatments,
            // 'patients'                  => $patients,
            'empMorningPeriodTimeDents'=> $empMorningPeriodTimeDents,
            'empEveningPeriodTimeDents'=> $empEveningPeriodTimeDents,
            'empNightPeriodTimeDents'=> $empNightPeriodTimeDents,
        ];
    //    return view('patient.appointment.index', compact('dutyTimeofDentists', 'treatments', 'patients', 'jobs',
    //                     'empMorningPeriodTimeDents', 'empEveningPeriodTimeDents', 'empNightPeriodTimeDents'));
    }

    public function create(REQUEST $request){
        $request->validate([
            'treatment_id' => 'numeric| required',
        ]);
        $job_id = $request->session()->get('job_id');
        $thdate                     = $request->get('date'); //"";
        $treatment_id               = $request->get('treatment_id');
        $dutyTimeModel              = new DutyTime();
        $dutyTimeNextDateArray      = [];
        $dateTimeFormatCtl          = new DateTimeFormatController();
        $thaiNextDateArray          = [];
        $patient_id                 = '222222';

        // ค้นหาหมอที่เข้าเวร วันที่คนไข้เลือก
        $date = $dateTimeFormatCtl->convertThaidateToEngDateFormat($thdate);

        //หาข้อมูล Treatment
        $treatment = Treatment::find($treatment_id);
        $res =  $this->index2($date);
        $dutyTimeofDentists        = $res['dutyTimeofDentists'];
        $empMorningPeriodTimeDents = $res['empMorningPeriodTimeDents'];
        $empEveningPeriodTimeDents = $res['empEveningPeriodTimeDents'];
        $empNightPeriodTimeDents = $res['empNightPeriodTimeDents'];
        $patient = $request->session()->get('patient');
        return view('patient.appointment.create', compact(
                'dutyTimeofDentists',
                'empMorningPeriodTimeDents',
                'empEveningPeriodTimeDents',
                'empNightPeriodTimeDents',
                'treatment',
                'patient',
                'job_id'
            ));

    }

    public function create2(REQUEST $request){
        $request->validate([
            'treatment_id' => 'numeric| required',
        ]);


        $thdate                     = $request->get('date'); //"";
        $treatment_id               = $request->get('treatment_id');
        $dutyTimeModel              = new DutyTime();
        $dutyTimeNextDateArray      = [];
        $dateTimeFormatCtl          = new DateTimeFormatController();
        $thaiNextDateArray          = [];


        // ค้นหาหมอที่เข้าเวร วันที่คนไข้เลือก
        $date = $dateTimeFormatCtl->convertThaidateToEngDateFormat($thdate);
        $where                  = ['date'=>$date];
        $dutyTimeofDentists     = $dutyTimeModel->dutyTime($where);
        if($dutyTimeofDentists->isEmpty()){
            //ถ้าไม่มีหมอเข้าเวร ตามการรักษาที่เลือก
            // หาวันที่หมอลงเวลาที่มากกว่าวันที่คนไข้เลือก
            $findNextDutyTime   = $dutyTimeModel->getDutyTimeNextDay($date);
            foreach ($findNextDutyTime as $nextDate){
                $thaidate = $dateTimeFormatCtl->thaiDateFormat($nextDate);

                array_push($thaiNextDateArray, [$nextDate, $thaidate]);
            }
            return view('patient.appointment.create', compact('dutyTimeofDentists', 'thaiNextDateArray'));
        }else{
            //ถ้ามีหมอเข้าเวร ตามรายการรักษาที่เลือก
            // $appointmentCtl = new AppointmentController();
            foreach ($dutyTimeofDentists as $dent){
                //สร้าง slot ตารางงานของหมอ แต่ละคน ที่สามารถรักษาโรคที่ผู้ป่วยเลือก
                $where = ['treatment_id' => $treatment_id, 'dent_id' => $dent->dent_id];

                $dent->skill_slot          = $this->findSlotByDateAndTreatment($date, $where);
                $treatmentModel             = new Treatment();
                $dent['selected_treatment'] = $treatmentModel->getTreatment($treatment_id);
                $selected_treatment_id      = $dent['selected_treatment']->id;

                for($i = 0; $i < count($dent->dentist->treatmentSkillRatio); $i++){
                    if($dent->dentist->treatmentSkillRatio[$i]['id'] == $selected_treatment_id){
                        $dent['processing_time'] = $dent->dentist->treatmentSkillRatio[$i]['ratio'] * $dent['selected_treatment']->treatment_mean_time;
                    }
                }
            }
            //กรองเอาหมอที่มี skill ที่คนไข้เลือกskill ที่ต้องการ ในวันที่เลือก
            $dentHasSkillPatientChoosed = $dutyTimeofDentists->filter(function ($val){
                return $val->skill_slot != [];
            });

            if($dentHasSkillPatientChoosed->isEmpty()){
                // ถ้าไม่มีหมอที่มี skill ที่ patient เลิือกการรักษา ในวันที่ patient เลือก
                // ให้แสดงข้อความ ไม่พบ ค้นหาวันที่ หมอที่มี skill เข้าเวร
                //และหา nextDate
                $status = 'nextDateByTreatment';
                $res  = DB::table('duty_time')
                    ->join('dentist', 'duty_time.dent_id', '=', 'dentist.id')
                    ->join('treatment_skill_ratio', 'dentist.id', '=', 'treatment_skill_ratio.dent_id')
                    ->where('treatment_skill_ratio.treatment_id', $treatment_id)
                    ->where('duty_time.date', '>', $date)
                    ->get();

                if($res->isNotEmpty()){
                    foreach ($res as $nextDate){
                        $thaidate = $dateTimeFormatCtl->thaiDateFormat($nextDate->date);

                        array_push($thaiNextDateArray, [$nextDate->date, $thaidate]);
                    }
                    return view('patient.appointment.create', compact('dutyTimeofDentists', 'thaiNextDateArray'));
                }else{
                    //ถ้าไม่มีหมอเข้าเวร
                    $noDutyTime = true;
                    return view('patient.appointment.create', compact('noDutyTime'));
                }

            }else{
                //ถ้ามีหมอเข้าเวร ในวันที่เลือก , treatment_id ที่เลือก
                $dutyTimeofDentists = $dentHasSkillPatientChoosed[0];
                $dutyTimeofDentists->date = $dateTimeFormatCtl->thaiDateFormat($date);
                $dentTistCount = $dutyTimeofDentists->count();

               return view('patient.appointment.create', compact('dutyTimeofDentists', 'dentTistCount'));

            }
        }
    }

    public function store(Request $request){
        $job = new Job();
        // $dateTimeFormatCtl  = new DateTimeFormatController();
        // $processing_slot = $request->get('processing_time');
        $hour_slot = 0;
        $slot_start = 0;
        $hour_slot = $this->findHourSlot($request->get('hour'));
        $min_slot = $request->get('min')/5;
        $slot_start = $hour_slot + $min_slot;

        $treatment_ratio = TreatmentSkillRatio::where('dent_id', $request->get('dent_id'))
                            ->where('treatment_id', $request->get('treatment_id'))
                            ->first();
        $job->date              = $request->get('date');//$dateTimeFormatCtl->convertThaidateStringToEngDateFormat($request->get('date'));
        $job->patient_id        = $request->get('patient_hn');
        $job->dent_id           = $request->get('dent_id');
        $job->treatment_id      = $request->get('treatment_id');
        $job->slot_start        = $slot_start;
        $job->slot_end          = ($slot_start + $treatment_ratio->ratio)-1;
        $job->period_type       = $request->get('period_type');
        $job->room_id           = $request->get('room');
        $job->status            = 'reserved';
        $job->save();
        return response()->json($job->id);
    }

    public function findHourSlot($hour){
        $slot = 0;
        if($hour ==9 || $hour == 13 || $hour ==16){
            $slot = 0;
        }
        else if($hour ==10 || $hour == 14 || $hour ==17){
            $slot = 12;
        }
        else if($hour ==11 || $hour == 15 || $hour ==18){
            $slot = 24;
        }
        else if($hour ==12 || $hour == 16 || $hour ==19){
            $slot = 36;
        }else if($hour == 20){
            $slot = 48;
        }
        return $slot;
    }

    public function appointment_summary(REQUEST $request, $jobId){
        // $summaryInfo = Job::where('id', $jobId)
        //                 ->with('dentist', 'treatment', 'patient', 'room')
        //                 ->get()->first();
        $appointmentCtrl             = new AppointmentController();
        $apiAppontmentCtrl = new apiAppontmentCtrl();
        $appointmentInfos =  $apiAppontmentCtrl->appointment_result($jobId);
        $timeArray = explode('-',$appointmentCtrl->convertSlotToTime($appointmentInfos->slot_start, $appointmentInfos->slot_end, $appointmentInfos->period_type));
        $appointmentInfos->start_time = $timeArray[0];
        $appointmentInfos->end_time = $timeArray[1];
        session('job_id', $appointmentInfos->id);
        $job_id = $request->session()->get('job_id');
        return view('patient.appointment.appointment_summary', compact('appointmentInfos', 'job_id'));
    }

    public function findSlotByDateAndTreatment($date, $where){
        $treatementSkillRatioModel = new TreatmentSkillRatio();
        //  หาหมอที่มี skill

        $findDentsByTreatmentSkill = $treatementSkillRatioModel->findDentsByTreatmentSkill($where);

        if($findDentsByTreatmentSkill->isEmpty()){
            return [];
        }

        $dutyTimeModel = new DutyTime();
        $dutyTimeOfDents = [];
        $treatmentModel             = new Treatment();
        $dent['selected_treatment'] = $treatmentModel->getTreatment($where['treatment_id']);

        // ได้ lists ของหมอที่มี skill และ date
        foreach ($findDentsByTreatmentSkill as $dent){
            //สร้าง ratioSlot
            $ratioSlot = ($dent->treatment->treatment_mean_time/5) * $dent->treatment->treatmentSkillRatio->ratio;
            $dent['treatment_processing_slot'] = $ratioSlot;

            //สร้าง slot ตารางงานของหมอ แต่ละคน
            // slot($dent , $date, $init_time_hr_temp, $last_time_hr_temp, $slot_num_temp, $hour_num_temp, $period_temp, $ratioSlot)
            $dent['morningSlot'] = $this->slot($dent, $date, 9, 10, 4, 1, 'am', $ratioSlot);
            $dent['eveningSlot'] = $this->slot($dent, $date, 12, 16, 48, 4, 'ev', $ratioSlot);
            $dent['nigthSlot'] = $this->slot($dent, $date, 16, 20, 48, 4, 'pm', $ratioSlot);

        }

        return $findDentsByTreatmentSkill;
    }

    public function slot($dent, $date, $init_time_hr_temp, $last_time_hr_temp, $slot_num_temp, $hour_num_temp, $period_temp, $ratioSlot){
        // หาslot ที่ไมว่างของหมอ
        $jobs = Job::where('dent_id', $dent->dent_id)
                ->where('date' , $date)
                ->where('period_type', $period_temp)
                ->with('patient', 'treatment')
                ->orderBy('slot_start', 'asc')
                ->get();


        // dd($dent);
        $count = 0;
        $init_time_hr = $init_time_hr_temp; //9;
        $last_time_hr = $last_time_hr_temp;//12;
        $init_time_min = 0;
        $slot_num = $slot_num_temp;//36;
        $hour_num = $hour_num_temp;
        $slotArray = [];
        for($i = 0; $i < $hour_num; $i++){
            //ช่วงเช้ามี 3   ชั่วโมง
            for($j = 0; $j < 12; $j++){
                $min = ($init_time_min + $j) * 5;
                $morningSlot = ["index" => $count, 'dent_id'=> $dent->id, "job_id" => 0, 'slot_type' => 'empty', 'status' => 'active',
                    'hour' => $init_time_hr, 'min'=>$min];
                array_push($slotArray, $morningSlot);
                $count++;
            }
            $init_time_hr = $init_time_hr+1;
        }

        foreach($jobs as $key => $job){
            // dd($job);
            $slot_start = $job->slot_start;
            $slot_end = $job->slot_end;

            for($i = $slot_start; $i <= $slot_end; $i++ ){
                $slotArray[$i]['status'] = 'inacative';
                if($slot_start == $slot_end){
                    //  slot fisrt และ  last เป้น index เดียวกันฝห
                    $slotArray[$i]['slot_type'] = 'start-end';
                    break;
                }
                else if($i == $slot_start && $slot_end != 0){
                    $slotArray[$i]['slot_type'] = 'start';
                }
                else if($i == $slot_end){
                    $slotArray[$i]['slot_type'] = 'end';
                }else if($i > $slot_start  && $i < $slot_end){

                    $childIndex = $i;
                    $slotArray[$childIndex]['status'] = 'inacative';
                    $slotArray[$childIndex]['slot_type'] = 'child';
                }
            }

        }
        $slotArrayFiltered = collect($slotArray)->filter(function($val){
            return $val['slot_type'] == 'start' || $val['slot_type'] == 'start-end';
        });
        $count = 1;
        foreach($slotArrayFiltered as $filtered){
            for($i = $filtered['index']; $i > 0; $i--){

                $index = $i - 1 ;

                if($slotArray[$index]['slot_type'] == 'end' || $slotArray[$index]['slot_type'] == 'first-end'){
                    break;
                }else if($count < $ratioSlot){
                    $slotArray[$index]['status'] = 'disabled';
                    $slotArray[$index]['slot_type'] = 'disabled';
                }

                echo $count++.'d';
                // $count++;
                // $slotArray[$a]['jobInfo'] = $job;
            }
        }


        return($slotArray);
    }

    public function slot1($dent, $date, $init_time_hr_temp, $last_time_hr_temp, $slot_num_temp, $hour_num_temp, $period_temp, $ratioSlot){
        // หาslot ที่ไมว่างของหมอ
        $jobs = Job::where('dent_id', $dent->dent_id)
                ->where('date' , $date)
                ->where('period_type', $period_temp)
                ->with('patient', 'treatment')
                ->orderBy('slot_start', 'asc')
                ->get();


        // dd($jobs);
        $count = 0;
        $init_time_hr = $init_time_hr_temp; //9;
        $last_time_hr = $last_time_hr_temp;//12;
        $init_time_min = 0;
        $slot_num = $slot_num_temp;//36;
        $hour_num = $hour_num_temp;
        $slotArray = [];
        for($i = 0; $i < $hour_num; $i++){
            //ช่วงเช้ามี 3   ชั่วโมง
            for($j = 0; $j < 12; $j++){
                $min = ($init_time_min + $j) * 5;
                $morningSlot = ["index" => $count, 'dent_id'=> $dent->id, "job_id" => 0, 'slot_type' => 'empty', 'status' => 'active',
                    'hour' => $init_time_hr, 'min'=>$min];
                array_push($slotArray, $morningSlot);
                $count++;
            }
            $init_time_hr = $init_time_hr+1;
        }


        foreach($jobs as $key => $job){
            // dd($job);
            $slot_start = $job->slot_start;
            $slot_end = $job->slot_end;

            if($slot_start == 0){
                 //่jobแรก
                $slotArray[$slot_start]['status'] = 'inacative';
                $slotArray[$slot_start]['slot_type'] = $slot_start == $slot_end ? 'start-end' : 'start';

                 //check slot หน้า ว่า active ไหม เทียบกับ $ratioSlot  จนถึง slot 0
                $count = 0;
                for($i = $slot_end; $i <= $slot_start; $i--){
                    $a = $i - 1 ;
                    ++$count;
                    // if($slotArray[$a]['slot_type'] == 'end' || $slotArray[$a]['slot_type'] == 'first-end'){
                    //     break;
                    // }
                    if($a == -1){
                        //เป็น slot index ที่ 0
                        $slotArray[$slot_start]['status'] = 'inacative';
                        $slotArray[$slot_start]['slot_type'] = $slot_start == $slot_end ? 'start-end' : 'start';
                    }
                    if($count < $ratioSlot){
                        $slotArray[$a]['status'] = 'inacative';
                        $slotArray[$a]['slot_type'] = 'child';
                    }
                    // $slotArray[$a]['jobInfo'] = $job;
                }

            }else if($key < (count($jobs) -1) && $key > 0){
                //job ระหว่าง first กับ lastตัวสุดท้ายของช่วงเวลา
                $slotArray[$slot_start]['slot_type'] = $slot_start == $slot_end ? 'start-end' : 'start';
                $slotArray[$slot_start]['status'] = 'inacative';

                $slotArray[$slot_end]['slot_type']  =  $slotArray[$slot_start]['slot_type'] != 'start-end' ? 'end' : 'start-end';
                $slotArray[$slot_end]['status']     =  'inacative';

                if($slotArray[$slot_start]['slot_type'] != 'start-end'){
                    //หา child
                    $range = $slot_end - $slot_start;
                    for($i = 1; $i < $range; $i++){
                        $a = $slot_start + $i;
                        $slotArray[$a]['slot_type'] = 'child';
                        $slotArray[$a]['status']     =  'inacative';
                        // $slotArray[$a]['jobInfo'] = $job;
                    }
                }

                //check slot หน้า ว่า active ไหม เทียบกับ $ratioSlot  จนถึง slot 0
                $count = 0;
                for($i = $slot_start; $i > 0; $i--){
                    $a = $i - 1 ;
                    ++$count;
                    if($slotArray[$a]['slot_type'] == 'end' || $slotArray[$a]['slot_type'] == 'first-end'){
                        break;
                    }
                    if($count < $ratioSlot){
                        $slotArray[$a]['status'] = 'inacative';
                        $slotArray[$a]['slot_type'] = 'child';
                    }
                    // $slotArray[$a]['jobInfo'] = $job;
                }
                // echo $slot_start;

            }else if($key == (count($jobs) -1) ){
                //job สุดท้าย
                $slotArray[$slot_start]['slot_type'] = $slot_start == $slot_end ? 'start-end' : 'start';
                $slotArray[$slot_start]['status'] = 'inacative';

                $slotArray[$slot_end]['slot_type']  =  $slotArray[$slot_start]['slot_type'] != 'start-end' ? 'end' : 'start-end';
                $slotArray[$slot_end]['status']     =  'inacative';

                if($slotArray[$slot_start]['slot_type'] != 'start-end'){
                    //หา child
                    $range = $slot_end - $slot_start;
                    for($i = 1; $i < $range; $i++){
                        $a = $slot_start + $i;
                        $slotArray[$a]['slot_type'] = 'child';
                        $slotArray[$a]['status']     =  'inacative';
                    }
                    // $slotArray[$a]['jobInfo'] = $job;
                }

                //check slot หน้า ว่า active ไหม เทียบกับ $ratioSlot  จนถึง slot 0
                $count = 0;
                for($i = $slot_start; $i > 0; $i--){
                    $a = $i - 1 ;
                    ++$count;
                    if($slotArray[$a]['slot_type'] == 'end' || $slotArray[$a]['slot_type'] == 'first-end'){
                        break;
                    }
                    if($count < $ratioSlot){
                        $slotArray[$a]['status'] = 'inacative';
                        $slotArray[$a]['slot_type'] = 'child';
                    }
                    // $slotArray[$a]['jobInfo'] = $job;
                }
                //check slot หน้า ว่า active ไหม เทียบกับ $ratioSlot  จนถึง slot  สุดท้ายของ period
                $lastSlot   = count($slotArray) - 1;
                $range      = $ratioSlot - 1;
                $count      = 0;

                for($i = 0; $i < $range; $i++){
                    $a = $lastSlot - $i;
                    $slotArray[$a]['status'] = 'inacative';
                    $slotArray[$a]['slot_type'] = 'child';
                    // $slotArray[$a]['jobInfo'] = $job;
                }
            }



        }
        // dd('dd');
        // foreach($jobs as $key => $job){
        //     $slot_start = $job->slot_start;
        //     $slot_end = $job->slot_end;
        //     $slotArray[$slot_start]['slot_type'] = 'start';
        //     $slotArray[$slot_end]['slot_type'] = 'end';
        //     $slotArray[$slot_start]['status'] = 'inacative';
        //     $slotArray[$slot_end]['status'] = 'inacative';

        //     if($slot_start == $slot_end){
        //         $slotArray[$slot_start]['slot_type'] = 'start-end';
        //         $slotArray[$slot_start]['status'] = 'inacative';
        //     }
        //     // หา child ของแต่ละfirst , last
        //     for($i = $slot_start; $i < $slot_end; $i++){
        //         $a = $i + 1;
        //         if($slotArray[$a]['slot_type'] == 'empty'){
        //             $slotArray[$a]['slot_type'] = 'child';
        //         }
        //         $slotArray[$a]['status'] = 'inacative';
        //     }
            //หา empty slotที่ มีคุณสมบัติ active ได้
                // $nextFirst = $key + 1;
                // if($nextFirst < count($jobs)){
                //     $emptyRangeSlotNum = $jobs[$nextFirst]->slot_start -  $slot_end;
                //     if($emptyRangeSlotNum > $ratioSlot){
                //         for($i = 1; $i <=  $ratioSlot; $i++){
                //             $a = $slot_end + $i;
                //             //  dd($a);
                //             if($i >= $ratioSlot ){
                //                 $slotArray[$a]['slot_type'] = 'no';
                //                 $slotArray[$a]['status'] = 'inacative';
                //             }
                //         }
                //     }else{
                //         for($i = 1; $i <=  $emptyRangeSlotNum; $i++){
                //             $a = $slot_end + $i;
                //             $slotArray[$a]['slot_type'] = 'no';
                //             $slotArray[$a]['status'] = 'inacative';
                //         }
                //     }
                // }else{
                //     //last สุดท้าย first จะเป็น slot สุดท้ายของ แต่ละ period
                //     $emptyRangeSlotNum = (count($slotArray) - 1) - $slot_end;
                //     if($emptyRangeSlotNum > $ratioSlot){
                //         for($i = 1; $i <=  $emptyRangeSlotNum; $i++){
                //             $a = $slot_end + $i;
                //             //  dd($a);
                //             if($i >= $emptyRangeSlotNum ){
                //                 $slotArray[$a]['slot_type'] = 'no';
                //                 $slotArray[$a]['status'] = 'inacative';
                //             }
                //         }
                //     }
                // }
            // dd($emptyRangeSlotNum);
        // }
        return($slotArray);
        foreach($jobs as $job){
            // $indexStart = $job->slot_start;

            // for($i = $indexStart; $i <= $job->slot_end; $i++) {
            //     if($indexStart == $i){
            //         $slotArray[$indexStart]['slot_type'] = $i == $job->slot_end ? 'first-last' : 'first'; // slot first และ slot last อยู่ slot เดียวกัน
            //         //check slot หน้า first และ first-last  ว่า < ratioSlot ไหม
            //         //ถ้าน้อยกว่า  ให้ disabled
            //         //first first-last
            //         for($j = 0; $j < $ratioSlot; $j++){
            //             $a = $i - $j;
            //             if($a == $i){
            //                 //first first-last  ตำแหน่งที่ 0
            //                 $slotArray[$a]['status'] = 'inacative';
            //                 break;
            //             }else{
            //                 if($slotArray[$a]['slot_type'] == 'last'){
            //                     break;
            //                 }
            //                 else{
            //                     $slotArray[$a]['status'] = 'inacative';
            //                 }
            //             }

            //         }

            //     }elseif($i == $job->slot_end && $i > $indexStart){
            //         //last
            //         $slotArray[$i]['slot_type'] = 'last';
            //         $slotArray[$i]['status'] = 'inacative';
            //     }else if($i > $indexStart && $i < $job->slot_end){
            //         //child
            //         $slotArray[$i]['slot_type'] = 'child';
            //         $slotArray[$i]['status'] = 'inacative';
            //     }else if($slotArray[$i]['slot_type'] == 'empty'){
            //         $nextEmpty = $i + 1;

            //     }
            //     $slotArray[$i]['job_id'] = $job->id;
            // }
        }
        return($slotArray);
    }

    private function findEmpMorningPeriodTimeDents($dutyTimeofDentists){
        $slotEmptyAllDents = [];
        for($i = 0; $i < collect($dutyTimeofDentists[0]->morningSlotChunk_3)->count(); $i++){
            $full = true;
           for($j = 0; $j < collect($dutyTimeofDentists[0]->morningSlotChunk_3[$i])->count(); $j++){
                if($dutyTimeofDentists[0]->morningSlotChunk_3[$i][$j]['slot_type'] == 'empty'){
                    $full = false;
                    break;
                }
           }
           if($full == true){
                // หาช่วงเวลาว่างช่วงนี้ของหมอคนอื่น
                $i_tmp = $i + 1;
                for($k = $i_tmp; $k < collect($dutyTimeofDentists)->count(); $k++){
                    // dd($k);
                    $otherFull = true;
                    for($l = 0; $l < collect($dutyTimeofDentists[$k]->morningSlotChunk_3[$i])->count(); $l++){
                        if($dutyTimeofDentists[$k]->morningSlotChunk_3[$i][$l]['slot_type'] == 'empty'){
                            $otherFull = false;
                            break;
                        }
                    }//for l
                    if($otherFull == true){
                        $dutyTimeofDentists[0]->morningSlotChunk_3[$i]['full'] = true ;
                        array_push($slotEmptyAllDents, $dutyTimeofDentists[0]->morningSlotChunk_3[$i]

                    );
                    }else{
                        $dutyTimeofDentists[$k]->morningSlotChunk_3[$i]['full']= false ;
                        array_push($slotEmptyAllDents, $dutyTimeofDentists[$k]->morningSlotChunk_3[$i]

                    );
                    }
                }//for k

           }else{
                $dutyTimeofDentists[0]->morningSlotChunk_3[$i]['full'] = false ;
                array_push($slotEmptyAllDents, $dutyTimeofDentists[0]->morningSlotChunk_3[$i]
            );
           }
       }

       return $slotEmptyAllDents;
    }

    private function findEmpEvenningPeriodTimeDents($dutyTimeofDentists){
        $slotEmptyAllDents = [];
        for($i = 0; $i < collect($dutyTimeofDentists[0]->evenningSlotChunk_3)->count(); $i++){
            $full = true;
           for($j = 0; $j < collect($dutyTimeofDentists[0]->evenningSlotChunk_3[$i])->count(); $j++){
                if($dutyTimeofDentists[0]->evenningSlotChunk_3[$i][$j]['slot_type'] == 'empty'){
                    $full = false;
                    break;
                }
           }
           if($full == true){
                // หาช่วงเวลาว่างช่วงนี้ของหมอคนอื่น
                $i_tmp = $i + 1;
                for($k = $i_tmp; $k < collect($dutyTimeofDentists)->count(); $k++){
                    // dd($k);
                    $otherFull = true;
                    for($l = 0; $l < collect($dutyTimeofDentists[$k]->evenningSlotChunk_3[$i])->count(); $l++){
                        if($dutyTimeofDentists[$k]->evenningSlotChunk_3[$i][$l]['slot_type'] == 'empty'){
                            $otherFull = false;
                            break;
                        }
                    }//for l
                    if($otherFull == true){
                        array_push($slotEmptyAllDents, $dutyTimeofDentists[0]->evenningSlotChunk_3[$i]);
                    }else{
                        // dd($dutyTimeofDentists[1]->evenningSlotChunk_3[$i]);
                        array_push($slotEmptyAllDents, $dutyTimeofDentists[$k]->evenningSlotChunk_3[$i] );
                    }
                }//for k

           }else{
                array_push($slotEmptyAllDents, $dutyTimeofDentists[0]->evenningSlotChunk_3[$i]);
           }
       }
       return $slotEmptyAllDents;
    }

    private function findEmpNightPeriodTimeDents($dutyTimeofDentists){
        $slotEmptyAllDents = [];
        for($i = 0; $i < collect($dutyTimeofDentists[0]->nightSlotChunk_3)->count(); $i++){
            $full = true;
           for($j = 0; $j < collect($dutyTimeofDentists[0]->nightSlotChunk_3[$i])->count(); $j++){
                if($dutyTimeofDentists[0]->nightSlotChunk_3[$i][$j]['slot_type'] == 'empty'){
                    $full = false;
                    break;
                }
           }
           if($full == true){
                // หาช่วงเวลาว่างช่วงนี้ของหมอคนอื่น
                $i_tmp = $i + 1;
                for($k = $i_tmp; $k < collect($dutyTimeofDentists)->count(); $k++){
                    // dd($k);
                    $otherFull = true;
                    for($l = 0; $l < collect($dutyTimeofDentists[$k]->nightSlotChunk_3[$i])->count(); $l++){
                        if($dutyTimeofDentists[$k]->nightSlotChunk_3[$i][$l]['slot_type'] == 'empty'){
                            $otherFull = false;
                            break;
                        }
                    }//for l
                    if($otherFull == true){
                        array_push($slotEmptyAllDents, $dutyTimeofDentists[0]->nightSlotChunk_3[$i]);
                    }else{
                        // dd($dutyTimeofDentists[1]->nightSlotChunk_3[$i]);
                        array_push($slotEmptyAllDents, $dutyTimeofDentists[$k]->nightSlotChunk_3[$i] );
                    }
                }//for k

           }else{
                array_push($slotEmptyAllDents, $dutyTimeofDentists[0]->nightSlotChunk_3[$i]);
           }
       }
       return $slotEmptyAllDents;
    }






}
