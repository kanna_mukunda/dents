<?php

namespace App\Http\Controllers;

use App\Models\room;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    public function index()
    {
        $rooms = Room::all();
        return view('room.index', compact('rooms'));
    }

    public function create()
    {
        return view('room.create');
    }

   
    public function store(Request $request)
    {
        
        $room = new Room();
        $room->room_name = $request->get('room_name');
        $room->floor = $request->get('floor');
        $room->status = $request->get('status');
        $room->comment = $request->get('comment');
        $room->created_at = date('Y-m-d H:i:s');
        $room->updated_at = date('Y-m-d H:i:s');
        $room->save();

        return redirect('room/index');
    }

    public function edit($roomId)
    {
        $room = Room::find($roomId);

        return view('room.edit', compact('room'));
    }

    public function update(Request $request, $roomId)
    {
        $room = Room::find($roomId);
        $room->room_name = $request->get('room_name');
        $room->floor = $request->get('floor');
        $room->status = $request->get('status');
        $room->comment = $request->get('comment');
        $room->created_at = date('Y-m-d H:i:s');
        $room->updated_at = date('Y-m-d H:i:s');
        $room->save();
        return redirect('room/index');
    }

    public function destroy($roomId)
    {
        $room = Room::find($roomId);
        $room->delete();
        return redirect('room/index');
    }
}
