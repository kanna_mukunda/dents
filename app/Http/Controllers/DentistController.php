<?php

namespace App\Http\Controllers;

use App\Models\Dentist;
use App\Models\TreatmentSkillRatio;
use Illuminate\Http\Request;

class DentistController extends Controller
{
    public function index(Request $request)
    {
        // $dentists = Dentist::all();
        // return $request;

         $keyword = $request->get('search');
        //  $request;
        // $checkDate =  explode(" ", $keyword);
         $perPage = 25;

         if (!empty($keyword)) {  
            $dentists = Dentist::where('dent_name', 'LIKE', "%$keyword%")
                ->orwhere('email', 'LIKE', "%$keyword%")
                ->orwhere('phone', 'LIKE', "%$keyword%")
               ->latest()->paginate($perPage);
         } else {
            $dentists = Dentist::latest()->paginate($perPage);
        }
        
        $dentsModel = new Dentist();

        // foreach($calendardoc as $calen){
        //     $calen->datetime = $calendardocModel->explodeDateTimeFormat($calen['date_book']);
        // }

        return view('dentist.index', compact('dentists'));
    }

    public function show($dentistId)
    {
        $dentist = Dentist::where('id', $dentistId)->get();
        $treatment_skill_ratios = TreatmentSkillRatio::
            with('dentist', 'treat')->get();
        // $dentist = TreatmentSkillRatio::where('ratio', $dentistId)->get();
        // $ratios[0]->treatmentSkillRatio->ratio ;
     //   return $dentist;
        return view('dentist.show', compact(['dentist','treatment_skill_ratios']));  

    }


    public function create()
    {
        return view('dentist.create');
    }

    public function store(Request $request)
    {
        $validate = $request->validate([
            'dent_name' => 'required'
        ], [
            'dent_name.required' => 'กรุณาใส่ชื่อหมอ'
        ]);
        $dentist = new Dentist();
        $dentist->dent_name = $request->get('dent_name');
        $dentist->phone = $request->get('phone');
        $dentist->email = $request->get('email');
        $dentist->created_at = date('Y-m-d H:i:s');
        $dentist->updated_at = date('Y-m-d H:i:s');
        $dentist->save();
        return redirect('dentist/index');
    }
    
    public function edit($dentistId)
    {
        $dentist = Dentist::find($dentistId);
        return view('dentist.edit', compact('dentist'));
    }

    public function update(Request $request, $dentistId)
    {
        $dentist = Dentist::find($dentistId);
        $dentist->dent_name = $request->get('dent_name');
        $dentist->phone = $request->get('phone');
        $dentist->email = $request->get('email');

        $dentist->created_at = date('Y-m-d H:i:s');
        $dentist->updated_at = date('Y-m-d H:i:s');
        $dentist->save();
        return redirect('dentist/index');
    }

    public function destroy($dentistId)
    {
        $dentist = Dentist::find($dentistId);
        $dentist->delete();
        return redirect('dentist/index');
    }
}


// php artisan crud:generate Calendar --fields='name#string;date_book#text;type_id#integer;doc_id#integer;deleted#integer;user_id#integer;clinic_id#integer' --view-path=treament_type --controller-namespace=TreatmentType  --form-helper=html