<?php

namespace App\Http\Controllers;

use App\Models\TreatmentSkillRatio;
use App\Models\Dentist;
use App\Models\Treat;
use Illuminate\Http\Request;

class TreatmentSkillRatioController extends Controller
{
    public function index()
    {
        $treatment_skill_ratios = TreatmentSkillRatio::
            with('dentist', 'treat')->get();

        return view('treatment_skill_ratio.index', compact('treatment_skill_ratios'));
    }

    public function create()
    {
        $dentists = Dentist::all();
        $treatments = Treat::all();
        return view('treatment_skill_ratio.create',compact('dentists', 'treatments'));
    }

    public function store(Request $request)
    {
        $treatment_skill_ratio = new TreatmentSkillRatio();
        $treatment_skill_ratio->dent_id        = $request->get('dent_id');
        $treatment_skill_ratio->treatment_id   = $request->get('treatment_id');
        $treatment_skill_ratio->ratio          = $request->get('ratio');
        $treatment_skill_ratio->created_at     = date('Y-m-d H:i:s');
        $treatment_skill_ratio->updated_at     = date('Y-m-d H:i:s');
        $treatment_skill_ratio->save();
        return redirect('treatment_skill_ratio/show/'.$treatment_skill_ratio->id);
    }

    public function show($treatmentSkillRatioId)
    {
        $ratios = TreatmentSkillRatio::where('id', $treatmentSkillRatioId)->get();
        $ratios[0]->treat->treatment_name;
        $ratios[0]->dentist->dent_name;
        // $ratios[0]->treatmentSkillRatio->ratio ;
        // dd($ratios);
        return view('treatment_skill_ratio.show', compact('ratios'));  

    }

    public function edit($treatmentSkillRatioId)
    {
        $treatmentSkillRatios = TreatmentSkillRatio::find($treatmentSkillRatioId);
        $dentists = Dentist::all();
        $treatments = Treat::all();
        return view('treatment_skill_ratio.edit', compact('treatmentSkillRatios', 'treatments', 'dentists'));
    }


    public function update(Request $request, $treatmentSkillRatioId)
    {
        $treatmentSkillRatio = TreatmentSkillRatio::find($treatmentSkillRatioId);

        $treatmentSkillRatio->dent_id        = $request->get('dent_id');
        $treatmentSkillRatio->treatment_id   = $request->get('treatment_id');
        $treatmentSkillRatio->ratio          = $request->get('ratio');
        $treatmentSkillRatio->created_at     = date('Y-m-d H:i:s');
        $treatmentSkillRatio->updated_at     = date('Y-m-d H:i:s');
        $treatmentSkillRatio->save();
        return redirect('treatment_skill_ratio/index');

    }

    public function destroy($treatmentSkillRatioId)
    {
        $treatmentSkillRatio = TreatmentSkillRatio::find($treatmentSkillRatioId);
        $treatmentSkillRatio->delete();
        return redirect('treatment_skill_ratio/index');


    }
}