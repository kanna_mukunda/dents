<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TreatmentImage extends Model
{
    protected $table = 'treatment_image';
}
