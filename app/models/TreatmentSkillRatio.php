<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TreatmentSkillRatio extends Model
{
    protected $table = 'treatment_skill_ratio';


    public function treatment(){
        return $this->belongsTo('App\Treatment', 'treatment_id', 'id');
    }

    public function treat(){
        return $this->belongsTo('App\Models\Treat', 'treatment_id', 'id');
    }

    public function dentist(){
        return $this->belongsTo('App\Models\Dentist', 'dent_id', 'id');
    }

    public function findDentsByTreatmentSkill($where){
        return TreatmentSkillRatio::where($where)
                // ->with('treatment','treatment.treatmentSkillRatio')
                ->orderBy('ratio', 'asc')->get();
    }
    
}
