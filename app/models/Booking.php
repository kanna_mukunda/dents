<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bookings';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['fullname', 'treatment_id', 'hn', 'docter_id', 'booking_date', 'calendar_doc_id', 'room_id', 'user_phone', 'user_status', 'desc', 'user_id', 'u_line_id', 'deleted', 'status_id'];

    public function treat(){
        return $this->BelongsTo('App\Models\Treat','treatment_id', 'id'); 
    }

    public function patient(){
        return $this->BelongsTo('App\Models\Patient', 'hn', 'hn');
    }

    public function booking_status(){
        return $this->BelongsTo('App\Models\BookingStatus', 'status_id', 'id');
    }

    public function calendar_doc(){
        return $this->BelongsTo('App\Models\CalendarDoc', 'calendar_doc_id', 'id');
    }

    public function dentist(){
        return $this->BelongsTo('App\Models\Dentist', 'docter_id', 'id');
    }

   
}
