<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CalendarDoc extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'calendar_doc';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'date_book', 'type_id', 'doc_id', 'deleted', 'user_id', 'clinic_id', 'status_id'];


    public function dentist(){
        return $this->belongsTo('App\Models\Dentist', 'doc_id', 'id');
    }

    public function treat(){
        return $this->belongsTo('App\Models\Treat', 'type_id', 'id');
    }

    public function explodeDateTimeFormat($datetimeStr){
        $datetimeArr = explode("|", $datetimeStr);
        return ['date' => $datetimeArr[0], 'starttime' => $datetimeArr[1], 'endtime' => $datetimeArr[2]];
    }
    
}
