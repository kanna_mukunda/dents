<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TreatmentType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'treatment_types';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'sub_type_id', 'desc', 'piority', 'price', 'status', 'deleted', 'user_id', 'clinic_id'];

    
}
