<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $table = 'patient';

    public function bill(){
        return $this->hasMany('App\Bill', 'hn', 'hn');
    }

    public function find_patient_by_name($name){
        $patient = Patient::where('f_name', 'Like', '%'.$name.'%')->get();
        return $patient;
    }

   
}
