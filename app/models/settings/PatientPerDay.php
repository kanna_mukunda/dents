<?php

namespace App\Models\settings;

use Illuminate\Database\Eloquent\Model;

class PatientPerDay extends Model
{
    protected $table = 'patient_per_day';
}
