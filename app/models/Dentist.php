<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dentist extends Model
{
    protected $table = 'dentist';

    protected $fillable = ['dent_name'];


    public function treatmentSkillRatio(){
        return $this->hasMany('App\Models\TreatmentSkillRatio','dent_id', 'id');
    }
    
    public function job(){
        return $this->hasMany('App\Models\Job', 'dent_id', 'id');
    }

    public function dutyTime(){
        return $this->hasMany('App\Models\DutyTime', 'dent_id', 'id');
    }

    public function getDutyTimeByDate($date, $dent_id){
        return DutyTime::where('date', $date)
                        ->where('dent_id', $dent_id)
                        ->first();
    }

    public function findDentsByTreatmentSkill($where){
        return TreatmentSkillRatio::where($where)
                // ->with('treatment','treatment.treatmentSkillRatio')
                ->orderBy('ratio', 'asc')->get();
    }

    public function treat(){
        return $this->hasMany('App\Models\Treat', 'treatment_id', 'id');
    }
}
