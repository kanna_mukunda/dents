<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Treat extends Model
{
    protected $table = 'treat';

    public function getTreatment($id){
        return Treatment::where('id', $id)->first();
    }

    public function treatmentSkillRatio(){
        return $this->hasOne('App\models\TreatmentSkillRatio', 'treatment_id', 'id');
    }
}
