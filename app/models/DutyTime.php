<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DutyTime extends Model
{
    protected $table = 'duty_time';

    public function Dentist(){
        return $this->belongsTo('App\Models\Dentist', 'dent_id', 'id');
    }

   
    public function dutyTime($where){
        return DutyTime::where($where)
                ->with( 'dentist', 'room',
                    // 'dentist.treatmentSkillRatio',
                    // 'dentist.treatmentSkillRatio.treatment', 
                    'dentist.job')->get();
        
    }

    public function getDutyTimeNextDay($date){
        $nextDate =  DutyTime::where('date', '>', $date)->get('date');
        return $nextDate->sort()->groupBy('date')->keys();
    }

    public function getDutyTimeNextDayOfDent($date, $dentId){
        return DutyTime::where('date', '>', $date)
                ->where('dent_id', $dentId)->get();
    }

    public function getAllDutyTimeNextDateByDentSkill($skill, $date){
        $dutyTime = DutyTime::where('date', '>', $date)
                    ->with( 'dentist', 
                            'dentist.treatmentSkillRatio',
                            'dentist.treatmentSkillRatio.treatment')
                    ->get();
        return $dutyTime->filter(function($val) use ($skill){
            $a = false;
            foreach ($val['dentist']['treatmentSkillRatio'] as $item){
                 if($item['treatment_id'] == 1){
                     $a = true;
                 }
            }
            return $a == true;
        });

    }

  

    public function room(){
        return $this->belongsTo('App\Models\room', 'room_id' , 'id');
    }
}
