<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'job';

    public function dentist(){
        return $this->belongsTo('App\Models\Dentist', 'dent_id', 'id');
    }

    public function patient(){
        return $this->belongsTo('App\Models\Patient', 'patient_id', 'hn');
    }

    public function treatment(){
        return $this->belongsTo('App\Treatment', 'treatment_id', 'id');
    }

    public function room(){
        return $this->belongsTo('App\Models\room', 'room_id', 'id');
    }

    public function changeJobStatus($status){

        $thai_status  ="";
        if($status == 'reserved'){
            $thai_status = 'นัดหมอแล้ว';
        }else if($status == 'cancel'){
            $thai_status = 'ยกเลิก';

        }else if($status == 'complete'){
            $thai_status = 'รักษาเสร็จแล้ว';

        }
        else if($status == 'processing'){
            $thai_status = 'กำลังทำการรักษา';

        }
        else if($status == 'standby'){
            $thai_status = 'มาตามนัด';

        }
        else if($status == 'postpone'){
            $thai_status = 'เลื่อนนัด';

        }
        return $thai_status;
    }

    public function convertSlotToTime($slot_start, $slot_end, $period_type){
        $init_time_hr = 10;
        $init_time_min = 00;
        $aa = [];
        for($i = 0; $i < 24; $i++){
            for($j = 0; $j < 10; $j++){}
            $slot = ($i+1)*5;
            $init_time_hr = $slot%60 == 0 ? $init_time_hr+1 : $init_time_hr;
            $init_time_min = $slot%60 == 0 ? 0 : $slot%60;

            $aa[$i] = ['slot'=> $slot, 'hour' => $init_time_hr, 'min'=>$init_time_min];
        }
        $start = collect($aa)->filter(function($val) use ($slot_start){
            return $slot_start == $val['slot'];
        });
        $startVal = array_values($start->toArray());

        $end = collect($aa)->filter(function($val) use ($slot_end){
            return $slot_end == $val['slot'];
        });
        $endVal = array_values($end->toArray());
        // dd($startVal);
        return $startVal[0]['hour'].":".$startVal[0]['min']."-".$endVal[0]['hour'].":".$endVal[0]['min'];
    }


}
