<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Treatment extends Model
{
    protected $table = 'treatment';

    public function getTreatment($id){
        return Treatment::where('id', $id)->first();
    }

    public function treatmentSkillRatio(){
        return $this->hasOne('App\models\TreatmentSkillRatio', 'treatment_id', 'id');
    }

    public function patient(){
        return $this->belongsTo('App\models\Patient', 'hn', 'hn');
    }
}
