<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Route::get('login/line/redirect', 'Api\LineController@redirectToProvider');
Route::get('login/line/callback','Api\LineController@handleProviderCallback');


Route::prefix('patient')->group(function(){
    Route::get('appointment/dashboard', 'patient\AppointController@dashboard');

    Route::get('appointment/index', 'patient\AppointController@index');
    Route::post('appointment/create', 'patient\AppointController@create');
    Route::post('appointment/store', 'patient\AppointController@store');
    Route::get('appointment/summary/{job_id}', 'patient\AppointController@appointment_summary');
    Route::post('appointment/authen', 'patient\AppointController@authen');
    Route::get('appointment/select_date_treate', 'patient\AppointController@select_date_treate');

});

Route::prefix('room')->group(function(){
    Route::get('index', 'RoomController@index');
    Route::get('create', 'RoomController@create');
    Route::post('store', 'RoomController@store');
    Route::get('edit/{id}', 'RoomController@edit');
    Route::patch('update/{id}', 'RoomController@update');
    Route::get('destroy/{id}', 'RoomController@destroy');
});

Route::prefix('appointment')->group(function(){
    Route::get('index/{date?}', 'AppointmentController@index');
    Route::get('store', 'AppointmentController@store');
    Route::post('create', 'AppointmentController@create');
    Route::get('patient_apointment/{date?}', 'AppointmentController@patient_apointment');
    Route::post('patient_select_time', 'AppointmentController@patient_select_time');
    Route::post('patient_store', 'AppointmentController@patient_store');
    Route::get('appointment_result/{job_id}','AppointmentController@appointment_result');
    Route::get('patient_appoint_summary/{patient_id}', 'AppointmentController@patient_appoint_summary');
    Route::get('view/{id}', 'AppointmentController@view');

    Route::get('appoint_by_staff', 'AppointmentController@appoint_by_staff');
    Route::get('appoint_by_staff_by_date/{date}', 'AppointmentController@appoint_by_staff_by_date');
    Route::post('appoint_by_staff_store', 'AppointmentController@appoint_by_staff_store');

    Route::get('find_dutytime_dents/{date}', 'AppointmentController@find_dutytime_dents');
});


Route::prefix('account')->group(function(){
    Route::post('store', 'AccountController@store');
    Route::get('invoice/{bill}', 'AccountController@invoice');
    Route::get('invoice_print/{bill}', 'AccountController@invoice_print');

});

Route::get('treatment/index', 'TreatmentController@index');
Route::get('treatment/create', 'TreatmentController@create');
Route::post('treatment/store', 'TreatmentController@store');
Route::get('treatment/edit/{id}', 'TreatmentController@edit');
Route::patch('treatment/update/{id}', 'TreatmentController@update');
Route::get('treatment/destroy/{id}', 'TreatmentController@destroy');

// treatment_skill_ratio
Route::get('treatment_skill_ratio/index', 'TreatmentSkillRatioController@index');
Route::get('treatment_skill_ratio/create', 'TreatmentSkillRatioController@create');
Route::post('treatment_skill_ratio/store', 'TreatmentSkillRatioController@store');
Route::get('treatment_skill_ratio/edit/{id}', 'TreatmentSkillRatioController@edit');
Route::patch('treatment_skill_ratio/update/{id}', 'TreatmentSkillRatioController@update');
Route::get('treatment_skill_ratio/destroy/{id}', 'TreatmentSkillRatioController@destroy');

//Patient
Route::prefix('patient')->group(function(){
Route::get('index', 'PatientController@index');
Route::get('view/{id}', 'PatientController@view');
Route::get('create', 'PatientController@create');
Route::post('store', 'PatientController@store');
Route::get('edit/{id}', 'PatientController@edit');
Route::patch('update/{id}', 'PatientController@update');
Route::get('destroy/{id}', 'PatientController@destroy');
Route::get('findPatientByName/{name}', 'PatientController@findPatientByName');
Route::get('about_us', 'patient\AppointController@about_us');

});
//Job
Route::prefix('job')->group(function(){
    Route::get('index/{date?}', 'JobController@index');
    Route::get('create', 'JobController@create');
    Route::post('store', 'JobController@store');
    Route::get('edit/{id}', 'JobController@edit');
    Route::patch('update/{id}', 'JobController@update');
    Route::get('destroy/{id}', 'JobController@destroy');
    Route::get('treatment_calculate_table/{date?}', 'JobController@treatment_calculate_table');
    Route::get('treatment_calulate/{job_id}', 'JobController@treatment_calulate');
    Route::get('invoice-print/{invoice_id}', 'JobController@invoice_print');
    Route::get('dent_job/{dent_id}', 'JobController@dent_job');
    Route::get('postpone_appointment', 'JobController@postpone_appointment');
    Route::post('update_status', 'JobController@update_status');
});

//DutyTime

Route::prefix('dutytime')->group(function () {
    Route::get('index', 'DutyTimeController@index');
    Route::get('create', 'DutyTimeController@create');
    Route::post('store', 'DutyTimeController@store');
    Route::get('edit/{id}', 'DutyTimeController@edit');
    Route::patch('update/{id}', 'DutyTimeController@update');
    Route::get('destroy/{id}', 'DutyTimeController@destroy');

});

//Dentist
Route::get('dentist/index', 'DentistController@index');
Route::get('dentist/create', 'DentistController@create');
Route::post('dentist/store', 'DentistController@store');
Route::get('dentist/edit/{id}', 'DentistController@edit');
Route::patch('dentist/update/{id}', 'DentistController@update');
Route::get('dentist/destroy/{id}', 'DentistController@destroy');


Route::prefix('settings/patient_per_day')->group(function(){
    Route::get('index', 'settings\PatientPerDayController@index');
    Route::get('create', 'settings\PatientPerDayController@create');
    Route::post('store', 'settings\PatientPerDayController@store');
});

Route::post('store', 'CalendarController@store');

Route::prefix('calendar')->group(function(){
    Route::get('index', 'CalendarController@index');
    Route::get('create', 'CalendarController@create');
    Route::post('store', 'CalendarController@store');
    Route::get('add_events', 'CalendarController@add_events');

});

Route::prefix('treatment_history')->group(function(){
    Route::get('create/{hn}', 'TreatmentHistoryController@create');
    Route::post('store', 'TreatmentHistoryController@store');

});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('reset', function (){
//     Artisan::call('route:clear');
//     Artisan::call('cache:clear');
//     Artisan::call('config:clear');
//     Artisan::call('config:cache');
// });
Route::group(['prefix' => 'admin', 'middleware' => ['role:superadmin']], function () {
    Route::resource('users', 'UsersController');

});