@extends('layouts.app')

@section('title')
เพิ่มข้อมูลจำนวนคนไข้ต่อวัน
@endsection
@section('navigate')
<a href="{{url('dutytime/index')}}">กำหนดจำนวนคนไข้ต่อวัน</a>
@endsection

@section('style')
 <!-- fullCalendar -->
 <meta name="csrf-token" id="csrf_token" content="{{ csrf_token() }}" />

 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.min.css">
<style>
  .list-group-item {
    position: relative;
    display: block;
    padding: .3rem 1.25rem;
    background-color: #fff;
    border: 1px solid rgba(0,0,0,.125);
}
.hidden {
        display: none
    }

    .show {
        display: block;
    }

</style>
 
@endsection


@section('content')
<div class="card">
    <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
    </div>
    <!-- /.card-header -->
    <div class="card-body pt-0">
        <div class="btn-group">
            <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="-52">
              <i class="fas fa-bars"></i>
            </button>
            <div class="dropdown-menu" role="menu">
              <a href="#" class="dropdown-item">Add new event</a>
            </div>
        </div>
        <div class="row">
            <div class=" col-6">
                <div id="calendar" style="width: 100%"></div>
            </div>
            <div class="col-6"></div>
        </div>
    </div><!-- /.card-body -->
</div>
   <!-- The Modal -->
   <div class="modal hidden" id="myModal">
       <div class="modal-dialog modal-sm">
           <div class="modal-content">

               <!-- Modal Header -->
               <div class="modal-header">
                   <h4 class="modal-title">กำหนดจำนวนคนไข้ต่อวัน</h4>
                   <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>

               <!-- Modal body -->
               <div class="modal-body">
                   <div class="form-grop  row mb-1">
                       <label class="col-sm-5 form-label">วันที่</label>
                       <input type="text" class="col-sm-7 form-control" value="" id="date_selected" readonly="readonly">
                   </div>
                   <div class="form-grop  row mb-1">
                       <label class="col-sm-5 form-label">จำนวนคนไข้</label>
                       <input type="text" class="col-sm-7 form-control" value="" id="patient_no" aria-readonly="true">
                   </div>
                   <div class="form-grop  row mb-1">
                       <label class="col-sm-5 form-label">จากเวลา</label>
                       <select class="col-sm-7 form-control" id="fromtime">
                           <option>เลือก..</option>
                           <option value="all">ทั้งวัน</option>
                           @for ($i = 9; $i < 21; $i++) <option value="{{$i}}">{{$i < 10 ? "0".$i : $i}} : 00</option>
                               @endfor
                       </select>
                   </div>
                   <div class="form-grop  row mb-1">
                       <label class="col-sm-5 form-label">ถึงเวลา</label>
                       <select class="col-sm-7 form-control" id="totime">
                           <option>เลือก..</option>
                           <option value="all">ทั้งวัน</option>
                           @for ($i = 9; $i < 21; $i++) <option value="{{$i}}">{{$i < 10 ? "0".$i : $i}} : 00</option>
                               @endfor
                       </select>
                   </div>

               </div>
               <div class="modal-footer">
                   <button type="button" class="btn btn-success confirm_btn">บันทึก</button>
               </div>

           </div>
           <!--modal-content-->
       </div>
   </div>
@endsection

@section('script')
<script src="https://unpkg.com/@fullcalendar/core@4.4.0/main.min.js"></script>
<script src="https://unpkg.com/@fullcalendar/core@4.4.0/locales-all.min.js"></script>
<script src="https://unpkg.com/@fullcalendar/interaction@4.4.0/main.min.js"></script>
<script src="https://unpkg.com/@fullcalendar/daygrid@4.4.0/main.min.js"></script>

<script>

    //   var date = new Date();
    var d;
    var m;
    var y;
      var calendarEl = document.getElementById("calendar");
      var calendar = new FullCalendar.Calendar(calendarEl, {
          
        plugins: ["interaction", "dayGrid", "timeGrid", "resourceTimeline"],
        header: {
          left: "prev,next today",
          center: "title",
          right: ""//"dayGridMonth,timeGridWeek,timeGridDay"
        },
        editable: true,
        navLinks: true,
        selectable: true,
        navLinkDayClick: function(date, jsEvent) {
            
            jsEvent.preventDefault();
            d = date.getDate();
            m = date.getMonth();
            y = date.getFullYear();
            var thDate = date.toLocaleDateString('th-TH', { timeZone: 'UTC' })
            $('#date_selected').val(thDate)
            console.log(date.toLocaleDateString('th-TH', { timeZone: 'UTC' }))
            showHiddenModal()
        //    window.location = '/appointment/index/'+day
        //     console.log('day', date);
        //     console.log('coords', jsEvent.pageX, jsEvent.pageY);
        },
        locale: 'th',
        
      });

    
      calendar.render();
    

      $('.confirm_btn').click(function (event) {
            event.preventDefault();
            showHiddenModal()
            let _token = $('#csrf_token').attr('content')

            let params = {
                d: d,
                m: m,
                y: y,
                from: $('#fromtime').val(),
                to: $('#totime').val(),
                patient_no: $('#patient_no').val(),
                _token: _token

            };
            console.log('params',params)
            $.post('/settings/patient_per_day/store', params)
                .done(function (data) {
                    console.log('data',data)
                    window.location = '/settings/patient_per_day/index' 
                })
        })


      function showHiddenModal() {
            if ($('#myModal').hasClass('hidden')) {
                $('#myModal').removeClass('hidden')
                $('#myModal').addClass('show')
            } else {
                $('#myModal').removeClass('show')
                $('#myModal').addClass('hidden')
            }
        }

        $('.close').click(function () {
            showHiddenModal()
        })

        $('#fromtime').change(function(){
            var from = $(this).val()
            console.log(from)
            if(from == 'all'){
                $('#totime').val('all')
            }
        })
 
    
      </script>
@endsection