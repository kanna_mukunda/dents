@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{asset('adminlte/plugins/date-picker/css/datepicker.min.css')}}" />
@endsection
@section('title')
จำนวนคนไข้ต่อวัน
@endsection
@section('navigate')
<a href="{{url('dutytime/index')}}">กำหนดจำนวนคนไข้ต่อวัน</a>
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
            </div>
            <div class="card-tools">
                <a href="{{ url('settings/patient_per_day/create')}}" class="btn btn-success">เพิ่มข้อมูล</a>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>วันที่</th>
                        <th>จำนวนคนไข้ต่อวัน/ช่วงเวลา</th>
                        <th>ตั้งแต่</th>
                        <th>ถึง</th>
                        <th>created at</th>
                        <th>updated_at</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($patient_per_day as $item)
                    <tr>
                        <td>{{$item->date}}</td>
                        <td>{{$item->patient_no}}</td>
                        <td>{{$item->start_period}}</td>
                        <td>{{$item->end_period}}</td>
                        <td>{{$item->created_at}}</td>
                        <td>{{$item->updated_at}}</td>
                        <td>
                            <a href="{{url('settings/patient_per_day/edit/'.$item->id)}}" class="btn btn-warning">แก้ไข</a>
                            <a href="{{url('settings/patient_per_day/destroy/'.$item->id)}}" class="btn btn-danger">ลบ</a>
                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    

@endsection
