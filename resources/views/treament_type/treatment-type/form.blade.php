<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'ชื่อประเภทการรักษา' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ isset($treatmenttype->name) ? $treatmenttype->name : ''}}" >
    {!! $errors->first('name', '<p class="help-block text-red">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('sub_type_id') ? 'has-error' : ''}}">
    <label for="sub_type_id" class="control-label">{{ ' ประเภทย่อย' }}</label>
    {{-- <input class="form-control" name="sub_type_id" type="number" id="sub_type_id" value="{{ isset($treatmenttype->sub_type_id) ? $treatmenttype->sub_type_id : ''}}" > --}}
    <select class="form-control" name="sub_type_id" type="number" id="sub_type_id" value="{{ isset($treatmenttype->sub_type_id) ? $treatmenttype->sub_type_id : ''}}">
        <option value="">เลือก..</option>
        <option value="1" {{isset($treatmenttype->sub_type_id) && $treatmenttype->sub_type_id == 1 ? 'selected' : ''}}>1</option>
        <option value="2" {{isset($treatmenttype->sub_type_id) && $treatmenttype->sub_type_id == 2 ? 'selected' : ''}}>2</option>
        <option value="3" {{isset($treatmenttype->sub_type_id) && $treatmenttype->sub_type_id == 3 ? 'selected' : ''}}>3</option>
        <option value="4" {{isset($treatmenttype->sub_type_id) && $treatmenttype->sub_type_id == 4 ? 'selected' : ''}}>4</option>
        <option value="5" {{isset($treatmenttype->sub_type_id) && $treatmenttype->sub_type_id == 5 ? 'selected' : ''}}>5</option>
    </select>
    {!! $errors->first('sub_type_id', '<p class="help-block text-red">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('piority') ? 'has-error' : ''}}">
    <label for="piority" class="control-label">{{ 'ลำดับความสำคัญ' }}</label>
    <input class="form-control" name="piority" type="number" id="piority" value="{{ isset($treatmenttype->piority) ? $treatmenttype->piority : ''}}" >
    {!! $errors->first('piority', '<p class="help-block text-red">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    <label for="price" class="control-label">{{ 'ราคา(บาท)' }}</label>
    <input class="form-control" name="price" type="number" id="price" value="{{ isset($treatmenttype->price) ? $treatmenttype->price : ''}}" >
    {!! $errors->first('price', '<p class="help-block text-red">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'คำอธิบาย' }}</label>
    <textarea class="form-control" rows="5" name="desc" type="textarea" id="desc" >{{ isset($treatmenttype->desc) ? $treatmenttype->desc : ''}}</textarea>
    {!! $errors->first('desc', '<p class="help-block text-red">:message</p>') !!}
</div>



<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'บันทึกการแก้ไข' : 'บันทึก' }}">
</div>
