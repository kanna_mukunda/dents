@extends('layouts.app')
@section('title')
ข้อมูลประเภทการรักษา
@endsection
@section('treament_type')
    active
@endsection
@section('navigate')
<a href="{{url('treament_type/treatment-type')}}">แก้ไขประเภทการรักษา</a>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">แก้ไขประเภทการรักษา #{{ $treatmenttype->name }}</div>
                    <div class="card-body">
                        <br />


                        <form method="POST" action="{{ url('/treament_type/treatment-type/' . $treatmenttype->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('treament_type.treatment-type.form', ['formMode' => 'edit'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
