@extends('layouts.app')
@section('title')
ข้อมูลประเภทการรักษา
@endsection
@section('treament_type')
    active
@endsection
@section('navigate')
<a href="{{url('treament_type/treatment-type')}}">เพิ่มประเภทการรักษา</a>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">สร้างประเภทการรักษา</div>
                    <div class="card-body">
                       

                        <form method="POST" action="{{ url('/treament_type/treatment-type') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('treament_type.treatment-type.form', ['formMode' => 'create'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
