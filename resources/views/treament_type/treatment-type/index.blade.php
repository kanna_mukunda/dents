@extends('layouts.app')
@section('title')
ข้อมูลประเภทการรักษา
@endsection
@section('treament_type')
    active
@endsection
@section('navigate')
<a href="{{url('treament_type/treatment-type')}}">ประเภทการรักษา</a>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">

        <div class="col-md-12">
            <a href="{{ url('/treament_type/treatment-type/create') }}" class="btn btn-primary mb-3"
                title="Add New TreatmentType">
                เพิ่มประเภทการรักษา
            </a>

            <div class="card">
                <div class="card-header">ตั้งค่าประเภทการรักษา</div>
                <div class="card-body">

                    <form method="GET" action="{{ url('/treament_type/treatment-type') }}" accept-charset="UTF-8"
                        class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..."
                                value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>

                    <br />
                    <br />
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ชื่อประเภทการรักษา</th>
                                    <th>ราคา</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($treatmenttype as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->price }}</td>
                                    <td>
                                        <a href="{{ url('/treament_type/treatment-type/' . $item->id) }}"
                                            title="View TreatmentType"><button class="btn btn-info btn-sm">ดู</button></a>
                                        <a href="{{ url('/treament_type/treatment-type/' . $item->id . '/edit') }}"
                                            title="Edit TreatmentType"><button class="btn btn-warning btn-sm"><i
                                                    class="fa fa-pencil-square-o" aria-hidden="true"></i>แก้ไข</button></a>

                                        <form method="POST"
                                            action="{{ url('/treament_type/treatment-type' . '/' . $item->id) }}"
                                            accept-charset="UTF-8" style="display:inline">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-sm"
                                                title="Delete TreatmentType"
                                                onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                    class="fa fa-trash-o" aria-hidden="true"></i>ลบ</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $treatmenttype->appends(['search' =>
                            Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
