@extends('layouts.app')
@section('title')
ข้อมูลประเภทการรักษา
@endsection
@section('navigate')
<a href="{{url('treament_type/treatment-type')}}">ประเภทการรักษา</a>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">ประเภทการรักษา {{ $treatmenttype->name }}</div>
                <div class="card-body">

                    {{-- <a href="{{ url('/treament_type/treatment-type') }}" title="Back"><button
                        class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                        Back</button></a> --}}
                    <a href="{{ url('/treament_type/treatment-type/' . $treatmenttype->id . '/edit') }}"
                        title="Edit TreatmentType"><button class="btn btn-primary btn-sm"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i> แก้ไข</button></a>

                    {{-- <form method="POST" action="{{ url('treament_type/treatmenttype' . '/' . $treatmenttype->id) }}"
                    accept-charset="UTF-8" style="display:inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger btn-sm" title="Delete TreatmentType"
                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                            aria-hidden="true"></i> Delete</button>
                    </form> --}}
                    <br />
                    <br />

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $treatmenttype->id }}</td>
                                </tr>
                                <tr>
                                    <th> ชื่อประเภทการรักษา </th>
                                    <td> {{ $treatmenttype->name }} </td>
                                </tr>
                                <tr>
                                    <th> ประเภทการรักษาย่อย</th>
                                    <td> {{ $treatmenttype->sub_type_id }} </td>
                                </tr>
                                <tr>
                                    <th> คำอธิบาย </th>
                                    <td> {{ $treatmenttype->desc }} </td>
                                </tr>
                                <tr>
                                    <th> สถานะ </th>
                                    <td> {{ $treatmenttype->status }} </td>
                                </tr>
                                <tr>
                                    <th> clinic_id </th>
                                    <td> {{ $treatmenttype->clinic_id }} </td>
                                </tr>
                                <tr>
                                    <th> user id </th>
                                    <td> {{ $treatmenttype->user_id }} </td>
                                </tr>

                                <tr>
                                    <th> created_at </th>
                                    <td> {{ $treatmenttype->created_at }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
