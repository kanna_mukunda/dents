@extends('layouts.app')
@section('calendar-doc')
active
@endsection


@section('title')
วันให้บริการประจำเดือน
@endsection
@section('navigate')
<a href="{{ url('calendar-doc') }}">ตั้งค่าวันให้บริการประจำเดือน</a>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">

        <div class="col-md-12">
            <a href="{{ url('/calendar-doc/create') }}" class="btn btn-success btn-sm mb-3" title="Add New CalendarDoc">
                <i class="fa fa-plus" aria-hidden="true"></i> เพิ่มข้อมูล
            </a>
            <div class="card">
                <div class="card-header">ตารางจัดการหมอ</div>
                <div class="card-body">

                    <form method="GET" action="{{ url('/calendar-doc') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>

                    <br />
                    <br />
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ชื่อหมอ</th>
                                    <th>วันเข้าเวร</th>
                                    <th>เวลา</th>
                                    <th>ประเภทการรักษา</th>
                                    <th>หมายเหตุ</th>

                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($calendardoc as $item)
                                <tr>
                                    {{-- {{dd($item)}} --}}
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{$item->dentist->dent_name}}</td>
                                    <td>
                                        <span>{{ $item->datetime['date'] }}</span>

                                    </td>
                                    <td>
                                        <span class="ml-2">{{$item->a_time}}</span>
                                    </td>
                                    <td>{{ $item->treat->treatment_name }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        <a href="{{ url('/calendar-doc/' . $item->id) }}" title="View CalendarDoc"><button class="btn btn-info btn-sm">ดู</button></a>
                                        <a href="{{ url('/calendar-doc/' . $item->id . '/edit') }}" title="Edit CalendarDoc"><button class="btn btn-warning btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>แก้ไข</button></a>

                                        <form method="POST" action="{{ url('/calendar-doc' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-sm" title="Delete CalendarDoc" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i>ลบ</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $calendardoc->appends(['search' =>
                            Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection