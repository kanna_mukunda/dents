@extends('layouts.app')
@section('style')

@endsection
@section('calendar_doc')
active
@endsection


@section('title')
สร้างวันให้บริการประจำเดือน
@endsection
@section('navigate')
<a href="{{ url('calendar-doc') }}">ตั้งค่าวันให้บริการประจำเดือน</a>
@endsection
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">จัดเวรหมอ</div>
                    <div class="card-body">
                        <br />


                        <form method="POST" action="{{ url('/calendar-doc') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('calendar_doc.calendar-doc.form', ['formMode' => 'create'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script>
      

    </script>
@endsection
