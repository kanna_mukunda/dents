
<div class="form-group {{ $errors->has('date_book') ? 'has-error' : ''}}">
    <label for="date_book" class="control-label">{{ 'วันเวลาเข้าเวรของหมอ' }}</label>

    <div class="row">
        <div class="col-md-4 col-xs-12 col-lg-4">
            <label for="date_book" class="control-label">{{ 'วันที่' }}</label>
            <input class="form-control datepicker"  name="date" type="text" id="date" value="{{isset($calendardoc->date_book) ? $calendardoc->date_book : ''}}">
        </div>
        <div class="col-md-3 col-xs-12 col-lg-3">
            <label for="date_book" class="control-label">{{ 'เวลาเริ่ม' }}</label>
            <input class="form-control timepicker"  name="start_time" type="text" id="start_time" value="">
        </div>
        <div class="col-md-3 col-xs-12 col-lg-3">
            <label for="date_book" class="control-label">{{ 'เวลาสิ้นสุด' }}</label>
            <input class="form-control timepicker"  name="end_time" type="text" id="end_time" value="">
        </div>
        <div class="col-md-2 col-xs-12 col-lg-2 text-center">
            <label for="date_book" class="control-label">{{ 'ทั้งวัน' }}</label>
            <input class="form-control"  name="allday" type="checkbox" id="allday" >
        </div>
    </div>
    {!! $errors->first('date_book', '<p class="help-block">:message</p>') !!}
</div>
{{-- <div class="form-group {{ $errors->has('type_id') ? 'has-error' : ''}}">
    <label for="type_id" class="control-label">{{ 'ประเภทการรักษา' }}</label>
    <select class="form-control" name="type_id" id="type_id">
        <option>เลือก..</option>
        @foreach ($treatments as $treat)
           <option value="{{ $treat->id}}">
                {{$treat->treatment_name}}
            </option>
        @endforeach
    </select>
    {!! $errors->first('type_id', '<p class="help-block">:message</p>') !!}
</div> --}}
<div class="form-group {{ $errors->has('doc_id') ? 'has-error' : ''}}">
    <label for="doc_id" class="control-label">{{ 'ชื่อหมอ' }}</label>
    <select class="form-control" name="doc_id" id="doc_id">
        <option>เลือก..</option>
        @foreach ($dentists as $dent)
           <option value="{{ $dent->id}}">
                {{$dent->dent_name}}
            </option>
        @endforeach
    </select>
    {!! $errors->first('doc_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'หมายเหตุ' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ isset($calendardoc->name) ? $calendardoc->name : ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>



<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>


@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>



<script>
    $('#allday').change(function(){
        if(this.checked){
            $('#start_time').val('09:00')
            $('#end_time').val('20:00')
        }else{
            $('#start_time').val('')
            $('#end_time').val('')
        }
    });

    $('.datepicker').datepicker({
        autoclose: true
    })


    $('.timepicker').datetimepicker({
      format: 'LT'
    })
   

</script>
    
@endsection