@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">ข้อมูลเวรหมอ {{ $calendardocs[0]->id }}</div>
                    <div class="card-body">

                        {{-- <a href="{{ url('/calendar-doc') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a> --}}
                        <a href="{{ url('/calendar-doc/' . $calendardocs[0]->id . '/edit') }}" title="Edit CalendarDoc"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> แก้ไข</button></a>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th> ชื่อหมอ</th>
                                        <th> ประเภทการรักษา</th>
                                        <th> วันเวลาเข้าเวร </th>
                                        <th> หมายเหตุ </th>
                                        <th>  </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($calendardocs as $calendardoc)
                                        
                                    <tr>
                                        <td>{{ $calendardoc->id }}</td>
                                   
                                        <td> {{ $calendardoc->dentist->dent_name }} </td>
                                   
                                        <td> {{ $calendardoc->treat->treatment_name }} </td>
                                   
                                        <td> 
                                            <span>{{ $calendardoc->datetime['date'] }}</span>
                                            {{-- <span class="ml-5">{{ $calendardoc->datetime['starttime'] }} -
                                            {{ $calendardoc->datetime['endtime'] }} น. </span> --}}
                                            <span class="ml-2">{{$item->a_time}}</span>
                                        </td>
                                        
                                  
                                        <td> {{ $calendardoc->name }} </td>
                                        <td></td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
