@extends('layouts.app')

@section('title')
 Api/Appointment
@endsection
@section('style')
 <!-- fullCalendar -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.min.css">

 
@endsection

@section('content')

<div class="card">
    <div class="card-header"></div>
    <div class="card-body">
        <div id='calendar'></div>
    </div>
</div>

@endsection

@section('script')
<script src="https://unpkg.com/@fullcalendar/core@4.4.0/main.min.js"></script>
<script src="https://unpkg.com/@fullcalendar/core@4.4.0/locales-all.min.js"></script>
<script src="https://unpkg.com/@fullcalendar/interaction@4.4.0/main.min.js"></script>
<script src="https://unpkg.com/@fullcalendar/daygrid@4.4.0/main.min.js"></script>

<script>

    // document.addEventListener("DOMContentLoaded", function() {
    //   var date = new Date();
    //   var d = date.getDate();
    //   var m = date.getMonth();
    //   var y = date.getFullYear();
    
      var calendarEl = document.getElementById("calendar");
      var calendar = new FullCalendar.Calendar(calendarEl, {
          
        plugins: ["interaction", "dayGrid", "timeGrid", "resourceTimeline"],
        header: {
          left: "prev,next today",
          center: "title",
          right: ""//"dayGridMonth,timeGridWeek,timeGridDay"
        },
        editable: true,
        navLinks: true,
        selectable: true,
        navLinkDayClick: function(date, jsEvent) {
            let day = date.toISOString()
           window.location = '/api/appointment/create/'+day
            console.log('day', date.toISOString());
            console.log('coords', jsEvent.pageX, jsEvent.pageY);
        },
        locale: 'th',
        events: [
        //   {
        //     title: "Sales Meeting",
        //     start: new Date(y, m, 2, 10, 30),
        //     end: new Date(y, m, 2, 11, 30),
        //     allDay: false
        //   },
        //   {
        //     title: "Marketing Meeting",
        //     start: new Date(y, m, 3, 11, 30),
        //     end: new Date(y, m, 3, 12, 30),
        //     allDay: false
        //   },
        //   {
        //     title: "Production Meeting",
        //     start: new Date(y, m, 4, 15, 30),
        //     end: new Date(y, m, 4, 16, 30),
        //     allDay: false
        //   }
        ],
      });

    
      calendar.render();
    // });

 
    
      </script>
@endsection