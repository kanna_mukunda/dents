@extends('layouts.app')

@section('title')
    แก้ไขข้อมูลผู้ใช้งาน
@endsection
@section('admin/users/edit')
    active
@endsection

@section('navigate')
    <a href="{{ url('/admin/users') }}">ผู้ใช้งานระบบ </a>
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">แก้ไขข้อมูลผู้ใช้งาน</h3>
            <div class="card-tools">
                <a href="{{ route('users.index') }}" class="btn btn-danger"><i class="fas fa-shield-alt"></i>
                    ดูผู้ใช้งานทั้งหมด</a>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form method="post" action="{{ url('admin/users/' . $user->id) }}" data-parsley-validate
                class="form-horizontal form-label-left">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
                    <label for="name" class="col-sm-2 col-form-label">ชื่อ นามสกุล</label>
                    <div class="col-sm-10">
                        <input type="text" value="{{ $user->name }}" id="name" name="name"
                            class="form-control col-md-7 col-xs-12">
                        @if ($errors->has('name'))
                            <span class="help-block">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }} row">
                    <label for="username" class="col-sm-2 col-form-label">ชื่อผู้ใช้</label>
                    <div class="col-sm-10">
                        <input type="text" value="{{ $user->username }}" id="username" name="username"
                            class="form-control col-md-7 col-xs-12">
                        @if ($errors->has('username'))
                            <span class="help-block">{{ $errors->first('username') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
                    <label for="email" class="col-sm-2 col-form-label">อีเมล</label>
                    <div class="col-sm-10">
                        <input type="text" value="{{ $user->email }}" id="email" name="email"
                            class="form-control col-md-7 col-xs-12">
                        @if ($errors->has('email'))
                            <span class="help-block">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} row">
                    <label for="phone" class="col-sm-2 col-form-label">เบอร์โทรศัพท์</label>
                    <div class="col-sm-10">
                        <input type="text" value="{{ $user->phone }}" id="phone" name="phone"
                            class="form-control col-md-7 col-xs-12">
                        @if ($errors->has('phone'))
                            <span class="help-block">{{ $errors->first('phone') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('idcard') ? ' has-error' : '' }} row">
                    <label for="idcard" class="col-sm-2 col-form-label">เลขบัตราประจำตัวประชาชน</label>
                    <div class="col-sm-10">
                        <input type="text" value="{{ $user->idcard }}" id="idcard" name="idcard"
                            class="form-control col-md-7 col-xs-12">
                        @if ($errors->has('idcard'))
                            <span class="help-block">{{ $errors->first('idcard') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }} row">
                    <label for="address" class="col-sm-2 col-form-label">ที่อยู่</label>
                    <div class="col-sm-10">
                        <textarea id="address" name="address" class="form-control col-md-7 col-xs-12">{{ $user->address}}</textarea>

                    </div>
                </div>

                <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }} row">
                    <label class="col-sm-2 col-form-label" for="category_id">ประเภทผู้ใช้
                        <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" id="role_id" name="role_id">
                            @if (count($roles))
                                @foreach ($roles as $row)
                                    <option value="{{ $row->id }}"
                                        {{ $row->id == $user->roles[0]->id ? 'selected="selected"' : '' }}>
                                        {{ $row->name }}</option>
                                @endforeach
                            @endif
                        </select>
                        @if ($errors->has('role_id'))
                            <span class="help-block">{{ $errors->first('role_id') }}</span>
                        @endif
                    </div>
                </div>

                <div class="ln_solid"></div>

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <input name="_method" type="hidden" value="PUT">
                        <button type="submit" class="btn btn-success">บันทึก</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
@endsection
