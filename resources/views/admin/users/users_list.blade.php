@extends('layouts.app')

@section('title')
ข้อมูลผู้ใช้งานระบบ
@endsection
@section('admin/users')
active
@endsection

@section('navigate')
<a href="{{ url('/admin/users') }}">ผู้ใช้งานระบบ </a>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <a href="{{route('users.create')}}" class="btn btn-primary col-md-2 mb-3">เพิ่มผู้ใช้งานระบบ</a>

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">ข้อมูลผู้ใช้งานระบบ</h3>

                <div class="card-tools">
                    <form method="GET" action="{{ url('/admin/users') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                        <tr>
                            <th>ชื่อ นามสกุล</th>
                            <th>ชื่อผู้ใช้</th>
                            <th>อีเมล</th>
                            <th>เบอร์โทรศัพท์</th>
                            <th>ที่อยู่</th>
                            <th style="font-size:0.875em">เลขบัตรประจำตัวประชนชน</th>
                            <th>ประเภทผู้ใช้</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->address }}</td>
                            <td>{{ $user->idcard }}</td>
                            <td>
                                @foreach($user->roles as $r)
                                {{$r->display_name}}
                                @endforeach
                            </td>
                            <td>
                                <a class="btn btn-warning btn-sm" href="{{  url('admin/users/'.$user->id.'/edit/') }}" class="btn btn-info btn-sm"><i class="fa fa-pencil" title="Role"></i>แก้ไข</a>
                             
                                <form method="POST" action="{{ url('admin/users/'.$user->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Booking" onclick="return confirm(&quot;ต้องการลบข้อมูลหรือไม่ ?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i>ลบ</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $users->links() }}
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
@endsection