@extends('layouts.app')
@section('active_treat_history')
    active
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <a href="{{url('treatment/create')}}" class="btn btn-primary col-md-2 mb-3">เพิ่ม</a>

      <div class="card">
        <div class="card-header">
            <h3 class="card-title">ตารางการรักษา</h3>

          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>#</th>
                <th>ชื่อ-สกุล คนไข้</th>
                <th>hn</th>
                <th>หมอเจ้าของไข้</th>
                <th>ชื่อการรักษา</th>
                <th>รายละเอียด</th>
                <th>ค่ารักษา(บาท)</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
                <?php $c = 1; ?>
                @foreach ($treatments as $item)
                <tr>
                    <td>{{$c++}}</td>
                    <td>{{$item->patient->f_name.' '.$item->l_name}}</td>
                    <td>{{$item->patient->hn}}</td>
                    <td>{{$item->treat }}</td>
                    <th>{{$item->dent}}</th>
                    <td class="text-center">{{$item->detail}}</td>
                    <td class="text-center">{{$item->cost }}</td>
                    <td>
                        <a href="{{url('treatment/edit/'.$item->id)}}" class="btn btn-warning">แก้ไข</a>
                        <a href="{{url('treatment/destroy/'.$item->id)}}" class="btn btn-danger">ลบ</a>
                    </td>
                  </tr>
                 
                @endforeach
              
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
@endsection