@extends('layouts.app')

@section('content')

<div class="card card-warning">
    <div class="card-header">
      <h3 class="card-title">แก้ไขรายการรักษา</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form role="form" action="{{ url('treatment/update/'.$treatment->id) }}" method="post">
        @csrf
        @method('patch')

        <div class="row">
          <div class="col-sm-6">
            <!-- text input -->
            <div class="form-group">
              <label>ชื่อการรักษา</label>
              <input type="text" class="form-control"  name="treatment_name" id="treatment_name"
                value="{{ $treatment->treatment_name}}">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label>ค่าเวลาMean การรักษา(นาที)</label>
              <input type="text" class="form-control"  name="treatment_mean_time" 
                id="treatment_mean_time" value="{{$treatment->treatment_mean_time }}">
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
              <!-- text input -->
              <div class="form-group">
                <label>ค่ารักษา(บาท)</label>
                <input type="text" class="form-control"  name="treatment_price" 
                id="treatment_price" value="{{ $treatment->treatment_price}}">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>ลำดับความสำคัญ</label>
                <select class="form-control" name="piority" id="piority">
                    <option> เลือก..</option>
                    @for ($i = 1; $i < 11; $i++)
                    <option value="{{$i}}" {{$i == $treatment->piority ? 'selected' : ''}}> {{$i}}</option>
                    @endfor

                  </select>
              </div>
            </div>
          </div>
          <button type="submit"class="btn btn-success">บันทึก</button>

      </form>
    </div>
    <!-- /.card-body -->
  </div>
@endsection