@extends('layouts.app')

@section('content')

<div class="card card-warning">
    <div class="card-header">
      <h3 class="card-title">เพิ่มรายการรักษา</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form role="form" action="{{ url('treatment/store')}}" method="post">
        @csrf
        <div class="row">
          <div class="col-sm-6">
            <!-- text input -->
            <div class="form-group">
              <label>ชื่อการรักษา</label>
              <input type="text" class="form-control"  name="treatment_name" id="treatment_name">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label>ค่าเวลาMean การรักษา(นาที)</label>
              <input type="text" class="form-control"  name="treatment_mean_time" id="treatment_mean_time">
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
              <!-- text input -->
              <div class="form-group">
                <label>ค่ารักษา(บาท)</label>
                <input type="text" class="form-control"  name="treatment_price" id="treatment_price">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>ลำดับความสำคัญ</label>
                <select class="form-control" name="piority" id="piority">
                    <option> เลือก..</option>
                    <option value="1"> 1</option>
                    <option value="2"> 2</option>
                    <option value="3"> 3</option>
                    <option value="4"> 4</option>
                    <option value="5"> 5</option>
                    <option value="6"> 6</option>
                    <option value="7"> 7</option>
                    <option value="8"> 8</option>
                    <option value="9"> 9</option>
                    <option value="10"> 10</option>
                  </select>
              </div>
            </div>
          </div>
          <button type="submit"class="btn btn-success">บันทึก</button>

      </form>
    </div>
    <!-- /.card-body -->
  </div>
@endsection