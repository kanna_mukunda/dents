@extends('layouts.app')
@section('style')
<!-- fullCalendar -->
<meta name="csrf-token" id="csrf_token" content="{{ csrf_token() }}" />

<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/core/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/daygrid/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/timegrid/main.min.css') }}">
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
    integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
    crossorigin="anonymous" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

@endsection

@section('title')
แก้ไขตารางเวรของหมอ
@endsection
@section('navigate')
<a href="{{url('dutytime/index')}}">จัดการตารางเวร</a>
@endsection
@section('content')

<div class="card card-warning">

    <div class="card-header">
        <h3 class="card-title">แก้ไขตารางเวรของหมอ</h3>
    </div>
    <form role="form" action="{{ url('dutytime/update/'.$dutytime->id)}}" method="Post">
      @csrf
      @method('patch')

        <!-- /.card-header -->
        <div class="card-body">
            @csrf
            <div class="row">
              <div class="col-md-3">
                  <!-- text input -->
                  <div class="form-group">
                      <label class="label-control">วันที่</label>
                      <input id="inputdatepicker" name="date" class="datepicker form-control"
                         data-date-format="mm/dd/yyyy" value="{{$dutytime->date}}">
                  </div>
              </div>
              <div class="col-sm-6 ">
                  <div class="form-group">
                      <label class="label-control">ชื่อ - สกุลหมอ</label>
                      <select class="form-control" name="dent_id" id="dent_id">
                          <option value="">เลือก..</option>
                          @foreach ($dentists as $item)
                          <option value="{{$item->id}}" {{$item->id == $dutytime->dent_id ? 'selected' : ''}}>{{$item->dent_name}}</option>
                          @endforeach
                      </select>
                  </div>
              </div>
              <div class="col-sm-3">
                  <div class="form-group">
                    <label class="label-control">ประจำหัองตรวจ</label>
                    <select class="form-control" name="room_id" id="room_id">
                        <option value="">เลือก..</option>
                        @foreach ($rooms as $item)
                        <option value="{{$item->id}}" {{$item->id == $dutytime->room_id ? 'selected' : ''}}>{{$item->room_name}}</option>
                        @endforeach
                    </select>
                  </div>
              </div>
            </div> <!--row -->
            
            <div class="row">
              <div class="col-sm-4 row">
                <div class="col-md-2 bg-warning text-center">ช่วงเช้า</div>
                  <div class="col-md-5">
                      <div class="form-group">
                          <label class="label-control">เริ่มเวลา</label>
                          <input id="inputdatepicker" name="am_start_time" class="timepicker form-control"
                          value="{{$dutytime->am_start_time}}">
                          {{-- <select class="form-control" name="am_start_time">
                            <option value="">เลือก..</option>
                            <option value="0" {{$dutytime->am_slot_start == "0" ? 'selected' : ''}}>09.00</option> 
                            <option value="6" {{$dutytime->am_slot_start == "6" ? 'selected' : ''}}>09.30</option>
                            <option value="12" {{$dutytime->am_slot_start == "12" ? 'selected' : ''}}>10.00</option>  
                            <option value="18" {{$dutytime->am_slot_start == "18" ? 'selected' : ''}}>10.30</option> 
                            <option value="24" {{$dutytime->am_slot_start == "24"? 'selected' : ''}}>11.00</option>  
                            <option value="30" {{$dutytime->am_slot_start == "30" ? 'selected' : ''}}>11.30</option>
                            <option value="36" {{$dutytime->am_slot_start == "36" ? 'selected' : ''}}>12.00</option>
                            <option value="30" {{$dutytime->am_slot_start == "42" ? 'selected' : ''}}>12.30</option>
                            <option value="36" {{$dutytime->am_slot_start == "48" ? 'selected' : ''}}>13.00</option>    
                          </select> --}}
                      </div>
                  </div>
                  <div class="col-md-5">
                      <div class="form-group">
                          <label class="label-control">สิ้นสุดเวลา</label>
                          <input id="inputdatepicker" name="am_end_time" class="timepicker form-control"
                          value="{{$dutytime->am_end_time}}">
                          {{-- <select class="form-control" name="am_end_time">
                            <option value="">เลือก..</option>
                            <option value="0" {{$dutytime->am_slot_end == "0" ? 'selected' : ''}}>09.00</option> 
                            <option value="6" {{$dutytime->am_slot_end == "6" ? 'selected' : ''}}>09.30</option>
                            <option value="12" {{$dutytime->am_slot_end == "12" ? 'selected' : ''}}>10.00</option>  
                            <option value="18" {{$dutytime->am_slot_end == "18" ? 'selected' : ''}}>10.30</option> 
                            <option value="24" {{$dutytime->am_slot_end == "24"? 'selected' : ''}}>11.00</option>  
                            <option value="30" {{$dutytime->am_slot_end == "30" ? 'selected' : ''}}>11.30</option>
                            <option value="36" {{$dutytime->am_slot_end == "36" ? 'selected' : ''}}>12.00</option>  
                            <option value="30" {{$dutytime->am_slot_end == "42" ? 'selected' : ''}}>12.30</option>
                            <option value="36" {{$dutytime->am_slot_end == "48" ? 'selected' : ''}}>13.00</option>
                          </select> --}}
                      </div>
                  </div>
                  <div class="col-md-2 bg-warning text-center"></div>
                  <div class="col-md-5">จำนวนคนไข้ต่อวัน</div>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="patient_no_am" value="{{$dutytime->patient_no_am}}">
                  </div>
                  <div class="col-md-2 ">คน</div>
              </div>
              <div class="col-md-4 row">
                <div class="col-md-2 bg-warning text-center">ช่วงบ่าย</div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="label-control">เริ่มเวลา</label>
                        <input id="inputdatepicker" name="ev_start_time" class="timepicker form-control"
                          value="{{$dutytime->ev_start_time}}">
                        {{-- <select class="form-control" name="ev_start_time">
                          <option value="">เลือก..</option>
                          <option value="0" {{$dutytime->ev_slot_start == "0" ? 'selected' : ''}} >13.00</option> 
                          <option value="6" {{$dutytime->ev_slot_start == "6" ? 'selected' : ''}} >13.30</option>
                          <option value="12" {{$dutytime->ev_slot_start == "12" ? 'selected' : ''}} >14.00</option>  
                          <option value="18" {{$dutytime->ev_slot_start == "18" ? 'selected' : ''}} >14.30</option> 
                          <option value="24" {{$dutytime->ev_slot_start == "24" ? 'selected' : ''}} >15.00</option>  
                          <option value="20" {{$dutytime->ev_slot_start == "30" ? 'selected' : ''}} >15.30</option>
                          <option value="36" {{$dutytime->ev_slot_start == "36" ? 'selected' : ''}} >16.00</option>  
                        </select> --}}
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="label-control">สิ้นสุดเวลา</label>
                        <input id="inputdatepicker" name="ev_end_time" class="timepicker form-control"
                          value="{{$dutytime->ev_end_time}}">
                        {{-- <select class="form-control" name="ev_end_time">
                          <option value="">เลือก..</option>
                          <option value="0" {{$dutytime->ev_slot_end == "0" ? 'selected' : ''}} >13.00</option> 
                          <option value="6" {{$dutytime->ev_slot_end == "6" ? 'selected' : ''}} >13.30</option>
                          <option value="12" {{$dutytime->ev_slot_end == "12" ? 'selected' : ''}} >14.00</option>  
                          <option value="18" {{$dutytime->ev_slot_end == "18" ? 'selected' : ''}} >14.30</option> 
                          <option value="24" {{$dutytime->ev_slot_end == "24" ? 'selected' : ''}} >15.00</option>  
                          <option value="20" {{$dutytime->ev_slot_end == "30" ? 'selected' : ''}} >15.30</option>
                          <option value="36" {{$dutytime->ev_slot_end == "36" ? 'selected' : ''}} >16.00</option>  
                        </select> --}}
                    </div>
                </div>
                <div class="col-md-2 bg-warning text-center"></div>
                <div class="col-md-5">จำนวนคนไข้ต่อวัน</div>
                <div class="col-md-3">
                  <input type="text" class="form-control" name="patient_no_ev" value="{{$dutytime->patient_no_ev}}">
                </div>
                <div class="col-md-2 ">คน</div>
              </div>

              <div class="col-md-4 row">
                <div class="col-md-2 bg-warning text-center">ช่วงค่ำ</div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="label-control">เริ่มเวลา</label>
                        <input id="inputdatepicker" name="pm_start_time" class="timepicker form-control"
                          value="{{$dutytime->pm_start_time}}">
                        {{-- <select class="form-control" name="pm_start_time">
                          <option value="">เลือก..</option>
                          <option value="0"  {{ $dutytime->pm_slot_start == "0" ? 'selected' : '' }}>16.00</option> 
                          <option value="6"  {{ $dutytime->pm_slot_start == "6" ? 'selected' : '' }}>16.30</option>
                          <option value="12" {{ $dutytime->pm_slot_start == "12"? 'selected' : '' }}>17.00</option>  
                          <option value="18" {{ $dutytime->pm_slot_start == "18"? 'selected' : '' }}>17.30</option> 
                          <option value="24" {{ $dutytime->pm_slot_start == "24"? 'selected' : '' }}>18.00</option>  
                          <option value="20" {{ $dutytime->pm_slot_start == "20"? 'selected' : '' }}>18.30</option>
                          <option value="36" {{ $dutytime->pm_slot_start == "36"? 'selected' : '' }}>19.00</option>
                          <option value="42" {{ $dutytime->pm_slot_start == "42"? 'selected' : '' }}>19.30</option>  
                          <option value="48" {{ $dutytime->pm_slot_start == "48"? 'selected' : '' }}>20.00</option> 
                        </select> --}}
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="label-control">สิ้นสุดเวลา</label>
                        <input id="inputdatepicker" name="pm_end_time" class="timepicker form-control"
                          value="{{$dutytime->pm_end_time}}"> 

                        {{-- <select class="form-control" name="pm_end_time">
                          <option value="">เลือก..</option>
                          <option value="0"  {{ $dutytime->pm_slot_end == "0"  ? 'selected' : '' }}>16.00</option> 
                          <option value="6"  {{ $dutytime->pm_slot_end == "6"  ? 'selected' : '' }}>16.30</option>
                          <option value="12" {{ $dutytime->pm_slot_end == "12" ? 'selected' : '' }}>17.00</option>  
                          <option value="18" {{ $dutytime->pm_slot_end == "18" ? 'selected' : '' }}>17.30</option> 
                          <option value="24" {{ $dutytime->pm_slot_end == "24" ? 'selected' : '' }}>18.00</option>  
                          <option value="20" {{ $dutytime->pm_slot_end == "20" ? 'selected' : '' }}>18.30</option>
                          <option value="36" {{ $dutytime->pm_slot_end == "36" ? 'selected' : '' }}>19.00</option>
                          <option value="42" {{ $dutytime->pm_slot_end == "42" ? 'selected' : '' }}>19.30</option>  
                          <option value="48" {{ $dutytime->pm_slot_end == "48" ? 'selected' : '' }}>20.00</option> 
                        </select> --}}
                    </div>
                  
                </div>
                <div class="col-md-2 bg-warning text-center"></div>
                <div class="col-md-5">จำนวนคนไข้ต่อวัน</div>
                <div class="col-md-3">
                  <input type="text" class="form-control" name="patient_no_pm" value="{{$dutytime->patient_no_pm}}">
                </div>
                <div class="col-md-2 ">คน</div>
              </div>
            </div><!--row-->
            <div class="row mt-3">
              <button type="submit" class="btn btn-success btn-block offset-md-9 col-md-3">บันทึก</button>
            </div>
        

    </form>
</div>
<!-- /.card-body -->
</div>
@endsection


@section('script')
<script src="{{ asset('fullcalendar/core@4.4.0/main.min.js') }}"></script>
<script src="{{ asset('fullcalendar/core@4.4.0/locales-all.min.js') }}"></script>
<script src="{{ asset('fullcalendar/interaction@4.4.0/main.min.js') }}"></script>
<script src="{{ asset('fullcalendar/daygrid@4.4.0/main.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
    integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.th.min.js"
    integrity="sha512-cp+S0Bkyv7xKBSbmjJR0K7va0cor7vHYhETzm2Jy//ZTQDUvugH/byC4eWuTii9o5HN9msulx2zqhEXWau20Dg=="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.th.min.js" integrity="sha512-cp+S0Bkyv7xKBSbmjJR0K7va0cor7vHYhETzm2Jy//ZTQDUvugH/byC4eWuTii9o5HN9msulx2zqhEXWau20Dg==" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: true,
            language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
            thaiyear: true //Set เป็นปี พ.ศ.
        }).datepicker("setDate", "0"); //กำหนดเป็นวันปัจุบัน

        $('.clinic_day_off_datepicker').datepicker({
            format: 'dd-mm-yyyy',
            todayBtn: true,
            thaiyear: true ,
            language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        }).datepicker("setDate", "0"); //กำหนดเป็นวันปัจุบัน

        
        var dateNow = new Date();

        $('.timepicker').datetimepicker({
            format: 'HH:mm',
            //stepping: 15,
            //enabledHours: [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
            

        })
    });

</script>
@endsection