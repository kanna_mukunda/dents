@extends('layouts.app')

@section('title')
ตารางเวรของหมอ
@endsection
@section('navigate')
<a href="{{ url('dutytime/index') }}">จัดการตารางเวร</a>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">ตารางเวรของหมอ</h3>

                <div class="card-tools">
                    <div class="input-group ">
                        <a href="{{ url('dutytime/create') }}"
                            class="btn btn-primary">เพิ่มข้อมูลตารางเวร</a>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>วันที่</th>
                            <th>ชื่อ - สกุลหมอ</th>
                            <th>เริ่มเวลา</th>
                            <th>สิ้นสุดเวลา</th>
                            <th>จำนวนคนไข้ที่รับได้</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $c = 1; ?>
                        @foreach($dutytimes as $item)
                            {{-- {{dd($item) }} --}}
                            <tr>
                                <td>{{ $c++ }}</td>
                                <td>{{ $item->date }}</td>
                                <td>{{ $item->dentist->dent_name }}</td>
                                <td>
                                    <ul class="list-group list-group-unbordered mb-3">
                                        @if($item->am_start_time != [])
                                            <li class="list-group-item">
                                                <b>เช้า</b> <a class="float-right">{{ $item->am_start_time }}</a>
                                            </li>
                                        @endif

                                        @if($item->ev_start_time != [])
                                            <li class="list-group-item">
                                                <b>บ่าย</b> <a class="float-right">{{ $item->ev_start_time }}</a>
                                            </li>
                                        @endif

                                        @if($item->pm_start_time != [])
                                            <li class="list-group-item">
                                                <b>ค่ำ</b> <a class="float-right">{{ $item->pm_start_time }}</a>
                                            </li>
                                        @endif

                                    </ul>

                                </td>
                                <td>
                                    <ul class="list-group list-group-unbordered mb-3">
                                        @if($item->am_end_time != [])
                                            <li class="list-group-item">
                                                <b></b> <a class="float-right">{{ $item->am_end_time }}</a>
                                            </li>
                                        @endif

                                        @if($item->ev_end_time != [])
                                            <li class="list-group-item">
                                                <b></b> <a class="float-right">{{ $item->ev_end_time }}</a>
                                            </li>
                                        @endif

                                        @if($item->pm_end_time != [])
                                            <li class="list-group-item">
                                                <b></b> <a class="float-right">{{ $item->pm_end_time }}</a>
                                            </li>
                                        @endif
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-group list-group-unbordered mb-3">
                                        @if($item->am_end_time != [])
                                            <li class="list-group-item text-right">
                                                {{ $item->patient_no_am }}
                                            </li>
                                        @endif

                                        @if($item->ev_end_time != [])
                                            <li class="list-group-item text-right">
                                                {{ $item->patient_no_ev }}
                                            </li>
                                        @endif

                                        @if($item->pm_end_time != [])
                                            <li class="list-group-item text-right">
                                                {{ $item->patient_no_pm }}
                                            </li>
                                        @endif
                                    </ul>
                                </td>
                                <td>
                                    <a href="{{ url('dutytime/edit/'.$item->id) }}"
                                        class="btn btn-warning">แก้ไข</a>
                                    <a href="{{ url('dutytime/destroy/'.$item->id) }}"
                                        class="btn btn-danger">ลบ</a>
                                </td>
                            </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
@endsection
