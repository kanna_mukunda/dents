@extends('layouts.app')

@section('title')
คนไข้เลือกวันรักษา
@endsection

@section('navigate')
<a href="#">เลือกวัน</a>
@endsection

@section('style')
<meta name="csrf-token" id="csrf_token" content="{{ csrf_token() }}" />

<style>
    .hidden {
        display: none
    }

    .show {
        display: block;
    }

    #aaa .col-md-2,
    #aaa .col-xs-6 {
        position: relative;
        min-height: 1px;
        padding-right: 2px;
        padding-left: 2px;
    }

</style>
@endsection
@section('content')
@if (1 != 1)
<div class="card">
    <div class="card-body ">

        {{-- <h1 class="text-center text-warning">วันที่เลือกไม่พบข้อมูล</h1>
          <h3>เลือกวันที่สามารถทำ</h3>

             @foreach ($dutyTimeNextDateArray as $key => $itemArr)
              <form action="{{url('appointment/patient_select_time')}}" class="mb-2" method="POST">
        @csrf
        <input type="hidden" name="treatment_id" value="{{$treatment_id}}">
        <input type="hidden" name="date" value="{{$key}}">
        <button type="submit" class="btn btn-primary">{{$key}}</button>
        </form>
        @endforeach
    </div> --}}
</div>
@else
<?php $a = 1; ?>
<h2>{{$dutyTimeofDentists[0]->date}}</h2>

<h2>{{$dutyTimeofDentists[0]['selected_treatment']->treatment_name}}</h2>
<?php $checkSkillCount = 1; ?>
@for ( $i = 0; $i < $dentTistCount; $i++ ) 
  @if ($dutyTimeofDentists[$i]['job_slot'][0]['status']=='noskill' )
    <?php $checkSkillCount++; ?> @else <div class="card card-blue">
    <div class="card-header">
        ห้องที่ {{$a++}}
    </div>
    <div class="card-body container-fluid">
        {{-- {{dd($dutyTimeofDentists[0]['job_slot'])}} --}}
        <div class="row" id="aaa">
            @foreach ($dutyTimeofDentists[$i]['job_slot'] as $item)

            <div class="col-xs-3 col-md-1 mb-2 col-sm-6">
                <?php $min = $item['min'] < 10 ? "0".$item['min'] : $item['min'];?>
                @if ($item['status'] == 'active')
                <a href="javascript:void(0)" class="btn btn-success btn-block active_slot"
                    data-index="{{$item['index']}}" data-slot="{{$item['slot']}}" data-status="{{$item['status']}}"
                    data-hour="{{$item['hour']}}" data-min="{{$min}}" data-date="{{$dutyTimeofDentists[$i]['date']}}"
                    data-dent_id="{{$dutyTimeofDentists[$i]['dent_id']}}"
                    data-selected_treatment_id={{$dutyTimeofDentists[$i]->selected_treatment->id}}
                    data-selected_treatment_name={{$dutyTimeofDentists[$i]->selected_treatment->treatment_name}}
                    data-room={{$a}} data-processing_time={{$dutyTimeofDentists[$i]['processing_time']}}>
                    <p>{{$item['hour'].":".$min}}</p>

                </a>
                @endif
                @if ($item['status'] == 'reserved')
                <a href="javascript:void(0)" class="btn btn-default btn-block bg-dark" style="cursor: default"
                    aria-readonly="true">
                    <p>{{$item['hour'].":".$min}}</p>

                </a>
                @endif
                @if ($item['status'] == 'warning')
                <a href="javascript:void(0)" class="btn btn-default btn-block bg-dark" style="cursor: default"
                    aria-readonly="true">
                    <p>{{$item['hour'].":".$min}}</p>

                </a>

                @endif
            </div>
            @endforeach
        </div>
    </div>
    @endif
    @endfor
    @if ($dentTistCount == $checkSkillCount)
    <div class="card">
        <div class="card-body">
            <h2 class="text-center">ไม่พบข้อมูล</h2>
        </div>
    </div>
    @endif

    <!-- The Modal -->
    <div class="modal hidden" id="myModal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">ยืนยันการนัดหมอ</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-grop  row mb-1">
                        <label class="col-sm-5 form-label">วันที่</label>
                        <input type="text" class="col-sm-7 form-control" value="" id="date_selected"
                            readonly="readonly">
                    </div>
                    <div class="form-grop  row mb-1">
                        <label class="col-sm-5 form-label">ต้องการรักษา</label>
                        <input type="text" class="col-sm-7 form-control" value="" id="selected_treatment_name"
                            aria-readonly="true">
                    </div>
                    <div class="form-grop  row mb-1">
                        <label class="col-sm-5 form-label">เวลา</label>
                        <input type="text" class="col-sm-7 form-control" value="" id="datetime">
                    </div>
                    <div class="form-grop  row mb-1">
                        <label class="col-sm-5 form-label">ห้องที่</label>
                        <input type="text" class="col-sm-7 form-control" value="" id="room">
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-success confirm_btn">บันทึก</button>
                </div>

            </div>
        </div>
    </div>
    @endif

    @section('script')
    <script>
        let hour;
        let min;
        let date;
        let room;
        let dent_id;
        let treatment_id
        let processing_time
        let slot
        $('.active_slot').click(function (event) {
            event.preventDefault();
            let index = $(this).data('index')
            slot = $(this).data('slot')
            let status = $(this).data('status')
            hour = $(this).data('hour')
            min = $(this).data('min')
            date = $(this).data('date')
            room = $(this).data('room')
            dent_id = $(this).data('dent_id')
            treatment_id = $(this).data('selected_treatment_id')
            processing_time = $(this).data('processing_time')
            let selected_treatment_name = $(this).data('selected_treatment_name')
            $('#date_selected').val(date)
            $('#selected_treatment_name').val(selected_treatment_name)
            $('#datetime').val(hour + ":" + min + " น.")
            $('#room').val(room)
            showHiddenModal()

        });

        $('.confirm_btn').click(function (event) {
            event.preventDefault();
            showHiddenModal()
            let _token = $('#csrf_token').attr('content')

            let params = {
                hour: hour,
                min: min,
                date: date,
                room: room,
                dent_id: dent_id,
                treatment_id: treatment_id,
                processing_time: processing_time,
                slot: slot,
                _token: _token

            };
            $.post('/appointment/patient_store', params)
                .done(function (data) {
                    window.location = 'appointment/appointment_result/' + data
                })
            console.log('date', date)
        })

        function showHiddenModal() {
            if ($('#myModal').hasClass('hidden')) {
                $('#myModal').removeClass('hidden')
                $('#myModal').addClass('show')
            } else {
                $('#myModal').removeClass('show')
                $('#myModal').addClass('hidden')
            }
        }

        $('.close').click(function () {
            showHiddenModal()
        })

    </script>
    @endsection

    @endsection
