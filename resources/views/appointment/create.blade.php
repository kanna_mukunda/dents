@extends('layouts.app')
@section('style')
<style>
    .table td {
        padding: 0.1rem !important
    }

</style>
@endsection
@section('content')
<div class="row">
    <div class="col-md-4">
        เลือกวัน<input type="text" value="2020-08-26" class="form-control" name="reserve_date" id="reserve_date">
    </div>
    <div class="col-md-3">
        ต้องการรักษา
        <select name="patient" id="patient" class="form-control">
            <option>เลือก..</option>
            @foreach ($treatmetns as $treatment)
            <option value="{{$treatment->id}}"> {{$treatment->treatment_name}}
                @endforeach
        </select>
    </div>

    <div class="col-md-3">
        ชื่อคนไข้
        <select name="treatment_selected" id="treatment_selected" class="form-control">
            <option>เลือก..</option>
            @foreach ($patients as $patient)
            <option value="{{$patient->id}}"> {{$patient->name}}
                @endforeach
        </select>
    </div>
    <div class="col-md-2">
        <label class="label-control"></label>
        <a href="{{url('appointment/create')}}" class="btn btn-primary form-control">ค้นหา</a>
    </div>

</div>
<?php $c = 0; ?>
<table class="table">
    <thead>
        <tr>
            <th>hour</th>
            {{-- {{dd($dutyTimeofDentists)}} --}}
            @foreach ($dutyTimeofDentists[0]->job_slot as $slot )
            <?php $c++; ?>
            @if($c == 3)
            <th colspan="3" class="text-center">{{$slot['hour']}}</th>
            <?php $c = 0; ?>
            @endif
            @if($slot['hour'] == 11 && $slot['min'] == 55)
            <th colspan="2" class="text-center">{{$slot['hour']}}</th>
            @endif
            @endforeach
        </tr>
        <tr>
            <th>minute</th>

            @foreach ($dutyTimeofDentists[0]->job_slot as $slot )
            <th>{{ $slot['min']}}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($dutyTimeofDentists as $dentist_duty_time )
        <tr>
            <td>{{$dentist_duty_time->dentist->dent_name}}</td>
            @foreach ($dentist_duty_time->job_slot as $slot)
            <td>
                @if ($slot['status'] == 'active')
                <a href="javascript:void(0)" class="btn btn-success myBtn">
                    <i class="fas fa-plus"></i>
                </a>
                @elseif($slot['status'] == 'reserved' || $slot['status'] == 'child')
                <a href="javascript:void(0)" class="btn btn-danger">
                    <i class="fas fa-check"></i>                </a>
                @elseif($slot['status'] == 'warning')
                <a href="javascript:void(0)" class="btn btn-warning">
                    <i class="fas fa-minus"></i>
                </a>
                @elseif($slot['status'] == 'noskill')
                <a href="javascript:void(0)" class="btn btn-danger">
                    <i class="fas fa-circle"></i>
                </a>
                @endif
            </td>
            @endforeach

        </tr>
        @endforeach

    </tbody>
</table>


{{-- <table class="table"> --}}
{{-- <tr>
            <th></th>
            @for ($i = 1; $i <= 24; $i++)
                <th>{{ $i*5 }} </th>
@endfor
</tr>
@foreach ($findDentsSkillMatchings as $item)
<tr>
    <td>{{ $item->dent_id }}: {{$item->dentist->dent_name}} </td> --}}
    {{-- {{dd($item->dentist->treatmentSkillRatio['skillselected']->treatment->treatment_mean_time)}} --}}
    {{-- <php  $a = ($item->dentist->treatmentSkillRatio['skillselected']->treatment->treatment_mean_time /5)  - 1; ?>

                @if ($item->dentist->job == 0)
                    @for ($i = 1; $i <= 24; $i++)
                      <td >
                          <a href="javascript:void(0)" class="btn btn-success myBtn" 
                              data-dent_id="{{ $item->dent_id }}"
    data-dentname="{{$item->dent_name}}"
    data-slot_start="{{$i*5}}"
    data-slot_end ="{{$item->next_start_slot}}"
    data-treatment_mean_time
    ="{{$item->dentist->treatmentSkillRatio['skillselected']->treatment->treatment_mean_time }}"
    data-ratio = {{$item->dentist->treatmentSkillRatio['skillselected']->ratio}}
    data-period_type = "am"
    >
    <i class="fas fa-plus"></i>
    </a>
    </td>
    @endfor
    @else --}}
    {{-- @foreach ($item->dentist->job['plot_slot'] as $slot)
                     @if(is_integer($slot))
                      <td >
                        <a href="javascript:void(0)" class="btn btn-success myBtn" 
                            data-dent_id="{{ $item->dent_id }}"
    data-dentname="{{$item->dent_name}}"
    data-slot_start="{{$i*5}}"
    data-slot_end ="{{$item->next_start_slot}}"
    data-treatment_mean_time
    ="{{$item->dentist->treatmentSkillRatio['skillselected']->treatment->treatment_mean_time }}"
    data-ratio = {{$item->dentist->treatmentSkillRatio['skillselected']->ratio}}
    data-period_type = "am"
    >
    <i class="fas fa-plus"></i>
    </a>
    </td>
    @else

    <td>
        <a href="javascript:void(0)" class="btn btn-warning" data-dent_id="{{ $item->dent_id }}"
            data-dentname="{{$item->dent_name}}" data-slot_start="{{$i*5}}" data-slot_end="{{$item->next_start_slot}}"
            data-treatment_mean_time="{{$item->dentist->treatmentSkillRatio['skillselected']->treatment->treatment_mean_time }}"
            data-ratio={{$item->dentist->treatmentSkillRatio['skillselected']->ratio}} data-period_type="am">
            <i class="fas fa-minus"></i>
        </a>
    </td>
    @endif

    @endforeach --}}
    {{-- ถ้ามี job ให้ วนลูปสร้าง ปุ่ม - --}}
    {{-- @foreach ($item->dentist->job as $job)
                      
                      @for ($i = 0; $i <= 24; $i++)

                        @if($job->slot_start >= $i*5  && $i*5 <= $job->slot_end)
                          <td >
                            <a href="" class="btn btn-warning">
                              <i class="fas fa-minus"></i>
                            </a>
                          </td>
                        @else
                          <td >
                            <a href="javascript:void(0)" class="btn btn-success myBtn" 
                                data-dent_id="{{ $item->dent_id }}"
    data-dentname="{{$item->dent_name}}"
    data-slot_start="{{$i*5}}"
    data-slot_end ="{{$item->next_start_slot}}"
    data-treatment_mean_time
    ="{{$item->dentist->treatmentSkillRatio['skillselected']->treatment->treatment_mean_time }}"
    data-ratio = {{$item->dentist->treatmentSkillRatio['skillselected']->ratio}}
    data-period_type = "am"
    >
    <i class="fas fa-plus"></i>
    </a>
    </td>
    @endif
    @endfor
    @endforeach --}}
    {{-- @endif --}}
    {{-- @for ($i = 1; $i <= 24; $i++)
                    @if($item->last_slot == $i*5)
                            <td >
                                <a href="javascript:void(0)" class="btn btn-success myBtn" 
                                    data-dent_id="{{ $item->dent_id }}"
    data-dentname="{{$item->dent_name}}"
    data-slot_start="{{$i*5}}"
    data-slot_end ="{{$item->next_start_slot}}"
    data-treatment_mean_time
    ="{{$item->dentist->treatmentSkillRatio['skillselected']->treatment->treatment_mean_time }}"
    data-ratio = {{$item->dentist->treatmentSkillRatio['skillselected']->ratio}}
    data-period_type = "am"
    >
    <i class="fas fa-plus"></i>
    </a>
    </td>
    @else
    @if ( $i*5 > $item->last_slot && $i*5 < $item->end_slot )
        <td>
            <div class="alert alert-warning col-md-2">[{{'a'}}]</div>
        </td>

        @else
        <td class="alert alert-primary col-md-2">[{{$i*5}}]</td>

        @endif
        @endif
        @endfor --}}
        {{-- </tr>   
        @endforeach
    </table> --}}

        <!-- Modal -->
        {{-- <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog"> --}}

        <!-- Modal content-->
        {{-- <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span> Save</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form">
            <div class="form-group">
                <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> วันที่</label>
                <input type="text" class="form-control" id="datemodal">
              </div>
            <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-user"></span> ชื่อคนไข้</label>
              <select class="form-control" id="patient">
                  <option>เลือก..</option>
                  @foreach ($patients as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
        @endforeach
        </select>
        </div>
        <div class="form-group">
            <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> ต้องการรักษา</label>
            <input type="text" class="form-control" id="modaltreatment_selected_text">
        </div>
        <div class="row">
            <div class="form-group col-md-5">
                <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> เริ่ม</label>
                <input type="text" class="form-control" id="modal_slot_start">
            </div>
            <div class="col-md-1">-</div>
            <div class="form-group col-md-5">
                <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> เสร็จ</label>
                <input type="text" class="form-control" id="modal_slot_end">
            </div>
        </div>


        <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-off"></span>
            Save</button>
        </form>
        </div>
        </div> --}}
        {{--       
    </div>
  </div>  --}}
        {{-- {{dd($findDentsSkillMatchings)}} --}}
        @endsection

        @section('script')

        <script>
            $(document).ready(function () {
                $(".myBtn").click(function () {
                    let reserve_date = $('#reserve_date').val();
                    let dent_name = $(this).data('dentname')
                    let slot_start = $(this).data('slot_start')
                    let slot_end = $(this).data('slot_end')
                    let dent_id = $(this).data('dent_id');
                    let period_type = $(this).data('period_type');
                    let treatment_mean_time = $(this).data('treatment_mean_time');
                    let ratio = $(this).data('ratio');
                    let _token = $('meta[name="csrf-token"]').attr('content');

                    let params = {
                        reserve_date: reserve_date,
                        slot_start: slot_start,
                        slot_end: slot_end,
                        dent_id: dent_id,
                        period_type: period_type,
                        treatment_mean_time: treatment_mean_time,
                        ratio: ratio,
                        _token: _token
                    };
                    console.log('params', params)

                    $.ajax({
                        type: 'POST',
                        url: "/appointment/store",
                        data: params,

                        success: function (data) {
                            console.log(data)
                        },
                        error: function (data) {

                        }
                    });
                    // $.post( "/appointment/store", params )
                    // .done(function( data ) {
                    //     alert( "Data Loaded: " + data );
                    // });
                });
            });

            $('#patient').keyup(function () {
                console.log($(this).val())
                $name = $(this).val();


            });

        </script>
        @endsection
