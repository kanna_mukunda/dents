@extends('layouts.app')
@section('title')
ดูตารางนัดหมาย
@endsection
@section('a_appointment_index')
active
@endsection
@section('navigate')
<a href="{{url('appointment/index')}}">ข้อมูลคลีนิคประจำวัน</a>
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        <div class="card-title">
            <h3>คนไข้นัดหมายวันนี้: 86 คน
        </div>
        <div class="card-tools">

            <small class="pull-right badge  ml-2 bg-gray">ปรับ </small>
            <small class="pull-right badge  ml-2 bg-warning">ขูดหินปูน </small>
            <small class="pull-right badge  ml-2 bg-orange">ถอนฟัน </small>
            <small class="pull-right badge  ml-2 bg-danger">อุดฟัน </small>
            <small class="pull-right badge  ml-2 bg-info">จัดฟัน </small>
            <small class="pull-right badge  ml-2 bg-purple">รักษารากฟัน </small>
            <small class="pull-right badge  ml-2 bg-navy">ฟันปลอม </small>
            <small class="pull-right badge  ml-2 bg-green">ทำครอบฟัน</small>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered datatable">
            <thead>
                <tr>
                    <th rowspan="2" class="text-center" width="10">#</th>
                    <th rowspan="2" class="text-center" width="160"> เวลา</th>
                    <th colspan="4" class="text-center color-palette">ห้อง</th>
                </tr>
                <tr>
                    <th class="text-center  color-palette ">
                       <a href="{{url('appointment/byroom/1')}}" class="btn btn-default">1</a>
                    </th>
                    <th class="text-center  color-palette">
                       <a href="{{url('appointment/byroom/2')}}" class="btn btn-default">2</a>
                    </th>
                    <th class="text-center  color-palette">
                       <a href="{{url('appointment/byroom/3')}}" class="btn btn-default">3</a>
                    </th>
                    <th class="text-center  color-palette">
                       <a href="{{url('appointment/byroom/4')}}" class="btn btn-default">4</a>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php $c = 1; $cc=0; ?>
                <?php 
                    $array1 = [
                        [//ช่วงเวลาที่ 0900
                            [//0900-0915
                                [//ห่องที่ 1
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'danger', 'patient'=> $patients[0]['f_name'].' '.$patients[0]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[2]['f_name'].' '.$patients[2]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[3]['f_name'].' '.$patients[3]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[4]['f_name'].' '.$patients[4]['l_name']],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> 'default', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'default', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'inactive'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'default', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'default', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'purple', 'patient'=> $patients[5]['f_name'].' '.$patients[5]['l_name']],
                                    'status' => 'active'
                                ]

                            ],//end /0900-0915
                            [//0900-0915
                                [//ห่องที่ 1
                                ['treat'=> 'ขูดหินปูน', 'bg'=> 'danger', 'patient'=> $patients[6]['f_name'].' '.$patients[6]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'navy', 'patient'=> $patients[7]['f_name'].' '.$patients[7]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'info', 'patient'=> $patients[10]['f_name'].' '.$patients[10]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=>  $patients[9]['f_name'].' '.$patients[9]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'orange', 'patient'=> $patients[11]['f_name'].' '.$patients[11]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[12]['f_name'].' '.$patients[12]['l_name']],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'purple', 'patient'=> $patients[13]['f_name'].' '.$patients[13]['l_name']],
                                    'status' => 'active'
                                ]

                            ],//end /0900-0915
                            [//0900-0915
                                [//ห่องที่ 1
                                ['treat'=> 'ขูดหินปูน', 'bg'=> 'orange', 'patient'=> $patients[14]['f_name'].' '.$patients[14]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[15]['f_name'].' '.$patients[15]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'inactive'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> '', 'bg'=> 'info', 'patient'=> $patients[16]['f_name'].' '.$patients[17]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'purple', 'patient'=> $patients[18]['f_name'].' '.$patients[18]['l_name']],
                                    'status' => 'active'
                                ]

                            ],//end /0900-0915
                            [//0900-0915
                                [//ห่องที่ 1
                                ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[19]['f_name'].' '.$patients[19]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[20]['f_name'].' '.$patients[20]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'purple', 'patient'=> $patients[30]['f_name'].' '.$patients[30]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'info', 'patient'=> $patients[21]['f_name'].' '.$patients[21]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=>$patients[22]['f_name'].' '.$patients[22]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'warning', 'patient'=> $patients[23]['f_name'].' '.$patients[23]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[24]['f_name'].' '.$patients[24]['l_name']],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'purple', 'patient'=> $patients[25]['f_name'].' '.$patients[25]['l_name']],
                                    'status' => 'active'
                                ]

                            ],//end /0900-0915
                        ],//end ช่วงเวลาที่ 0900
                       [//ช่วงเวลาที่ 0900
                            [//0900-0915
                                [//ห่องที่ 1
                                ['treat'=> 'ขูดหินปูน', 'bg'=> 'danger', 'patient'=> $patients[26]['f_name'].' '.$patients[26]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[27]['f_name'].' '.$patients[27]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'navy', 'patient'=> $patients[28]['f_name'].' '.$patients[28]['l_name']],
                                    ['treat'=> '', 'bg'=> 'gray', 'patient'=> $patients[29]['f_name'].' '.$patients[29]['l_name']],
                                    ['treat'=> '', 'bg'=> 'gray', 'patient'=> $patients[30]['f_name'].' '.$patients[30]['l_name']],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'inactive'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'inactive'
                                ]

                            ],//end /0900-0915
                            [//0900-0915
                                [//ห่องที่ 1
                                ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'inactive'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'danger', 'patient'=> $patients[31]['f_name'].' '.$patients[31]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'warning', 'patient'=> $patients[32]['f_name'].' '.$patients[32]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[33]['f_name'].' '.$patients[33]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[34]['f_name'].' '.$patients[34]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[35]['f_name'].' '.$patients[35]['l_name']],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'gray', 'patient'=> $patients[36]['f_name'].' '.$patients[36]['l_name']],
                                    'status' => 'active'
                                ]

                            ],//end /0900-0915
                            [//0900-0915
                                [//ห่องที่ 1
                                ['treat'=> 'ขูดหินปูน', 'bg'=> 'warning', 'patient'=> $patients[37]['f_name'].' '.$patients[37]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'info', 'patient'=> $patients[38]['f_name'].' '.$patients[38]['l_name']],
                                    ['treat'=> 'danger', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[39]['f_name'].' '.$patients[39]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'info', 'patient'=> $patients[40]['f_name'].' '.$patients[40]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[41]['f_name'].' '.$patients[41]['l_name']],
                                    ['treat'=> '', 'bg'=> 'info', 'patient'=> $patients[42]['f_name'].' '.$patients[42]['l_name']],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'warning', 'patient'=> $patients[42]['f_name'].' '.$patients[42]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[43]['f_name'].' '.$patients[43]['l_name']],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'purple', 'patient'=> $patients[44]['f_name'].' '.$patients[44]['l_name']],
                                    'status' => 'active'
                                ]

                            ],//end /0900-0915
                            [//0900-0915
                                [//ห่องที่ 1
                                ['treat'=> 'ขูดหินปูน', 'bg'=> 'warning', 'patient'=> $patients[45]['f_name'].' '.$patients[45]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[46]['f_name'].' '.$patients[46]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'navy', 'patient'=> $patients[47]['f_name'].' '.$patients[47]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'warning', 'patient'=> $patients[48]['f_name'].' '.$patients[48]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'warning', 'patient'=> $patients[49]['f_name'].' '.$patients[49]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[50]['f_name'].' '.$patients[50]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'info', 'patient'=> $patients[51]['f_name'].' '.$patients[51]['l_name']],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'warning', 'patient'=> $patients[52]['f_name'].' '.$patients[52]['l_name']],
                                    'status' => 'active'
                                ]

                            ],//end /0900-0915
                        ],//end ช่วงเวลาที่ 0900
                    ];
                //    dd($array1[$cc][$j][0]['status']);
                ?>
                @for ($i = 9; $i < 11; $i++) 
                    <?php $add15 = 15; ?> 
                    @for ($j=0; $j < 4; $j++)
                     <tr>
                        <td class="text-center">{{ $c++ }}</td>
                        <td class="text-center h5">
                            {{ $i <10 ? '0'.$i : $i }}.{{ $j * 15 > 0 ? $j * 15 : '0'.$j * 15}}
                            -
                            <?php 
                                        $minuteTo = ($j * 15)+$add15;
                                        $hrTo = $minuteTo  == 60 ? $i + 1 : $i;
                                        echo $hrTo < 10 ? '0'.$hrTo : $hrTo;
                                        echo ".";
                                        echo $minuteTo == 60 ? '00' : $minuteTo;
                                    ?>

                        </td>

                        <td class="text-center">
                            <div class="color-palette-set">
                                @if ($array1[$cc][$j][0]['status'] == 'active')

                                <div class="bg-{{ $array1[$cc][$j][0][0]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][0][0]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][0][1]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][0][1]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][0][2]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][0][2]['patient'] }}</span></div>
                                @else
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                <div class="bg-default color-palette mb-1"><span>ว่าง</span></div>
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                @endif
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="color-palette-set">
                                @if ($array1[$cc][$j][1]['status'] == 'active')

                                <div class="bg-{{ $array1[$cc][$j][1][0]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][1][0]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][1][1]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][1][1]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][1][2]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][1][2]['patient'] }}</span></div>
                                @else
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                <div class="bg-default color-palette mb-1"><span>ว่าง</span></div>
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                @endif
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="color-palette-set">
                                @if ($array1[$cc][$j][2]['status'] == 'active')

                                <div class="bg-{{ $array1[$cc][$j][2][0]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][2][0]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][2][1]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][2][1]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][2][2]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][2][2]['patient'] }}</span></div>
                                @else
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                <div class="bg-default color-palette mb-1"><span>ว่าง</span></div>
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                @endif
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="color-palette-set">
                                @if ($array1[$cc][$j][3]['status'] == 'active')

                                <div class="bg-{{ $array1[$cc][$j][3][0]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][3][0]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][3][1]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][3][1]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][3][2]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][3][2]['patient'] }}</span></div>
                                @else
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                <div class="bg-default color-palette mb-1"><span>ว่าง</span></div>
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                @endif
                            </div>
                        </td>
                    </tr>
                    @endfor
                    <?php $cc++;?>

                    @endfor
                    {{-- <tr>    
                        <td class="text-center">45</td>
                        <td class="text-center">20:00</td>
                        <td class="text-center">
                            <div class="color-palette-set">
                                <div class="bg-teal color-palette mb-1"><span>Disabled</span></div>
                                <div class="bg-teal color-palette mb-1"><span>#39CCCC</span></div>
                                <div class="bg-teal color-palette mb-1"><span>Active</span></div>
                            </div>
                        </td>
                        <td class="text-center">ว่าง</td>
                        <td class="text-center">
                            <div class="color-palette-set">
                                <div class="bg-teal color-palette mb-1"><span>Disabled</span></div>
                                <div class="bg-teal color-palette mb-1"><span>#39CCCC</span></div>
                                <div class="bg-teal color-palette mb-1"><span>Active</span></div>
                            </div>
                        </td>
                        <td class="text-center">ว่าง</td>
                    </tr> --}}
            </tbody>
        </table>

    </div>
</div>
@endsection
