@extends('layouts.app')
@section('title')
ประวัติคนไข้ Hn : {{$patient->hn}}
@endsection
@section('patient')
active
@endsection

@section('navigate')
<a href="{{url('patient/index')}}">ข้อมูลคนไข้ </a>
@endsection
@section('style')
<meta name="csrf-token" id="csrf_token" content="{{ csrf_token() }}" />

  <style>
    .hidden {
        display: none
    }

    .show {
        display: block;
    }
    .profile-user-img {
        border: 3px solid #007bff;
        margin: 0 auto;
        padding: 3px;
        width: 163px;
    }

    th {
        text-align: center
    }

    .btn-block {
        cursor: not-allowed
    }
    #cal_treatment_lists_info{
        min-height:400px
    }

</style>
@endsection

@section('content')
<div class="row">
    <div class="col-12 col-sm- 12">
        <div class="card card-primary card-outline card-tabs">
            <div class="card-header p-0 pt-1 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill"
                            href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home"
                            aria-selected="false">ประวัติส่วนตัว</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill"
                            href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile"
                            aria-selected="false">MEDICAL / DENTAL HISTORY</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="custom-tabs-three-treatment-cal-tab" data-toggle="pill"
                            href="#custom-tabs-three-treatment-cal" role="tab" aria-controls="custom-tabs-three-treatment-cal"
                            aria-selected="false">คำนวณค่ารักษา</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-three-settings-tab" data-toggle="pill"
                            href="#custom-tabs-three-settings" role="tab" aria-controls="custom-tabs-three-settings"
                            aria-selected="false">นัดหมาย</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-three-messages-tab" data-toggle="pill"
                            href="#custom-tabs-three-messages" role="tab" aria-controls="custom-tabs-three-messages"
                            aria-selected="false">ประวัติการรักษา</a>
                    </li>

                    {{-- <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-three-messages2-tab" data-toggle="pill"
                            href="#custom-tabs-three-messages2" role="tab" aria-controls="custom-tabs-three-messages2"
                            aria-selected="false">ประวัติซื้อเวชภัณฑ์</a>
                    </li> --}}
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-tabContent">
                    <div class="tab-pane fade active show" id="custom-tabs-three-home" role="tabpanel"
                        aria-labelledby="custom-tabs-three-home-tab">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card card-info card-outline">

                                    <div class="card-body box-profile">
                                        <div class="text-center">
                                            <img class="profile-user-img img-fluid img-circle"
                                                src="{{asset('adminlte/dist/img/default-profile.jpg')}}"
                                                alt="User profile picture">
                                        </div>

                                        <h3 class="profile-username text-center">{{$patient->f_name}}
                                            {{$patient->l_name}}</h3>

                                        <p class="text-muted text-center">คนไข้</p>

                                        <ul class="list-group list-group-unbordered mb-3">
                                            <li class="list-group-item">
                                                <b>HN</b> <a class="float-right">{{$patient->hn}}</a>
                                            </li>
                                            <li class="list-group-item text-center">
                                                <?php echo DNS1D::getBarcodeHTML($patient->hn, 'EAN8',5,33); ?>
                                            </li>
                                            <li class="list-group-item">
                                                <b>เพศ</b> <a class="float-right">{{$patient->sex}}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>ว/ด/ป เกิด</b> <a class="float-right">{{$patient->b_date_th}}</a>
                                            </li>

                                        </ul>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card card-info">
                                    <div class="card-header">
                                        ข้อมูลทั่วไป
                                    </div>
                                    <div class="card-body box-profile">
                                        <ul class="list-group list-group-unbordered mb-3">
                                            <li class="list-group-item">
                                                <b>เลขบัตรประจำตัวประชาชน</b> <a
                                                    class="float-right">{{$patient->phone}}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>ที่อยู่</b>
                                                <a class="float-right">
                                                    {{$patient->address}}
                                                </a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>เบอร์โทรศัพท์</b> <a class="float-right">{{$patient->phone}}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Line ID</b> <a class="float-right">personalLine</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>ผู้ติดต่อได้กรณีฉุกเฉิน</b> <a class="float-right">
                                                    {{$patient->r_name == "" ? '-' :  $patient->r_name." โทร.".$patient->r_phone}}
                                                </a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>โรคประจำตัว</b> <a
                                                    class="float-right">{{$patient->cd != "" ? $patient->cd : "-"}}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>ประวัติแพ้ยา</b> <a
                                                    class="float-right">{{$patient->ah != "" ? $patient->ah : "-"}}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>ประวัติแพ้สารเคมี</b> <a
                                                    class="float-right">{{$patient->ca != "" ? $patient->ca : "-"}}</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel"
                        aria-labelledby="custom-tabs-three-profile-tab">
                        <div class="card bg-info">
                            <div class="card-header">MEDICAL / DENTAL HISTORY</div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->heart_t == 'y' ? 'checked' : ''}} id="heart_trouble"
                                                name="heart_trouble">
                                            <label for="heart_trouble" name="heart_trouble"
                                                class="custom-control-label">Heart Trouble</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->rheumatic_f == 'y' ? 'checked' : ''}} id="rheumatic_fever"
                                                name="rheumatic_fever">
                                            <label for="rheumatic_fever" name="rheumatic_fever"
                                                class="custom-control-label">Rheumatic
                                                fever</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->hypotension == 'y' ? 'checked' : ''}} id="hypotension"
                                                name="hypotension">
                                            <label for="hypotension" name="hypotension"
                                                class="custom-control-label">Hypotension</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->hypertension == 'y' ? 'checked' : ''}} id="hypertension"
                                                name="hypertension">
                                            <label for="hypertension" name="hypertension"
                                                class="custom-control-label">Hypertension</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->blood_d == 'y' ? 'checked' : ''}} id="blood_disease"
                                                name="blood_disease">
                                            <label for="blood_disease" name="blood_disease"
                                                class="custom-control-label">Blood Disease</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->tuberculasis == 'y' ? 'checked' : ''}} id="tuberculasis"
                                                name="tuberculasis">
                                            <label for="tuberculasis" name="tuberculasis"
                                                class="custom-control-label">Tuberculasis</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->liver_d == 'y' ? 'checked' : ''}} id="liver_disease"
                                                name="liver_disease">
                                            <label for="liver_disease" name="liver_disease"
                                                class="custom-control-label">Liver Disease</label>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->diabetes_m == 'y' ? 'checked' : ''}} id="dusbetes_melitus"
                                                name="dusbetes_melitus">
                                            <label for="dusbetes_melitus" name="dusbetes_melitus"
                                                class="custom-control-label">Dusbetes
                                                melitus</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->allergy == 'y' ? 'checked' : ''}} id="allergy"
                                                name="allergy">
                                            <label for="allergy" name="allergy"
                                                class="custom-control-label">Allergy</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->std == 'y' ? 'checked' : ''}} id="std" name="std">
                                            <label for="std" name="std" class="custom-control-label">STD</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->other != '' ? 'checked' : ''}} id="other" name="other">
                                            <label for="other" name="other" class="custom-control-label">Other</label>
                                            <input type="text" class="form-control" id="othertext" name="othertext"
                                                value="{{$patient->other}}">

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- คำนวณค่ารักษา --}}
                    <div class="tab-pane fade " id="custom-tabs-three-treatment-cal" role="tabpanet"
                        aria-labelledby="custom-tabs-three-treatment-cal-tab">
                        <div id="cal_treatment_added_lists">
                            <div class="row">
                                <div class="col-md-3">
                                    {{-- ข้อมูลคนไข้ และหมอผู้ทำการรักษา --}}
                                    <div class="card card-primary card-outline">
                                        <div class="card-body box-profile">
                                            <div class="text-center">
                                            <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/default-profile.jpg')}}" alt="User profile picture">
                                            </div>

                                            <h3 class="profile-username text-center">
                                                {{ $postpone_appointment[0]->patient->f_name." ". $postpone_appointment[0]->patient->l_name}}
                                            </h3>

                                            <p class="text-muted text-center">Hn : <span id="hn">{{$postpone_appointment[0]->patient->hn}}</span></p>

                                            <ul class="list-group list-group-unbordered mb-3">
                                            <li class="list-group-item">
                                                <b>วันที่</b> <a class="float-right">{{$postpone_appointment[0]->thDate}}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>เวลาเริ่ม</b> <a class="float-right">
                                                    <?php
                                                        $start_time = explode('-',$postpone_appointment[0]->treatment_time);
                                                        echo $start_time[0];
                                                    ?>
                                                </a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>เวลาเสร็จ</b> <a class="float-right"></a>
                                            </li>
                                             <li class="list-group-item">
                                                <b>หมอ</b> <a class="float-right">
                                                    {{$postpone_appointment[0]->dentist->dent_name}}
                                                </a>
                                            </li>
                                            </ul>

                                            {{-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> --}}
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    {{-- lists รายการรักษา --}}
                                    <div id="cal_treatment_lists">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>ชื่อการรักษา</th>
                                                    <th>รายละเอียด</th>
                                                    <th>ราคา</th>
                                                    <th>จำนวน</th>
                                                    <th class="treatment_sum"></th>
                                                </tr>
                                            </thead>
                                            <tbody id="cal_treatment_lists_info">

                                            </tbody>
                                        </table>

                                        <div class="row">
                                            <!-- accepted payments column -->
                                            <div class="col-6">

                                            </div>
                                            <!-- /.col -->
                                            <div class="col-6">

                                            <div class="table-responsive">
                                                <table class="table">
                                                <tbody><tr>
                                                    <th style="width:50%">รวม:</th>
                                                    <td id="cal_summary"></td>
                                                </tr>
                                                <tr>
                                                    <th>รับมา</th>
                                                    <td><input type="text" name="customer_cash" id="customer_cash" class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <th>ทอน:</th>
                                                    <td id="turn_cash"></td>
                                                </tr>

                                                </tbody></table>
                                            </div>
                                            </div>
                                            <!-- /.col -->
                                        </div><!--row-->

                                    </div>
                                    <div>
                                         <input type="hidden" name="dent_id" id="dent_id" value="{{$postpone_appointment[0]->dentist->id}}">

                                        <a href="javascript:void(0)" class="btn btn-success float-right mb-4 col-md-2" id="cal_detail_btn">บันทึก</a>
                                    </div>
                                     <hr style="clear:both">
                                     <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="cal_treatment_id">ชื่อการรักษา</label>
                                                <select class="form-control" name="cal_treatment_id" id="cal_treatment_id">
                                                    <option>เลือก..</option>
                                                    @foreach($treatments as $key => $treatment)
                                                        <option value="{{$treatment->id}}"> {{$treatment->treatment_name}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="cal_detail">รายละเอียด</label>
                                                <input type="text" class="form-control" id="cal_detail" name="cal_detail">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label for="cal_amount">จำนวน</label>
                                                <input type="text" class="form-control" id="cal_amount" name="cal_amount">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="cal_price">ราคา(บาท)</label>
                                                <input type="text" class="form-control" id="cal_price" name="cal_price">
                                            </div>
                                        </div>
                                        <div class="col-m-1">
                                            <div class="form-group">
                                                <label for="cal_add_btn_label">&nbsp;</label>
                                                <a href="javascript:void(0)" class="btn btn-success round form-control cal_add_btn"><i class="fas fa-plus-circle"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="custom-tabs-three-messages" role="tabpanel"
                        aria-labelledby="custom-tabs-three-messages-tab">
                        <table class="table table-bordered datatable">
                            <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>บิลที่</th>
                                    <th style="width:15%">วันที่</th>
                                    <th>รายการ</th>
                                    <th>ค่ารักษา(บาท)</th>
                                    <th>แพทย์ประจำวัน</th>
                                    {{-- <th>ผลการรักษา / นัดหมายครั้งต่อไป</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($patient->bill as $bill)
                                <tr>
                                    <td>1.</td>
                                    <td>{{$bill->bill}}</td>
                                    <td>{{$bill->date}}</td>
                                    <td>{{$bill->treatment->treatment_name}} </td>
                                    <td>{{$bill->cost}}</td>
                                    <td>{{$bill->dentist->dent_name}}</td>
                                </tr>

                                @endforeach


                            </tbody>
                        </table>

                        <div class="row">
                                <div class="col-md-12">
                                <div class="timeline">
                                    @foreach($patient->bill as $key => $bill)

                                            <div class="time-label">
                                            <span class="bg-red">{{$bill->date}}</span>
                                            </div>
                                            <!-- /.timeline-label -->
                                            <!-- timeline item -->
                                            <div>
                                            <i class="fas fa-user bg-blue"></i>
                                            <div class="timeline-item">
                                                <span class="time"><i class="fas fa-clock"></i></span>
                                                <h3 class="timeline-header">{{$bill->treatment->treatment_name}}</h3>

                                                <div class="timeline-body">
                                                    {{$bill->detail}}
                                                    <hr>
                                                    <div class="timeline-body">
                                                        <img src="http://placehold.it/150x100" alt="...">
                                                        <img src="http://placehold.it/150x100" alt="...">
                                                        <img src="http://placehold.it/150x100" alt="...">
                                                        <img src="http://placehold.it/150x100" alt="...">
                                                        <img src="http://placehold.it/150x100" alt="...">
                                                    </div>
                                                </div>
                                                <div class="timeline-footer">
                                                <a href="{{url('treatment_history/create/'.$bill->patient->hn)}}" class="btn btn-primary btn-sm">เพิ่มข้อมูล</a>
                                                <a href="{{url('treatment_history/edit/'.$bill->patient->hn)}}" class="btn btn-warning btn-sm">แก้ไขข้อมูล</a>
                                                </div>
                                            </div>
                                            </div>
                                    @endforeach
                                </div>

                            </div>
                        </div><!--row-->
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-three-settings" role="tabpanel"
                        aria-labelledby="custom-tabs-three-settings-tab">
                        <div class="card">
                            <div class="card-header p-2">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item"><a class="nav-link active" href="#activity"
                                            data-toggle="tab">การนัดหมาย</a>
                                    </li>
                                    {{-- <li class="nav-item"><a class="nav-link " href="#timeline"
                                            data-toggle="tab">ส่ง SMS</a></li> --}}
                                </ul>
                            </div><!-- /.card-header -->
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="activity">
                                      <a href="{{url('/appointment/appoint_by_staff')}}" class="btn btn-success">นัดทำฟันครั้งถัดไป</a>
                                      <a href="javascript:void(0)"  class="btn btn-warning cancelOrPosponeBtn"
                                        data-id= "{{$postpone_appointment[0]->id}}"
                                        data-date= "{{$postpone_appointment[0]->date}}"
                                        data-thdate = "{{$postpone_appointment[0]->thDate}}"
                                        data-dent_id= "{{$postpone_appointment[0]->dent_id}}"
                                        data-dent_name = "{{$postpone_appointment[0]->dentist->dent_name}}"
                                        data-patient_id = "{{$postpone_appointment[0]->patient_id}}"
                                        data-patient_name = "{{$postpone_appointment[0]->patient->f_name.' '.$postpone_appointment[0]->patient->l_name}}"
                                        data-hn = "{{$postpone_appointment[0]->patient->hn}}"
                                        data-treatment_id = "{{$postpone_appointment[0]->treatment->id}}"
                                        data-treatment_name = "{{$postpone_appointment[0]->treatment->treatment_name}}"
                                        data-treatment_time = "{{$postpone_appointment[0]->treatment_time}}"
                                        data-status = "{{$postpone_appointment[0]->status}}"
                                        data-time = "{{$postpone_appointment[0]->treatment_time}}"

                                      >นัดเลื่อนนัด/ยกเลิกนัด</a>

                                        <div class="table-responsive p-0">
                                            <table class="table table-hover text-nowrap datatable">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>วันที่นัดคนไข้</th>
                                                    <th>ชื่อ - สกุลหมอ</th>
                                                    <th>ขื่อ - สกุล คนไข้</th>
                                                    <th>เลข HN</th>
                                                    <th>การรักษา</th>
                                                    <td>ห้องตรวจ</td>
                                                    <th>เวลา</th>
                                                    {{-- <th>สถานะ</th> --}}
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $c = 1; ?>
                                                    @foreach ($postpone_appointment as $item)
                                                    <tr>
                                                    <td>{{$c++}}</td>
                                                    <td>{{$item->thDate}}</td>
                                                    <td>{{$item->dentist->dent_name}}</td>
                                                    <td>{{$item->patient->f_name}} {{$item->patient->l_name}}</td>
                                                    <td>{{$item->patient->hn}}</td>
                                                    <td>{{$item->treatment->treatment_name}}</td>
                                                    <td>{{$item->dentist->dutyTime[0]->room->room_name}}</td>
                                                    <td>{{$item->treatment_time}}</td>
                                                    {{-- <td>{{$item->thStatus}}</td> --}}
                                                    <td>
                                                        {{-- {{url('job/meetingconfirm/'.$item->id)}} --}}
                                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-info"   class="btn {{$item->btn_style}} updateStatusBtn"
                                                                data-id= "{{$item->id}}"
                                                                data-date= "{{$item->date}}"
                                                                data-thdate = "{{$item->thDate}}"
                                                                data-dent_id= "{{$item->dent_id}}"
                                                                data-dent_name = "{{$item->dentist->dent_name}}"
                                                                data-patient_id = "{{$item->patient_id}}"
                                                                data-patient_name = "{{$item->patient->f_name.' '.$item->patient->l_name}}"
                                                                data-hn = "{{$item->patient->hn}}"
                                                                data-treatment_id = "{{$item->treatment->id}}"
                                                                data-treatment_name = "{{$item->treatment->treatment_name}}"
                                                                data-treatment_time = "{{$item->treatment_time}}"
                                                                data-status = "{{$item->status}}"
                                                                data-time = "{{$item->treatment_time}}"
                                                        >{{$item->thStatus}}</a>

                                                    </td>
                                                    </tr>

                                                    @endforeach

                                                </tbody>
                                            </table>

                                        </div><!--card-->
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane" id="timeline">
                                        <!-- The timeline -->
                                        sms
                                    </div>
                                    <!-- /.tab-pane -->

                                </div>
                                <!-- /.tab-content -->
                            </div><!-- /.card-body -->
                        </div>
                    </div>
                    {{-- <div class="tab-pane fade" id="custom-tabs-three-messages2" role="tabpanel"
                        aria-labelledby="custom-tabs-three-messages2-tab">
                       ซื้อ
                    </div> --}}
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>




{{-- -----------------------MODAL------------------------------ --}}
<div class="modal fade hidden" id="modal-info">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">แก้ไขสถานะ</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="card card-info card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle" style="width: 80px !important"
                                src="{{ asset('adminlte/dist/img/default-profile.jpg') }}"
                                alt="User profile picture">
                        </div>

                        <h3 class="profile-username text-center" id="patient_name_modal"> </h3>

                        <p class="text-muted text-center">คนไข้</p>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>วันที่นัดหมอ</b> <a class="float-right" id="date_modal">2020-08-26</a>
                            </li>
                            <li class="list-group-item">
                                <b>เวลานัด</b> <a class="float-right" id="time_modal">11:40-11:40 น.</a>
                            </li>
                            <li class="list-group-item">
                                <b>หมอผู้รับผิดชอบ</b> <a class="float-right" id="dent_name_modal">หมอม่อน</a>
                            </li>
                            <li class="list-group-item">
                                <b>การรักษา</b> <a class="float-right" id="treatment_modal">ปรับ</a>
                            </li>
                        </ul>
                        <div class="form-group">
                            <label class="control-label">สถานะ</label>
                            <select class="form-control" id="status_modal">
                                <option value="">เลือก..</option>
                                <option value="reserved">นัดหมอแล้ว</option>
                                <option value="processing">มาตามนัด</option>
                                <option value="postpone">เลื่อนนัด</option>
                                <option value="cancel">ยกเลิกนัด</option>
                            </select>

                        </div>
                        <div id="comment_modal" class="hidden mb-1">
                            <label class="label-control">หมายเหตุ</label>
                            <textarea class="form-control" rows="2" name="comment" id="comment"></textarea>
                        </div>
                        <button type="button" id="save_btn_modal"
                            class="btn btn-primary btn-block">บันทึกการมาตามนัด</button>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@endsection



@section('script')
  <script>
      let id
      let dent_id
      let date
      let thdate
      let time
      let patient_id
      let patient_name
      let hn
      let treatment_name
      let treatment_time
      let status
      let this_

      $('.cancelOrPosponeBtn').click(function(){
        //this_         = $(this);
      id              = $(this).data('id')
      dent_id         = $(this).data('dent_id')
      date            = $(this).data('date')
      thdate          = $(this).data('thdate')
      time            = $(this).data('time')
      dent_name       = $(this).data('dent_name')
      patient_id      = $(this).data('patient_id')
      patient_name    = $(this).data('patient_name')
      hn              = $(this).data('hn')
      treatment_id    = $(this).data('treatment_id')
      treatment_name  = $(this).data('treatment_name')
      treatment_time  = $(this).data('treatment_time')
      status          = $(this).data('status')

      $('#patient_name_modal').html(patient_name)
      $('#dent_name_modal').html(dent_name)
      $('#date_modal').html(thdate)
      $('#time_modal').html(time)
      $('#treatment_modal').html(treatment_name)
      showHiddenModal()
    });
    $('.updateStatusBtn').click(function(){
        this_         = $(this);
      id              = $(this).data('id')
      dent_id         = $(this).data('dent_id')
      date            = $(this).data('date')
      thdate          = $(this).data('thdate')
      time            = $(this).data('time')
      dent_name       = $(this).data('dent_name')
      patient_id      = $(this).data('patient_id')
      patient_name    = $(this).data('patient_name')
      hn              = $(this).data('hn')
      treatment_id    = $(this).data('treatment_id')
      treatment_name  = $(this).data('treatment_name')
      treatment_time  = $(this).data('treatment_time')
      status          = $(this).data('status')

      $('#patient_name_modal').html(patient_name)
      $('#dent_name_modal').html(dent_name)
      $('#date_modal').html(thdate)
      $('#time_modal').html(time)
      $('#treatment_modal').html(treatment_name)
      showHiddenModal()
    });

    $('#save_btn_modal').click(function (){

      let _token = $('meta[name="csrf-token"]').attr('content');

      let params = {
          job_id: id,
          status: $('#status_modal').val(),
          comment: $('#comment').val(),
          _token: _token
      };

      $.ajax({
          type: 'POST',
          url: "/job/update_status",
          data: params,

          success: function (data) {
              console.log('data',data)
            $(this_).html(data)
            showHiddenModal()
          },
          error: function (data) {

          }
      });
    });


    $('#status_modal').change(function(){
      if($(this).val() == 'cancel' || $(this).val() == 'postpone'){
        if($(this).val() == 'postpone'){
        }
        $('#comment_modal').removeClass('hidden')
        $('#comment_modal').addClass('show')
      }else{
        $('#comment_modal').removeClass('show')
        $('#comment_modal').addClass('hidden')
      }
    })


    function showHiddenModal() {
        if ($('#modal-info').hasClass('hidden')) {
            $('#modal-info').removeClass('hidden')
            $('#modal-info').addClass('show')
        } else {
            $('#modal-info').removeClass('show')
            $('#modal-info').addClass('hidden')
        }
    }

    $('.close').click(function () {

        showHiddenModal()
    })

  </script>

  <script>
    $('.cal_add_btn').click(function(){
        let cal_treatment_name = $('#cal_treatment_id option:selected').text();
        let cal_treatment_id = $('#cal_treatment_id option:selected').val();
        let cal_price = $('#cal_price').val();
        let cal_detail = $('#cal_detail').val();
        let cal_amount = $('#cal_amount').val();
        let sum =0;

        let text = `<tr class="treatment_cost_info">
                        <td class="cal_treatment_id">${cal_treatment_name}</td>
                        <td class="cal_detail">${cal_detail}</td>
                        <td class="cal_price">${cal_price}</td>
                        <td class="cal_amount">${cal_amount}</td>
                        <td class="cal_price_class">${cal_amount * cal_price}</td>
                        <td>
                            <input type="hidden" name="cal_hn" class="cal_treatment_id" value="${cal_treatment_id}">
                            <button class="btn btn-warning del_append">ลบ</button>
                        </td>
                    </tr>
                    <tr>

        `;
        $('#cal_treatment_lists_info').append(text);
        $('.cal_price_class').each(function(){
           sum += parseFloat($(this).text())
        });

        $('#cal_summary').html(sum);
        sum =0;
    });

    $("body").delegate(".del_append",'click' , function() {
        $(this).parent().parent().remove()
    });


    $('#customer_cash').on('keyup', function(){
        let turn_cash =  $(this).val() - $('#cal_summary').text();
        $('#turn_cash').html(turn_cash)
    });


    $('#cal_detail_btn').on('click', function(){
        let _token = $('meta[name="csrf-token"]').attr('content');
        let arr = [];
         let dent_id = $('#dent_id').val();
        let hn =  $('#hn').text();
        let cal_summary = $('#cal_summary').text();
        let customer_cash = $('#customer_cash').val();
        let turn_cash = $('#turn_cash').text();
        $('.treatment_cost_info').each(function(){
            let price = $(this).find('.cal_price_class').text();//ผลรวมของราคา x จำนวน ของแต่ละ treatment
            let treatment_id = $(this).find('input.cal_treatment_id').val();
            let detail = $(this).find('.cal_detail').text();
            let amount = $(this).find('.cal_amount').text();

            arr.push({
                price: price,
                treatment_id: treatment_id,
                detail: detail,
                amount: amount,
            });
        });


        let params = {
            dent_id: dent_id,
            hn: hn,
            total: cal_summary,
            customer_cash: customer_cash,
            turn_cash: turn_cash,
            detail_arr: arr,
            _token: _token
        };

        $.ajax({
            type: 'POST',
            url: "/account/store",
            data: params,

            success: function (data) {
                window.location.href = '/account/invoice/'+data
            },
            error: function (data) { }
        });

    });
  </script>

@endsection
