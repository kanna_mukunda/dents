@extends('layouts.app')

@section('title')
นัดทำฟัน
@endsection
@section('navigate')
<a href="{{ url('dutytime/index') }}">กำหนดปฏิทินประจำเดือน</a>
@endsection

@section('style')
<!-- fullCalendar -->
<meta name="csrf-token" id="csrf_token" content="{{ csrf_token() }}" />

<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/core/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/daygrid/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/timegrid/main.min.css') }}">
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
    integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
    crossorigin="anonymous" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

<style>
    .list-group-item {
        position: relative;
        display: block;
        padding: .3rem 1.25rem;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .125);
    }

    .hidden {
        display: none
    }

    .show {
        display: block;
    }

    .bgred {
        background-color: gray;
    }

    .disabled {
        cursor: not-allowed;

    }
    table{
        width:100%;
        table-layout: auto;

    }
    td.hour {
        min-width: 100px;
        max-width: 100px;
        overflow: hidden;
     }

     #clinic_day_off_lists_div{
         /*min-height:210px*/
     }

     .center_div {
        margin-top:1%;
    }

</style>

@endsection


@section('content')


<div class="card card-primary card-tabs">
    <div class="card-header p-0 pt-1">
        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="calendar-main-tab" data-toggle="pill" href="#calendar-main"
                    role="tab" aria-controls="calendar-main" aria-selected="true">ดูแบบเดือน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " id="clinic-day-off-tab" data-toggle="pill" href="#clinic-day-off"
                    role="tab" aria-controls="clinic-day-off" aria-selected="false">ดูแบบตารางเวรของหมอ</a>
            </li>

        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content" id="custom-tabs-one-tabContent">

            <div class="tab-pane fade  active show " id="calendar-main" role="tabpanel"
                aria-labelledby="calendar-main-tab">
                <div class="row">
                    <div class=" col-12">
                        <div id="calendar" style="width: 100%"></div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="clinic-day-off" role="tabpanel"aria-labelledby="clinic-day-off-tab">
            <div class="card card-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>วันที่</th>
                    @for($i = 9; $i <= 20; $i++)
                        <th class="text-center">
                            {{$i < 10 ? '0'.$i : $i}}:00
                        </th>
                    @endfor
                </tr>
            </thead>
            <tbody>
                @foreach($day_number_arr as $key => $day_number)

                    <tr>
                        <td>{{$day_number['day_number']}}</td>
                        @for($i = 9; $i <= 20; $i++)

                            <td class="text-center {{$day_number['day_type'] == 'dayOff' ? 'bg-info' : ''}} ">
                                @if($day_number['day_type'] == 'dayOff')
                                    {{-- วันหยุด --}}
                                    {{$day_number['day_name']}}
                                @elseif($day_number['day_type'] == 'dayWork')
                                    {{-- วันทำงาน กับวันลากิจของหมอ --}}
                                    {{-- 1.เอาข้อ มูลการเข้าเวรของหมอ มาแสดง --}}
                                    @foreach($day_number['dents']['work'] as $key => $dentWork)
                                        {{-- เชค ช่วงเช้า --}}
                                        @if($i >= $dentWork['dentInfo']['am_hour_start'] && $i <=  $dentWork['dentInfo']['am_hour_end'])
                                            <a  href="{{ url('appointment/index') }}" class="btn btn-block btn-outline-success btn-sm appointment_btn"
                                                data-hour="{{$i}}"
                                            >
                                                w-{{ $dentWork['dentInfo']['Dentist']['dent_name'] }}
                                            </a>
                                        @endif

                                        {{-- เชค ช่วงเช้า --}}
                                        @if($i >= $dentWork['dentInfo']['ev_hour_start'] && $i <=  $dentWork['dentInfo']['ev_hour_end'])
                                            <a  href="{{ url('appointment/index') }}" class="btn btn-block btn-outline-success btn-sm appointment_btn"
                                                data-hour="{{$i}}"
                                            >
                                                w-{{ $dentWork['dentInfo']['Dentist']['dent_name'] }}
                                            </a>
                                        @endif

                                        {{-- เชค ช่วงเช้า --}}
                                        @if($i >= $dentWork['dentInfo']['pm_hour_start'] && $i <=  $dentWork['dentInfo']['pm_hour_end'])
                                            <a  href="{{ url('appointment/index') }}" class="btn btn-block btn-outline-success btn-sm appointment_btn"
                                                data-hour="{{$i}}"
                                            >
                                            w-{{ $dentWork['dentInfo']['Dentist']['dent_name'] }}
                                            </a>
                                        @endif
                                    @endforeach

                                    @foreach($day_number['dents']['vacation'] as $key => $dentDayOff)
                                    {{-- {{dd($dentWork['dentInfo']['Dentist']['dent_name'])}} --}}
                                        <div class="btn btn-block btn-secondary btn-sm">v-{{ $dentDayOff['dentInfo']['dent_name'] }}</div>
                                    @endforeach
                                @else
                                    {{-- วันยังว่าง --}}
                                    <div style="padding:18% 10%"  class="appointment_btn"
                                                data-hour="{{$i}}"
                                    ></div>
                                @endif

                            </td>
                        @endfor
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>
            </div><!-- tab calendar-main -->


        </div>
    </div>
    <!-- /.card -->
</div>



<div class="card">
    <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
    </div>
    <!-- /.card-header -->
    <div class="card-body pt-0">
        <div class="text-center">
            {{-- <a href="javascript:void(0)" id="addEventBtn" class="col-md-2 btn btn-success">Add new event</a> --}}
        </div>
        <div class="row">
            <div class=" col-12">
                {{-- <div id="calendar" style="width: 100%"></div> --}}
            </div>
            {{-- <div class="col-6"></div> --}}
        </div>
    </div><!-- /.card-body -->
</div>
{{-- Modal --}}

<div class="modal hidden" id="myModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">กำหนดปฏิทินประจำเดือน</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="{{ url('calendar/store') }}" method="post">
                    @csrf
                    <div class="form-grop  row mb-1">
                        <label class="col-sm-2 form-label">ชื่อวันหยุด</label>
                        <input type="text" class="col-sm-7 form-control" value="" name="title" id="title">
                    </div>

                    <div class="form-grop  row mb-1">
                        <label class="col-sm-2 form-label">จากวันที่</label>
                        <input id="inputdatepicker" name="start_date" class="datepicker col-sm-4 form-control">
                        <label class="col-sm-2 form-label">เวลา</label>
                        <input id="inputdatepicker" name="start_time" class="timepicker col-sm-4 form-control">

                    </div>
                    <div class="form-grop  row mb-1">
                        <label class="col-sm-2 form-label">ถึงวันที่</label>
                        <input id="inputdatepicker" name="end_date" class="datepicker col-sm-4 form-control">
                        <label class="col-sm-2 form-label">เวลา</label>
                        <input id="inputdatepicker" name="end_time" class="timepicker col-sm-4 form-control">

                    </div>
                    <div class="form-grop  row mb-1">
                        <label class="col-sm-5 form-label">สถานะ</label>
                        <select class="form-control col-sm-7" name="status">
                            <option>เลือก..</option>
                            <option value="active">Active</option>
                            <option value="inactive">InActive</option>
                        </select>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success confirm_btn">บันทึก</button>
            </div>
            </form>

        </div>
        <!--modal-content-->
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('fullcalendar/core@4.4.0/main.min.js') }}"></script>
<script src="{{ asset('fullcalendar/core@4.4.0/locales-all.min.js') }}"></script>
<script src="{{ asset('fullcalendar/interaction@4.4.0/main.min.js') }}"></script>
<script src="{{ asset('fullcalendar/daygrid@4.4.0/main.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
    integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.th.min.js"
    integrity="sha512-cp+S0Bkyv7xKBSbmjJR0K7va0cor7vHYhETzm2Jy//ZTQDUvugH/byC4eWuTii9o5HN9msulx2zqhEXWau20Dg=="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.th.min.js" integrity="sha512-cp+S0Bkyv7xKBSbmjJR0K7va0cor7vHYhETzm2Jy//ZTQDUvugH/byC4eWuTii9o5HN9msulx2zqhEXWau20Dg==" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: true,
            language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
            thaiyear: true //Set เป็นปี พ.ศ.
        }).datepicker("setDate", "0"); //กำหนดเป็นวันปัจุบัน

        $('.clinic_day_off_datepicker').datepicker({
            format: 'dd-mm-yyyy',
            todayBtn: true,
            thaiyear: true ,
            language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        }).datepicker("setDate", "0"); //กำหนดเป็นวันปัจุบัน


        var dateNow = new Date();

        $('.timepicker').datetimepicker({
            format: 'HH:mm',
            stepping: 30,
            enabledHours: [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
            //defaultDate:moment(dateNow).hours(0).minutes(0).seconds(0).milliseconds(0)

        })
    });

</script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        let addEvents;
        var calendarEl = document.getElementById('calendar');
        $.get('../calendar/add_events')
            .done(function (data) {
                addEvents = data
                // console.log(addEvents)

                var calendar = new FullCalendar.Calendar(calendarEl, {
                    dateClick: function (info) {
                        // console.log( info.event.extendedProps.status)

                    window.location.href = './appoint_by_staff_by_date/' + info.dateStr

                    },
                    eventClick: function(info) {
                        console.log('Event: ' + info.event.extendedProps.status);

                        // change the border color just for fun
                        info.el.style.borderColor = 'red';
                    },

                    plugins: ["interaction", "dayGrid", "timeGrid", "resourceTimeline"],
                    initialView: 'dayGridMonth',
                    selectable: true,
                    editable: true,
                    unselectAuto: true,
                    dropable: true,
                    eventResizableFromStart: true,
                    events: addEvents,
                    locale: 'th',
                });

                calendar.render()
                $('.fc-time').each(function(index){
                    $(this).text('- ')
                })

                $('.fc-title').css('color','white')
            });


    });

    $(document).ready(function(){
        $
    })

    $('#addEventBtn').click(function () {
        showHiddenModal();
    });

    function showHiddenModal() {
        if ($('#myModal').hasClass('hidden')) {
            $('#myModal').removeClass('hidden')
            $('#myModal').addClass('show')
        } else {
            $('#myModal').removeClass('show')
            $('#myModal').addClass('hidden')
        }
    }

    $('.close').click(function () {
        showHiddenModal()
    })
        let i=0;

    $('#clinic_day_off_add_btn').click(function(){
        //เพิ่มรายการ วันหยุดคลินิค
        let day_off_name = $('#day_off_name').val();
        let day_off_to_date = $('#day_off_to_date').val();
        let day_off_from_date = $('#day_off_from_date').val();
        let day_off_to_time = $('#day_off_to_time').val();
        let day_off_from_time = $('#day_off_from_time').val();
        let day_off_dent_id = $('#day_off_dent_id option:selected').val()
        let day_off_dent_name = $('#day_off_dent_id option:selected').text()
        let text = `
            <tr>
                <td style='width:30%'>
                    <input type="text" class="form-control" name="day_off[${i}]['name']" value="${day_off_name}">
                </td>
                <td>
                    <input type="text" class="form-control" name="day_off[${i}]['dent_name']" value="${day_off_dent_name}">
                    <input type="hidden" class="form-control" name="day_off[${i}]['dent_id']" value="${day_off_dent_id}">
                </td>
                <td>
                    <div class="row">
                    <input type="text" class="form-control col-md-7" name="day_off[${i}]['from_date']" value="${day_off_from_date}">

                    <input type="text" class="form-control col-md-5" name="day_off[${i}]['from_time']" value="${day_off_from_time}">
                    </div>
                </td>
                <td>
                    <div class="row">
                    <input type="text" class="form-control col-md-7" name="day_off[${i}]['to_date']" value="${day_off_to_date}">

                    <input type="text" class="form-control col-md-5" name="day_off[${i}]['to_time']" value="${day_off_to_time}">
                    </div>
                </td>

                <td><a href="javascript:void(0)" class="btn btn-danger btn-sm clinic_day_off_lists_del_btn"> ลบ </a></td>

            </tr>

        `;
        $('#clinic_day_off_lists').append(text)
        i++;
    });

    $('body').delegate('.clinic_day_off_lists_del_btn', 'click', function(){
        $(this).parent().parent().remove();
    });

$(function() {

  $('input[name="datefilter"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
  });

  $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
  });

  $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});

</script>
@endsection

