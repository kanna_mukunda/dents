@extends('layouts.app')
@section('title')
{{-- วันที่ {{$dutyTimeofDentists->thaiDate}} --}}
@endsection
@section('a_appointment_index')
    {{-- active --}}
@endsection
@section('navigate')
    <a href="{{url('appointment/index')}}">ข้อมูลคลีนิคประจำวัน</a>
@endsection
@section('style')
<meta name="csrf-token" id="csrf_token" content="{{ csrf_token() }}" />

<style>
    .table td {
        padding: 0.1rem !important
    }

    .list-group-item {
        position: relative;
        display: block;
        padding: .2rem 1.25rem;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .125);
    }

    .profile-user-img,
    .profile-user-img-success,
    .profile-user-img-warning,
    .profile-user-img-cancel,
    .profile-user-img-nextday,
    .profile-user-img-empty,
    .profile-user-img-info {
        border: 3px solid #adb5bd;
        margin: 0 auto;
        padding: 3px;
        width: 80px;
    }

    .profile-user-img-success {
        border: 4px solid green;
    }

    .profile-user-img-info {
        border: 4px solid lightskyblue;
    }

    .profile-user-img-warning {
        border: 4px solid yellow;
    }

    .profile-user-img-cancel {
        border: 4px solid black;
    }

    .profile-user-img-nextday {
        border: 4px solid lightcoral;
    }

    .profile-user-img-empty {
        border: 4px solid #01ff70;
    }

    .room {
        text-align: center;
        /* width: 1%; */
    }
    .table td {
    padding: 0.3rem !important;
}
div.scrollmenu {
  /* background-color: #333; */
  overflow: auto;
  white-space: nowrap;
}

div.scrollmenu .col-md-3 {
  display: inline-block;
}

div.scrollmenu .col-md-3:hover {
  background-color: #007bff;
}
.card-title{
    font-size: 20px;
    font-weight: bold
}

#slotdiv .col-md-4, #slotdiv .col-md-12{
    position: relative;
    width: 100%;
    padding-right: 0px;
    padding-left: 0px;
}
#slotdiv .col-md-4 .btn-sm {
    padding: .25rem .5rem;
    font-size: .875rem;
    line-height: 1.5;
    border-radius: 0;
}

 .hidden {
        display: none
    }

    .show {
        display: block;
    }

    .disabled_slot{
        cursor: not-allowed;
    }
    #aaa .col-md-2,
    #aaa .col-xs-6 {
        /* position: relative; */
        /* min-height: 1px;
        padding-right: 2px;
        padding-left: 2px; */
    }

    .round{
        border-radius:50px;
        padding:15px 25px;
    };
    .prog{
       margin-left:20%;
    }

</style>
@endsection


@section('content')

<div class="card card-primary card-outline">
    <div class="card-header">
        <div class="card-title">
            <span class="info-box-icon bg-primary rounded  elevation-1 p-2"><i class="fas fa-calendar-alt"></i></span>
            ข้อมูลคนไข้ที่นัดวันนี้
        </div>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" href="#summary" data-toggle="tab">สรุปข้อมูลการนัดแยกตามห้องตรวจ</a></li>
            @for ($i = 0; $i < $dutyTimeofDentists->count(); $i++)
                <li class="nav-item">
                    <a class="nav-link" href="#room{{$i}}" data-toggle="tab">
                        {{$dutyTimeofDentists[$i]->room->room_name}}
                    </a>
                </li>
            @endfor
        </ul>
        <br>
        <div class="tab-content">

            <div class="tab-pane active" id="summary">
                <div class="row">
                    @foreach ($dutyTimeofDentists as $dent)
                    <div class="col-md-6">

                        <div class="info-box bg-warning">
                            <span class="info-box-icon bg-warning"><i class="far fa-copy"></i></span>

                            <div class="info-box-content">
                                <h3>:: {{$dent['dentist']->dent_name}}</h3>

                                <h5> {{$dent['room']->room_name}}</h5>
                            </div>
                          </div>


                        <div class="card collapsed-card">
                            <div class="card-header">
                                <h2 class="card-title">ช่วงเช้า 09:00 - 12:00</h2>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse"
                                        data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:40%">เวลา</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $morningCount = 0; ?>
                                        @foreach ($dent['morningSlotChunk_3'] as $morning)
                                        <tr>
                                            <td style="word-spacing :.1em;">
                                                <?php
                                                        $lastHour = $morning[2]['min_l_slot']/60 == 1 ? $morning[2]['hour'] +1 : $morning[2]['hour'];
                                                        $lastMin  = $morning[2]['min_l_slot']/60 == 1 ?  "00" : $morning[2]['min_l_slot'];
                                                        $firstHour = $morning[0]['hour'] < 10 ? '0'.$morning[0]['hour'] : $morning[0]['hour'];
                                                        $firstMin = $morning[0]['min_f_slot'] < 10 ? '0'.$morning[0]['min_f_slot'] : $morning[0]['min_f_slot'];
                                                      ?>
                                                {{$firstHour}}:{{$firstMin}}
                                                -
                                                {{$lastHour}}:{{$lastMin }}
                                            </td>
                                            <td>

                                                <div class="row" id="slotdiv">
                                                    @foreach ($morning as $item)
                                                        <?php
                                                            $rowSpanCount = collect($morning)->filter(function($val, $key){
                                                                if($key == 'chunk'){
                                                                    return $val == [];
                                                                }

                                                            });
                                                        ?>
                                                    @if($rowSpanCount->count() > 0)
                                                        <div class="col-md-12">
                                                            <a class="btn btn-default btn-sm btn-block" href="javascript:void(0)">
                                                                <b>-</b>
                                                                <div>ว่าง</div>
                                                            </a>
                                                        </div>
                                                        <?php break; ?>
                                                    @else
                                                        @if (isset($item['index']))
                                                            <div class="col-md-4">
                                                                @if ($item['attribute']['treatment_name'] != '')
                                                                    <a class="btn {{$item['attribute']['btn_type']}} btn-sm btn-block" href="script:void(0)"
                                                                        style="cursor:not-allowed">
                                                                        {{$item['attribute']['patient_name']}}
                                                                        <hr>
                                                                        <div>{{$item['attribute']['treatment_name']}}</div>
                                                                    </a>
                                                                @else
                                                                    <a class="btn btn-success btn-sm btn-block active_slot" style="color:white"
                                                                            data-dent_id="{{ $item['dent_id']}}"
                                                                            data-hour="{{ $item['hour']}}"
                                                                            data-min="{{$item['min']}}"
                                                                            data-min_f_slot="{{$item['min_f_slot']}}"
                                                                            data-min_l_slot="{{ $item['min_l_slot']}}"
                                                                            data-date = "{{$dutyTimeofDentists[0]['date']}}"
                                                                            data-patient_id = ""
                                                                            data-period_type = "am"
                                                                        {{--  data-treatment_id = "{{$treatment->id}}"
                                                                            data-selected_treatment_name="{{$treatment->treatment_name}}"--}}
                                                                            data-room="{{$dent->room->id}}"
                                                                            data-status={{$item['status']}}
                                                                            data-processing_time="{{$dutyTimeofDentists[0]['processing_time']}}"

                                                                    >ว่าง
                                                                    </a>

                                                                @endif
                                                            </div>
                                                        @endif
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <div class="card collapsed-card">
                            <div class="card-header">
                                <h2 class="card-title">ช่วงบ่าย 13:00 - 16:00</h2>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse"
                                        data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:40%">เวลา</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $morningCount = 0; ?>
                                        @foreach ($dent['eveningSlotChunk_3'] as $ev)
                                        <tr>
                                            <td style="word-spacing :.1em;">
                                                <?php
                                                        $lastHour = $ev[2]['min_l_slot']/60 == 1 ? $ev[2]['hour'] +1 : $ev[2]['hour'];
                                                        $lastMin  = $ev[2]['min_l_slot']/60 == 1 ?  "00" : $ev[2]['min_l_slot'];
                                                        $firstHour = $ev[0]['hour'] < 10 ? '0'.$ev[0]['hour'] : $ev[0]['hour'];
                                                        $firstMin = $ev[0]['min_f_slot'] < 10 ? '0'.$ev[0]['min_f_slot'] : $ev[0]['min_f_slot'];
                                                      ?>
                                                {{$firstHour}}:{{$firstMin}}
                                                -
                                                {{$lastHour}}:{{$lastMin }}
                                            </td>
                                            <td>

                                                <div class="row" id="slotdiv">
                                                    @foreach ($ev as $item)
                                                        <?php
                                                            $rowSpanCount = collect($ev)->filter(function($val, $key){
                                                                if($key == 'chunk'){
                                                                    return $val == [];
                                                                }

                                                            });
                                                        ?>
                                                    @if($rowSpanCount->count() > 0)
                                                        <div class="col-md-12">
                                                            <a class="btn btn-default btn-sm btn-block" href="javascript:void(0)">
                                                                <b>-</b>
                                                                <div>ว่าง</div>
                                                            </a>
                                                        </div>
                                                        <?php break; ?>
                                                    @else
                                                        @if (isset($item['index']))
                                                            <div class="col-md-4">
                                                                @if ($item['attribute']['treatment_name'] != '')
                                                                    <a class="btn {{$item['attribute']['btn_type']}} btn-sm btn-block" href="script:void(0)"
                                                                        style="cursor:not-allowed">
                                                                        {{$item['attribute']['patient_name']}}
                                                                        <hr>
                                                                        <div>{{$item['attribute']['treatment_name']}}</div>
                                                                    </a>
                                                                @else
                                                                    <a class="btn btn-success btn-sm btn-block active_slot" style="color:white"
                                                                            data-dent_id="{{ $item['dent_id']}}"
                                                                            data-hour="{{ $item['hour']}}"
                                                                            data-min="{{$item['min']}}"
                                                                            data-min_f_slot="{{$item['min_f_slot']}}"
                                                                            data-min_l_slot="{{ $item['min_l_slot']}}"
                                                                            data-date = "{{$dutyTimeofDentists[0]['date']}}"
                                                                            data-patient_id = ""
                                                                            data-period_type = "am"
                                                                        {{--  data-treatment_id = "{{$treatment->id}}"
                                                                            data-selected_treatment_name="{{$treatment->treatment_name}}"--}}
                                                                            data-room="{{$dent->room->id}}"
                                                                            data-status={{$item['status']}}
                                                                            data-processing_time="{{$dutyTimeofDentists[0]['processing_time']}}"

                                                                    >ว่าง
                                                                    </a>

                                                                @endif
                                                            </div>
                                                        @endif
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <div class="card collapsed-card">
                            <div class="card-header">
                                <h2 class="card-title">ช่วงค่ำ 16:00 - 20:00</h2>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse"
                                        data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:40%">เวลา</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $morningCount = 0; ?>
                                        @foreach ($dent['nightSlotChunk_3'] as $pm)
                                        <tr>
                                            <td style="word-spacing :.1em;">
                                                <?php
                                                        $lastHour = $pm[2]['min_l_slot']/60 == 1 ? $pm[2]['hour'] +1 : $pm[2]['hour'];
                                                        $lastMin  = $pm[2]['min_l_slot']/60 == 1 ?  "00" : $pm[2]['min_l_slot'];
                                                        $firstHour = $pm[0]['hour'] < 10 ? '0'.$pm[0]['hour'] : $pm[0]['hour'];
                                                        $firstMin = $pm[0]['min_f_slot'] < 10 ? '0'.$pm[0]['min_f_slot'] : $pm[0]['min_f_slot'];
                                                      ?>
                                                {{$firstHour}}:{{$firstMin}}
                                                -
                                                {{$lastHour}}:{{$lastMin }}
                                            </td>
                                            <td>

                                                <div class="row" id="slotdiv">
                                                    @foreach ($pm as $item)
                                                        <?php
                                                            $rowSpanCount = collect($pm)->filter(function($val, $key){
                                                                if($key == 'chunk'){
                                                                    return $val == [];
                                                                }

                                                            });
                                                        ?>
                                                    @if($rowSpanCount->count() > 0)
                                                        <div class="col-md-12">
                                                            <a class="btn btn-default btn-sm btn-block" href="javascript:void(0)">
                                                                <b>-</b>
                                                                <div>ว่าง</div>
                                                            </a>
                                                        </div>
                                                        <?php break; ?>
                                                    @else
                                                        @if (isset($item['index']))
                                                            <div class="col-md-4">
                                                                @if ($item['attribute']['treatment_name'] != '')
                                                                    <a class="btn {{$item['attribute']['btn_type']}} btn-sm btn-block" href="script:void(0)"
                                                                        style="cursor:not-allowed">
                                                                        {{$item['attribute']['patient_name']}}
                                                                        <hr>
                                                                        <div>{{$item['attribute']['treatment_name']}}</div>
                                                                    </a>
                                                                @else
                                                                    <a class="btn btn-success btn-sm btn-block active_slot" style="color:white"
                                                                            data-dent_id="{{ $item['dent_id']}}"
                                                                            data-hour="{{ $item['hour']}}"
                                                                            data-min="{{$item['min']}}"
                                                                            data-min_f_slot="{{$item['min_f_slot']}}"
                                                                            data-min_l_slot="{{ $item['min_l_slot']}}"
                                                                            data-date = "{{$dutyTimeofDentists[0]['date']}}"
                                                                            data-patient_id = ""
                                                                            data-period_type = "am"
                                                                        {{--  data-treatment_id = "{{$treatment->id}}"
                                                                            data-selected_treatment_name="{{$treatment->treatment_name}}"--}}
                                                                            data-room="{{$dent->room->id}}"
                                                                            data-status={{$item['status']}}
                                                                            data-processing_time="{{$dutyTimeofDentists[0]['processing_time']}}"

                                                                    >ว่าง
                                                                    </a>

                                                                @endif
                                                            </div>
                                                        @endif
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>

                        {{-- //////////////////////   ช่วงบ่าย  ///////////////////////// --}}
                        {{-- <div class="card collapsed-card">
                            <div class="card-header">
                                <h2 class="card-title">ช่วงบ่าย 13:00 - 16:00</h2>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse"
                                        data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:40%">เวลา</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($dent['eveningSlotChunk_3'] as $morning)
                                        <tr>
                                            <td style="word-spacing :.1em;">
                                                <php
                                                            $lastHour = $morning[2]['min_l_slot']/60 == 1 ? $morning[2]['hour'] +1 : $morning[2]['hour'];
                                                            $lastMin  = $morning[2]['min_l_slot']/60 == 1 ?  "00" : $morning[2]['min_l_slot'];
                                                            $firstHour = $morning[0]['hour'] < 10 ? '0'.$morning[0]['hour'] : $morning[0]['hour'];
                                                            $firstMin = $morning[0]['min_f_slot'] < 10 ? '0'.$morning[0]['min_f_slot'] : $morning[0]['min_f_slot'];
                                                          ?>
                                                {{$firstHour}}:{{$firstMin}}
                                                -
                                                {{$lastHour}}:{{$lastMin }}
                                            </td>
                                            <td>

                                                <div class="row" id="slotdiv">
                                                    @foreach ($morning as $item)
                                                    <php
                                                            $rowSpanCount = collect($morning)->filter(function($val, $key){
                                                                if($key == 'chunk'){
                                                                    return $val == [];
                                                                }

                                                            });
                                                        ?>
                                                    @if($rowSpanCount->count() > 0)
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default btn-sm btn-block" href="javascript:void(0)"
                                                            style="cursor:not-allowed">
                                                            <b>-</b>
                                                            <div>ว่าง</div>
                                                        </a>
                                                    </div>
                                                    <php break; ?>
                                                    @else

                                                    @if (isset($item['index']))
                                                    <div class="col-md-4">
                                                        <a class="btn {{$item['attribute']['btn_type']}} btn-sm btn-block" href="#">
                                                            {{$item['attribute']['patient_name']}}
                                                            <hr>
                                                            <div>{{$item['attribute']['treatment_name']}}</div>
                                                        </a>
                                                    </div>
                                                    @endif
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div> --}}

                        {{-- //////////////////////   ช่วงค่ำ ///////////////////////// --}}
                        {{-- <div class="card collapsed-card">
                            <div class="card-header">
                                <h2 class="card-title">ช่วงค่ำ 1700 - 20:00</h2>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse"
                                        data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:40%">เวลา</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($dent['nightSlotChunk_3'] as $morning)
                                        <tr>
                                            <td style="word-spacing :.1em;">
                                                <php
                                                            $lastHour = $morning[2]['min_l_slot']/60 == 1 ? $morning[2]['hour'] +1 : $morning[2]['hour'];
                                                            $lastMin  = $morning[2]['min_l_slot']/60 == 1 ?  "00" : $morning[2]['min_l_slot'];
                                                            $firstHour = $morning[0]['hour'] < 10 ? '0'.$morning[0]['hour'] : $morning[0]['hour'];
                                                            $firstMin = $morning[0]['min_f_slot'] < 10 ? '0'.$morning[0]['min_f_slot'] : $morning[0]['min_f_slot'];
                                                          ?>
                                                {{$firstHour}}:{{$firstMin}}
                                                -
                                                {{$lastHour}}:{{$lastMin }}
                                            </td>
                                            <td>

                                                <div class="row" id="slotdiv">
                                                    @foreach ($morning as $item)
                                                    <php
                                                            $rowSpanCount = collect($morning)->filter(function($val, $key){
                                                                if($key == 'chunk'){
                                                                    return $val == [];
                                                                }

                                                            });
                                                        ?>
                                                    @if($rowSpanCount->count() > 0)
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default btn-sm btn-block" href="javascript:void(0)"
                                                            style="cursor:not-allowed">
                                                            <b>-</b>
                                                            <div>ว่าง</div>
                                                        </a>
                                                    </div>
                                                    <php break; ?>
                                                    @else

                                                    @if (isset($item['index']))
                                                    <div class="col-md-4">
                                                        <a class="btn {{$item['attribute']['btn_type']}} btn-sm btn-block" href="#">
                                                            {{$item['attribute']['patient_name']}}
                                                            <hr>
                                                            <div>{{$item['attribute']['treatment_name']}}</div>
                                                        </a>
                                                    </div>
                                                    @endif
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div> --}}
                    </div>
                    @endforeach

                </div>

            </div>

            {{-- rooom1 --}}
            @foreach ($dutyTimeofDentists as $key => $item)
            <div class="tab-pane" id="room{{$key}}">
                {{-- <h3>ข้อมูลตารางนัดคนไข้ ห้องที่ 1</h3> --}}
                <div class="card card-warning card-outline mb-4">
                    <div class="card-header">
                        <h3 class="card-title">
                            หมอเข้าเวรประจำ {{$item->room->room_name}}
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="col-md-3">
                            <div class="card card-primary card-outline">
                                <div class="card-body box-profile">
                                    <div class="text-center">
                                        <img class="profile-user-img img-fluid img-circle"
                                            src="{{asset('adminlte/dist/img/default-profile.jpg')}}" alt="User profile picture">
                                    </div>

                                    <h3 class="profile-username text-center">{{$item->dentist->dent_name}}</h3>

                                    <p class="text-muted text-center"></p>

                                    <ul class="list-group list-group-unbordered mb-3">
                                        {{-- @foreach ($item->job_grouped as $job)
                                            <li class="list-group-item">
                                                <b>{{$job[0]->treatment->treatment_name}}</b> <a class="float-right">{{$job['count']}} case</a>
                                            </li>

                                        @endforeach --}}

                                    </ul>

                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>

                    </div>
                    <!-- /.card-body -->
                </div>
                <div class="card card-outline card-warning">
                    <div class="card-header">
                        <div class="card-title">คนไข้ที่ทำการรักษา</div>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped projects">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 1%">
                                        #
                                    </th>
                                    <th class="text-center" style="width: 10%">เวลา</th>
                                    <th class="text-center">ชื่อคนไข้</th>
                                    <th class="text-center">เบอร์ติดต่อ</th>
                                    <th class="text-center">อาการ</th>
                                    <th class="text-center">ผู้รับผิดชอบ</th>
                                    <th class="text-center">วันที่ทำรายการ</th>
                                    <th class="text-center">สถานะ</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $c = 1; ?>
                                @foreach ($item->infoByRoom as  $item)

                                <tr>

                                    <td class="text-center" style="width: 1%">
                                        {{$c++}}
                                    </td>
                                    <td class="text-center" style="width: 10%">{{$item['firstHourMin'] ."-". $item['lastHourMin']}}</td>
                                    <td class="text-center">{{$item['patient_name']}}</td>
                                    <td class="text-center">{{$item['patient_phone']}}</td>
                                    <td class="text-center">{{$item['treatment']}}</td>
                                    <td class="text-center">{{$item['dent_name']}}</td>
                                    <td class="text-center">{{$item['created_at']}}</td>
                                    <td class="text-center">{{$item['status']}}</td>
                                    <td>
                                        <a href="{{url('appointment/edit/'.$item['job_id'])}}" class="btn btn-warning">แก้ไข</a>
                                        <a href="{{url('appointment/destroy/'.$item['job_id'])}}" class="btn btn-danger">ลบ</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endforeach


        </div><!--tab-content-->

    </div>

</div>


<div class="modal hidden" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">ยืนยันการนัดหมอ</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-grop  row mb-1">
                    <label class="col-sm-5 form-label">วันที่</label>
                    <input type="text" class="col-sm-7 form-control" value="" id="date_selected"
                        readonly="readonly">
                </div>
                <div class="form-grop  row mb-1">
                    <label class="col-sm-5 form-label">หมอ</label>
                    <input type="text" class="col-sm-7 form-control" value="{{ $dutyTimeofDentists[0]['dentist']->dent_name }}" id="date_selected"
                        readonly="readonly">
                </div>

                <div class="form-grop  row mb-1">
                    <label class="col-sm-5 form-label">ห้องตรวจ</label>
                    <input type="text" class="col-sm-7 form-control" value="" id="room"
                        readonly="readonly">
                </div>
                <div class="form-grop  row mb-1">
                    <label class="col-sm-5 form-label">เวลา</label>
                    <input type="text" class="col-sm-7 form-control" value="" id="datetime"  readonly="readonly">
                </div>

                <div class="form-grop  row mb-1">
                    <label class="col-sm-5 form-label">ชื่อคนไข้</label>
                    <select class="col-sm-7 col-md-7 form-control select2 " name="patient_name" id="patient_name" style="width: 400px; height:100px">
                        <option>เลือก...</option>
                        @foreach($patients as $key => $patient)
                            <option value="{{$patient->hn}}"> {{ $patient->f_name.' '.$patient->l_name }}</option>
                        @endforeach
                    </select>

                </div>

                <div class="form-grop  row mb-1">
                    <label class="col-sm-5 form-label">ต้องการรักษา</label>
                    <select class="col-sm-7 form-control select2" name="selected_treatment_name" id="selected_treatment_name" style="width: 400px; height:100px">
                        <option>เลือก...</option>
                        @foreach($treatments as $key => $treatment)
                            <option value="{{$treatment->id}}"> {{ $treatment->treatment_name }}</option>
                        @endforeach
                    </select>
                </div>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success confirm_btn">บันทึก</button>
            </div>

        </div>
    </div>
</div>

@endsection


    @section('script')
    <script>
        let index;
        let hour;
        let min;
        let date;
        let room;
        let dent_id;
        let treatment_id;
        let processing_time;
        let period_type;
        $('.active_slot').click(function (event) {
            event.preventDefault();
            // index = $(this).data('index')
            let status = $(this).data('status')
            hour = $(this).data('hour')
            min = $(this).data('min')
            date = $(this).data('date')
            room = $(this).data('room')
            dent_id = $(this).data('dent_id')
            period_type  = $(this).data('period_type')
            // processing_time = $(this).data('processing_time')
            let selected_treatment_name = $(this).data('selected_treatment_name')
            $('#date_selected').val(date)
            $('#selected_treatment_name').val(selected_treatment_name)
            $('#datetime').val(hour + ":" + min + " น.")
            $('#room').val(room)
            showHiddenModal()

        });

        $('.confirm_btn').click(function (event) {
            event.preventDefault();
            showHiddenModal()
            treatment_id = parseInt($('#selected_treatment_name option:selected').val())
            let patient_hn = parseInt($('#patient_name option:selected').val())
            let _token = $('#csrf_token').attr('content')

            let params = {
                hour: hour,
                min: min,
                date: date,
                room: room,
                dent_id: dent_id,
                treatment_id: treatment_id,
                patient_hn: patient_hn,
                period_type: period_type,
                // index: index,
                _token: _token

            };
            console.log('params',params)
            $.post('../appoint_by_staff_store', params)
                .done(function (data) {
                    console.log('data',data)
                    window.location = '../appointment_result/' + data.id
                })
        })

        function showHiddenModal() {
            if ($('#myModal').hasClass('hidden')) {
                $('#myModal').removeClass('hidden')
                $('#myModal').addClass('show')
            } else {
                $('#myModal').removeClass('show')
                $('#myModal').addClass('hidden')
            }
        }

        $('.close').click(function () {
            showHiddenModal()
        })

    </script>




@endsection
