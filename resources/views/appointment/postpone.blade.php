@extends ('layouts.app')
@section('title')
ขอเลื่อนนัด
@endsection
<!-- CSS only -->
{{-- <<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

 @section('style')

@endsection



@section('content')

<div class="card">

    <div class="card-body">
    
        <div class="row">
            <div class="col-md-3">
                <div class="box box-primary">

                  <div class="card bg-warning">
                    <div class="card-header">
                      <h3 class="card-title">Warning</h3>
      
                      <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                          <i class="fas fa-minus"></i>
                        </button>
                      </div>
                      <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <form role="form">
                        <div class="box-body">
                            <h6>เลขประจำตัวประชาชน</h6>
                            <br>
                            <form action="#" method="get">
                                <div class="input-group">
                                    <input type="text" name="q" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </form>

                        </div>
                        <br>
                        <!-- /.box-body -->

                    </form>
                    </div>
                    <!-- /.card-body -->
                  </div>
                    
                </div>
            </div>


            <div class="col-md-9"> 
              <div class="row">
                <div class="col-md-6">

                  <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                      <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" alt="User profile picture">
                      </div>
      
                      <h3 class="profile-username text-center">มณีนุช อมรพิสุทธ์</h3>
      
                      <p class="text-muted text-center">Hn: 4009730</p>
      
                      <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                          <b>วันที่นัด</b> <a class="float-right">10 พค. 2563</a>
                        </li>
                        <li class="list-group-item">
                          <b>รายการรักษา</b> <a class="float-right">ถอนฟัน</a>
                        </li>
                        <li class="list-group-item">
                          <b>หมอเจ้าของไข้</b> <a class="float-right">หมอม่อน</a>
                        </li>
                      </ul>
      
                    </div>
                    <!-- /.card-body -->
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title">เลื่อนนัด</h3>
      
                      <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                          <i class="fas fa-minus"></i>
                        </button>
                      </div>
                      <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <form role="form">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">เลือกวันนัด</label>
                                <input type="text" class="form-control datepicker" id="exampleInputEmail1"
                                    placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">เวลานัด</label>
                                <input type="text" class="form-control timepicker" id="exampleInputPassword1"
                                    placeholder="">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                    </div>
                    <!-- /.card-body -->
                  </div>

                </div>
              </div>

            </div>
        </div>

    </div>

</div>






@endsection

@section('script')
    
<!-- JavaScript Bundle with Popper -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="https://cdn.jsdelivr.net/npm/timepicker@1.13.16/jquery.timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-datepicker@1.9.0/dist/locales/bootstrap-datepicker.ar.min.js"></script>
<script>

$('.datepicker').datepicker({
  langauge: 'th-th'
})

$('.timepicker').timepicker({
    timeFormat: 'h:mm p',
    interval: 15,
    minTime: '10',
    maxTime: '8:00pm',
    // defaultTime: '11',
    startTime: '10:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true,
    langauge: 'th-th'
});
</script>
@endsection