@extends('layouts.app')
@section('style')
<style>

</style>
@endsection

@section('active_appointByStaff')
    active
@endsection
@section('navigate')
    <a href="{{url('appointment/appointment_by_staff')}}">นัดทำฟัน</a>
@endsection
@section('title')
สรุปใบนัดทำฟัน คนไข้ Hn: {{$appointmentInfos->patient->hn}}
@endsection
@section('content')
<div class="cord">
    <div class="card card-body">
        <div class="row" >
            <div class="col-md-4"></div>
            <div class="col-md-4 " id="printDiv">

                <!-- Profile Image -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">ใบนัดทำฟัน</h3>
                    </div>
                    <div class="card-body box-profile">
                            <h4>
                                <i class="fas fa-globe"></i> คลินิกกลางเมืองทันตกรรม
                            </h4>
                            <address>
                                <strong>คลินิกกลางเมืองทันตกรรม </strong><br>
                                62/215 ถ.กลางเมือง อ.เมือง ต.ในเมือง เทศบาลนครขอนแก่น 40000<br>
                            </address>
                        <hr>
                        <div class="row pb-2">
                            <div class="col-md-4">
                                <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/default-profile.jpg')}}"
                                    alt="User profile picture">
                            </div>
                            <div class="col-md-8 mt-2">
                                <h3 class="profile-username text-center">
                                    {{$appointmentInfos->patient->f_name.' '.$appointmentInfos->patient->l_name}}
                                </h3>

                                <p class="text-muted text-center">คนไข้ Hn: {{$appointmentInfos->patient->hn}}</p>
                            </div>
                        </div>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>นัดทำฟัน</b> <a class="float-right">{{$appointmentInfos->treatment->treatment_name}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>หมอเจ้าของ Case</b> <a class="float-right">{{$appointmentInfos->dentist->dent_name}} </a>
                            </li>
                            <li class="list-group-item">
                                <b>วันที่นัด</b> <a class="float-right">{{$appointmentInfos->date}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>เวลานัด</b> <a class="float-right">{{$appointmentInfos->start_time}} น.</a>
                            </li>
                        </ul>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="col-md-4">
                <a href="javascript:void(0)" class="btn btn-outline-primary" id="print_btn">ปริ้นใบนัดทำฟัน</a>
            </div>

        </div>
    </div>
</div>
@endsection


@section('script')
    <script>
        $('#print_btn').click(function() {
            var headstr = "<html><head><title>Booking Details</title></head><body>";
            var footstr = "</body>";
            var newstr = document.getElementById('printDiv').innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            return false;
        })

        </script>

@endsection
