@extends('layouts.app')
@section('title')
ตารางนัดประจำวัน
@endsection
@section('a_appointment_index')
active
@endsection
@section('navigate')
<a href="{{url('appointment/index')}}">ข้อมูลคลีนิคประจำวัน</a>
@endsection
@section('style')

<style>
    .table td {
        padding: 0.1rem !important
    }

    .list-group-item {
        position: relative;
        display: block;
        padding: .2rem 1.25rem;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .125);
    }

    .profile-user-img,
    .profile-user-img-success,
    .profile-user-img-warning,
    .profile-user-img-cancel,
    .profile-user-img-nextday,
    .profile-user-img-empty,
    .profile-user-img-info {
        border: 3px solid #adb5bd;
        margin: 0 auto;
        padding: 3px;
        width: 80px;
    }

    .profile-user-img-success {
        border: 4px solid green;
    }

    .profile-user-img-info {
        border: 4px solid lightskyblue;
    }

    .profile-user-img-warning {
        border: 4px solid yellow;
    }

    .profile-user-img-cancel {
        border: 4px solid black;
    }

    .profile-user-img-nextday {
        border: 4px solid lightcoral;
    }

    .profile-user-img-empty {
        border: 4px solid #01ff70;
    }

    .room {
        text-align: center;
        /* width: 1%; */
    }

    .table td {
        padding: 0.3rem !important;
    }

    div.scrollmenu {
        /* background-color: #333; */
        overflow: auto;
        white-space: nowrap;
    }

    div.scrollmenu .col-md-3 {
        display: inline-block;
    }

    div.scrollmenu .col-md-3:hover {
        background-color: #007bff;
    }

    .card-title {
        font-size: 20px;
        font-weight: bold
    }

    #slotdiv .col-md-4,
    #slotdiv .col-md-12 {
        position: relative;
        width: 100%;
        padding-right: 0px;
        padding-left: 0px;
    }

    #slotdiv .col-md-4 .btn-sm {
        padding: .25rem .5rem;
        font-size: .875rem;
        line-height: 1.5;
        border-radius: 0;
    }

    .danger {
        background-color: #f2dede;
    }

    .info {
        background-color: #d9edf7
    }

</style>
@endsection


@section('content')
@if(collect($dutyTimeofDentists)->isEmpty())
<div class="card">
    <div class="card-header">
        <h5>วันที่ {{date('d')}} พฤศจิกายน 2563</h5>
    </div>
    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th>หมอประจำวันนี้: 4 คน</th>
                    @foreach ($treatments as $treatment)
                    <th> {{ $treatment->treatment_name }} </th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                <tr class="info">
                    <td>หมอโย</td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                </tr>
                <tr class="danger">
                    <td>หมอม่อน</td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                </tr>
                <tr class="info">
                    <td>หมอเอ</td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-default"><i class="fas fa-minus-square"></i></td>
                    <td class="text-center text-default"><i class="fas fa-minus-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                </tr>
                <tr class="danger">
                    <td>หมอแทน</td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-green"><i class="fas fa-check-square"></i></td>
                    <td class="text-center text-default"><i class="fas fa-minus-square"></i></td>
                    <td class="text-center text-default"><i class="fas fa-minus-square"></i></td>
                    <td class="text-center text-default"><i class="fas fa-minus-square"></i></td>
                    <td class="text-center text-default"><i class="fas fa-minus-square"></i></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="card-title">
            <h3>คนไข้นัดหมายวันนี้: 86 คน
        </div>
        <div class="card-tools">

            <small class="pull-right badge  ml-2 bg-gray">ปรับ </small>
            <small class="pull-right badge  ml-2 bg-warning">ขูดหินปูน </small>
            <small class="pull-right badge  ml-2 bg-orange">ถอนฟัน </small>
            <small class="pull-right badge  ml-2 bg-danger">อุดฟัน </small>
            <small class="pull-right badge  ml-2 bg-info">จัดฟัน </small>
            <small class="pull-right badge  ml-2 bg-purple">รักษารากฟัน </small>
            <small class="pull-right badge  ml-2 bg-navy">ฟันปลอม </small>
            <small class="pull-right badge  ml-2 bg-green">ทำครอบฟัน</small>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered datatable">
            <thead>
                <tr>
                    <th rowspan="2" class="text-center" width="10">#</th>
                    <th rowspan="2" class="text-center" width="160"> เวลา</th>
                    <th colspan="4" class="text-center color-palette">ห้อง</th>
                </tr>
                <tr>
                    <th class="text-center  color-palette ">
                       <a href="{{url('appointment/byroom/1')}}" class="btn btn-default">1</a>
                    </th>
                    <th class="text-center  color-palette">
                       <a href="{{url('appointment/byroom/2')}}" class="btn btn-default">2</a>
                    </th>
                    <th class="text-center  color-palette">
                       <a href="{{url('appointment/byroom/3')}}" class="btn btn-default">3</a>
                    </th>
                    <th class="text-center  color-palette">
                       <a href="{{url('appointment/byroom/4')}}" class="btn btn-default">4</a>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php $c = 1; $cc=0; ?>
                <?php 
                    $array1 = [
                        [//ช่วงเวลาที่ 0900
                            [//0900-0915
                                [//ห่องที่ 1
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'danger', 'patient'=> $patients[0]['f_name'].' '.$patients[0]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[2]['f_name'].' '.$patients[2]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[3]['f_name'].' '.$patients[3]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[4]['f_name'].' '.$patients[4]['l_name']],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> 'default', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'default', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'inactive'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'default', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'default', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'purple', 'patient'=> $patients[5]['f_name'].' '.$patients[5]['l_name']],
                                    'status' => 'active'
                                ]

                            ],//end /0900-0915
                            [//0900-0915
                                [//ห่องที่ 1
                                ['treat'=> 'ขูดหินปูน', 'bg'=> 'danger', 'patient'=> $patients[6]['f_name'].' '.$patients[6]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'navy', 'patient'=> $patients[7]['f_name'].' '.$patients[7]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'info', 'patient'=> $patients[10]['f_name'].' '.$patients[10]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=>  $patients[9]['f_name'].' '.$patients[9]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'orange', 'patient'=> $patients[11]['f_name'].' '.$patients[11]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[12]['f_name'].' '.$patients[12]['l_name']],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'purple', 'patient'=> $patients[13]['f_name'].' '.$patients[13]['l_name']],
                                    'status' => 'active'
                                ]

                            ],//end /0900-0915
                            [//0900-0915
                                [//ห่องที่ 1
                                ['treat'=> 'ขูดหินปูน', 'bg'=> 'orange', 'patient'=> $patients[14]['f_name'].' '.$patients[14]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[15]['f_name'].' '.$patients[15]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'inactive'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> '', 'bg'=> 'info', 'patient'=> $patients[16]['f_name'].' '.$patients[17]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'purple', 'patient'=> $patients[18]['f_name'].' '.$patients[18]['l_name']],
                                    'status' => 'active'
                                ]

                            ],//end /0900-0915
                            [//0900-0915
                                [//ห่องที่ 1
                                ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[19]['f_name'].' '.$patients[19]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[20]['f_name'].' '.$patients[20]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'purple', 'patient'=> $patients[30]['f_name'].' '.$patients[30]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'info', 'patient'=> $patients[21]['f_name'].' '.$patients[21]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=>$patients[22]['f_name'].' '.$patients[22]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'warning', 'patient'=> $patients[23]['f_name'].' '.$patients[23]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[24]['f_name'].' '.$patients[24]['l_name']],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'purple', 'patient'=> $patients[25]['f_name'].' '.$patients[25]['l_name']],
                                    'status' => 'active'
                                ]

                            ],//end /0900-0915
                        ],//end ช่วงเวลาที่ 0900
                       [//ช่วงเวลาที่ 0900
                            [//0900-0915
                                [//ห่องที่ 1
                                ['treat'=> 'ขูดหินปูน', 'bg'=> 'danger', 'patient'=> $patients[26]['f_name'].' '.$patients[26]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[27]['f_name'].' '.$patients[27]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'navy', 'patient'=> $patients[28]['f_name'].' '.$patients[28]['l_name']],
                                    ['treat'=> '', 'bg'=> 'gray', 'patient'=> $patients[29]['f_name'].' '.$patients[29]['l_name']],
                                    ['treat'=> '', 'bg'=> 'gray', 'patient'=> $patients[30]['f_name'].' '.$patients[30]['l_name']],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'inactive'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'inactive'
                                ]

                            ],//end /0900-0915
                            [//0900-0915
                                [//ห่องที่ 1
                                ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'inactive'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'danger', 'patient'=> $patients[31]['f_name'].' '.$patients[31]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'warning', 'patient'=> $patients[32]['f_name'].' '.$patients[32]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[33]['f_name'].' '.$patients[33]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[34]['f_name'].' '.$patients[34]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[35]['f_name'].' '.$patients[35]['l_name']],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'gray', 'patient'=> $patients[36]['f_name'].' '.$patients[36]['l_name']],
                                    'status' => 'active'
                                ]

                            ],//end /0900-0915
                            [//0900-0915
                                [//ห่องที่ 1
                                ['treat'=> 'ขูดหินปูน', 'bg'=> 'warning', 'patient'=> $patients[37]['f_name'].' '.$patients[37]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'info', 'patient'=> $patients[38]['f_name'].' '.$patients[38]['l_name']],
                                    ['treat'=> 'danger', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[39]['f_name'].' '.$patients[39]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'info', 'patient'=> $patients[40]['f_name'].' '.$patients[40]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[41]['f_name'].' '.$patients[41]['l_name']],
                                    ['treat'=> '', 'bg'=> 'info', 'patient'=> $patients[42]['f_name'].' '.$patients[42]['l_name']],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'warning', 'patient'=> $patients[42]['f_name'].' '.$patients[42]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[43]['f_name'].' '.$patients[43]['l_name']],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'purple', 'patient'=> $patients[44]['f_name'].' '.$patients[44]['l_name']],
                                    'status' => 'active'
                                ]

                            ],//end /0900-0915
                            [//0900-0915
                                [//ห่องที่ 1
                                ['treat'=> 'ขูดหินปูน', 'bg'=> 'warning', 'patient'=> $patients[45]['f_name'].' '.$patients[45]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'green', 'patient'=> $patients[46]['f_name'].' '.$patients[46]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 2
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'navy', 'patient'=> $patients[47]['f_name'].' '.$patients[47]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 3
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'warning', 'patient'=> $patients[48]['f_name'].' '.$patients[48]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'warning', 'patient'=> $patients[49]['f_name'].' '.$patients[49]['l_name']],
                                    ['treat'=> '', 'bg'=> 'default', 'patient'=> ''],
                                    'status' => 'active'
                                ],
                                [//ห่องที่ 4
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'gray', 'patient'=> $patients[50]['f_name'].' '.$patients[50]['l_name']],
                                    ['treat'=> 'ขูดหินปูน', 'bg'=> 'info', 'patient'=> $patients[51]['f_name'].' '.$patients[51]['l_name']],
                                    ['treat'=> 'รักษารากฟัน', 'bg'=> 'warning', 'patient'=> $patients[52]['f_name'].' '.$patients[52]['l_name']],
                                    'status' => 'active'
                                ]

                            ],//end /0900-0915
                        ],//end ช่วงเวลาที่ 0900
                    ];
                //    dd($array1[$cc][$j][0]['status']);
                ?>
                @for ($i = 9; $i < 11; $i++) 
                    <?php $add15 = 15; ?> 
                    @for ($j=0; $j < 4; $j++)
                     <tr>
                        <td class="text-center">{{ $c++ }}</td>
                        <td class="text-center h5">
                            {{ $i <10 ? '0'.$i : $i }}.{{ $j * 15 > 0 ? $j * 15 : '0'.$j * 15}}
                            -
                            <?php 
                                        $minuteTo = ($j * 15)+$add15;
                                        $hrTo = $minuteTo  == 60 ? $i + 1 : $i;
                                        echo $hrTo < 10 ? '0'.$hrTo : $hrTo;
                                        echo ".";
                                        echo $minuteTo == 60 ? '00' : $minuteTo;
                                    ?>

                        </td>

                        <td class="text-center">
                            <div class="color-palette-set">
                                @if ($array1[$cc][$j][0]['status'] == 'active')

                                <div class="bg-{{ $array1[$cc][$j][0][0]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][0][0]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][0][1]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][0][1]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][0][2]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][0][2]['patient'] }}</span></div>
                                @else
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                <div class="bg-default color-palette mb-1"><span>ว่าง</span></div>
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                @endif
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="color-palette-set">
                                @if ($array1[$cc][$j][1]['status'] == 'active')

                                <div class="bg-{{ $array1[$cc][$j][1][0]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][1][0]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][1][1]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][1][1]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][1][2]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][1][2]['patient'] }}</span></div>
                                @else
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                <div class="bg-default color-palette mb-1"><span>ว่าง</span></div>
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                @endif
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="color-palette-set">
                                @if ($array1[$cc][$j][2]['status'] == 'active')

                                <div class="bg-{{ $array1[$cc][$j][2][0]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][2][0]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][2][1]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][2][1]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][2][2]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][2][2]['patient'] }}</span></div>
                                @else
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                <div class="bg-default color-palette mb-1"><span>ว่าง</span></div>
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                @endif
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="color-palette-set">
                                @if ($array1[$cc][$j][3]['status'] == 'active')

                                <div class="bg-{{ $array1[$cc][$j][3][0]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][3][0]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][3][1]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][3][1]['patient'] }}</span></div>
                                <div class="bg-{{ $array1[$cc][$j][3][2]['bg'] }} color-palette mb-1">
                                    <span>{{ $array1[$cc][$j][3][2]['patient'] }}</span></div>
                                @else
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                <div class="bg-default color-palette mb-1"><span>ว่าง</span></div>
                                <div class="bg-default color-palette mb-1"><span>&nbsp;</span></div>
                                @endif
                            </div>
                        </td>
                    </tr>
                    @endfor
                    <?php $cc++;?>

                    @endfor
                    {{-- <tr>    
                        <td class="text-center">45</td>
                        <td class="text-center">20:00</td>
                        <td class="text-center">
                            <div class="color-palette-set">
                                <div class="bg-teal color-palette mb-1"><span>Disabled</span></div>
                                <div class="bg-teal color-palette mb-1"><span>#39CCCC</span></div>
                                <div class="bg-teal color-palette mb-1"><span>Active</span></div>
                            </div>
                        </td>
                        <td class="text-center">ว่าง</td>
                        <td class="text-center">
                            <div class="color-palette-set">
                                <div class="bg-teal color-palette mb-1"><span>Disabled</span></div>
                                <div class="bg-teal color-palette mb-1"><span>#39CCCC</span></div>
                                <div class="bg-teal color-palette mb-1"><span>Active</span></div>
                            </div>
                        </td>
                        <td class="text-center">ว่าง</td>
                    </tr> --}}
            </tbody>
        </table>

    </div>
</div>
{{-- แสดงตารางคนไข้นัดหมายแยกตามห้องและช่วงเวลา --}}

@else
วันที่ {{$dutyTimeofDentists->thaiDate}}

{{-- จำนวน​ Case --}}
<div class="card card-primary card-outline mb-4 collapsed-card">
    <div class="card-header">
        <div class="card-title">
            <span class="info-box-icon bg-primary rounded elevation-1 p-2"><i class="fas fa-tooth"></i></span>

            จำนวน Case
        </div>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            @foreach ($treatments as $item)
            <div class="col-12 col-sm-6 col-md-4">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-cog"></i></span>

                    <div class="info-box-content">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a href="#" class="nav-link h5">
                                    {{$item->treatment_name}} <span class="float-right  h5">{{$item->count}}</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            @endforeach

            <!-- /.col -->
        </div>
    </div>
</div>
{{-- ทัตแพทย์เวร --}}
<div class="card card-primary card-outline mb-4 collapsed-card">
    <div class="card-header">
        <div class="card-title">
            <span class="info-box-icon bg-primary rounded elevation-1 p-2"><i class="fas fa-user-md"></i></span>
            หมอเข้าเวร
        </div>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div><!-- /.card-header -->
    <div class="card-body">
        <div class="tab-content">
            <div class="row">
                <div class="col-md-3 offset-md-9 col-sm-6 col-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="fas fa-user-md"></i></span>

                        <div class="info-box-content">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a href="#" class="nav-link h6">
                                        หมอเข้าเวร <span class="float-right">{{$dutyTimeofDentists->count()}} คน</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>

            <div class="scrollmenu">
                {{-- {{dd($dutyTimeofDentists[0]->dentist->dent_name)}} --}}
                @foreach ($dutyTimeofDentists as $item)
                {{-- {{dd($item)}} --}}
                <div class="col-md-3">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle"
                                    src="{{asset('adminlte/dist/img/default-profile.jpg')}}" alt="User profile picture">
                            </div>

                            <h3 class="profile-username text-center">{{$item->dentist->dent_name}}</h3>

                            <p class="text-muted text-center"></p>

                            {{-- <ul class="list-group list-group-unbordered mb-3">
                                    @foreach ($item->job_grouped as $job)
                                        <li class="list-group-item">
                                            <b>{{$job[0]->treatment->treatment_name}}</b> <a
                                class="float-right">{{$job['count']}} case</a>
                            </li>

                            @endforeach

                            </ul> --}}

                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="card card-primary card-outline">
    <div class="card-header">
        <div class="card-title">
            <span class="info-box-icon bg-primary rounded  elevation-1 p-2"><i class="fas fa-calendar-alt"></i></span>
            ข้อมูลคนไข้ที่นัดวันนี้
        </div>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" href="#appointment-table"
                    data-toggle="tab">สรุปตารางนัดคนไข้</a></li>

            <li class="nav-item"><a class="nav-link" href="#summary"
                    data-toggle="tab">สรุปข้อมูลการนัดแยกตามห้องตรวจ</a></li>
            @for ($i = 0; $i < $dutyTimeofDentists->count(); $i++)
                <li class="nav-item">
                    <a class="nav-link" href="#room{{$i}}" data-toggle="tab">
                        {{$dutyTimeofDentists[$i]->room->room_name}}
                    </a>
                </li>
                @endfor
        </ul>
        <br>
        <div class="tab-content">
            <div class="active tab-pane" id="appointment-table">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <div class="card-title">
                            <span class="info-box-icon bg-primary rounded  elevation-1 p-2"><i
                                    class="fab fa-bitcoin"></i></span>
                            ตารางคำนวณค่ารักษา
                        </div>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive p-0">
                            <table class="table table-hover text-nowrap datatable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        {{-- <th>Invoice No.</th> --}}
                                        <th>วันที่</th>
                                        <th>HN</th>
                                        <th>ขื่อ - สกุล คนไข้</th>
                                        <th>การรักษา</th>
                                        <th>ชื่อ - สกุลหมอ</th>
                                        {{-- <th>เวลา</th> --}}
                                        <th>สถานะ</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $c = 1; ?>
                                    @foreach ($jobs as $item)
                                    <tr>
                                        <td>{{$c++}}</td>
                                        {{-- <td>{{'inv-'.$item->date.$item->patient_id.$item->treatment_id}}</td> --}}
                                        <?php
                        $arr = explode('-', $item->date);
                        $date = $arr[2].'-'.$arr[1].'-'.($arr[0]+543)
                    ?>
                                        <td>{{$date}}</td>
                                        <td>{{$item->patient->hn}}</td>
                                        <td>{{$item->patient->f_name}} {{$item->patient->l_name}}</td>
                                        <td>{{$item->treatment->treatment_name}}</td>
                                        <td>{{$item->dentist->dent_name}}</td>
                                        {{-- <td>{{$item->convertSlotToTime($item->slot_start, $item->slot_end, $item->period_type)}}
                                        </td> --}}
                                        <td class="text-center">
                                            <span
                                                class="btn {{$item->status == 'complete' ? 'btn-outline-success' :'btn-outline-danger'}} ">
                                                {{$item->changeJobStatus($item->status)}}
                                            </span>
                                        </td>
                                        <td>

                                            <a href="{{url('appointment/view/'.$item->patient->hn)}}"
                                                class="btn btn-warning ">ดูข้อมูล</a>

                                        </td>
                                    </tr>

                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="summary">
                <div class="row">
                    @foreach ($dutyTimeofDentists as $dent)
                    <div class="col-md-4">

                        <div class="info-box bg-warning">
                            <span class="info-box-icon bg-warning"><i class="far fa-copy"></i></span>

                            <div class="info-box-content">
                                <h3> {{$dent['room']->room_name}}</h3>
                            </div>
                        </div>


                        <div class="card collapsed-card">
                            <div class="card-header">
                                <h2 class="card-title">ช่วงเช้า 09:00 - 12:00</h2>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse"
                                        data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:40%">เวลา</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($dent['morningSlotChunk_3'] as $morning)
                                        <tr>
                                            <td style="word-spacing :.1em;">
                                                <?php
                                                        $lastHour = $morning[2]['min_l_slot']/60 == 1 ? $morning[2]['hour'] +1 : $morning[2]['hour'];
                                                        $lastMin  = $morning[2]['min_l_slot']/60 == 1 ?  "00" : $morning[2]['min_l_slot'];
                                                        $firstHour = $morning[0]['hour'] < 10 ? '0'.$morning[0]['hour'] : $morning[0]['hour'];
                                                        $firstMin = $morning[0]['min_f_slot'] < 10 ? '0'.$morning[0]['min_f_slot'] : $morning[0]['min_f_slot'];
                                                      ?>
                                                {{$firstHour}}:{{$firstMin}}
                                                -
                                                {{$lastHour}}:{{$lastMin }}
                                            </td>
                                            <td>

                                                <div class="row" id="slotdiv">
                                                    @foreach ($morning as $item)
                                                    <?php
                                                        $rowSpanCount = collect($morning)->filter(function($val, $key){
                                                            if($key == 'chunk'){
                                                                return $val == [];
                                                            }

                                                        });
                                                    ?>
                                                    @if($rowSpanCount->count() > 0)
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default btn-sm btn-block"
                                                            href="javascript:void(0)">
                                                            <b>-</b>
                                                            <div>ว่าง</div>
                                                        </a>
                                                    </div>
                                                    <?php break; ?>
                                                    @else

                                                    @if (isset($item['index']))
                                                    <div class="col-md-4">
                                                        @if ($item['attribute']['treatment_name'] != '')
                                                        <a class="btn {{$item['attribute']['btn_type']}} btn-sm btn-block"
                                                            href="script:void(0)" style="cursor:not-allowed">
                                                            <b>{{$item['attribute']['patient_name']}}</b>
                                                            <div>{{$item['attribute']['treatment_name']}}</div>
                                                        </a>
                                                        @else
                                                        <a class="btn {{$item['attribute']['btn_type']}} btn-sm btn-block"
                                                            href="#">
                                                            <b>-</b>
                                                            <div>ว่าง</div>
                                                        </a>
                                                        @endif
                                                    </div>
                                                    @endif
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>

                        {{-- //////////////////////   ช่วงบ่าย  ///////////////////////// --}}
                        <div class="card collapsed-card">
                            <div class="card-header">
                                <h2 class="card-title">ช่วงบ่าย 13:00 - 16:00</h2>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse"
                                        data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:40%">เวลา</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($dent['eveningSlotChunk_3'] as $morning)
                                        <tr>
                                            <td style="word-spacing :.1em;">
                                                <?php
                                                            $lastHour = $morning[2]['min_l_slot']/60 == 1 ? $morning[2]['hour'] +1 : $morning[2]['hour'];
                                                            $lastMin  = $morning[2]['min_l_slot']/60 == 1 ?  "00" : $morning[2]['min_l_slot'];
                                                            $firstHour = $morning[0]['hour'] < 10 ? '0'.$morning[0]['hour'] : $morning[0]['hour'];
                                                            $firstMin = $morning[0]['min_f_slot'] < 10 ? '0'.$morning[0]['min_f_slot'] : $morning[0]['min_f_slot'];
                                                          ?>
                                                {{$firstHour}}:{{$firstMin}}
                                                -
                                                {{$lastHour}}:{{$lastMin }}
                                            </td>
                                            <td>

                                                <div class="row" id="slotdiv">
                                                    @foreach ($morning as $item)
                                                    <?php
                                                            $rowSpanCount = collect($morning)->filter(function($val, $key){
                                                                if($key == 'chunk'){
                                                                    return $val == [];
                                                                }

                                                            });
                                                        ?>
                                                    @if($rowSpanCount->count() > 0)
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default btn-sm btn-block"
                                                            href="javascript:void(0)" style="cursor:not-allowed">
                                                            <b>-</b>
                                                            <div>ว่าง</div>
                                                        </a>
                                                    </div>
                                                    <?php break; ?>
                                                    @else

                                                    @if (isset($item['index']))
                                                    <div class="col-md-4">
                                                        <a class="btn {{$item['attribute']['btn_type']}} btn-sm btn-block"
                                                            href="#">
                                                            <b>{{$item['attribute']['patient_name']}}</b>
                                                            <div>{{$item['attribute']['treatment_name']}}</div>
                                                        </a>
                                                    </div>
                                                    @endif
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>

                        {{-- //////////////////////   ช่วงค่ำ ///////////////////////// --}}
                        <div class="card collapsed-card">
                            <div class="card-header">
                                <h2 class="card-title">ช่วงค่ำ 1700 - 20:00</h2>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse"
                                        data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:40%">เวลา</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($dent['nightSlotChunk_3'] as $morning)
                                        <tr>
                                            <td style="word-spacing :.1em;">
                                                <?php
                                                            $lastHour = $morning[2]['min_l_slot']/60 == 1 ? $morning[2]['hour'] +1 : $morning[2]['hour'];
                                                            $lastMin  = $morning[2]['min_l_slot']/60 == 1 ?  "00" : $morning[2]['min_l_slot'];
                                                            $firstHour = $morning[0]['hour'] < 10 ? '0'.$morning[0]['hour'] : $morning[0]['hour'];
                                                            $firstMin = $morning[0]['min_f_slot'] < 10 ? '0'.$morning[0]['min_f_slot'] : $morning[0]['min_f_slot'];
                                                          ?>
                                                {{$firstHour}}:{{$firstMin}}
                                                -
                                                {{$lastHour}}:{{$lastMin }}
                                            </td>
                                            <td>

                                                <div class="row" id="slotdiv">
                                                    @foreach ($morning as $item)
                                                    <?php
                                                            $rowSpanCount = collect($morning)->filter(function($val, $key){
                                                                if($key == 'chunk'){
                                                                    return $val == [];
                                                                }

                                                            });
                                                        ?>
                                                    @if($rowSpanCount->count() > 0)
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default btn-sm btn-block"
                                                            href="javascript:void(0)" style="cursor:not-allowed">
                                                            <b>-</b>
                                                            <div>ว่าง</div>
                                                        </a>
                                                    </div>
                                                    <?php break; ?>
                                                    @else

                                                    @if (isset($item['index']))
                                                    <div class="col-md-4">
                                                        <a class="btn {{$item['attribute']['btn_type']}} btn-sm btn-block"
                                                            href="#">
                                                            <b>{{$item['attribute']['patient_name']}}</b>
                                                            <div>{{$item['attribute']['treatment_name']}}</div>
                                                        </a>
                                                    </div>
                                                    @endif
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    @endforeach

                </div>
                {{-- <table class="table table-striped projects">
                    <thead>
                        <tr>
                            <th style="width: 1%">
                                #
                            </th>
                            <th style="width: 10%">

                            </th>
                            <th class="text-center" style="width:89%" colspan="4">
                                ห้องตรวจ
                            </th>

                            <th>

                            </th>
                        </tr>
                        <tr>
                            <th style="width: 1%">

                            </th>
                            <th style="width: 10%">
                                เวลา
                            </th>
                            @for($i = 0; $i < $dutyTimeofDentists->count(); $i++)
                                <th class="room">ห้อง {{$dutyTimeofDentists[$i]->room->room_name}}</th>
                @endfor

                </tr>
                </thead>
                <tbody>



                    @for ($i = 0; $i < 36; $i++) <?php
                                $btn_type_0= $dutyTimeofDentists[0]['morningSlot'][$i]['attribute']['btn_type'];
                                $btn_type_1= $dutyTimeofDentists[1]['morningSlot'][$i]['attribute']['btn_type'];
                                $patient_name_0 = "-";
                                $patient_name_1 = "-";
                                $treatment_name_0 = "ว่าง";
                                $treatment_name_1 = "ว่าง";

                                if($dutyTimeofDentists[0]['morningSlot'][$i]['attribute']['treatment_name'] != ""){
                                    $attribute = $dutyTimeofDentists[0]['morningSlot'][$i]['attribute'];
                                    $patient_name_0 = $dutyTimeofDentists[0]->morningSlot[$i]['attribute']['patient_name'];
                                    $treatment_name_0   = $attribute['treatment_name'];
                                    $btn_type_0         = $attribute['btn_type'];

                                }
                                if($dutyTimeofDentists[1]['morningSlot'][$i]['attribute']['treatment_name'] != ""){
                                    $attribute = $dutyTimeofDentists[1]['morningSlot'][$i]['attribute'];
                                    $patient_name_1 = $dutyTimeofDentists[1]->morningSlot[$i]['attribute']['patient_name'];
                                    $treatment_name_1   = $attribute['treatment_name'];
                                    $btn_type_1         = $attribute['btn_type'];

                                }


                            ?> <tr>
                        <td>{{ $i }}</td>
                        <td class="text-center">
                            {{$dutyTimeofDentists[0]->morningSlot[$i]['hour'].":".$dutyTimeofDentists[0]->morningSlot[$i]['min']}}
                        </td>
                        <td class="text-center">
                            <a class="btn {{$btn_type_0}} btn-sm btn-block" href="#">
                                <b>{{$patient_name_0}}</b>
                                <div>{{$treatment_name_0}}</div>
                            </a>

                        </td>
                        <td class="text-center">
                            <a class="btn {{$btn_type_1}} btn-sm btn-block" href="#">
                                <b>{{$patient_name_1}}</b>
                                <div>{{$treatment_name_1}}</div>
                            </a>

                        </td>
                        </tr>

                        @endfor
                </tbody>
                </table> --}}
            </div>

            {{-- rooom1 --}}
            @foreach ($dutyTimeofDentists as $key => $item)
            <div class="tab-pane" id="room{{$key}}">
                {{-- <h3>ข้อมูลตารางนัดคนไข้ ห้องที่ 1</h3> --}}
                <div class="card card-warning card-outline mb-4">
                    <div class="card-header">
                        <h3 class="card-title">
                            หมอเข้าเวรประจำ {{$item->room->room_name}}
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="col-md-3">
                            <div class="card card-primary card-outline">
                                <div class="card-body box-profile">
                                    <div class="text-center">
                                        <img class="profile-user-img img-fluid img-circle"
                                            src="{{asset('adminlte/dist/img/user5-128x128.jpg')}}"
                                            alt="User profile picture">
                                    </div>

                                    <h3 class="profile-username text-center">{{$item->dentist->dent_name}}</h3>

                                    <p class="text-muted text-center"></p>

                                    {{-- <ul class="list-group list-group-unbordered mb-3">
                                        @foreach ($item->job_grouped as $job)
                                            <li class="list-group-item">
                                                <b>{{$job[0]->treatment->treatment_name}}</b> <a
                                        class="float-right">{{$job['count']}} case</a>
                                    </li>

                                    @endforeach

                                    </ul> --}}

                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>

                    </div>
                    <!-- /.card-body -->
                </div>
                <div class="card card-outline card-warning">
                    <div class="card-header">
                        <div class="card-title">คนไข้ที่ทำการรักษา</div>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped projects">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 1%">
                                        #
                                    </th>
                                    <th class="text-center" style="width: 10%">เวลา</th>
                                    <th class="text-center">ชื่อคนไข้</th>
                                    <th class="text-center">เบอร์ติดต่อ</th>
                                    <th class="text-center">อาการ</th>
                                    <th class="text-center">ผู้รับผิดชอบ</th>
                                    <th class="text-center">วันที่ทำรายการ</th>
                                    <th class="text-center">สถานะ</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $c = 1; ?>
                                @foreach ($item->infoByRoom as $item)

                                <tr>

                                    <td class="text-center" style="width: 1%">
                                        {{$c++}}
                                    </td>
                                    <td class="text-center" style="width: 10%">
                                        {{$item['firstHourMin'] ."-". $item['lastHourMin']}}</td>
                                    <td class="text-center">{{$item['patient_name']}}</td>
                                    <td class="text-center">{{$item['patient_phone']}}</td>
                                    <td class="text-center">{{$item['treatment']}}</td>
                                    <td class="text-center">{{$item['dent_name']}}</td>
                                    <td class="text-center">{{$item['created_at']}}</td>
                                    <td class="text-center">{{$item['status']}}</td>
                                    <td>
                                        <a href="{{url('appointment/edit/'.$item['job_id'])}}"
                                            class="btn btn-warning">แก้ไข</a>
                                        <a href="{{url('appointment/destroy/'.$item['job_id'])}}"
                                            class="btn btn-danger">ลบ</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endforeach


        </div>
        <!--tab-content-->

    </div>

</div>

@endif






@endsection


