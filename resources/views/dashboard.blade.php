@extends('layouts.app')
@section('title')
   Dashboard
@endsection
@section('style')
 <!-- fullCalendar -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.min.css">
<style>
  .list-group-item {
    position: relative;
    display: block;
    padding: .3rem 1.25rem;
    background-color: #fff;
    border: 1px solid rgba(0,0,0,.125);
}

</style>

@endsection
@section('navigate')
<a href="{{url('dashboard')}}">Dashboard</a>
@endsection
@section('content')
<div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
          <div class="inner">
            <h3>4 <span style="font-size: 20px">คน</span></h3>

            <p>หมอประจำคลีนิค</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
          <div class="inner">
            <h3>4558 <span style="font-size: 20px">คน</span></h3>

            <p>สมาชิก</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
          <div class="inner">
            <h3>2 <span style="font-size: 20px">คน</span></h3>

            <p>หมอเข้าวันนี้</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
          <div class="inner">
            <h3>23 <span style="font-size: 20px">คน</span></h3>

            <p>คนไข้นัดทำฟันวันนี้</p>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
          {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
        </div>
      </div>
      <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->

      <!-- right col (We are only adding the ID to make the widgets sortable)-->
      <section class="col-lg-6 connectedSortable ui-sortable">

        <!-- Calendar -->
        <div class="card">
          <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">

            <h3 class="card-title">
              <i class="far fa-calendar-alt"></i>
              Calendar
            </h3>
            <!-- tools card -->
            <div class="card-tools">
              <!-- button with a dropdown -->
              <div class="btn-group">
                <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="-52">
                  <i class="fas fa-bars"></i></button>
                <div class="dropdown-menu" role="menu">
                  <a href="#" class="dropdown-item">Add new event</a>

                </div>
              </div>

            </div>
            <!-- /. tools -->
          </div>
          <!-- /.card-header -->
          <div class="card-body pt-0">
            <!--The calendar -->
            <div id="calendar" style="width: 100%"></div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </section>

      <section class="col-lg-6">
        <div class="card card-info">
          <div class="card-header">
            รายการรักษา วันที่ {{date('d-m')}}-{{date('Y')+543}}
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-6 ">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user-md"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">หมอเข้าเวร</span>
                    <span class="info-box-number">
                      2
                      <small>คน</small>
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>

              <div class="col-6 ">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">คนไข้</span>
                    <span class="info-box-number">23</span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
            </div>
            <div class="card card-primary card-outline">
              <div class="card-header">
                รายการรักษา
              </div>
              <div class="card-body box-profile">

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>ปรับ</b> <a class="float-right">23</a>
                  </li>
                  <li class="list-group-item">
                    <b>ถอนฟัน</b> <a class="float-right">0</a>
                  </li>
                  <li class="list-group-item">
                    <b>รักษารากพัน</b> <a class="float-right">0</a>
                  </li>
                  <li class="list-group-item">
                    <b>ขูดหินปูน</b> <a class="float-right">0</a>
                  </li>
                  <li class="list-group-item">
                    <b>วีเนียร์</b> <a class="float-right">0</a>
                  </li>
                  <li class="list-group-item">
                    <b>อุดฟัน</b> <a class="float-right">0</a>
                  </li>
                </ul>

              </div>
              <!-- /.card-body -->
            </div>
              <!-- fix for small devices only -->
              <div class="clearfix hidden-md-up"></div>

              <!-- /.col -->

              <hr>
              <div class="col-12 ">
                <a href="{{url('appointment/index')}}" id="appointment-info-btn" class="btn btn-info">ดูรายละเอียด</a>
              </div>
              <!-- /.col -->
            </div>
          </div>
        </div>

      </section>
      <!-- right col -->
    </div>
    <!-- /.row (main row) -->
  </div>
@endsection

@section('script')
<script src="https://unpkg.com/@fullcalendar/core@4.4.0/main.min.js"></script>
<script src="https://unpkg.com/@fullcalendar/core@4.4.0/locales-all.min.js"></script>
<script src="https://unpkg.com/@fullcalendar/interaction@4.4.0/main.min.js"></script>
<script src="https://unpkg.com/@fullcalendar/daygrid@4.4.0/main.min.js"></script>

<script>

    // document.addEventListener("DOMContentLoaded", function() {
    //   var date = new Date();
    //   var d = date.getDate();
    //   var m = date.getMonth();
    //   var y = date.getFullYear();

      var calendarEl = document.getElementById("calendar");
      var calendar = new FullCalendar.Calendar(calendarEl, {

        plugins: ["interaction", "dayGrid", "timeGrid", "resourceTimeline"],
        header: {
          left: "prev,next today",
          center: "title",
          right: ""//"dayGridMonth,timeGridWeek,timeGridDay"
        },
        editable: true,
        navLinks: true,
        selectable: true,
        navLinkDayClick: function(date, jsEvent) {
            let day = date
           window.location = '/appointment/index/'+day
            console.log('day', date);
            console.log('coords', jsEvent.pageX, jsEvent.pageY);
        },
        locale: 'th',

      });


      calendar.render();


      $('#appointment-info-btn').click(function(){

      });



      </script>
@endsection
