@extends('layouts.app')

@section('style')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" crossorigin="anonymous">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<!-- if using RTL (Right-To-Left) orientation, load the RTL CSS file after fileinput.css by uncommenting below -->
<!-- link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/css/fileinput-rtl.min.css" media="all" rel="stylesheet" type="text/css" /-->
<!-- the font awesome icon library if using with `fas` theme (or Bootstrap 4.x). Note that default icons used in the plugin are glyphicons that are bundled only with Bootstrap 3.x. -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
<!-- piexif.min.js is needed for auto orienting image files OR when restoring exif data in resized images and when you
    wish to resize images before upload. This must be loaded before fileinput.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/piexif.min.js" type="text/javascript"></script>
<!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview.
    This must be loaded before fileinput.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/sortable.min.js" type="text/javascript"></script>
<!-- popper.min.js below is needed if you use bootstrap 4.x (for popover and tooltips). You can also use the bootstrap js
   3.3.x versions without popper.min.js. -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<!-- bootstrap.min.js below is needed if you wish to zoom and preview file content in a detail modal
    dialog. bootstrap 4.x is supported. You can also use the bootstrap js 3.3.x versions. -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<!-- the main fileinput plugin file -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/fileinput.min.js"></script>
<!-- following theme script is needed to use the Font Awesome 5.x theme (`fas`) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/themes/fas/theme.min.js"></script>
<!-- optionally if you need translation for your language then include the locale file as mentioned below (replace LANG.js with your language locale) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/locales/LANG.js"></script>

@endsection
@section('content')
<form action="{{url('treatment_history/store')}}" method="POST"  enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="label-control">ชื่อคนไข้</label>
                <input type="text" class="form-control" name="patient_hn"  value="{{$patient->f_name.' '.$patient->l_name}}" readonly>
            </div>
            <div class="form-group">
                {{-- {{dd($patient['bill'][0]['treatment'])}} --}}
                <label class="label-control">ชื่อการรักษา</label>
                <input type="text" class="form-control" name="treatment_name"  value="{{$patient['bill'][0]->treatment->treatment_name}}" readonly>
                <input type="hidden" name="treatment_id"  value="{{$patient['bill'][0]->treatment->id}}">

            </div>
            <div class="form-group">
                <label class="label-control">วันทึ่ทำการรักษา</label>
                <input type="text" class="form-control" name="date"  value="{{$patient['bill'][0]->date}}" readonly>
            </div>
            <div class="form-group">
                <label class="label-control">หมอผู้รักษา</label>
                <input type="text" class="form-control"  value="{{$patient['bill'][0]->dentist->dent_name}}" readonly>
                <input type="hidden" name="dent_id"  value="{{$patient['bill'][0]->dentist->id}}">

            </div>
            <div class="form-group">
                <label class="label-control">คำอธิบาย</label>
                <input type="text" class="form-control"  name="detail" value="{{$patient['bill'][0]->detail}}" readonly>
            </div>
            <div class="form-group">
                <label class="label-control">เพิ่มรูปภาพ</label>
                <input id="input-id" type="file" class="file" multiple name="file[]" data-preview-file-type="text">
            </div>
        </div>
    </div>
    <input type="hidden" name="bill_id" value="{{$patient['bill'][0]->id}}">
    <button type="submit" class="btn btn-primary">บันทึก</button>
</form>
@endsection

@section('script')
<script>
 $(document).ready(function(){
    $("#input-id").fileinput();

    // with plugin options
    $("#input-id").fileinput({'showUpload':false, 'previewFileType':'any'});
 });

</script>
@endsection
