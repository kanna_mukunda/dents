@extends('layouts.mobile')
@section('style')
<script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            {{-- @include('admin.sidebar') --}}
            <div class="col-xs-12 col-md-6 col-lg-4">
                <div class="card">
                    <div class="card-header" style="text-align: center">
                        <img src="{{asset('images/cdc_logo.jpg')}}" style=" max-width: 70%;
                        height: auto;">
                    </div>
                    <div class="card-body">
                        {{-- @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif --}}

                        <form method="POST" action="{{ url('/booking/booking') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('booking.booking.form', ['formMode' => 'create'])

                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

