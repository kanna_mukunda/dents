@extends('layouts.app')

@section('title')
 นัดทำฟัน
@endsection
@section('active_booking')
active
@endsection

@section('navigate')
<a href="{{ url('booking/booking') }}">นัดทำฟัน </a>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            {{-- @include('admin.sidebar') --}}

            <div class="col-md-12">
                <a href="{{ url('/booking/booking/new/emp_create') }}" class="btn btn-success mb-3" title="Add New Booking">
                    <i class="fa fa-plus" aria-hidden="true"></i> เพิ่มข้อมูล
                </a>
                <div class="card">
                    {{-- <div class="card-header">Booking</div> --}}
                    <div class="card-body">
                       

                        <form method="GET" action="{{ url('/booking/booking') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>สถานะ</th>
                                        <th>การรักษา</th>
                                        <th>ชื่อ-สกุล</th>
                                        <th>เบอร์ติดต่อ</th>
                                        <th>หมอ</th>
                                        <th>วัน/เวลาที่นัด</th>
                                         <th>วันที่ทำรายการ</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($booking as $item)
                                    <tr>
                                       
                                        <td>{{ $loop->iteration }}</td>
                                        <td><?=htmlspecialchars_decode($item->booking_status->name);?></td>
                                        <td>{{ $item->treat->treatment_name }}</td>
                                        <td>{{ $item->patient->f_name.' '.$item->patient->l_name }}</td>
                                        <td>{{ $item->patient->phone }}</td>
                                        <td>{{ $item->dentist->dent_name }}</td>
                                        {{-- <td>{{ $item->user_status == 1 ? 'ลูกค้าเดิม' : 'ลูกค้าใหม่' }}</td> --}}
                                        <td>
                                            <?php 
                                                $date = new DateTime($item->booking_date);
                                                echo $date->format('d/m/Y  H:i');
                                            ?>
                                        </td>
                                        <td>
                                            <?php 
                                                $date = new DateTime($item->created_at);
                                                echo $date->format('d/m/Y  H:i');
                                            ?>
                                        </td>
                                        <td>
                                            <a href="{{ url('/booking/booking/emp_show/' . $item->id) }}" title="View Booking"><button class="btn btn-info btn-sm">ดู</button></a>
                                            <a href="{{ url('/booking/booking/' . $item->id.'/emp_edit') }}" title="View Booking"><button class="btn btn-warning btn-sm">แก้ไข</button></a>
                                            {{-- <a href="{{ url('/booking/booking/' . $item->id . '/edit') }}" title="Edit Booking"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> แก้ไข</button></a> --}}

                                            <form method="POST" action="{{ url('/booking/booking' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Booking" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i>ลบ</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $booking->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
