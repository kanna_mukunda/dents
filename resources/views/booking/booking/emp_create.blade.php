@extends('layouts.app')
@section('title')
 เพิ่มข้อมูลนัดทำฟัน
@endsection
@section('active_booking')
    active
@endsection
@section('navigate')
<a href="{{ url('booking/booking') }}">นัดทำฟัน</a>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            {{-- @include('admin.sidebar') --}}
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="card">
                    
                    <div class="card-body">
                        {{-- @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif --}}

                        <form method="POST" action="{{ url('/booking/emp_booking') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('booking.booking.emp_form', ['formMode' => 'create'])

                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('script')
<script>
    //init LIFF
    function initializeApp(data) {
    let urlParams = new URLSearchParams(window.location.search);
    $('#name').val(urlParams.toString());
    $('#lineid').val(data.context.userId);
    
    $('#UserInfo').val(profile.displayName);
    }
    //ready
    $(function () {
    //init LIFF
    liff.init(function (data) {
    initializeApp(data);
    });
    //ButtonGetProfile
    $('#ButtonGetProfile').click(function () {
    liff.getProfile().then(
    profile=> {
    $('#UserInfo').val(profile.displayName);
    alert('done');
    }
    );
    });
    //ButtonSendMsg #QueryString
    $('#ButtonSendMsg').click(function () {
    liff.sendMessages([
    {
    type: 'text',
    // text: $('#userid').val() + $('#QueryString').val() + $('#msg').val()
    text: $('คุณเคยลงทะเบียนแล้ว').val()
    }
    ])
    .then(() => {
    alert('done');
    })
    });
    });
    </script>
@endsection