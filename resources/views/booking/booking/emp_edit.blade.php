@extends('layouts.app')

@section('title')
 แก้ไขนัดทำฟัน
@endsection
@section('active_booking')
    active
@endsection
@section('navigate')
<a href="{{ url('booking/booking') }}">นัดทำฟัน</a>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">แก้ไขการจอง #{{ $booking->id }}</div>
                    <div class="card-body">
                        <!-- <a href="{{ url('/booking/booking') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a> -->
                        <br />
                        <br />


                        <form method="POST" action="{{ url('booking/booking/'.$booking->id.'/emp_update') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('booking.booking.emp_form', ['formMode' => 'edit'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
