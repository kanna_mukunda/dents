@extends('layouts.mobile')

@section('style')

    <style>
        .confirmtext{
            color:red;
            font-size: 2rem;

        }

        .text-black{
            color: black;
            font-size: 1rem;
        }

        @media (max-width: 575.98px) { 
            
            .confirmtext{
            font-size: 1.6rem;
            
            }
         }

        /* // Small devices (landscape phones, less than 768px) */
        @media (max-width: 767.98px) { 
            .confirmtext{
            font-size: 1.6rem;
            }
         }

        /* // Medium devices (tablets, less than 992px) */
        @media (max-width: 991.98px) { 
            .confirmtext{
            font-size: 1.6rem;
            /* color:blue; */
            }
         }

        /* // Large devices (desktops, less than 1200px) */
        @media (max-width: 1199.98px) { 
            .confirmtext{
            font-size: 1.3rem;
            /* color: black; */
            }
            .text-black{
                font-size: 1.2rem;
            }
         }
    </style>

@endsection

@section('content')
<div class="container">
    <div class="row">
        {{-- @include('admin.sidebar') --}}

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-header" style="text-align: center">
                        <img src="{{asset('images/cdc_logo.jpg')}}" style=" max-width: 70%;
                            height: auto;">
                    </div>
                </div>
                <div class="card-body" style="text-align:center">
                <img src="{{asset('images/success-clinic-01.png')}}" style=" max-width: 50%;
                            height: auto;">
                    <h2 style="color:green">
                        <b>ส่งข้อมูลเรียบร้อย!</b>
                    </h2>
                    <br>
                    <p class="confirmtext text-center">กรุณารอการยืนยันการนัด <br>หมายจากระบบสักครู่!</p>
                    <br>
                    <p class="confirmtext text-black">เราจะติดต่อกลับภายใน 15 นาที!</p>
                    <br>
                    <br>
                    <p>( หมายเหตุ : หากท่านทำการนัดหลัง 20:00 น. ท่านจะได้รับการยืนยันตั้งแต่ 08:00 น. ของวันถัดไป )</p>



                </div>
                

        </div>
    </div>
</div>
</div>
</div>
@endsection