@extends('layouts.mobile')

@section('content')
<div class="container">
    <div class="row">
        {{-- @include('admin.sidebar') --}}

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-header" style="text-align: center">
                        <img src="{{asset('images/cdc_logo.jpg')}}" style=" max-width: 70%;
                            height: auto;">
                    </div>
                </div>
                <div class="card-body" style="text-align:center">
                    <h4>
                        กรุณาตรวจสอบข้อมูลด้านล่าง และ กดปุ่มสร้างการนัดหมาย
                    </h4>
                    {{-- <a href="{{ url('/booking/booking/' . $booking->id . '/edit') }}" title="แก้ไขการจอง"><button
                        class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                            aria-hidden="true"></i>แก้ไขการจอง</button></a> --}}

                    <br>
                    <i class="fa fa-calendar" style="color:#00CED1">
                        <?php
                            $ex = explode(' ', $booking->booking_date);
                            $hrMin  = explode(':',$ex[1]);
                            $dt = date('d/m/Y',strtotime($booking->booking_date));
                            $dtime = date('H:i',strtotime($booking->booking_date));
                        ?>
                        <a class="float">{{$dt}}</a>
                    </i>
                    <i class="fa fa-clock" style="color:#00CED1">
                        <a class="float">{{$dtime}} น.</a>
                    </i>
                    <br>
                    <b>ชื่อ นามสกุล :</b> <a style="color:#4682B4"> {{$booking->fullname}}</a>
                    <br>
                    <b>เบอร์ติดต่อ :</b> <a class="float">{{$booking->user_phone}}</a>
                    <br>
                    <b>ประเภทการรักษา :</b> <a class="float">{{$booking->treat->treatment_name}}</a>
                    <br>
                    <br>
                   

                    <a href="{{url('booking/booking/complete/'.$booking->id)}}"
                        class="btn btn-success btn-block"><b>สร้างนัดหมาย</b></a>

                    <a href="{{ url('/booking/booking/' . $booking->id . '/edit') }}"
                        class="btn btn-primary btn-block"><b>แก้ไขนัดหมาย</b></a>

               
                        <!-- /.card-body -->
                </div>
                {{-- <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $booking->id }}</td>
                </tr>
                <tr>
                    <th> Treatment Id </th>
                    <td> {{ $booking->booking->treatment_name }} </td>
                </tr>
                <tr>
                    <th> Hn </th>
                    <td> {{ $booking->hn }} </td>
                </tr>
                <tr>
                    <th> Dent Id </th>
                    <td> {{ $booking->dent_id }} </td>
                </tr>
                </tbody>
                </table>
            </div> --}}

        </div>
    </div>
</div>
</div>
</div>
@endsection