@extends('layouts.mobile')

@section('content')
    <div class="container">
        <div class="row">
            {{-- @include('admin.sidebar') --}}

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-header" style="text-align: center">
                            <img src="{{asset('images/cdc_logo.jpg')}}" style=" max-width: 70%;
                            height: auto;">
                        </div>
                    </div>
                    <div class="card-body">

                        <a href="{{ url('/booking/booking') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/booking/booking/' . $booking->id . '/edit') }}" title="แก้ไขการจอง"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                      
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $booking->id }}</td>
                                    </tr>
                                    <tr><th> Treatment Id </th><td> {{ $booking->booking->treatment_name }} </td></tr><tr><th> Hn </th><td> {{ $booking->hn }} </td></tr><tr><th> Dent Id </th><td> {{ $booking->dent_id }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
