@section('style')
    <script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>

    <style>
        .hidden {
            display: none;
        }

        .card-active {
            border: blue solid 2px;
        }

    </style>
@endsection

<div class="row">
    <input type="hidden" id="formMode" value="{{$formMode}}">
    <input type="hidden" id="calendar_doc_id" value="{{ isset($booking) ? $booking->cadendar_doc_id : 0 }}">
    <div class="col-4">
        <div class="form-group {{ $errors->has('hn') ? 'has-error' : '' }}">
            <label for="hn" class="control-label">ชื่อ-สกุล ลูกค้า <span class="text-red">*</span></label>
            <select class="form-control select2" name="hn" id="hn">
                <option value="">เลือก...</option>
                @foreach ($patients as $patient)
                    <option value="{{ $patient->hn }}"
                        {{ isset($booking->hn) && $booking->hn == $patient->hn ? 'selected' : '' }}>
                        {{ $patient->f_name . ' ' . $patient->l_name }}</option>
                @endforeach
            </select>
            {!! $errors->first('hn', '<p class="help-block text-red">:message</p>') !!}
        </div>
    </div>
    <div class="col-4">
        <div class="form-group user_detail {{ $errors->has('user_phone') ? 'has-error' : '' }}">
            <label for="user_phone" class="control-label">เบอร์ติดต่อ <span class="text-red">*</span></label>
            <input class="form-control user_detail" name="user_phone" type="text" id="user_phone"
                value="{{ isset($booking->user_phone) ? $booking->user_phone : '' }}">
            {!! $errors->first('user_phone', '<p class="help-block text-red">:message</p>') !!}
        </div>
    </div>
    <div class="col-4">
        <div class="form-group {{ $errors->has('treatment_id') ? 'has-error' : '' }}">
            <label for="treatment_id" class="control-label">ประเภทการรักษา <span class="text-red">*</span></label>
            <select class="form-control" name="treatment_id" id="treatment_id">
                <option value="">เลือก...</option>
                @foreach ($treats as $treat)
                    <option value="{{ $treat->id }}"
                        {{ isset($booking->treatment_id) && $booking->treatment_id == $treat->id ? 'selected' : '' }}>
                        {{ $treat->treatment_name }}</option>
                @endforeach
            </select>
            {!! $errors->first('treatment_id', '<p class="help-block text-red">:message</p>') !!}
        </div>
    </div>
</div>


<div class="row">
    <div class="col-4">
        <div class="form-group  {{ $errors->has('doc_id') ? 'has-error' : '' }}">
            <label for="doc_id" class="control-label">เลือกหมอ <span class="text-red">*</span></label>
            {{-- {{dd($calendar_docs[0]['id'])}} --}}

            <select class="form-control" name="doc_id" id="doc_id">
                @if ($formMode == 'edit')
                    <option value="0">เลือก...</option>
                    @foreach ($calendar_docs as $calendar_doc)
                        <option value="{{ $calendar_doc['id'] }}"
                            {{ $calendar_doc['id'] == $booking->docter_id ? 'selected' : '' }}>
                            {{ $calendar_doc['dent_name'] }}</option>
                    @endforeach
                @endif
            </select>
           
            {!! $errors->first('doc_id', '<p class="help-block text-red">:message</p>') !!}
        </div>
    </div>
    <div class="col-4">
        <div class="form-group  {{ $errors->has('appointment_date') ? 'has-error' : '' }}">
            <label for="appointment_date" class="control-label">เลือกวัน <span class="text-red">*</span></label>

            <select class="form-control" name="appointment_date" id="appointment_date">
                @if ($formMode == 'edit')
                    <?php $date = explode('|', $booking->calendar_doc->date_book)[0]; ?>
                    <option value="{{ $date }}">{{ $date }}</option>
                @endif
            </select>
            {!! $errors->first('appointment_date', '<p class="help-block text-red">:message</p>') !!}
        </div>
    </div>
    <div class="col-4">
        <div class="form-group  {{ $errors->has('appointment_time') ? 'has-error' : '' }}">
            <label for="appointment_time" class="control-label">เวลาที่ต้องการนัด <span
                    class="text-red">*</span></label>
            <select class="form-control" name="appointment_time" id="appointment_time">
                @if ($formMode == 'edit')
                <?php $a_time = explode('|', $booking->calendar_doc->a_time)[0]; ?>
                <option value="{{ $a_time }}">{{ $a_time }}</option>
                @endif
            </select>
            {!! $errors->first('appointment_time', '<p class="help-block text-red">:message</p>') !!}
        </div>
    </div>

</div>

<div class="form-group user_detail {{ $errors->has('desc') ? 'has-error' : '' }}">
    <label for="desc" class="control-label">อธิบายอาการ</label>
    <textarea class="form-control user_detail" name="desc"
        id="desc">{{ isset($booking->desc) ? $booking->desc : '' }}</textarea>
</div>



<input class="form-control user_detail" type="hidden" id="lineid" name="lineid">
<div class="form-group">
    <input class="btn btn-primary btn-block col-2" id="ButtonSendMsg" type="submit"
        value="{{ $formMode === 'edit' ? 'บันทึก' : 'บันทึกการนัดหมาย' }}">
</div>


@section('script')
    <script>
        let curr_doc_id = ''

        $(document).ready(function () {
            let formMode = $('#formMode').val();
            if (formMode === 'edit') {
                let doc_id = $('#doc_id').val()
                let treatment_id = $('#treatment_id').val()
                let date = $('#appointment_date').val()
                get_calendar_doc_date(doc_id, 'edit');

                get_time_by_date_and_doc(date, doc_id, treatment_id, formMode = 'edit')
            }

        })
        $('#hn').change(function () {
            let hn = $(this).val()
            $.get('../get_user_info/' + hn).done(function (data) {
                if (data !== null) {
                    $('#user_phone').val(data.phone)
                }
            });
        });

        $('#treatment_id').change(function () {
            //get ประเภทการรักษา
            let treatment_id = $(this).val()

            //เลือกหมอ
            $.get('../get_calendar_doc/' + treatment_id).done(function (data) {
                if (data.length === 0) {
                    $('#doc_id').html('<option>ไม่มีหมอที่สามารถรักษาอาการที่เลือก</option>')
                    $('#showDocs').html('')
                    $('.labeldoc').addClass('hidden')
                    $('#avilableDate').addClass('hidden')


                } else {
                    let text = '<option value="0">เลือก...</option>';
                    data.forEach(element => {
                        text += `<option value="${element.id}">${element.dent_name}</option>`;

                    });

                    $('#doc_id').html(text)
                }

            }); //$get
            $('#appointment_time').html('')
            $('#appointment_date').html('')
        });


        $('#doc_id').change(function () {
            let doc_id = $(this).val()
            console.log(doc_id)
            get_calendar_doc_date(doc_id);
            $(this).parent().parent('.activecard').addClass('card-active')

            $(this).addClass('docBtnSelected')
            $('#appointment_time').html('')
            // $(this).parent().append(`<input type="hidden" class="hidden-doc-id" name="doc_id" value="${curr_doc_id}">`);

        });

        function get_calendar_doc_date(doc_id, formMode = 'create') {
            $.get(`../get_date_of_doc/${doc_id}`).done(function (data) {

                if (data.length === 0) {
                    $('#appointment_date').html('<option>ไม่มีวันที่</option>')
                    $('#showDocs').html('')
                    $('.labeldoc').addClass('hidden')
                    $('#avilableDate').addClass('hidden')


                } else {
                    console.log($('#appointment_date').val())
                    let text = '<option>เลือก...</option>';

                    if (formMode === 'edit') {
                        let appointDate = $('#appointment_date').val()
                        data.forEach(element => {
                            if (element === appointDate)
                                text += `<option value="${element}" selected>${element}</option>`;
                            else
                                text += `<option value="${element}">${element}</option>`;
                        });
                    } else {
                        data.forEach(element => {
                            text += `<option value="${element}">${element}</option>`;
                        });
                    }

                    console.log(text)
                    $('#appointment_date').html(text)
                }
            });
        }//get_calendar_doc_date


         $('#appointment_date').change(function () {
            //เอาเวลาที่สามารถจองได้มาแสดง
            let date = $(this).val();
            let doc_id = $('#doc_id').val()
            let treatment_id = $('#treatment_id').val()
            get_time_by_date_and_doc(date, doc_id, treatment_id, 'create')
        })


        function get_time_by_date_and_doc(date, doc_id, treatment_id, formMode = 'create') {
            let id = $('#calendar_doc_id').val();

            $.get(`../get_time_by_date_and_doc/${date}/${doc_id}/${treatment_id}/${id}`).done(function (data) {
                if (data.length === 0) {
                    $('#appointment_time').html('<option>ไม่มีเวลาว่าง</option>')
                    $('#appointment_timeappointment_time').html('');

                } else {
                    let text = '<option>เลือก...</option>';
                    if (formMode === 'edit') {
                        let timeArray = $('#appointment_time').val().split(':');
                        let next_min_temp = timeArray[1][0] == 0 ? timeArray[1][1] : timeArray[1];

                        let next_min = next_min_temp < 10 ? "0" + next_min_temp : next_min_temp;
                        //เวลา ที่จองได้ถัดไป
                        let selected_time = '';

                        if (next_min_temp >= 0 && next_min_temp < 30) {
                            selected_time = timeArray[0] + ":00";
                        } else if (next_min_temp >= 30 && next_min_temp < 60) {
                            selected_time = timeArray[0] + ":30";
                        }
                        data.forEach(element => {
                            if (selected_time === element)
                                text += `<option value="${element}" selected>${element}</option>`;
                            else
                                text += `<option value="${element}">${element}</option>`;
                        });

                    }

                    data.forEach(element => {
                        text += `<option value="${element}">${element}</option>`;
                    });

                    $('#appointment_time').html(text)
                }

            });
        }

        // $(document).on('click', '.docselectbtn', function () {
        //     let doc_id = $(this).data('dent_id') //$(this).val();
        //     curr_doc_id = doc_id;
        //     $('.docselectbtn').each(function () {
        //         // $('.active').addClass('bg-info')
        //         $(this).removeClass('docBtnSelected');
        //         $(this).parent().parent('.activecard').removeClass('card-active')
        //         if ($(this).siblings().hasClass('hidden-doc-id')) {
        //             $(this).siblings('input').remove();
        //         }
        //     })
        //     $(this).parent().parent('.activecard').addClass('card-active')

        //     $(this).addClass('docBtnSelected')
        //     $('#appointment_time').html('')
        //     $(this).parent().append(`<input type="hidden" class="hidden-doc-id" name="doc_id" value="${curr_doc_id}">`);

        // });



        // $('#doc_id').change(function () {
        //     let doc_id = $(this).val()
        //     get_calendar_doc_date(doc_id);
        // })

        

       

       

        //             $(document).on('click', '.doc_name', function () {
        //                 $('.doc_time').each(function (v) {
        //                     $(this).prop('disabled', true)
        //                 })
        //                 $(this).siblings().find('.doc_time').attr('disabled', false)
        //                 $('.user_detail').prop('disabled', false)
        //             });


        //             //init LIFF
        //             function initializeApp(data) {
        //                 let urlParams = new URLSearchParams(window.location.search);
        //                 $('#name').val(urlParams.toString());
        //                 $('#lineid').val(data.context.userId);

        //                 $('#UserInfo').val(profile.displayName);
        //             }
        //             //ready
        //             $(function () {
        //                 //init LIFF
        //                 liff.init(function (data) {
        //                     initializeApp(data);
        //                 });
        //                 //ButtonGetProfile
        //                 $('#ButtonGetProfile').click(function () {
        //                     liff.getProfile().then(
        //                         profile => {
        //                             $('#UserInfo').val(profile.displayName);
        //                             alert('done');
        //                         }
        //                     );
        //                 });
        //                 //ButtonSendMsg #QueryString
        //                 $('#ButtonSendMsg').click(function () {
        //                     liff.sendMessages([{
        //                             type: 'text',
        //                             // text: $('#userid').val() + $('#QueryString').val() + $('#msg').val()
        //                             text: $('คุณเคยลงทะเบียนแล้ว').val()
        //                         }])
        //                         .then(() => {
        //                             alert('done');
        //                         })
        //                 });
        //             });

    </script>
@endsection
