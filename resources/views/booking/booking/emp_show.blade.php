@extends('layouts.app')
@section('title')
 ข้อมูลนัดทำฟัน
@endsection
@section('active_booking')
    active
@endsection
@section('navigate')
<a href="{{ url('booking/booking') }}">นัดทำฟัน</a>
@endsection
@section('content')
<div class="container">
    <div class="row">
        {{-- @include('admin.sidebar') --}}

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{-- <div class="card-header" style="text-align: center">
                        <img src="{{asset('images/cdc_logo.jpg')}}" style=" max-width: 70%;
                            height: auto;">
                    </div>--}}
                    <a href="{{ url('/booking/booking/' . $booking->id . '/emp_edit') }}"
                        class="btn btn-warning btn-block col-2"><b>แก้ไขนัดหมาย</b></a>

                </div> 
                <div class="card-body" style="text-align:center">
                    <h4 class="text-success">
                        ทำการจองนัดทำฟันเรียบร้อยแล้ว
                    </h4>
                 
                    <br>
                    <div class="row">
                        <div class="col-4">
                            <div class="card card-primary card-outline">
                                <div class="card-body box-profile">
                                  <div class="text-center">
                                    <img class="profile-user-img img-fluid img-circle" src="{{asset('/adminlte/dist/img/default-profile.jpg')}}" alt="User profile picture">
                                  </div>
                  
                                  <h3 class="profile-username text-center">{{$booking->patient->f_name.' '.$booking->patient->l_name}}</h3>
                  
                                  <p class=" text-success text-center">ลูกค้า</p>
                  
                                  <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                      <b class="float-left">Hn</b> <a class="float-right">{{$booking->hn}}</a>
                                    </li>
                                    <li class="list-group-item">
                                      <b class="float-left">เบอร์ติดต่อ</b> <a class="float-right">{{$booking->patient->phone}}</a>
                                    </li>
                                  
                                  </ul>
                  
                                </div>
                                <!-- /.card-body -->
                              </div>
                        </div>
                        <div class="col-8">
                            <div class="card card-primary">
                                <div class="card-header">
                                  <h3 class="card-title">ข้อมูลการจองนัดทำฟัน</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <?php
                                    $ex = explode(' ', $booking->booking_date);
                                    $hrMin  = explode(':',$ex[1]);
                                    $dt = date('d/m/Y',strtotime($booking->booking_date));
                                    $dtime = date('H:i',strtotime($booking->booking_date));
                                ?>
                                  <strong><i class="fas fa-book mr-1"></i> วันที่นัด</strong>
                  
                                  <p class=" text-success">
                                    {{$dt}}
                                  </p>
                  
                                  <hr>
                  
                                  <strong><i class="fas fa-clock mr-1"></i> เวลา</strong>
                  
                                  <p class=" text-success">{{$dtime}} น.</p>
                  
                                  <hr>
                  
                                  <strong><i class="fas fa-tooth mr-1"></i>ประเภทการรักษา</strong>
                  
                                  <p class=" text-success">{{$booking->treat->treatment_name}} </p>
                  
                                  <hr>

                                  <strong><i class="fas fa-tooth mr-1"></i>อธิบายอาการ</strong>
                  
                                  <p class=" text-success">{{$booking->desc}} </p>
                  
                                  <hr>
                  
                                </div>
                                <!-- /.card-body -->
                              </div>
                        </div>
                    </div>
                    
               
                        <!-- /.card-body -->
                </div>
                

        </div>
    </div>
</div>
</div>
</div>
@endsection