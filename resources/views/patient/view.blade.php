@extends('layouts.app')
@section('title')
ประวัติคนไข้ Hn : {{$patient->hn}}
@endsection
@section('patient')
active
@endsection

@section('navigate')
<a href="{{url('patient/index')}}">ข้อมูลคนไข้ </a>
@endsection

@section('style')
<style>
    .profile-user-img {
        border: 3px solid #007bff;
        margin: 0 auto;
        padding: 3px;
        width: 163px;
    }

    th {
        text-align: center
    }

    .btn-block {
        cursor: not-allowed
    }

</style>
@endsection

@section('content')
<div class="row">
    <div class="col-12 col-sm- 12">
        <div class="card card-primary card-outline card-tabs">
            <div class="card-header p-0 pt-1 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill"
                            href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home"
                            aria-selected="false">ประวัติส่วนตัว</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill"
                            href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile"
                            aria-selected="false">MEDICAL / DENTAL HISTORY</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-three-messages-tab" data-toggle="pill"
                            href="#custom-tabs-three-messages" role="tab" aria-controls="custom-tabs-three-messages"
                            aria-selected="false">ประวัติการรักษา</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-three-settings-tab" data-toggle="pill"
                            href="#custom-tabs-three-settings" role="tab" aria-controls="custom-tabs-three-settings"
                            aria-selected="false">ประวัติการนัดหมาย</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-three-messages2-tab" data-toggle="pill"
                            href="#custom-tabs-three-messages2" role="tab" aria-controls="custom-tabs-three-messages2"
                            aria-selected="false">ประวัติซื้อเวชภัณฑ์</a>
                    </li> --}}
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-tabContent">
                    <div class="tab-pane fade active show" id="custom-tabs-three-home" role="tabpanel"
                        aria-labelledby="custom-tabs-three-home-tab">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card card-info card-outline">

                                    <div class="card-body box-profile">
                                        <div class="text-center">
                                            <img class="profile-user-img img-fluid img-circle"
                                                src="{{asset('adminlte/dist/img/default-profile.jpg')}}"
                                                alt="User profile picture">
                                        </div>

                                        <h3 class="profile-username text-center">{{$patient->f_name}}
                                            {{$patient->l_name}}</h3>

                                        <p class="text-muted text-center">คนไข้</p>

                                        <ul class="list-group list-group-unbordered mb-3">
                                            <li class="list-group-item">
                                                <b>HN</b> <a class="float-right">{{$patient->hn}}</a>
                                            </li>
                                            <li class="list-group-item text-center">
                                                <?php echo DNS1D::getBarcodeHTML($patient->hn, 'EAN8',5,33); ?>
                                            </li>
                                            <li class="list-group-item">
                                                <b>เพศ</b> <a class="float-right">{{$patient->sex}}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>ว/ด/ป เกิด</b> <a class="float-right">{{$patient->b_date}}</a>
                                            </li>

                                        </ul>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card card-info">
                                    <div class="card-header">
                                        ข้อมูลทั่วไป
                                    </div>
                                    <div class="card-body box-profile">
                                        <ul class="list-group list-group-unbordered mb-3">
                                            <li class="list-group-item">
                                                <b>เลขบัตรประจำตัวประชาชน</b> <a
                                                    class="float-right">{{$patient->phone}}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>ที่อยู่</b>
                                                <a class="float-right">
                                                    {{$patient->address}}
                                                </a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>เบอร์โทรศัพท์</b> <a class="float-right">{{$patient->phone}}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Line ID</b> <a class="float-right">personalLine</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>ผู้ติดต่อได้กรณีฉุกเฉิน</b> <a class="float-right">
                                                    {{$patient->r_name == "" ? '-' :  $patient->r_name." โทร.".$patient->r_phone}}
                                                </a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>โรคประจำตัว</b> <a
                                                    class="float-right">{{$patient->cd != "" ? $patient->cd : "-"}}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>ประวัติแพ้ยา</b> <a
                                                    class="float-right">{{$patient->ah != "" ? $patient->ah : "-"}}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>ประวัติแพ้สารเคมี</b> <a
                                                    class="float-right">{{$patient->ca != "" ? $patient->ca : "-"}}</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel"
                        aria-labelledby="custom-tabs-three-profile-tab">
                        <div class="card bg-info">
                            <div class="card-header">MEDICAL / DENTAL HISTORY</div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->heart_t == 'y' ? 'checked' : ''}} id="heart_trouble"
                                                name="heart_trouble">
                                            <label for="heart_trouble" name="heart_trouble"
                                                class="custom-control-label">Heart Trouble</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->rheumatic_f == 'y' ? 'checked' : ''}} id="rheumatic_fever"
                                                name="rheumatic_fever">
                                            <label for="rheumatic_fever" name="rheumatic_fever"
                                                class="custom-control-label">Rheumatic
                                                fever</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->hypotension == 'y' ? 'checked' : ''}} id="hypotension"
                                                name="hypotension">
                                            <label for="hypotension" name="hypotension"
                                                class="custom-control-label">Hypotension</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->hypertension == 'y' ? 'checked' : ''}} id="hypertension"
                                                name="hypertension">
                                            <label for="hypertension" name="hypertension"
                                                class="custom-control-label">Hypertension</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->blood_d == 'y' ? 'checked' : ''}} id="blood_disease"
                                                name="blood_disease">
                                            <label for="blood_disease" name="blood_disease"
                                                class="custom-control-label">Blood Disease</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->tuberculasis == 'y' ? 'checked' : ''}} id="tuberculasis"
                                                name="tuberculasis">
                                            <label for="tuberculasis" name="tuberculasis"
                                                class="custom-control-label">Tuberculasis</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->liver_d == 'y' ? 'checked' : ''}} id="liver_disease"
                                                name="liver_disease">
                                            <label for="liver_disease" name="liver_disease"
                                                class="custom-control-label">Liver Disease</label>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->diabetes_m == 'y' ? 'checked' : ''}} id="dusbetes_melitus"
                                                name="dusbetes_melitus">
                                            <label for="dusbetes_melitus" name="dusbetes_melitus"
                                                class="custom-control-label">Dusbetes
                                                melitus</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->allergy == 'y' ? 'checked' : ''}} id="allergy"
                                                name="allergy">
                                            <label for="allergy" name="allergy"
                                                class="custom-control-label">Allergy</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->std == 'y' ? 'checked' : ''}} id="std" name="std">
                                            <label for="std" name="std" class="custom-control-label">STD</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox"
                                                {{$patient->other != '' ? 'checked' : ''}} id="other" name="other">
                                            <label for="other" name="other" class="custom-control-label">Other</label>
                                            <input type="text" class="form-control" id="othertext" name="othertext"
                                                value="{{$patient->other}}">

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-three-messages" role="tabpanel"
                        aria-labelledby="custom-tabs-three-messages-tab">
                        {{-- <a href="#" class="btn btn-info">เพิ่มข้อมูล</a> --}}
                        <table class="table table-bordered datatable">
                            <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th style="width:15%">วันที่</th>
                                    <th>อาการ</th>
                                    <th>แพทย์ประจำวัน</th>
                                    <td>ผลการรักษา/นัดหมายครั้งต่อไป</td>
                                    {{-- <th>ผลการรักษา / นัดหมายครั้งต่อไป</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                {{-- @foreach ($patient->bill as $item)
                                <tr>
                                    <td>1.</td>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->dateTh}}</td>
                                    <td>{{$item->treat}} </td>
                                    <td>{{$item->cost}}</td>
                                    <td>{{$item->dent}}</td>
                                    {{-- <td>
                                       นัดเตรียมช่องปากและฟันซี่ในอีก 
                                    </td> 

                                </tr>
                                @endforeach --}}
                                <tr>
                                    <td>1.</td>
                                    <td>20 สิงหาคม 2563</td>
                                    <td>ถอนฟัน</td>
                                    <td>หมอโย</td>
                                    <td>นัดเตรียมช่องปากและฟันซี่ในอีก</td>

                                </tr>
                                <tr>
                                    <td>2.</td>
                                    <td>20 ตุลาคม 2563</td>
                                    <td>รักษารากฟัน</td>
                                    <td>หมอโย</td>
                                    <td>ผลปกติ/ รักษาเสร็จสิ้น</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-three-settings" role="tabpanel"
                        aria-labelledby="custom-tabs-three-settings-tab">
                        <div class="card">
                            {{-- <div class="card-header p-2"> --}}
                                {{-- <ul class="nav nav-pills">
                                    <li class="nav-item"><a class="nav-link active" href="#activity"
                                            data-toggle="tab">การนัดหมาย</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link " href="#timeline"
                                            data-toggle="tab">ส่ง SMS</a></li>
                                </ul> --}}
                            {{-- </div><!-- /.card-header --> --}}
                            <div class="card-body">
                                <table class="table table-bordered datatable">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th style="width:15%">วันที่</th>
                                            <th>เวลา</th>
                                            <th>อาการ</th>
                                            <td>แพทย์ประจำวัน</td>
                                            {{-- <th>ผลการรักษา / นัดหมายครั้งต่อไป</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                        <tr>
                                            <td>1.</td>
                                            <td>20 สิงหาคม 2563</td>
                                            <td>10.00-10.15</td>
                                            <td>ถอนฟัน</td>
                                            <td>หมอโย</td>
        
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>20 ตุลาคม 2563</td>
                                            <td>18.00-19.00</td>
                                            <td>รักษารากฟัน</td>
                                            <td>หมอโย</td>
                                        </tr>
        
                                    </tbody>
                                </table>
                                {{-- <div class="tab-content">
                                    <div class="tab-pane active" id="activity">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th colspan="5">
                                                        <div class="row">
                                                            <div class="col-md-4 offset-md-7 text-right">
                                                                <span class="badge bg-warning">ถอนฟัน</span>
                                                                <span class="badge bg-info">รักษารากฟัน</span>
                                                                <span class="badge bg-primary">ปรับ</span>
                                                                <span class="badge badge-secondary ">จัดฟัน</span>
                                                            </div>

                                                        </div>

                                                    </th>

                                                </tr>
                                                <tr>
                                                    <th style="width: 10px">#</th>
                                                    <th style="width:15%">วันที่</th>
                                                    <th>เวลา</th>
                                                    <th>อาการ</th>
                                                    <th>แพทย์ประจำวัน</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($patient->nextAppointment as $item)
                                                <tr>
                                                    <td>1.</td>
                                                    <td>{{$item['date']}}</td>
                                                    <td class="text-center">
                                                        {{$item['slot_start']}} - {{$item['slot_end']}}
                                                    </td>
                                                    <td class="text-center">
                                                        <span class="btn btn-warning btn-block">
                                                            {{$item['treatment']['treatment_name']}}
                                                        </span>
                                                    </td>
                                                    <td>{{$item['dentist']['dent_name']}}</td>
                                                </tr>
                                                @endforeach


                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane" id="timeline">
                                        <!-- The timeline -->
                                        sms
                                    </div>
                                    <!-- /.tab-pane -->

                                </div> --}}
                                <!-- /.tab-content -->
                            </div><!-- /.card-body -->
                        </div>
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-three-messages2" role="tabpanel"
                        aria-labelledby="custom-tabs-three-messages2-tab">
                       ซื้อ
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>




@endsection
