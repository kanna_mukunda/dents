@extends('layouts.api2')

@section('title')
ทำการนัดหมาย
@endsection

@section('navigate')
<a href="#">เลือกวัน</a>
@endsection

@section('style')
<meta name="csrf-token" id="csrf_token" content="{{ csrf_token() }}" />

<style>
    .hidden {
        display: none
    }

    .show {
        display: block;
    }

    .disabled_slot{
        cursor: not-allowed;
    }
    #aaa .col-md-2,
    #aaa .col-xs-6 {
        /* position: relative; */
        /* min-height: 1px;
        padding-right: 2px;
        padding-left: 2px; */
    }

    .round{
        border-radius:50px;
        padding:15px 25px;
    };
    .prog{
       margin-left:20%;
    }
</style>
@endsection
@section('content')
<div class="row">
    <div class="col-1">

    </div>
    <div class="col-2">
        <button type="button" class="btn btn-success round">1</button>
    </div>
    <div class="col-1">

    </div>
    <div class="col-2">
        <button type="button" class="btn btn-warning round">2</button>
    </div>
    <div class="col-1">

    </div>
    <div class="col-2">
        <button type="button" class="btn btn-default round">3</button>
    </div>

</div>
<hr>

{{-- <h4 class="text-center">เลือกเวลาการรักษา</h4> --}}

<div class="card collapsed-card  col-md-5">
    <div class="card-header">
        <h2 class="card-title">ช่วงเช้า 09:00 - 12:00</h2>
        <div class="card-tools">
            <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse"
                data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body" style="display:block">
        <table class="table">
            <thead>
                <tr>
                    <th style="width:50%">เวลา</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                 @foreach ($empMorningPeriodTimeDents as $morning)
                <tr>
                    <td style="word-spacing :.1em;">
                        <?php
                                            $lastHour = $morning[2]['min_l_slot']/60 == 1 ? $morning[2]['hour'] +1 : $morning[2]['hour'];
                                            $lastMin  = $morning[2]['min_l_slot']/60 == 1 ?  "00" : $morning[2]['min_l_slot'];
                                            $firstHour = $morning[0]['hour'] < 10 ? '0'.$morning[0]['hour'] : $morning[0]['hour'];
                                            $firstMin = $morning[0]['min_f_slot'] < 10 ? '0'.$morning[0]['min_f_slot'] : $morning[0]['min_f_slot'];
                                          ?>
                        {{$firstHour}}:{{$firstMin}}
                        -
                        {{$lastHour < 10 ? '0'.$lastHour : $lastHour}}:{{$lastMin }}
                    </td>
                    <td>

                        <div class="row" id="slotdiv">
                            <div class="col-md-12">

                                @if($morning['full'] == false)
                                    @for($i = 0; $i < collect($morning)->count()-1; $i++)
                                        @if($morning[$i]['status'] == "active")
                                        <?php
                                            $room = app\models\DutyTime::where('dent_id', $morning[$i]['dent_id'])
                                                        ->where('date', $dutyTimeofDentists[0]['date'])
                                                        ->first();

                                        ?>
                                        <a class="btn btn-success btn-sm btn-block active_slot" style="color:white"
                                                data-dent_id="{{ $morning[$i]['dent_id']}}"
                                                data-hour="{{ $morning[$i]['hour']}}"
                                                data-min="{{$morning[$i]['min']}}"
                                                data-min_f_slot="{{$morning[$i]['min_f_slot']}}"
                                                data-min_l_slot="{{ $morning[$i]['min_l_slot']}}"
                                                data-date = "{{$dutyTimeofDentists[0]['date']}}"
                                                data-patient_id = "444444"
                                                data-period_type = "am"
                                                data-treatment_id = "{{$treatment->id}}"
                                                data-selected_treatment_name="{{$treatment->treatment_name}}"
                                                data-room="{{$room->room_id}}"
                                                data-status={{$morning[$i]['status']}}
                                                {{-- data-processing_time="{{$dutyTimeofDentists[0]['processing_time']}}" --}}

                                        >ว่าง
                                        </a>
                                    <?php break; ?>
                                        @endif
                                    @endfor
                                @else
                                <a class="btn btn-default btn-sm btn-block" href="javascript:void(0)"
                                    style="cursor:not-allowed">
                                    <div>ไม่ว่าง</div>
                                </a>
                                @endif
                            </div>

                        </div>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</div>


<div class="modal hidden" id="myModal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">ยืนยันการนัดหมอ</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-grop  row mb-1">
                    <label class="col-sm-5 form-label">คนไข้</label>
                <input type="text" class="col-sm-7 form-control" value="{{$patient->f_name.' '.$patient->l_name}}"
                        readonly="readonly">
                        <input type="hidden" value="{{$patient->hn}}"  id="patient_hn">
                </div>
                <div class="form-grop  row mb-1">
                    <label class="col-sm-5 form-label">วันที่</label>
                    <input type="text" class="col-sm-7 form-control" value="" id="date_selected"
                        readonly="readonly">
                </div>
                <div class="form-grop  row mb-1">
                    <label class="col-sm-5 form-label">ต้องการรักษา</label>
                    <input type="text" class="col-sm-7 form-control" value="" id="selected_treatment_name"
                        aria-readonly="true">
                </div>
                <div class="form-grop  row mb-1">
                    <label class="col-sm-5 form-label">เวลา</label>
                    <input type="text" class="col-sm-7 form-control" value="" id="datetime">
                </div>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success confirm_btn">บันทึก</button>
            </div>

        </div>
    </div>
</div>
@endsection

    @section('script')
    <script>
        let index;
        let hour;
        let min;
        let date;
        let room;
        let dent_id;
        let treatment_id;
        let processing_time;
        let period_type;
        $('.active_slot').click(function (event) {
            event.preventDefault();
            // index = $(this).data('index')
            let status = $(this).data('status')
            hour = $(this).data('hour')
            min = $(this).data('min')
            date = $(this).data('date')
            room = $(this).data('room')
            dent_id = $(this).data('dent_id')
            treatment_id = $(this).data('treatment_id')
            period_type  = $(this).data('period_type')
            // processing_time = $(this).data('processing_time')
            let selected_treatment_name = $(this).data('selected_treatment_name')
            $('#date_selected').val(date)
            $('#selected_treatment_name').val(selected_treatment_name)
            $('#datetime').val(hour + ":" + min + " น.")
            $('#room').val(room)
            showHiddenModal()

        });

        $('.confirm_btn').click(function (event) {
            event.preventDefault();
            showHiddenModal()
            let _token = $('#csrf_token').attr('content')

            let params = {
                hour: hour,
                min: min,
                date: date,
                room: room,
                dent_id: dent_id,
                treatment_id: treatment_id,
                // processing_time: processing_time,
                period_type: period_type,
                patient_hn: $('#patient_hn').val(),
                _token: _token

            };
            console.log('params',params)
            $.post('../appointment/store', params)
                .done(function (data) {
                    console.log('data',data)
                    window.location.href = '../appointment/summary/'+data;
                })
        })

        function showHiddenModal() {
            if ($('#myModal').hasClass('hidden')) {
                $('#myModal').removeClass('hidden')
                $('#myModal').addClass('show')
            } else {
                $('#myModal').removeClass('show')
                $('#myModal').addClass('hidden')
            }
        }

        $('.close').click(function () {
            showHiddenModal()
        })

    </script>
@endsection

