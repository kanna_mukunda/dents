@extends('layouts.app')
@section('title')
{{-- วันที่ {{$dutyTimeofDentists->thaiDate}} --}}
@endsection
@section('a_appointment_index')
active
@endsection
@section('navigate')
<a href="{{url('appointment/index')}}">ข้อมูลคลีนิคประจำวัน2</a>
@endsection
@section('style')
<style>
    .table td {
        padding: 0.1rem !important
    }

    .list-group-item {
        position: relative;
        display: block;
        padding: .2rem 1.25rem;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .125);
    }

    .profile-user-img,
    .profile-user-img-success,
    .profile-user-img-warning,
    .profile-user-img-cancel,
    .profile-user-img-nextday,
    .profile-user-img-empty,
    .profile-user-img-info {
        border: 3px solid #adb5bd;
        margin: 0 auto;
        padding: 3px;
        width: 80px;
    }

    .profile-user-img-success {
        border: 4px solid green;
    }

    .profile-user-img-info {
        border: 4px solid lightskyblue;
    }

    .profile-user-img-warning {
        border: 4px solid yellow;
    }

    .profile-user-img-cancel {
        border: 4px solid black;
    }

    .profile-user-img-nextday {
        border: 4px solid lightcoral;
    }

    .profile-user-img-empty {
        border: 4px solid #01ff70;
    }

    .room {
        text-align: center;
        /* width: 1%; */
    }

    .table td {
        padding: 0.3rem !important;
    }

    div.scrollmenu {
        /* background-color: #333; */
        overflow: auto;
        white-space: nowrap;
    }

    div.scrollmenu .col-md-3 {
        display: inline-block;
    }

    div.scrollmenu .col-md-3:hover {
        background-color: #007bff;
    }

    .card-title {
        font-size: 20px;
        font-weight: bold
    }

    #slotdiv .col-md-4,
    #slotdiv .col-md-12 {
        position: relative;
        width: 100%;
        padding-right: 0px;
        padding-left: 0px;
    }

    #slotdiv .col-md-4 .btn-sm {
        padding: .25rem .5rem;
        font-size: .875rem;
        line-height: 1.5;
        border-radius: 0;
    }

</style>
@endsection


@section('content')


<div class="card card-primary card-outline">
    <div class="card-header">
        <div class="card-title">
            <span class="info-box-icon bg-primary rounded  elevation-1 p-2"><i class="fas fa-calendar-alt"></i></span>
            ตารางนัดคนไข้
        </div>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">

        <div class="card collapsed-card  col-md-5">
            <div class="card-header">
                <h2 class="card-title">ช่วงเช้า 09:00 - 12:00</h2>
                <div class="card-tools">
                    <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse"
                        data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body" style="display:block">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width:50%">เวลา</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                         @foreach ($empMorningPeriodTimeDents as $morning)
                        <tr>
                            <td style="word-spacing :.1em;">
                                <?php 
                                                    $lastHour = $morning[2]['min_l_slot']/60 == 1 ? $morning[2]['hour'] +1 : $morning[2]['hour'];
                                                    $lastMin  = $morning[2]['min_l_slot']/60 == 1 ?  "00" : $morning[2]['min_l_slot'];
                                                    $firstHour = $morning[0]['hour'] < 10 ? '0'.$morning[0]['hour'] : $morning[0]['hour'];
                                                    $firstMin = $morning[0]['min_f_slot'] < 10 ? '0'.$morning[0]['min_f_slot'] : $morning[0]['min_f_slot'];
                                                  ?>
                                {{$firstHour}}:{{$firstMin}}
                                -
                                {{$lastHour}}:{{$lastMin }}
                            </td>
                            <td>

                                <div class="row" id="slotdiv">
                                    <div class="col-md-12">

                                        @if($morning['full'] == false)
                                            @for($i = 0; $i < collect($morning)->count()-1; $i++)
                                                @if($morning[$i]['status'] == "active")
                                                <a class="btn btn-success btn-sm btn-block info" style="color:white"  
                                                        data-dent_id="{{ $morning[$i]['dent_id']}}" 
                                                        data-hour="{{ $morning[$i]['hour']}}" 
                                                        data-min="{{$morning[$i]['min']}}" 
                                                        data-min_f_slot="{{$morning[$i]['min_f_slot']}}" 
                                                        data-min_l_slot="{{ $morning[$i]['min_l_slot']}}" 
                                                        data-date = "{{$dutyTimeofDentists[0]['date']}}" 
                                                        data-patient_id = "444444" 
                                                        data-period_type = "am" 
                                                        data-treatment_id = "1" 
                                                >ว่าง
                                                </a>
                                            <?php break; ?>
                                                @endif
                                            @endfor
                                        @else
                                        <a class="btn btn-default btn-sm btn-block" href="javascript:void(0)"
                                            style="cursor:not-allowed">
                                            <div>ไม่ว่าง</div>
                                        </a>
                                        @endif
                                    </div>

                                </div>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
      
    </div>

</div>

@endsection


@section('script')
    <script>
        $('.info').click(function(){
            let hour = $(this).data('hour');
            console.log(hour)
        })
    </script>
@endsection