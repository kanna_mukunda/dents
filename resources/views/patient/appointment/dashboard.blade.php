@extends('layouts.api2')
@section('style')

@endsection
@section('title')
ยินดีต้อนรับ
@endsection

@section('navigate')
<a href="{{ url('patient/appointment/index') }}">คลินิค</a>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
    <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/default-profile.jpg')}}" alt="User profile picture">
          </div>

          <h3 class="profile-username text-center">{{$job[0]->patient->f_name .' '.$job[0]->patient->f_name}}</h3>

          <p class="text-muted text-center">HN:{{$job[0]->patient->hn}} </p>

          <ul class="list-group list-group-unbordered mb-3">
            <li class="list-group-item">
              <b>เบอร์โทร</b> <a class="float-right">{{$job[0]->patient->phone}}</a>
            </li>
            {{-- <li class="list-group-item">
              <b>Following</b> <a class="float-right">543</a>
            </li>
            <li class="list-group-item">
              <b>Friends</b> <a class="float-right">13,287</a>
            </li> --}}
          </ul>

          <a href="{{url('patient/appointment/select_date_treate')}}" class="btn btn-primary btn-block"><b>ทำการนัดหมาย</b></a>
        </div>
        <!-- /.card-body -->
      </div>

</div>
@if(collect($job)->isNotEmpty())
<div class="col-lg-12 col-12">
    <!-- small card -->
    <div class="small-box bg-info">
      <div class="inner">
        <h3>ข้อมูลการนัดทำฟัน</h3>

        <h4>รายการ : {{$job[0]->treatment->treatment_name}}</h4>
        <h4>วันที่ : {{$job[0]->date}}</h4>
      </div>
      <a href="{{url('line/appointment/postpone')}}" class="btn btn-warning"><b>เลื่อนนัดหมาย</b></a>

      <div class="icon">
        <i class="fas fa-user-plus"></i>
      </div>
      <hr>
      <a href="{{url('patient/appointment/summary/'.$job_id)}}" class="small-box-footer">
        ข้อมูลเพิ่มเติม <i class="fas fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
</div>
@endif

@endsection
