@extends('layouts.api2')
@section('style')

@endsection
@section('title')
ติดต่อเรา
@endsection

@section('navigate')
<a href="{{ url('patient/appointment/index') }}">คลินิค</a>
@endsection

@section('content')

<br>
<div class="row d-flex align-items-stretch">
<div class="col-12 col-sm-12 col-md-12 d-flex align-items-stretch">
    <div class="card bg-light">
      <div class="card-header text-muted border-bottom-0">
      </div>
      <div class="card-body pt-0">
        <div class="row">
          <div class="col-12">
            <h2 class="lead"><b>คลินิกกลางเมืองทันตกรรม : CDC Khonkaen</b></h2>
            <p class="text-muted text-sm"><b> </p>
            <ul class="ml-4 mb-0 fa-ul text-muted">
              <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> 62/215 ถ.กลางเมือง  <br>ต.ในเมือง <br> อ.เมือง จ.ขอนแก่น <br> อ.เมือง40000</li>
              <li class="small"><hr></li>
              <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> โทร #: 097 319 7753</li>
            </ul>
          </div>

        </div>
      </div>

    </div>
  </div>
</div>
@endsection


@section('script')


@endsection
