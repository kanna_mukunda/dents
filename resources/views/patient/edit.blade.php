@extends('layouts.app')

@section('content')

<div class="card card-warning">
    <div class="card-header">
      <h3 class="card-title">แก้ไขข้อมูลคนไข้</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form role="form" action="{{ url('patient/update/'.$patient->id) }}" method="post">
        @csrf
        @method('patch')
        <div class="row">
          <div class="col-sm-12">
            <!-- text input -->
            <div class="form-group">
              <label>เลข Hn</label>
              <input type="text" class="form-control"  name="hn" id="hn"
                value="{{$patient->hn}}">
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label>ชื่อ - สกุล</label>
              <input type="text" class="form-control"  name="name" id="name"
                value="{{$patient->name}}">
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label>เบอร์โทรศัพท์</label>
              <input type="text" class="form-control"  name="phone" id="phone"
                value="{{$patient->phone}}">
            </div>
          </div>
        </div>
        
          <button type="submit"class="btn btn-success">บันทึก</button>

      </form>
    </div>
    <!-- /.card-body -->
  </div>
@endsection