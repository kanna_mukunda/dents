@extends('layouts.app')

@section('title')
เพิ่มข้อมูลคนไข้
@endsection
@section('patient')
active
@endsection
@section('navigate')
<a href="{{ url('patient/index') }}">ข้อมูลคนไข้</a>
@endsection
@section('style')
<style>
    #resizable {
        width: 100px;
        height: 100px;
        background: #ccc;
    }

</style>

@endsection
@section('content')
<div class="card card-warning">
    <div class="card-header">
        <h3 class="card-title">เพิ่มข้อมูลคนไข้</h3>
    </div>

    <div class="card-body">
        <div class="row">
            {{-- from ข้อมูลคนไข้ --}}
            <div class="col-md-12">
                <form role="form" action="{{ url('patient/store') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="form-group ml-3 ">
                            <label for="inputHn" class="col-form-label">Hn</label>
                            <input type="text" class="form-control" id="inputHn" name="hn" value="{{$lastHn->hn}}" readonly>
                        </div>
                        <div class="form-group ml-3 ">
                            <label for="inputIdCard" class=" col-form-label">เลขบัตรประชาชน</label>
                            <input type="text" class="form-control" id="inputIdCard" name="idcard" placeholder="">
                        </div>
                        <div class="form-group ml-3">
                            <label for="inputName" class="col-form-label">ชื่อ</label>
                            <input type="text" class="form-control" id="inputName" name="name">
                        </div>
                        <div class="form-group ml-3">
                            <label for="inputLastName" class="col-form-label">นามสกุล</label>
                            <input type="text" class="form-control" id="inputLastName" name="lastName">
                        </div>
                    </div>
                    <!--row-->

                    <div class="row">
                        <div class="form-group ml-3">
                            <label for="inputName" class="col-form-label">เพศ</label>
                            <select class="form-control" id="gender" name="gender">
                                <option>เลือก...</option>
                                <option value="ชาย">ชาย</option>
                                <option value="หญิง">หญิง</option>
                            </select>
                        </div>
                        <div class="form-group ml-3">
                            <label for="inputLastName" class="col-form-label">อายุ</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="age" id="age">
                                <div class="input-group-append">
                                    <span class="input-group-text">ปี</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ml-3">
                            <label for="inputLastName" class="col-form-label">น้ำหนัก</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="weight" id="weight">
                                <div class="input-group-append">
                                    <span class="input-group-text">กก.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ml-3">
                            <label for="inputLastName" class="col-form-label">ส่วนสูง</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="tall" id="tall">
                                <div class="input-group-append">
                                    <span class="input-group-text">ซม.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--row-->

                    <div class="row">
                        <div class="form-group ml-3">
                            <label for="inputLastName" class="col-form-label">วัน/เดือน/ปี เกิด</label>
                            <input type="text" id="birthday" name="birthday" class="form-control date">
                        </div>
                        <div class="form-group ml-3">
                            <label for="inputLastName" class="col-form-label">เบอร์โทรศัพท์</label>
                            <input type="text" id="phone" name="phone" class="form-control">

                        </div>

                        <div class="form-group ml-3 col-md-6">
                            <label for="inputLastName" class="col-form-label">ที่อยู่</label>
                            <input type="text" id="address" name="address" class="form-control ">
                        </div>
                    </div>
                    <!--row-->

                    <div class="row">
                        <div class="form-group ml-3">
                            <label for="inputLastName" class="col-form-label">ญาติ</label>
                            <input type="text" id="relative_person" name="relative_person" class="form-control">
                        </div>
                        <div class="form-group ml-3">
                            <label for="inputLastName" class="col-form-label">เบอร์ติดต่อญาติ</label>
                            <input type="text" id="relative_person_phone" name="relative_person_phone"
                                class="form-control">

                        </div>
                    </div>
                    <!--row-->
                    <div class="row">
                        <div class="form-group ml-3">
                            <label for="inputLastName" class="col-form-label">โรคประจำตัว</label>
                            <input type="text" id="congenital_disease" name="congenital_disease" class="form-control">

                        </div>
                        <div class="form-group ml-3">
                            <label for="inputLastName" class="col-form-label">ประวัติแพ้ยา</label>
                            <input type="text" id="drug_allergy_history" name="drug_allergy_history"
                                class="form-control">

                        </div>
                        <div class="form-group ml-3">
                            <label for="inputLastName" class="col-form-label">ประวัติแพ้สารเคมี</label>
                            <input type="text" id="chemical_allergy_history" name="chemical_allergy_history"
                                class="form-control">

                        </div>

                    </div>
                    <!--roww-->
                    <div class="card">
                        <div class="card-header">MEDICAL / DENTAL HISTORY</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="custom-control">
                                        <input class="" type="checkbox" id="heart_trouble" name="heart_trouble">
                                        <label for="customCheckbox2" class="control-label">Heart Trouble</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="custom-control">
                                        <input class="" type="checkbox" id="rheumatic_fever" name="rheumatic_fever">
                                        <label for="customCheckbox2" class="control-label">Rheumatic fever</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="custom-control">
                                        <input class="" type="checkbox" id="hypotension" name="hypotension">
                                        <label for="customCheckbox2" class="control-label">Hypotension</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="custom-control">
                                        <input class="" type="checkbox" id="hypertension" name="hypertension">
                                        <label for="customCheckbox2" class="control-label">Hypertension</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="custom-control">
                                        <input class="" type="checkbox" id="blood_disease" name="blood_disease">
                                        <label for="customCheckbox2" class="control-label">Blood Disease</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="custom-control">
                                        <input class="" type="checkbox" id="tuberculasis" name="tuberculasis">
                                        <label for="customCheckbox2" class="control-label">Tuberculasis</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="custom-control">
                                        <input class="" type="checkbox" id="liver_disease" name="liver_disease">
                                        <label for="customCheckbox2" class="control-label">Liver Disease</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="custom-control">
                                        <input class="" type="checkbox" id="dusbetes_melitus" name="dusbetes_melitus">
                                        <label for="customCheckbox2" class="control-label">Dusbetes melitus</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="custom-control">
                                        <input class="" type="checkbox" id="allergy" name="allergy">
                                        <label for="customCheckbox2" class="control-label">Allergy</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="custom-control">
                                        <input class="" type="checkbox" id="std" name="std">
                                        <label for="customCheckbox2" class="control-label">STD</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="custom-control ">
                                        <input class="" type="checkbox" id="other" name="other">
                                        <label for="customCheckbox2" class="control-label">Other</label>
                                        <input type="text" class="form-control" id="other_value" name="other_value"
                                            value="-">
                                    </div>
                                </div>
                            </div>
                            <!--row -->
                        </div>
                    </div>
            </div>
        </div>
        <input type="submit" value="บันทึก" class="btn btn-success">
        </form>
    </div>
    <div class="col-md-4">

    </div>
</div>
<!--row -->

</div>
<!-- /.card-body -->
</div>
@endsection

@section('script')


@endsection
