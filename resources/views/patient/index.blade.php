@extends('layouts.app')
@section('title')
ตารางข้อมูลคนไข้
@endsection

@section('patient')
active
@endsection

@section('navigate')
<a href="{{ url('patient/index') }}">ข้อมูลคนไข้</a>
@endsection
@section('content')
<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">ตารางข้อมูลคนไข้</h3>

                <div class="card-tools">
                    <a href="{{ url('patient/create') }}"
                        class="btn btn-primary">เพิ่มข้อมูลคนไข้</a>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Hn</th>
                            <th>ชื่อ - สกุล</th>
                            <th>เลขบัตรประชาชน</th>
                            <th>เบอร์โทรศัพท์</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $c = 1; ?>
                        @foreach($patients as $item)
                            <tr>
                                <td>{{ $c++ }}</td>
                                <td>{{ $item->hn }}</td>
                                <td class="text-center">
                                    {{ $item->f_name." ".$item->l_name }}</td>
                                <td class="text-center">{{ $item->id }}</td>
                                <td class="text-center">{{ $item->phone }}</td>
                                <td>
                                    <a href="{{ url('patient/view/'.$item->hn) }}"
                                        class="btn btn-info btn-sm">ดู</a>
                                    <a href="{{ url('patient/edit/'.$item->hn) }}"
                                        class="btn btn-warning btn-sm">แก้ไข</a>
                                    <a href="{{ url('patient/destroy/'.$item->hn) }}"
                                        class="btn btn-danger btn-sm">ลบ</a>
                                </td>
                            </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
@endsection
