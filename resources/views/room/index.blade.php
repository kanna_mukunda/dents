@extends('layouts.app')

@section('title')
 ห้องตรวจ
@endsection
@section('room')
active
@endsection
@section('navigate')
<a href="{{url('room/index')}}">ตั้งค่าห้องตรวจ</a>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
      <a href="{{url('room/create')}}" class="btn btn-primary mb-3">เพิ่มข้อมูล</a>

      <div class="card">
        <div class="card-header">
            <h3 class="card-title">ตารางข้อมูลห้องตรวจ</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-hover text-nowrap datatable">
            <thead>
              <tr>
                <th>#</th>
                <th>ชื่อห้อง</th>
                <th>ชั้นที่</th>
                <th>สถานะ</th>
                <th>หมายเหตุ</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
             
                <?php $c = 1; ?>
                @foreach ($rooms as $item)
                <tr>
                  <td>{{$c++}}</td>
                  <td>{{$item->room_name}}</td>
                  <td>{{$item->floor}}</td>
                  <td>{{$item->status }}</td>
                  <td>{{$item->comment}}</td>
                  <td>
                      <a href="{{url('room/edit/'.$item->id)}}" class="btn btn-warning btn-sm">แก้ไข</a>
                      <a href="{{url('room/destroy/'.$item->id)}}" class="btn btn-danger btn-sm">ลบ</a>
                  </td>
                  </tr>
                 
                @endforeach
              
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
@endsection

