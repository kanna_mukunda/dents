@extends('layouts.mobile')

@section('title')
ผู้ใช้งานระบบ
@endsection
@section('navigate')
<a href="{{url('room/index')}}">ตั้งค่าข้อมูลผู้ใช้งานระบบ</a>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
      {{-- <a href="{{url('room/create')}}" class="btn btn-primary mb-3">เพิ่มข้อมูลผู้ใช้งานระบบ</a> --}}
      <div class="row">
        <div class="col-md-9">
          <img src="{{asset('images/a.jpg')}}"  style="width: 100%">
        </div>
        <div class="col-md-3">
          <div class="register-box border-0" style="margin-top: 3rem">
            <div class="card card-outline border-0">
              <div class="card-header text-center border-0">
                <a href="../../index2.html" class="h1"><b>Admin</b>LTE</a>
              </div>
              <div class="card-body border-0">
                <p class="login-box-msg">Register a new membership</p>
          
                <form action="../../index.html" method="post">
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Full name">
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fas fa-user"></span>
                      </div>
                    </div>
                  </div>
                  <div class="input-group mb-3">
                    <input type="email" class="form-control" placeholder="Email">
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                      </div>
                    </div>
                  </div>
                  <div class="input-group mb-3">
                    <input type="password" class="form-control" placeholder="Password">
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                      </div>
                    </div>
                  </div>
                  <div class="input-group mb-3">
                    <input type="password" class="form-control" placeholder="Retype password">
                    <div class="input-group-append">
                      <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-8">
                      <div class="icheck-primary">
                        <input type="checkbox" id="agreeTerms" name="terms" value="agree">
                        <label for="agreeTerms">
                         I agree to the <a href="#">terms</a>
                        </label>
                      </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                      <button type="submit" class="btn btn-primary btn-block">Register</button>
                    </div>
                    <!-- /.col -->
                  </div>
                </form>
          
                <div class="social-auth-links text-center">
                  <a href="#" class="btn btn-block btn-primary">
                    <i class="fab fa-facebook mr-2"></i>
                    Sign up using Facebook
                  </a>
                  <a href="#" class="btn btn-block btn-danger">
                    <i class="fab fa-google-plus mr-2"></i>
                    Sign up using Google+
                  </a>
                </div>
          
                <a href="login.html" class="text-center">I already have a membership</a>
              </div>
              <!-- /.form-box -->
            </div><!-- /.card -->
          </div>
        </div>
      </div>





      {{-- <div class="card"> --}}
        {{-- <div class="card-header">
            <h3 class="card-title">ตารางข้อมูลห้องตรวจ</h3>
        </div> --}}
        <!-- /.card-header -->
        {{-- <div class="card-body table-responsive p-0">
          <table class="table table-hover text-nowrap datatable">
            <thead>
              <tr>
                <th>#</th>
                <th>ชื่อ-สกุล</th>
                <th>สถานะ</th>
                <th>เบอร์โทรศัพท์</th>
                <th>อีเมล</th>
                <th>สิทธิ์ผู้ใช้งาน</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td> สมพงศ์ สอนมา</td>
                <td>ผู้ดูแลระบบ</td>
                <td>0145697002</td>
                <td>cdc_admin@gmail.com</td>
                <td>อ่าน บันทึก เพิ่ม ลบ แก้ไข กำหนดสิทธิ์ผู้ใช้งาน</td>
                <td>
                  <a href="" class="btn btn-info">ดู</a>
                  <a href="" class="btn btn-warning">แก้ไข</a>
                  <a href="" class="btn btn-danger">ลบ</a>
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>กรกมล ใจดี</td>
                <td>พนักงาน</td>
                <td>0944340001</td>
                <td>kk_001@gmail.com</td>
                <td>อ่าน บันทึก เพิ่ม ลบ แก้ไข</td>
                <td>
                  <a href="" class="btn btn-info">ดู</a>
                  <a href="" class="btn btn-warning">แก้ไข</a>
                  <a href="" class="btn btn-danger">ลบ</a>
                </td>
              </tr>
              <tr>
                <td>3</td>
                <td> อรชร เข็มกลัด</td>
                <td>พนักงาน</td>
                <td>093432353</td>
                <td>somchail_k@gmail.com</td>
                <td>อ่าน บันทึก เพิ่ม ลบ แก้ไข</td>
                <td>
                  <a href="" class="btn btn-info">ดู</a>
                  <a href="" class="btn btn-warning">แก้ไข</a>
                  <a href="" class="btn btn-danger">ลบ</a>
                </td>
              </tr>
            
              
            </tbody>
          </table>
        </div> --}}
        <!-- /.card-body -->
      {{-- </div> --}}
      <!-- /.card -->
    </div>
  </div>
@endsection

