@extends('layouts.app')

@section('title')
 เพิ่มข้อมูลห้องตรวจ
@endsection
@section('room')
active
@endsection
@section('navigate')
<a href="{{url('room/index')}}">จัดการข้อมูลห้องตรวจ</a>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            ฟอร์มเพิ่มข้อมูลห้องตรวจ
        </div>
        <div class="card-body">
            <form action="{{url('room/store')}}" method="POST">
                @csrf

                <div class="form-group row">
                    <label for="" class="label label-control col-md-2">ชื่อห้องตรวจ</label>
                    <input type="text" class="form-control col-md-5" name="room_name" value="">
                </div>
                <div class="form-group row">
                    <label for="" class="label label-control col-md-2">ชั้นที่</label>
                    <input type="text" class="form-control col-md-5" name="floor" value="">
                </div>
                <div class="form-group row">
                    <label for="" class="label label-control col-md-2">สถานะ</label>
                    <select class="form-control col-md-5" name="status">
                        <option value="active" selected> เปิดใช้งาน</option>
                        <option value="inactive">ปิดใช้งาน</option>
                    </select>
                </div>
                <div class="form-group row">
                    <label for="" class="label label-control col-md-2">หมายเหตุ</label>
                    <textarea class="form-control col-md-5" name="comment"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit"class="btn btn-primary btn-block offset-md-4 col-md-3">บันทึก</button>
                </div>
            </form>
        </div>
    </div>
   
ิิ@endsection