@extends('layouts.app')

@section('title')
 แก้ไขข้อมูลห้องตรวจ
@endsection
@section('room')
active
@endsection
@section('navigate')
<a href="{{url('room/index')}}">จัดการข้อมูลห้องตรวจ</a>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            ฟอร์มแก้ไขข้อมูลห้องตรวจ
        </div>
        <div class="card-body">
            <form action="{{url('room/update/'.$room->id)}}" method="POST">
                @csrf
                @method('patch')
                <div class="form-group row">
                    <label for="" class="label label-control col-md-2">ชื่อห้องตรวจ</label>
                    <input type="text" class="form-control col-md-5" name="room_name" value="{{ $room->room_name }}">
                </div>
                <div class="form-group row">
                    <label for="" class="label label-control col-md-2">ชั้นที่</label>
                    <input type="text" class="form-control col-md-5" name="floor" value="{{$room->floor}}">
                </div>
                <div class="form-group row">
                    <label for="" class="label label-control col-md-2">สถานะ</label>
                    <select class="form-control col-md-5" name="status">
                        <option>เลือก..</option>
                        <option value="active" {{$room->status == 'active' ? 'selected' : ''}}>active</option>
                        <option value="inactive" {{$room->status == 'inactive' ? 'selected' : ''}}>inactive</option>
                    </select>
                </div>
                <div class="form-group row">
                    <label for="" class="label label-control col-md-2">หมายเหตุ</label>
                    <textarea class="form-control col-md-5" name="comment">{{ $room->comment}}"</textarea>
                </div>
                <div class="form-group">
                    <button type="submit"class="btn btn-primary btn-block offset-md-4 col-md-3">บันทึก</button>
                </div>
            </form>
        </div>
    </div>
   
ิิ@endsection