
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>กลางเมืองทันตกรรม </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('/adminlte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('/adminlte/dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('adminlte/plugins/bootstrap/js/bootstrap.min.js')}}">
  <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

   <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('adminlte/plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

  <link href="https://fonts.googleapis.com/css2?family=Prompt:wght@100;200;300&display=swap" rel="stylesheet">
<style>
  div{
    font-family: 'Prompt', sans-serif !important;
  }
  .card-title .p-2 {
    padding: .5rem .7rem!important;
  }

</style>
  @yield('style')
</head>
<body class="hold-transition sidebar-mini" >


<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
                <li class="nav-item d-none d-sm-inline-block">

                    <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                        <p>
                            <i class="nav-icon fas fa-power-off"></i>
                            ออกจากระบบ
                        </p>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
  </nav>
  <!-- /.navbar -->

  

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('dashboard')}}" class="brand-link text-center">
      <img src="{{asset('/images/cdc_logo.jpg')}}"
           alt="AdminLTE Logo" style="width: 95%"
           >
      {{-- <span class="brand-text font-weight-light">กลางเมืองทตกรรม </span> --}}
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      {{-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div> --}}

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="{{url('dashboard')}}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                ภาพรวม
              </p>
            </a>

          </li>

          <li class="nav-item">
            <a href="{{url('appointment/index')}}" class="nav-link @yield('active_appointment')">
              <i class="nav-icon fa fa-calendar-day"></i>
              <p>
                คิวประจำวันนี้
              </p>
            </a>

          </li>
          <li class="nav-item">
            <a href="{{url('appointment/appoint_mobile')}}" class="nav-link @yield('active_appointment')">
              <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                ดูตารางนัดหมาย
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{url('booking/booking')}}" class="nav-link @yield('active_booking')">
              <i class="nav-icon fa fa-teeth-open"></i>
              <p>
                นัดทำฟัน
              </p>
            </a>
          </li>
          
          <li class="nav-item has-treeview">
            <a href="{{url('appointment/postpone')}}" class="nav-link">
              <i class="nav-icon fa fa-teeth"></i>
              <p>
              เลื่อนนัดทำฟัน
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{url('patient/index')}}" class="nav-link">
              <i class="nav-icon fa fa-user-shield"></i>
              <p>
                 เวชระเบียนประวัติคนไข้
              </p>
            </a>
          </li>


        
          <li class="nav-header">
              <p>
                ตั้งค่าข้อมูลคลินิก
              </p>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{url('calendar-doc')}}" class="nav-link @yield('calendar-doc')">
              {{-- <a href="{{url('calendar/index')}}" class="nav-link"> --}}

              <i class="nav-icon fa fa-calendar-check"></i>
              <p>
                 วันให้บริการประจำเดือน
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{url('dentist/index')}}" class="nav-link @yield('dentist')">
              <i class="nav-icon fa fa-user-md"></i>
              <p>
                ข้อมูลหมอ
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            <a href="{{url('treatment_skill_ratio/index')}}" class="nav-link @yield('treatment_skill_ratio')">
              <i class="nav-icon fa fa-clock"></i>
              <p>
                อัตราเร็วหมอต่อการรักษา
              </p>
            </a>
          </li>
         
           <li class="nav-item has-treeview">
            <a href="{{url('treament_type/treatment-type')}}" class="nav-link @yield('treament_type')">
              <i class="nav-icon fa fa-list-alt"></i>
              <p>
                รายการรักษา
              </p>
            </a>
          </li> 
          <li class="nav-item">
            <a href="{{ url('room/index')}}" class="nav-link @yield('room')">
              <i class="nav-icon fa fa-hospital-alt"></i>
              <p>
                ตั้งค่าห้องตรวจ
              </p>
            </a>
          </li>
          @role('superadmin')
          <li class="nav-header">
              <p>
                ตั้งค่าผู้ใช้งานระบบ
              </p>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{url('admin/users')}}" class="nav-link @yield('admin/users')">
              {{-- <a href="{{url('calendar/index')}}" class="nav-link"> --}}

              <i class="nav-icon fa fa-user-friends"></i>
              <p>
                 ผู้ใช้งานระบบ
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{url('admin/users/create')}}" class="nav-link @yield('admin/users/create')">
              <i class="nav-icon fa fa-user-plus"></i>
              <p>
                เพิ่มผู้ใช้งาน
              </p>
            </a>
          </li>

          @endrole
          <li class="nav-header"></li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>@yield('title')</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">@yield('navigate')/@yield('title')</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
          @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {{-- <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.
  </footer> --}}

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
{{-- <script src="{{asset('adminlte/plugins/jquery/jquery.min.js')}}"></script> --}}
<!-- Bootstrap 4 -->
{{-- <script src="{{asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/select2/js/select2.full.min.js')}}"></script> --}}
<!-- Bootstrap4 Duallistbox -->
<!-- AdminLTE App -->
{{-- <script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script> --}}
<!-- AdminLTE for demo purposes -->
{{-- <script src="{{asset('adminlte/dist/js/demo.js')}}"></script> --}}
<!-- DataTables -->
{{-- <script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script> --}}
{{-- <script src="{{asset('adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script> --}}

<script src="{{asset('adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('adminlte/plugins/select2/js/select2.full.min.js')}}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{asset('adminlte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js')}}"></script>
<!-- InputMask -->
<script src="{{asset('adminlte/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/inputmask/jquery.inputmask.min.js')}}"></script>
<!-- date-range-picker -->
<script src="{{asset('adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- bootstrap color picker -->
<script src="{{asset('adminlte/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Bootstrap Switch -->
<script src="{{asset('adminlte/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
<!-- BS-Stepper -->
{{-- <script src="{{asset('adminlte/plugins/bs-stepper/js/bs-stepper.min.js')}}"></script> --}}
<!-- dropzonejs -->
{{-- <script src="{{asset('adminlte/plugins/dropzone/min/dropzone.min.js')}}"></script> --}}
<!-- AdminLTE App -->
<script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('adminlte/dist/js/demo.js')}}"></script>


@yield('script')

<script>
  
  // $(function () {
  //   $('.datatable').DataTable({
  //     "paging": true,
  //     "lengthChange": false,
  //     "searching": true,
  //     "ordering": true,
  //     "info": true,
  //     "autoWidth": false,
  //     "responsive": false,
  //     "language": {
  //     "info": "เเสดงข้อมูลจาก 1 ถึง 10 ของข้อมูลทั้งหมด 30 ข้อมูล",
  //   }
  //   });

  // });
$('.select2').select2()

  function myFunction(){
    alert("Page is loaded");
  }
</script>

</body>
</html>
