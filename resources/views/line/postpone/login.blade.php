@extends('layouts.api3')
@section('style')
<link rel="stylesheet"
    href="{{ asset('adminlte/plugins/date-picker/css/datepicker.min.css') }}" />
<style>
    .round {
        border-radius: 50px;
        padding: 15px 25px;
    }

    ;

    .prog {
        margin-left: 20%;
    }

</style>
@endsection

@section('title')
เลือกวันและการรักษา
@endsection

@section('navigate')
<a href="{{ url('patient/appointment/index') }}">ผู้ใช้งาน</a>
@endsection

@section('content')
<div class="card card-warning">
    <div class="card-header">
        <h3 class="card-title">เลื่อนนัดทำฟัน</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-12">
                <form action="{{url('line/postpone/authen')}}" method="post">
                    @csrf
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-id-card-alt"></i></span>

                        <div class="info-box-content">
                            <div class="form-group">
                                <label class="label-control">เลขบัตรประชาชน</label>

                                {{-- <h2 class="info-box-text">Login</h2> --}}
                                <span class="info-box-number">
                                    <input type="text" class="form-control" name="id">
                                </span>


                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                    </div>
                    <div class="row mt-2 mb-2">
                        <div class="col-6"></div>
                        <!-- /.col -->
                        <div class="col-6">
                            <button type="submit" class="btn btn-warning btn-block">ต่อไป</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



            @endsection

