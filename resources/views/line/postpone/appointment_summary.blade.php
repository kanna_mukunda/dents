@extends('layouts.api2')
@section('style')
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
    integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
    crossorigin="anonymous" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

<link rel="stylesheet" href="{{asset('adminlte/plugins/date-picker/css/datepicker.min.css')}}" />
<style>
    .hidden {
        display: none
    }

    .show {
        display: block;
    }

</style>
@endsection

@section('active_patientAppointInfo')
    active
@endsection
@section('navigate')
    <a href="{{url('appointment/appointment_by_staff')}}">นัดทำฟัน</a>
@endsection
@section('title')
นัดทำฟัน คนไข้ Hn: {{$appointmentInfos->patient->hn}}
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">เลื่อนนัด</h3>
    </div>
    <div class="card card-body">
        <div class="row" >
            <div class="col-md-4"></div>
            <div class="col-md-4 " id="printDiv">

                <!-- Profile Image -->
                <div class="card  collapsed-card">
                    <div class="card-header">
                        <h3 class="card-title">ข้อมูลใบนัดทำฟัน</h3>

                        <div class="card-tools">

                            <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                              <i class="fas fa-minus"></i>
                            </button>
                          </div>

                    </div>
                    <div class="card-body box-profile">
                        <a href="index3.html" class="brand-link">
                            <img src="{{asset('adminlte/dist/img/AdminLTELogo.png')}}"
                                 alt="AdminLTE Logo"
                                 class="brand-image img-circle elevation-3"
                                 style="opacity: .8">
                            <span class="brand-text font-weight-light "> <h2>CDC DentalClinic</h2> </span>
                          </a>
                            <address>
                                <ul class="ml-4 mb-0 fa-ul text-muted">
                                    <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> 62/215 ถ.กลางเมือง ต.ในเมือง  อ.เมือง จ.ขอนแก่น 40000</li>
                                    <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> โทร #: 097 319 7753</li>
                                  </ul>
                            </address>
                        <hr>
                        <div class="row pb-2">

                            <div class="col-md-8 mt-2">
                                <h3 class="profile-username text-center">
                                    {{$appointmentInfos->patient->f_name.' '.$appointmentInfos->patient->l_name}}
                                </h3>

                                <p class="text-muted text-center">คนไข้ Hn: {{$appointmentInfos->patient->hn}}</p>
                            </div>
                        </div>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>นัดทำฟัน</b> <a class="float-right">{{$appointmentInfos->treatment->treatment_name}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>หมอเจ้าของ Case</b> <a class="float-right">{{$appointmentInfos->dentist->dent_name}} </a>
                            </li>
                            <li class="list-group-item">
                                <b>วันที่นัด</b> <a class="float-right">{{$appointmentInfos->date}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>เวลานัด</b> <a class="float-right">{{$appointmentInfos->start_time}} น.</a>
                            </li>
                        </ul>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            {{-- <div class="col-md-4">
                <a href="javascript:void(0)" class="btn btn-outline-primary" id="print_btn">ปริ้นใบนัดทำฟัน</a>
            </div> --}}

        </div>
    </div>
</div>
<hr>
<form action="{{url('line/postpone_store')}}" method="POST">
    @csrf
            <div class="card card-info card-outline m-2">
                <div class="card-title">

                </div>
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" style="width: 80px !important"
                            src="{{ asset('adminlte/dist/img/default-profile.jpg') }}"
                            alt="User profile picture">
                    </div>

                    <h3 class="profile-username text-center" id="patient_name_modal"> </h3>

                    <p class="text-muted text-center">คนไข้</p>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>วันที่</b> <a class="float-right" id="date_modal">

                                <input id="inputdatepicker" name="date" class="datepicker form-control" data-date-format="dd/mm/yyyy">

                            </a>
                        </li>
                        <li class="list-group-item">
                            <b>เวลา</b> <a class="float-right" id="time_modal">
                                <input id="inputdatepicker" name="time" class="timepicker form-control">

                            </a>
                        </li>

                    </ul>

                    <div id="comment_modal" class="mb-1">
                        <label class="label-control">หมายเหตุ</label>
                        <textarea class="form-control" rows="2" name="comment" id="comment"></textarea>
                    </div>
                    <input type="hidden" name="job_id" value={{$appointmentInfos->id}}>

                    <input type="hidden" name="treatment_id" value={{$appointmentInfos->treatment->id}}>
                    <input type="hidden" name="hn" value={{$appointmentInfos->patient->hn}}>
                    <input type="hidden" name="dent_id" value={{$appointmentInfos->dentist->id}}>
                    <button type="submit"
                        class="btn btn-primary btn-block">บันทึกการเลื่อนหนัด</button>
                </div>
                <!-- /.card-body -->
            </div>

</form>
@endsection


@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
    integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.th.min.js"
    integrity="sha512-cp+S0Bkyv7xKBSbmjJR0K7va0cor7vHYhETzm2Jy//ZTQDUvugH/byC4eWuTii9o5HN9msulx2zqhEXWau20Dg=="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.th.min.js" integrity="sha512-cp+S0Bkyv7xKBSbmjJR0K7va0cor7vHYhETzm2Jy//ZTQDUvugH/byC4eWuTii9o5HN9msulx2zqhEXWau20Dg==" crossorigin="anonymous"></script>
<script>
$(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            todayBtn: true,
            language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
            thaiyear: true //Set เป็นปี พ.ศ.
        }).datepicker("setDate", "0"); //กำหนดเป็นวันปัจุบัน

        $('.clinic_day_off_datepicker').datepicker({
            format: 'dd-mm-yyyy',
            todayBtn: true,
            thaiyear: true ,
            language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        }).datepicker("setDate", "0"); //กำหนดเป็นวันปัจุบัน


        var dateNow = new Date();

        $('.timepicker').datetimepicker({
            format: 'HH:mm',
            stepping: 15,
            enabledHours: [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],

            defaultDate:moment(dateNow).hours(0).minutes(0).seconds(0).milliseconds(0)

        })
    });
</script>

@endsection


