@extends('layouts.api2')


@section('active_patientAppointInfo')
    active
@endsection
@section('navigate')
    <a href="{{url('appointment/appointment_by_staff')}}">นัดทำฟัน</a>
@endsection
@section('title')
นัดทำฟัน คนไข้ Hn: {{$appointmentInfos->patient->hn}}
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">ผลการเลื่อนนัด</h3>
    </div>
    <div class="card card-body">
        <div class="row" >
            <div class="col-md-4"></div>
            <div class="col-md-4 " id="printDiv">

                <!-- Profile Image -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">ข้อมูลใบนัดทำฟัน</h3>

                    </div>
                    <div class="card-body box-profile">
                        <a href="index3.html" class="brand-link">
                            <img src="{{asset('adminlte/dist/img/AdminLTELogo.png')}}"
                                 alt="AdminLTE Logo"
                                 class="brand-image img-circle elevation-3"
                                 style="opacity: .8">
                            <span class="brand-text font-weight-light "> <h2>CDC DentalClinic</h2> </span>
                          </a>
                            <address>
                                <ul class="ml-4 mb-0 fa-ul text-muted">
                                    <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> 62/215 ถ.กลางเมือง ต.ในเมือง  อ.เมือง จ.ขอนแก่น 40000</li>
                                    <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> โทร #: 097 319 7753</li>
                                  </ul>
                            </address>
                        <hr>
                        <div class="row pb-2">

                            <div class="col-md-8 mt-2">
                                <h3 class="profile-username text-center">
                                    {{$appointmentInfos->patient->f_name.' '.$appointmentInfos->patient->l_name}}
                                </h3>

                                <p class="text-muted text-center">คนไข้ Hn: {{$appointmentInfos->patient->hn}}</p>
                            </div>
                        </div>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>นัดทำฟัน</b> <a class="float-right">{{$appointmentInfos->treatment->treatment_name}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>หมอเจ้าของ Case</b> <a class="float-right">{{$appointmentInfos->dentist->dent_name}} </a>
                            </li>
                            <li class="list-group-item">
                                <b>วันที่นัด</b> <a class="float-right">{{$appointmentInfos->date}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>เวลานัด</b> <a class="float-right">{{$appointmentInfos->start_time}} น.</a>
                            </li>
                        </ul>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            {{-- <div class="col-md-4">
                <a href="javascript:void(0)" class="btn btn-outline-primary" id="print_btn">ปริ้นใบนัดทำฟัน</a>
            </div> --}}

        </div>
    </div>
</div>

@endsection


