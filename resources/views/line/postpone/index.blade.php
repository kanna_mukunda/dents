@extends('layouts.api3')
@section('style')
<link rel="stylesheet"
    href="{{ asset('adminlte/plugins/date-picker/css/datepicker.min.css') }}" />
<style>
    .round {
        border-radius: 50px;
        padding: 15px 25px;
    }

    ;

    .prog {
        margin-left: 20%;
    }

</style>
@endsection

@section('title')
เลือกวันและการรักษา
@endsection

@section('navigate')
<a href="{{ url('patient/appointment/index') }}">ผู้ใช้งาน</a>
@endsection

@section('content')
<div class="card card-primary">

    <div class="card-body">
        <div class="row">
            <div class="col-md-12">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                  <div class="card-body box-profile">

                    <h3 class="profile-username text-center">ขั้นตอนการเลื่อนนัดหมาย</h3>


                    <ul class="list-group list-group-unbordered mb-3">
                      <li class="list-group-item">
                        <b>1. กรอกข้อมูลส่วนตัว</b> </li>

                      <li class="list-group-item">
                        <b>2. แสดงข้อมูลผู้ใช้งาน</b>
                      </li>
                      <li class="list-group-item">
                        <b>3. เลือกวันและเวลาที่ต้องการเลื่อนนัด</b>
                      </li>
                      <li class="list-group-item">
                        <b>4. ทำการเลื่อนนัดหมาย</b>
                      </li>

                      <li class="list-group-item">
                        <b>5. ตรวจสอบข้อมูลการนัดเลื่อนนัด</b>
                      </li>
                      <li class="list-group-item">
                        <b>6. เสร็จสิ้นการเลื่อนนัด</b>
                      </li>
                    </ul>

                    <a href="{{url('line/postpone/login')}}" class="btn btn-primary btn-block"><b>ทำการเลื่อนนัดหมาย</b></a>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->


        </div>
    </div>
</div>



            @endsection

