@extends('layouts.api2')
@section('style')
<link rel="stylesheet"
    href="{{ asset('adminlte/plugins/date-picker/css/datepicker.min.css') }}" />
<style>
    .round {
        border-radius: 50px;
        padding: 15px 25px;
    }

    ;

    .prog {
        margin-left: 20%;
    }

</style>
@endsection

@section('title')
เลือกวันและการรักษา
@endsection

@section('navigate')
<a href="{{ url('patient/appointment/index') }}">ผู้ใช้งาน</a>
@endsection

@section('content')
   {{-- <div class="row">
    <div class="col-1">

    </div>
    <div class="col-2">
        <button type="button" class="btn btn-success round">1</button>
    </div>
    <div class="col-1">

    </div>
    <div class="col-2">
        <button type="button" class="btn btn-default round">2</button>
    </div>
    <div class="col-1">

    </div>
    <div class="col-2">
        <button type="button" class="btn btn-default round">3</button>
    </div>

</div>
<hr> --}}

{{-- <h4 class="text-center">เลือกวันและการรักษา</h4> --}}
<form action="{{ url('patient/appointment/create') }}" method="post">
            @csrf
            <div class="card bg-info">
                {{-- <div class="card-header">
                    เลือกวันและการรักษาที่ต้องการ
                </div> --}}
               <div class="card-body">
                    <div class="form-group">
                        <label for="inputdatepicker" class="col-md-2 control-label">เลือกวัน</label>
                        <input id="inputdatepicker" name="date" class="datepicker form-control">
                    </div>
                    <div class="form-group">
                        <label for="inputdatepicker" class="control-label">เลือกอาการที่ต้องการรักษา</label>
                        <select class="form-control" name="treatment_id" id="treatment_id">
                            <option>เลือก..</option>
                            @foreach($treatments as $treatment)
                                <option value="{{ $treatment->id }}">{{ $treatment->treatment_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">ค้นหาเวลานัดหมอ</button>
                </div>
            </div>

            </form>

@endsection


@section('script')
            <script src="{{ asset('adminlte/plugins/date-picker/datepicker.min.js') }}">
            </script>
            <script src="{{ asset('adminlte/plugins/date-picker/datepicker.th.min.js') }}">
            </script>
            <script
                src="{{ asset('adminlte/plugins/date-picker/locales/bootstrap-datepicker.th.js') }}">
            </script>
            <script>
                $(document).ready(function () {
                    $('.datepicker').datepicker({
                        format: 'dd-mm-yyyy',
                        todayBtn: true,
                        language: 'th-th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                        // thaiyear: true  ,
                        orientation: "bottom right" //Set เป็นปี พ.ศ.
                    }).datepicker("setDate", "0"); //กำหนดเป็นวันปัจุบัน
                });

            </script>
            @endsection
