@extends('layouts.app')

@section('title')
 ดูอัตราส่วนเวลาต่อการรักษาของหมอ
@endsection
@section('treatment_skill_ratio')
active
@endsection

@section('navigate')
<a href="{{ url('treatment_skill_ratio/index') }}">อัตราส่วนเวลาต่อการรักษาของหมอ </a>
@endsection
@section('content')

<div class="col-md-4" style="position: absolute;left: 50%;transform: translate(-35%);">
    <a href="{{url('treatment_skill_ratio/edit/'.$ratios[0]->id)}} " class="btn btn-warning">แก้ไข</a>
    <br>
    <br>
    <div class="card card-info ">
        <div class="card-header">
            ข้อมูลอัตราเร็วหมอต่อการรักษา
        </div>
        <div class="card-body box-profile ">
            <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                    <b>ชื่อหมอ</b>
                    <a class="float-right">{{$ratios[0]->dentist->dent_name}}</a>
                </li>
                <li class="list-group-item">
                    <b>ชื่อการรักษา</b>
                    <a class="float-right">{{$ratios[0]->treat->treatment_name}}</a>
                </li>
                <li class="list-group-item">
                    <b>อัตราส่วนเวลาต่อการรักษา (1 : นาที)</b>
                    <a class="float-right">{{$ratios[0]->ratio}}</a>
                </li>
            </ul>
        </div>
        <!-- /.card-body -->
    </div>
</div>

@endsection