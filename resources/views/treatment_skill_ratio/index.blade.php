@extends('layouts.app')

@section('title')
 อัตราส่วนเวลาต่อการรักษาของหมอ
@endsection
@section('treatment_skill_ratio')
active
@endsection

@section('navigate')
<a href="{{ url('treatment_skill_ratio/index') }}">อัตราส่วนเวลาต่อการรักษาของหมอ </a>
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <a href="{{url('treatment_skill_ratio/create')}}" class="btn btn-primary col-md-2 mb-3">เพิ่ม</a>

    <div class="card">
      <div class="card-header">
        <h3 class="card-title">อัตราส่วนเวลาต่อการรักษาของหมอ</h3>

        <div class="card-tools">
          <form method="GET" action="{{ url('treatment_skill_ratio/index') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
            <div class="input-group">
              <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
              <span class="input-group-append">
                <button class="btn btn-secondary" type="submit">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap">
          <thead>
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">ชื่อหมอ</th>
              <th class="text-center">ชื่อการรักษา</th>
              <th class="text-center">อัตราส่วนเวลาต่อการรักษา (x5นาที)</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php $c = 1; ?>
            @foreach ($treatment_skill_ratios as $item)
            <tr>

              <td class="text-center">{{$c++}}</td>
              <td class="text-center">{{$item->dentist->dent_name}}</td>
              <td class="text-center">{{collect($item->treat)->isEmpty() ? '' : $item->treat->treatment_name }}</td>
              <td class="text-center">{{$item->ratio }}</td>
              <td>
                <a href="{{url('treatment_skill_ratio/show/'.$item->id)}}" class="btn btn-info btn-sm">ดู</a>
                <a href="{{url('treatment_skill_ratio/edit/'.$item->id)}}" class="btn btn-warning btn-sm">แก้ไข</a>
                <a href="{{url('treatment_skill_ratio/destroy/'.$item->id)}}" class="btn btn-danger btn-sm">ลบ</a>
              </td>
            </tr>

            @endforeach

          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
</div>
@endsection