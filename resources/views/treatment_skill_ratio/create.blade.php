@extends('layouts.app')

@section('title')
 เพิ่มอัตราส่วนเวลาต่อการรักษาของหมอ
@endsection
@section('treatment_skill_ratio')
active
@endsection

@section('navigate')
<a href="{{ url('treatment_skill_ratio/index') }}">อัตราส่วนเวลาต่อการรักษาของหมอ </a>
@endsection
@section('content')

<div class="card">
    <div class="card-header">
      <h3 class="card-title">เพิ่มอัตราส่วนการรักษาต่อหน่วยเวลา</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form role="form" action="{{ url('treatment_skill_ratio/store')}}" method="post">
        @csrf
        <div class="row">
          <div class="col-sm-12">
            <!-- text input -->
            <div class="form-group">
              <label>ชื่อหมอ</label>
              <select class="form-control" name="dent_id" id="dent_id">
                <option>เลือก..</option>
                @foreach ($dentists as $item)
                    <option value="{{$item->id}}"> {{$item->dent_name}} </option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label>ชื่อการรักษา</label>
              <select class="form-control" name="treatment_id" id="treatment_id">
                <option>เลือก..</option>
                @foreach ($treatments as $item)
                    <option value="{{$item->id}}"> {{$item->treatment_name}} </option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
              <!-- text input -->
              <div class="form-group">
                <label>อัตราส่วน ( 1 : 5นาที )</label>
                <input type="text" class="form-control"  name="ratio" id="ratio">
              </div>
            </div>
         
          </div>
          <button type="submit"class="btn btn-success">บันทึก</button>

      </form>
    </div>
    <!-- /.card-body -->
  </div>
@endsection