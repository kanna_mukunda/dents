@extends('layouts.app')
@section('title')
  งานของหมอ
@endsection
@section('navigate')
<a href="{{url('job/index')}}">การรักษา</a>
@endsection

@section('style')
<meta name="csrf-token" id="csrf_token" content="{{ csrf_token() }}" />

  <style>
    .hidden {
        display: none
    }

    .show {
        display: block;
    }
  </style>
@endsection
@section('content')
<div class="card card-primary card-outline mb-4">
  <div class="card-header">
      <div class="card-title">
          <span class="info-box-icon bg-primary rounded elevation-1 p-2"><i class="fas fa-notes-medical"></i></span>
          ตารางงานของหมอ
      </div>
      <div class="card-tools row">
        <a href="{{url('job/create')}}" class="btn btn-primary btn-block col-md-10">เพิ่มงานของหมอ</a>

          <button type="button" class="btn btn-tool col-md-1" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
          </button>
      </div>
  </div>
  <div class="card-body">
    <div class="row mb-3">
      <div class="col-md-2 offset-md-10">        
      </div>
    </div>
    <div class="table-responsive p-0">
      <table class="table table-hover text-nowrap datatable">
        <thead>
          <tr>
            <th>#</th>
            <th>วันที่นัดคนไข้</th>
            <th>ชื่อ - สกุลหมอ</th>
            <th>ขื่อ - สกุล คนไข้</th>
            <th>เลข HN</th>
            <th>การรักษา</th>
            <td>ห้องตรวจ</td>
            <th>เวลา</th>
            <th>สถานะ</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
            <?php $c = 1; ?>
            @foreach ($jobs as $item)
            <tr>
              <td>{{$c++}}</td>
              <td>{{$item->thDate}}</td>
              <td>{{$item->dentist->dent_name}}</td>
              <td>{{$item->patient->f_name}} {{$item->patient->l_name}}</td>
              <td>{{$item->patient->hn}}</td>
              <td>{{$item->treatment->treatment_name}}</td>
              <td>{{$item->dentist->dutyTime[0]->room->room_name}}</td>
              <td>{{$item->treatment_time}}</td>
              <td>{{$item->thStatus}}</td>
              <td>
                {{-- {{url('job/meetingconfirm/'.$item->id)}} --}}
                  <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-info"   class="btn btn-primary updateStatusBtn"
                        data-id= "{{$item->id}}" 
                        data-date= "{{$item->date}}"
                        data-thdate = "{{$item->thDate}}"
                        data-dent_id= "{{$item->dent_id}}"
                        data-dent_name = "{{$item->dentist->dent_name}}"
                        data-patient_id = "{{$item->patient_id}}"
                        data-patient_name = "{{$item->patient->f_name.' '.$item->patient->l_name}}"
                        data-hn = "{{$item->patient->hn}}"
                        data-treatment_id = "{{$item->treatment->id}}"
                        data-treatment_name = "{{$item->treatment->treatment_name}}"
                        data-treatment_time = "{{$item->treatment_time}}"
                        data-status = "{{$item->status}}"
                        data-time = "{{$item->treatment_time}}"
                  >เปลี่ยนสถานะ</a>
                  {{-- <a href="{{url('job/edit/'.$item->id)}}" class="btn btn-warning">แก้ไข</a> --}}
                  <a href="{{url('job/destroy/'.$item->id)}}" class="btn btn-danger">ลบ</a>
              </td>
              </tr>
             
            @endforeach
          
        </tbody>
      </table>
    </div>
  </div><!-- card-body-->
</div><!--card-->


<div class="modal fade"  id="modal-info">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">แก้ไขสถานะ</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="card card-info card-outline">
          <div class="card-body box-profile">
            <div class="text-center">
              <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" alt="User profile picture">
            </div>

            <h3 class="profile-username text-center" id="patient_name_modal">	</h3>

            <p class="text-muted text-center">คนไข้</p>

            <ul class="list-group list-group-unbordered mb-3">
              <li class="list-group-item">
                <b>วันที่นัดหมอ</b> <a class="float-right" id="date_modal">>2020-08-26</a>
              </li>
              <li class="list-group-item">
                <b>เวลานัด</b> <a class="float-right" id="time_modal">>11:40-11:40 น.</a>
              </li>
              <li class="list-group-item">
                <b>หมอผู้รับผิดชอบ</b> <a class="float-right" id="dent_name_modal">">หมอม่อน</a>
              </li>
              <li class="list-group-item">
                <b>การรักษา</b> <a class="float-right" id="treatment_modal">ปรับ</a>
              </li>
            </ul>
            <div class="form-group">
              <label  class="control-label">สถานะ</label>
              <select class="form-control"  id="status_modal">
                <option value="">เลือก..</option>
                <option value="reserve">นัดหมอแล้ว</option>
                <option value="processing">มาตามนัด</option>
                <option value="postpone">เลื่อนนัด</option>
                <option value="cancel">ยกเลิกนัด</option>
              </select>
             
            </div>
            <div id="comment_modal" class="hidden mb-1">
              <label class="label-control">หมายเหตุ</label>
              <textarea class="form-control" rows="2" name="comment" id="comment"></textarea>
            </div>
            <button type="button" id="save_btn_modal" class="btn btn-primary btn-block">บันทึกการมาตามนัด</button>
          </div> 
          <!-- /.card-body -->
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection

@section('script')
  <script>
      let id              
      let dent_id        
      let date          
      let thdate        
      let time            
      let patient_id      
      let patient_name    
      let hn              
      let treatment_name  
      let treatment_time  
      let status          
    $('.updateStatusBtn').click(function(){
      id              = $(this).data('id') 
      dent_id         = $(this).data('dent_id')
      date            = $(this).data('date')
      thdate          = $(this).data('thdate')
      time            = $(this).data('time')
      dent_name       = $(this).data('dent_name')
      patient_id      = $(this).data('patient_id')
      patient_name    = $(this).data('patient_name')
      hn              = $(this).data('hn')
      treatment_id    = $(this).data('treatment_id')
      treatment_name  = $(this).data('treatment_name')
      treatment_time  = $(this).data('treatment_time')
      status          = $(this).data('status')

      $('#patient_name_modal').html(patient_name)
      $('#dent_name_modal').html(dent_name)
      $('#date_modal').html(thdate)
      $('#time_modal').html(time)
      $('#treatment_modal').html(treatment_name)
      showHiddenModal()
    });

    $('#save_btn_modal').click(function (){
      console.log($('#status_modal').val())
      let _token = $('meta[name="csrf-token"]').attr('content');

      let params = {
          job_id: id,
          status: $('#status_modal').val(),
          comment: $('#comment').val(),
          _token: _token
      };
      console.log('params',params)
      $.ajax({
          type: 'POST',
          url: "/job/update_status",
          data: params,

          success: function (data) {
              showHiddenModal()
              window.location = '/job/index'
          },
          error: function (data) {

          }
      });
    });


    $('#status_modal').change(function(){
      if($(this).val() == 'cancel' || $(this).val() == 'postpone'){
        if($(this).val() == 'postpone'){
          alert('ท่านต้องการเลื่อนนัด\n\tระบบจะทำการยกเลิกนัดเดิม\n\tจากนั้นให้ท่านไปทำการนัดเวลาใหม่')
        }
        $('#comment_modal').removeClass('hidden')
        $('#comment_modal').addClass('show')
      }else{
        $('#comment_modal').removeClass('show')
        $('#comment_modal').addClass('hidden')
      }
    })


    function showHiddenModal() {
        if ($('#myModal').hasClass('hidden')) {
            $('#myModal').removeClass('hidden')
            $('#myModal').addClass('show')
        } else {
            $('#myModal').removeClass('show')
            $('#myModal').addClass('hidden')
        }
    }

    $('.close').click(function () {

        showHiddenModal()
    })

  </script>

@endsection