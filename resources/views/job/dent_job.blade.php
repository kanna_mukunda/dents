@extends('layouts.app')

@section('title')
งานของหมอ
@endsection

@section('style')
<style>
    div#treatment_history {
        height: 510px;
        overflow: auto;
    }

</style>
@endsection

@section('navigate')
<a href="{{url('job/index')}}">งานการรักษา</a>
@endsection
@section('content')
<div class="card">
    <div class="card-header"></div>
    <div class="card-body">
        <div class="row">
            {{-- from ข้อมูลคนไข้  --}}
            <div class="col-md-8">
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">HN</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="inputEmail3">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">เลขบัตรประชาชน</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="inputEmail3">
                    </div>
                </div> <!--row-->

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">ชื่อ</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="inputEmail3">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">สกุล</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="inputEmail3">
                    </div>
                </div> <!--row-->

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-1 col-form-label">เพศ</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="inputEmail3" value="ชาย">
                    </div>
                    <label for="inputEmail3" class="col-sm-1 col-form-label">อายุ</label>
                    <div class="col-sm-1">
                        <input type="text" class="form-control" id="inputEmail3" value="41">
                    </div>
                    <label for="inputEmail3" class="col-sm-1 col-form-label">ปี</label>

                    <label for="inputEmail3" class="col-sm-1 col-form-label">น้ำหนัก</label>
                    <div class="col-sm-1">
                        <input type="text" class="form-control" id="inputEmail3" value="45">
                    </div>
                    <label for="inputEmail3" class="col-sm-1 col-form-label">กก.</label>
                </div> <!--row-->

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">ว/ด/ป เกิด</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="inputEmail3" value="10 สิงหาคม 2522">
                    </div>
                    
                    <label for="inputEmail3" class="col-sm-2 col-form-label">เบอร์โทรศัพท์</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="inputEmail3" value="0933333333">
                    </div>
                </div> <!--row-->
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">ที่อยู่</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail3" value="102/23 หมู่ 23 ตำบลบ้านเป็ด อำเภอเมือง จังหวัดขอนแก่น">
                    </div>
                </div> <!--row-->
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">ญาติ</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="inputEmail3" value="ทองดี ทองเค">
                    </div>
                    <label for="inputEmail3" class="col-sm-2 col-form-label">เบอร์โทรศัพญาติ</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="inputEmail3" value="0933333333">
                    </div>
                </div> <!--row-->
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">โรคประจำตัว</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail3" value="-">
                    </div>
                </div> <!--row-->
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">ประวัติแพ้ยา</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail3" value="-">
                    </div>
                </div> <!--row-->
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">ประวัติแพ้สารเคมี</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail3" value="-">
                    </div>
                </div> <!--row-->

                <div class="card">
                    <div class="card-header">MEDICAL / DENTAL HISTORY</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="customCheckbox2" checked="">
                                    <label for="customCheckbox2" class="custom-control-label">Heart Trouble</label>
                                </div>
                            </div>
                            <div class="col-md-3">Rheumatic fever</div>
                            <div class="col-md-3">Hypotension</div>
                            <div class="col-md-3">Hypertension</div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">Blood Disease</div>
                            <div class="col-md-3">Tuberculasis</div>
                            <div class="col-md-3">Liver Disease</div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">Dusbetes melitus</div>
                            <div class="col-md-3">Allergy</div>
                            <div class="col-md-3">STD</div>
                            <div class="col-md-3">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Other</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="inputEmail3" value="-">
                                    </div>
                                </div> <!--row -->
                            </div>
                        </div>
                    </div>
                </div>
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </div>
            {{-- ประวัติการรักษา --}}
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        ประวัติการรักษา ของ 
                    </div>
                </div>
                <div class="card-body" id="treatment_history">
                    <div class="timeline timeline-inverse">
                        <!-- timeline time label -->
                        <div class="time-label">
                            <span class="bg-danger">
                                10 Feb. 2014
                            </span>
                        </div>
                        <!-- /.timeline-label -->
                        <!-- timeline item -->
                        <div>
                            <i class="fas fa-envelope bg-primary"></i>
    
                            <div class="timeline-item">
                                <span class="time"><i class="far fa-clock"></i> 12:05</span>
    
                                <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>
    
                                <div class="timeline-body">
                                    Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                    weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                    jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                    quora plaxo ideeli hulu weebly balihoo...
                                </div>
                                <div class="timeline-footer">
                                    <a href="#" class="btn btn-primary btn-sm">Read more</a>
                                    <a href="#" class="btn btn-danger btn-sm">Delete</a>
                                </div>
                            </div>
                        </div>
                        <!-- END timeline item -->
                        <!-- timeline item -->
                        <div>
                            <i class="fas fa-user bg-info"></i>
    
                            <div class="timeline-item">
                                <span class="time"><i class="far fa-clock"></i> 5 mins ago</span>
    
                                <h3 class="timeline-header border-0"><a href="#">Sarah Young</a> accepted your friend
                                    request
                                </h3>
                            </div>
                        </div>
                        <!-- END timeline item -->
                        <!-- timeline item -->
                        <div>
                            <i class="fas fa-comments bg-warning"></i>
    
                            <div class="timeline-item">
                                <span class="time"><i class="far fa-clock"></i> 27 mins ago</span>
    
                                <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>
    
                                <div class="timeline-body">
                                    Take me to your leader!
                                    Switzerland is small and neutral!
                                    We are more like Germany, ambitious and misunderstood!
                                </div>
                                <div class="timeline-footer">
                                    <a href="#" class="btn btn-warning btn-flat btn-sm">View comment</a>
                                </div>
                            </div>
                        </div>
                        <!-- END timeline item -->
                        <!-- timeline time label -->
                        <div class="time-label">
                            <span class="bg-success">
                                3 Jan. 2014
                            </span>
                        </div>
                        <!-- /.timeline-label -->
                        <!-- timeline item -->
                        <div>
                            <i class="fas fa-camera bg-purple"></i>
    
                            <div class="timeline-item">
                                <span class="time"><i class="far fa-clock"></i> 2 days ago</span>
    
                                <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>
    
                                <div class="timeline-body">
                                    <img src="http://placehold.it/150x100" alt="...">
                                    <img src="http://placehold.it/150x100" alt="...">
                                    <img src="http://placehold.it/150x100" alt="...">
                                    <img src="http://placehold.it/150x100" alt="...">
                                </div>
                            </div>
                        </div>
                        <!-- END timeline item -->
                        <div>
                            <i class="far fa-clock bg-gray"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
