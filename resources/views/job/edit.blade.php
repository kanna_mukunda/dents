@extends('layouts.app')

@section('title')
    แก้ข้อมูลงานของหมอ
@endsection
@section('content')

<div class="card card-warning">
    <div class="card-header">
      <h3 class="card-title">แก้ข้อมูลงานของหมอ</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form role="form" action="{{ url('job/update/'.$job->id) }}" method="post">
        @csrf
        @method('patch')
        <div class="row">
          <div class="col-sm-12">
            <!-- text input -->
            <div class="form-group">
              <label>วันที่</label>
              <input type="text" class="form-control"  name="date" id="date" value="2020-08-26">
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label>ชื่อ - สกุลหมอ</label>
              <select class="form-control" name="dent_id" id="dent_id">
                <option>เลือก..</option>
                @foreach ($dentists as $item)
                  <option value="{{$item->id}}" {{$item->id == $job->dent_id ? 'selected' : ''}}>{{$item->dent_name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label>การรักษา</label>
              <select class="form-control" name="treatment_id" id="treatment_id">
                <option>เลือก..</option>
                @foreach ($treatments as $item)
                  <option value="{{$item->id}}" {{$item->id == $job->treatment_id ? 'selected' : ''}}>{{$item->treatment_name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label>ชื่อคนไข้</label>
              <select class="form-control" name="patient_id" id="patient_id">
                <option>เลือก..</option>
                @foreach ($patients as $item)
                  <option value="{{$item->id}}" {{$item->id == $job->patient_id ? 'selected' : ''}}>{{$item->name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label>เวลา</label>
              <input type="text" name="time" id="time" class="form-control" value="10:00">
            </div>
        </div>
        
          <button type="submit"class="btn btn-success">บันทึก</button>

      </form>
    </div>
    <!-- /.card-body -->
  </div>
@endsection