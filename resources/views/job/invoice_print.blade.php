
@extends('layouts.invoice_print')
@section('content')

          <div class="invoice p-3 mb-3">
              <div id="invoice">
                  <!-- title row -->
                  <div class="row">
                      <div class="col-12">
                          <h4>
                              <i class="fas fa-globe"></i> AdminLTE, Inc.
                              <small class="float-right">Date: 2/10/2014</small>
                          </h4>
                      </div>
                      <!-- /.col -->
                  </div>
                  <!-- info row -->
                  <div class="row invoice-info">
                      <div class="col-sm-4 invoice-col">
                          From
                          <address>
                              <strong>Admin, Inc.</strong><br>
                              795 Folsom Ave, Suite 600<br>
                              San Francisco, CA 94107<br>
                              Phone: (804) 123-5432<br>
                              Email: info@almasaeedstudio.com
                          </address>
                      </div>
                      <!-- /.col -->
                      <div class="col-sm-4 invoice-col">
                          To
                          <address>
                              <strong>John Doe</strong><br>
                              795 Folsom Ave, Suite 600<br>
                              San Francisco, CA 94107<br>
                              Phone: (555) 539-1037<br>
                              Email: john.doe@example.com
                          </address>
                      </div>
                      <!-- /.col -->
                      <div class="col-sm-4 invoice-col">
                          <b>Invoice #007612</b><br>
                          <br>
                          <b>Order ID:</b> 4F3S8J<br>
                          <b>Payment Due:</b> 2/22/2014<br>
                          <b>Account:</b> 968-34567
                      </div>
                      <!-- /.col -->
                  </div>
                  <!-- /.row -->

                  <!-- Table row -->
                  <div class="row">
                      <div class="col-12 table-responsive">
                          <table class="table table-striped">
                              <thead>
                                  <tr>
                                      <th>จำนวน</th>
                                      <th>รายการรักษา</th>
                                      <th>Description</th>
                                      <th>ราคา (บาท)</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td>1</td>
                                      <td>Call of Duty</td>
                                      <td>El snort testosterone trophy driving gloves handsome</td>
                                      <td>200</td>
                                  </tr>
                                  <tr>
                                      <td>1</td>
                                      <td>Grown Ups Blue Ray</td>
                                      <td>Tousled lomo letterpress</td>
                                      <td>400.50</td>
                                  </tr>
                              </tbody>
                          </table>
                      </div>
                      <!-- /.col -->
                  </div>
                  <!-- /.row -->

                  <div class="row">
                      <!-- accepted payments column -->
                      <div class="col-6">
                          <p class="lead">Payment Methods:</p>
                          <img src="{{asset('adminlte/dist/img/credit/visa.png')}}" alt="Visa">
                          <img src="{{asset('adminlte/dist/img/credit/mastercard.png')}}" alt="Mastercard">
                          <img src="{{asset('adminlte/dist/img/credit/american-express.png')}}" alt="American Express">
                          <img src="{{asset('adminlte/dist/img/credit/paypal2.png')}}" alt="Paypal">

                          <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                              Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya
                              handango imeem
                              plugg
                              dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                          </p>
                      </div>
                      <!-- /.col -->
                      <div class="col-6">
                          <p class="lead">Amount Due 2/22/2014</p>

                          <div class="table-responsive">
                              <table class="table">
                                  <tbody>
                                      <tr>
                                          <th>รวม:</th>
                                          <td>600.50 บาท</td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                      <!-- /.col -->
                  </div>
                  <!-- /.row -->
              </div><!-- id invoice -->
          </div>
      
  @endsection

  @section('script')

  <script>
    $(document).ready(function(){
        var printContents = document.getElementById("invoice").innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents
    });
  </script>
@endsection

