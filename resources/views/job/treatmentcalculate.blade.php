@extends('layouts.app')
@section('title')
    คำนวณค่ารักษา
@endsection
@section('navigate')
<a href="{{url('job/index')}}">งานการรักษา</a> 
@endsection

@section('treatment_cal')
    active
@endsection

@section('style')
    <style>
        .small-box>.inner {
            padding: 4px;
            text-align: center
        }
        th{
            text-align: center
        }
    </style>
@endsection

@section('content')
<div class="card card-primary card-outline">
    <div class="card-header">
        <div class="card-title">
            <span class="info-box-icon bg-primary rounded  elevation-1 p-2"><i class="fab fa-bitcoin"></i></span>
            ตารางคำนวณค่ารักษา
        </div>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive p-0">
            <table class="table table-hover text-nowrap datatable">
              <thead>
                <tr>
                  <th>#</th>
                  {{-- <th>Invoice No.</th> --}}
                  <th>วันที่</th>
                  <th>HN</th>
                  <th>ขื่อ - สกุล คนไข้</th>
                  <th>การรักษา</th>
                  <th>ชื่อ - สกุลหมอ</th>
                  {{-- <th>เวลา</th> --}}
                  <th>สถานะ</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                  <?php $c = 1; ?>
                  @foreach ($jobs as $item)
                  <tr>
                    <td>{{$c++}}</td>
                    {{-- <td>{{'inv-'.$item->date.$item->patient_id.$item->treatment_id}}</td> --}}
                    <td>{{$item->date}}</td>
                    <td>{{$item->patient->hn}}</td>
                    <td>{{$item->patient->f_name}}  {{$item->patient->l_name}}</td>
                    <td>{{$item->treatment->treatment_name}}</td>
                    <td>{{$item->dentist->dent_name}}</td>
                    {{-- <td>{{$item->convertSlotToTime($item->slot_start, $item->slot_end, $item->period_type)}}</td> --}}
                    <td class="text-center">
                        <span class="btn {{$item->status == 'complete' ? 'btn-outline-success' :'btn-outline-danger'}} ">
                                {{$item->changeJobStatus($item->status)}}
                        </span>
                    </td>
                    <td>
                      {{-- {{url('job/meetingconfirm/'.$item->id)}} --}}
                      {{-- <div class="row">
                          <div class="col-md-6"> --}}
                            {{-- @if ($item->status == 'complete') --}}
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-info" class="btn btn-primary ">คำนวนค่ารักษา</a>
                            {{-- @endif --}}
                          {{-- </div>
                          <div class="col-md-3"> --}}
                            <a href="{{url('job/edit/'.$item->id)}}" class="btn btn-warning ">แก้ไข</a>
                          {{-- </div>
                          <div class="col-md-3"> --}}
                            <a href="{{url('job/destroy/'.$item->id)}}" class="btn btn-danger ">ลบ</a>
                          {{-- </div>
                      </div> --}}
                     
                        
                    </td>
                    </tr>
                   
                  @endforeach
                
              </tbody>
            </table>
          </div>
        </table>
    </div>
  </div>


  <div class="modal fade"  id="modal-info">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Invoice</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          {{-- <div class="card card-info card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" alt="User profile picture">
              </div>
  
              <h3 class="profile-username text-center">9PoM9dV1XH	</h3>
  
              <p class="text-muted text-center">คนไข้</p>
  
              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <b>วันที่นัดหมอ</b> <a class="float-right">2020-08-26</a>
                </li>
                <li class="list-group-item">
                  <b>เวลานัด</b> <a class="float-right">11:40-11:40 น.</a>
                </li>
                <li class="list-group-item">
                  <b>ชื่อเจ้าผู้รับผิดชอบ</b> <a class="float-right">หมอม่อน</a>
                </li>
                <li class="list-group-item">
                  <b>การรักษา</b> <a class="float-right">ปรับ</a>
                </li>
                <li class="list-group-item">
                    <b>สถานะ</b> <a class="float-right">รักษาเสร็จแล้ว</a>
                  </li>
              </ul>
              <div class="form-group">
                <label  class="control-label">สถานะ</label>
                <select class="form-control">
                  <option>เลือก..</option>
                  <option>มาตามนัด</option>
                  <option>เลื่อนนัด</option>
                  <option>ยกเลิกนัด</option>
                </select>
              </div>
             
              <button type="button" class="btn btn-primary btn-block">บันทึกการมาตามนัด</button>
            </div>
            <!-- /.card-body -->
          </div> --}}
          <div class="invoice p-3 mb-3">
            <div id="invoice">
                <!-- title row -->
                <div class="row">
                    <div class="col-12">
                        <h4>
                        <i class="fas fa-globe"></i> AdminLTE, Inc.
                        <small class="float-right">Date: 2/10/2014</small>
                        </h4>
                    </div>
                    <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        From
                        <address>
                        <strong>Admin, Inc.</strong><br>
                        795 Folsom Ave, Suite 600<br>
                        San Francisco, CA 94107<br>
                        Phone: (804) 123-5432<br>
                        Email: info@almasaeedstudio.com
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        To
                        <address>
                        <strong>John Doe</strong><br>
                        795 Folsom Ave, Suite 600<br>
                        San Francisco, CA 94107<br>
                        Phone: (555) 539-1037<br>
                        Email: john.doe@example.com
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        <b>Invoice #007612</b><br>
                        <br>
                        <b>Order ID:</b> 4F3S8J<br>
                        <b>Payment Due:</b> 2/22/2014<br>
                        <b>Account:</b> 968-34567
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- Table row -->
                <div class="row">
                    <div class="col-12 table-responsive">
                        <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>จำนวน</th>
                            <th>รายการรักษา</th>
                            <th>Description</th>
                            <th>ราคา  (บาท)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Call of Duty</td>
                            <td>El snort testosterone trophy driving gloves handsome</td>
                            <td>200</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Grown Ups Blue Ray</td>
                            <td>Tousled lomo letterpress</td>
                            <td>400.50</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                <!-- accepted payments column -->
                    <div class="col-6">
                        <p class="lead">Payment Methods:</p>
                        <img src="{{asset('adminlte/dist/img/credit/visa.png')}}" alt="Visa">
                        <img src="{{asset('adminlte/dist/img/credit/mastercard.png')}}" alt="Mastercard">
                        <img src="{{asset('adminlte/dist/img/credit/american-express.png')}}" alt="American Express">
                        <img src="{{asset('adminlte/dist/img/credit/paypal2.png')}}" alt="Paypal">

                        <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem
                        plugg
                        dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                        </p>
                    </div>
                    <!-- /.col -->
                    <div class="col-6">
                        <p class="lead">Amount Due 2/22/2014</p>

                        <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr>
                            <th>รวม:</th>
                            <td>600.50 บาท</td>
                            </tr>
                        </tbody></table>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div><!-- id invoice -->
            <!-- this row will not appear when printing -->
            <div class="row no-print">
              <div class="col-12">
                <a href="{{url('job/invoice-print/1')}}" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a>
                <button type="button" class="btn btn-success float-right">
                    <i class="far fa-credit-card"></i> บันทึก
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  @endsection

  @section('script')

  <script>
      function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
  </script>
      
  @endsection
