@extends('layouts.app')
@section('title')
  เพิ่มงานของหมอ
@endsection
@section('navigate')
<a href="{{url('job/index')}}">การรักษา</a>
@endsection

@section('style')
  
@endsection
@section('content')
<div class="card card-primary card-outline mb-4">
  <div class="card-header">
      <div class="card-title">
          <span class="info-box-icon bg-primary rounded elevation-1 p-2"><i class="fas fa-notes-medical"></i></span>
          ฟอร์มเพิ่มข้อมูลงานของหมอ
      </div>
      <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
          </button>
      </div>
  </div>
  <div class="card-body">
    <form role="form" action="{{ url('job/store')}}" method="post">
      @csrf
      <div class="row">
        <div class="col-sm-12">
          <!-- text input -->
          <div class="form-group">
            <label>วันที่</label>
            <input type="text" class="form-control"  name="date" id="date" value="2020-08-26">
          </div>
        </div>
        <div class="col-sm-12">
          <div class="form-group">
            <label>ชื่อ - สกุลหมอ</label>
            <select class="form-control" name="dent_id" id="dent_id">
              <option>เลือก..</option>
              @foreach ($dentists as $item)
                <option value="{{$item->id}}">{{$item->dent_name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="form-group">
            <label>การรักษา</label>
            <select class="form-control" name="treatment_id" id="treatment_id">
              <option>เลือก..</option>
              @foreach ($treatments as $item)
                <option value="{{$item->id}}">{{$item->treatment_name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="form-group">
            <label>ชื่อคนไข้</label>
            <select class="form-control" name="patient_id" id="patient_id">
              <option>เลือก..</option>
              @foreach ($patients as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="form-group">
            <label>เวลา</label>
            <input type="text" name="time" id="time" class="form-control" value="10:00">
          </div>
      </div>
      
        <button type="submit"class="btn btn-success">บันทึก</button>

    </form>
  </div><!--card-body-->
</div><!--card-->

@endsection