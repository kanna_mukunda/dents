@extends('layouts.app')
 @section('style')
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />
 @endsection

@section('content')
<form action="{{url('appointment/patient_select_time')}}" method="post">
    @csrf
    <div class="form-group">
        <label for="inputdatepicker" class="col-md-2 control-label">datepicker</label>
            <input id="inputdatepicker" name="date" class="datepicker form-control" data-date-format="mm/dd/yyyy">
    </div>
    <div class="form-group">
        <select class="form-control" name="treatment_id" id="treatment_id">
            <option>เลือก..</option>
            @foreach ($treatments as $treatment)
                <option value="{{ $treatment->id }}">{{ $treatment->treatment_name }}</option>
            @endforeach
        </select>
    </div>
    <button type="submit"class="btn btn-info">ค้นหาเวลานัดหมอ</button>
</form>

@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.th.min.js" integrity="sha512-cp+S0Bkyv7xKBSbmjJR0K7va0cor7vHYhETzm2Jy//ZTQDUvugH/byC4eWuTii9o5HN9msulx2zqhEXWau20Dg==" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                todayBtn: true,
                language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                thaiyear: true              //Set เป็นปี พ.ศ.
            }).datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน
        });
  
  </script>
@endsection