<div class="card">
    <div class="card-header p-2">
      <ul class="nav nav-pills">
        <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">ข้อมูลหมอเข้าเวร</a></li>
        <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">ข้อมูลการนัดทำฟันของคนไข้</a></li>
      </ul>
    </div><!-- /.card-header -->
    <div class="card-body">
      <div class="tab-content">
        <div class="tab-pane active" id="activity">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>
            
                    <div class="info-box-content">
                      <span class="info-box-text">หมอเข้าเวร</span>
                      <span class="info-box-number">
                        4
                        <small>คน</small>
                      </span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box mb-3">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-up"></i></span>
            
                    <div class="info-box-content">
                      <span class="info-box-text">งานปรับ</span>
                      <span class="info-box-number">4 <small>Case</small></span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->
            
                <!-- fix for small devices only -->
                <div class="clearfix hidden-md-up"></div>
            
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box mb-3">
                    <span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>
            
                    <div class="info-box-content">
                      <span class="info-box-text">งานขูดหินปูน</span>
                      <span class="info-box-number">4 <small>Case</small></span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                  <div class="info-box mb-3">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
            
                    <div class="info-box-content">
                      <span class="info-box-text">งานถอนฟัน</span>
                      <span class="info-box-number">2 <small>Case</small></span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->
              </div>
              <div class="row">
                <div class="col-md-3">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                          <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/user5-128x128.jpg')}}" alt="User profile picture">
                          </div>
                    
                          <h3 class="profile-username text-center">หมอนุ่น</h3>
                    
                          <p class="text-muted text-center"></p>
                    
                          <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                              <b>ปรับ</b> <a class="float-right">2 case</a>
                            </li>
                            <li class="list-group-item">
                              <b>ขูดหินปูน</b> <a class="float-right">1 case</a>
                            </li>
                            <li class="list-group-item">
                              <b>ถอนฟัน</b> <a class="float-right">1 case</a>
                            </li>
                          </ul>
                    
                        </div>
                        <!-- /.card-body -->
                      </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                          <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="User profile picture">
                          </div>
                    
                          <h3 class="profile-username text-center">หมอแทน</h3>
                    
                    
                          <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>ปรับ</b> <a class="float-right"> - </a>
                              </li>
                              <li class="list-group-item">
                                <b>ขูดหินปูน</b> <a class="float-right">1 case</a>
                              </li>
                              <li class="list-group-item">
                                <b>ถอนฟัน</b> <a class="float-right">1 case</a>
                              </li>
                          </ul>
                    
                       
                        </div>
                        <!-- /.card-body -->
                      </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                          <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/user3-128x128.jpg')}}" alt="User profile picture">
                          </div>
                    
                          <h3 class="profile-username text-center">หมอน๊อต</h3>
                    
                    
                          <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>ปรับ</b> <a class="float-right">2 case</a>
                              </li>
                              <li class="list-group-item">
                                <b>ขูดหินปูน</b> <a class="float-right">2 case</a>
                              </li>
                              <li class="list-group-item">
                                <b>ถอนฟัน</b> <a class="float-right">- </a>
                              </li>
                          </ul>
                    
                        </div>
                        <!-- /.card-body -->
                      </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                          <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" alt="User profile picture">
                          </div>
                    
                          <h3 class="profile-username text-center">หมอเอ</h3>
                    
                    
                          <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>ปรับ</b> <a class="float-right"> - </a>
                              </li>
                              <li class="list-group-item">
                                <b>ขูดหินปูน</b> <a class="float-right">- </a>
                              </li>
                              <li class="list-group-item">
                                <b>ถอนฟัน</b> <a class="float-right">1 case</a>
                              </li>
                          </ul>
                    
                        </div>
                        <!-- /.card-body -->
                      </div>
                </div>
            </div>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="timeline">
          <!-- The timeline -->
          <table class="table table-striped projects">
            <thead>
                <tr>
                    <th style="width: 1%">
                        #
                    </th>
                    <th style="width: 10%">

                    </th>
                    <th class="text-center" style="width:60%" colspan="4">
                        ห้องตรวจ
                    </th>

                    <th>

                    </th>
                </tr>
                <tr>
                    <th style="width: 1%">

                    </th>
                    <th style="width: 10%">
                        เวลา
                    </th>
                    <th class="room">1</th>
                    <th class="room">2</th>
                    <th class="room">3</th>
                    <th class="room">4</th>
                    <th>
                        สถานะการตรวจ
                    </th>

                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        <a>
                            10.00 - 10.15
                        </a>
                        <br>
                        <small>
                            &nbsp;
                        </small>
                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-success img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="User profile picture">
                        <p class="text-muted text-center">Jonathan Burke </p>
                        <p>ปรับ</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-warning img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/user5-128x128.jpg')}}" alt="User profile picture">
                        <p class="text-muted text-center">Nina Mcintire</p>
                        <p>ถอนฟัน</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-success img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/user3-128x128.jpg')}}" alt="User profile picture">
                        <p class="text-muted text-center">Sarah Ross</p>
                        <p>ปรับ</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-success img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" alt="User profile picture">
                        <p class="text-muted text-center">Adam Jones</p>
                        <p>ปรับ</p>

                    </td>
                    <td class="project_progress">
                        <div class="row">
                            <div class="col-md-6 pt-5"">
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-green" role="progressbar" aria-volumenow="75"
                                        aria-volumemin="0" aria-volumemax="100" style="width: 75%">
                                    </div>
                                </div>
                                <small>
                                    75% Complete
                                </small>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item text-success">
                                        <b>รักษาเสร็จ</b> <a class="float-right"><span
                                                class="badge badge-success right">3</span></a>
                                    </li>
                                    <li class="list-group-item text-warning">
                                        <b>กำลังรักษา</b> <a class="float-right"><span
                                                class="badge badge-warning right">1</span></a>
                                    </li>

                                </ul>

                            </div>
                        </div>





                    </td>

                </tr>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        <a>
                            10.15 - 10.30
                        </a>
                        <br>
                        <small>
                            &nbsp;
                        </small>
                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-success img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/user6-128x128.jpg')}}" alt="User profile picture">
                        <p class="text-muted text-center">Jonathan Burke </p>
                        <p>ขูดหินปูน</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-warning img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/user5-128x128.jpg')}}" alt="User profile picture">
                        <p class="text-muted text-center">Nina Mcintire</p>
                        <p>ถอนฟัน</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-cancel img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/user7-128x128.jpg')}}" alt="User profile picture">
                        <p class="text-muted text-center">Denna jone</p>
                        <p>ยกเลิกนัด</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-warning img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/user8-128x128.jpg')}}" alt="User profile picture">
                        <p class="text-muted text-center">Mama Tomyam</p>
                        <p>ขูดหินปูน</p>

                    </td>
                    <td class="project_progress">
                        <div class="row">
                            <div class="col-md-6 pt-5">
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-green" role="progressbar" aria-volumenow="50"
                                        aria-volumemin="0" aria-volumemax="100" style="width: 50%">
                                    </div>
                                </div>
                                <small>
                                    50% Complete
                                </small>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item text-success">
                                        <b>รักษาเสร็จ</b> <a class="float-right"><span
                                                class="badge badge-success right">1</span></a>
                                    </li>
                                    <li class="list-group-item text-warning">
                                        <b>กำลังรักษา</b> <a class="float-right"><span
                                                class="badge badge-warning right">2</span></a>
                                    </li>
                                    <li class="list-group-item text-cancel">
                                        <b>ยกเลิกนัด</b> <a class="float-right"><span
                                                class="badge badge-warning right">1</span></a>
                                    </li>

                                </ul>

                            </div>
                        </div>





                    </td>

                </tr>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        <a>
                            10.30 - 10.45
                        </a>
                        <br>
                        <small>
                            &nbsp;
                        </small>
                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-success img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" alt="User profile picture">
                        <p class="text-muted text-center">Gordon Brawn </p>
                        <p>ปรับ</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-success img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/user5-128x128.jpg')}}" alt="User profile picture">
                        <p class="text-muted text-center">Nina Mcintire</p>
                        <p>ถอนฟัน</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-empty img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/boxed-bg.png')}}" alt="User profile picture">
                        <p class="text-muted text-center">ว่าง</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-success img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/user8-128x128.jpg')}}" alt="User profile picture">
                        <p class="text-muted text-center">Mama Tomyam</p>
                        <p>ขูดหินปูน</p>
                        
                    </td>
                    <td class="project_progress">
                        <div class="row">
                            <div class="col-md-6 pt-3">
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-green" role="progressbar" aria-volumenow="100"
                                        aria-volumemin="0" aria-volumemax="100" style="width: 100%">
                                    </div>
                                </div>
                                <small>
                                    100% Complete
                                </small>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item text-success">
                                        <b>รักษาเสร็จ</b> <a class="float-right"><span
                                                class="badge badge-success right">3</span></a>
                                    </li>
                                

                                </ul>

                            </div>
                        </div>





                    </td>

                </tr>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        <a>
                            11.00 - 11.15
                        </a>
                        <br>
                        <small>
                            &nbsp;
                        </small>
                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-info img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/avatar.png')}}" alt="User profile picture">
                        <p class="text-muted text-center">นาย ก </p>
                        <p>รอการรักษา</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-info img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/avatar2.png')}}" alt="User profile picture">
                        <p class="text-muted text-center">นาง ข</p>
                        <p>รอการรักษา</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-info img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/avatar3.png')}}" alt="User profile picture">
                        <p class="text-muted text-center">นส.ค</p>
                        <p>รอการรักษา</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-empty img-fluid img-circle "
                        src="{{asset('adminlte/dist/img/boxed-bg.png')}}" alt="User profile picture">
                    <p class="text-muted text-center">ว่าง</p>
                    <p>

                    </td>
                    <td class="project_progress">
                        <div class="row">
                            <div class="col-md-6 pt-3">
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-green" role="progressbar" aria-volumenow="0"
                                        aria-volumemin="0" aria-volumemax="0" style="width: 0%">
                                    </div>
                                </div>
                                <small>
                                    0% Complete
                                </small>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item text-info">
                                        <b>รอการรักษา</b> <a class="float-right"><span
                                                class="badge badge-info right">3</span></a>
                                    </li>
                                

                                </ul>

                            </div>
                        </div>
                    </td>

                </tr>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        <a>
                            11.15 - 11.30
                        </a>
                        <br>
                        <small>
                            &nbsp;
                        </small>
                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-info img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/avatar.png')}}" alt="User profile picture">
                        <p class="text-muted text-center">นาย คอ </p>
                        <p>รอการรักษา</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-info img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/avatar2.png')}}" alt="User profile picture">
                        <p class="text-muted text-center">นาง จอ</p>
                        <p>รอการรักษา</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-info img-fluid img-circle "
                            src="{{asset('adminlte/dist/img/avatar3.png')}}" alt="User profile picture">
                        <p class="text-muted text-center">นส.รอ</p>
                        <p>รอการรักษา</p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-info img-fluid img-circle "
                        src="{{asset('adminlte/dist/img/avatar4.png')}}" alt="User profile picture">
                    <p class="text-muted text-center">นส.ฌา</p>
                    <p>รอการรักษา</p>

                    </td>
                    <td class="project_progress">
                        <div class="row">
                            <div class="col-md-6 pt-3">
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-green" role="progressbar" aria-volumenow="0"
                                        aria-volumemin="0" aria-volumemax="0" style="width: 0%">
                                    </div>
                                </div>
                                <small>
                                    0% Complete
                                </small>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item text-info">
                                        <b>รอการรักษา</b> <a class="float-right"><span
                                                class="badge badge-info right">4</span></a>
                                    </li>
                                

                                </ul>

                            </div>
                        </div>

                </tr>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        <a>
                            11.30 - 11.45
                        </a>
                        <br>
                        <small>
                            &nbsp;
                        </small>
                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-empty img-fluid img-circle "
                        src="{{asset('adminlte/dist/img/boxed-bg.png')}}" alt="User profile picture">
                    <p class="text-muted text-center">ว่าง</p>


                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-empty img-fluid img-circle "
                        src="{{asset('adminlte/dist/img/boxed-bg.png')}}" alt="User profile picture">
                    <p class="text-muted text-center">ว่าง</p>


                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-empty img-fluid img-circle "
                        src="{{asset('adminlte/dist/img/boxed-bg.png')}}" alt="User profile picture">
                    <p class="text-muted text-center">ว่าง</p>
                    <p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-empty img-fluid img-circle "
                        src="{{asset('adminlte/dist/img/boxed-bg.png')}}" alt="User profile picture">
                    <p class="text-muted text-center">ว่าง</p>
                    <p>

                    </td>
                    <td class="project_progress">
                        <div class="row">
                            <div class="col-md-6 pt-3">
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-green" role="progressbar" aria-volumenow="0"
                                        aria-volumemin="0" aria-volumemax="0" style="width: 0%">
                                    </div>
                                </div>
                                <small>
                                    0% Complete
                                </small>
                            </div>
                            <div class="col-md-6">
                                {{-- <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item text-info">
                                        <b>รอการรักษา</b> <a class="float-right"><span
                                                class="badge badge-info right">3</span></a>
                                    </li>
                                

                                </ul> --}}

                            </div>
                        </div>
                    </td>

                </tr>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        <a>
                            11.45 - 12.00
                        </a>
                        <br>
                        <small>
                            &nbsp;
                        </small>
                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-empty img-fluid img-circle "
                        src="{{asset('adminlte/dist/img/boxed-bg.png')}}" alt="User profile picture">
                    <p class="text-muted text-center">ว่าง</p>


                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-empty img-fluid img-circle "
                        src="{{asset('adminlte/dist/img/boxed-bg.png')}}" alt="User profile picture">
                    <p class="text-muted text-center">ว่าง</p>


                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-empty img-fluid img-circle "
                        src="{{asset('adminlte/dist/img/boxed-bg.png')}}" alt="User profile picture">
                    <p class="text-muted text-center">ว่าง</p>
                    <p>

                    </td>
                    <td class="text-center">

                        <img class="profile-user-img-empty img-fluid img-circle "
                        src="{{asset('adminlte/dist/img/boxed-bg.png')}}" alt="User profile picture">
                    <p class="text-muted text-center">ว่าง</p>
                    <p>

                    </td>
                    <td class="project_progress">
                        <div class="row">
                            <div class="col-md-6 pt-3">
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-green" role="progressbar" aria-volumenow="0"
                                        aria-volumemin="0" aria-volumemax="0" style="width: 0%">
                                    </div>
                                </div>
                                <small>
                                    0% Complete
                                </small>
                            </div>
                            <div class="col-md-6">
                                {{-- <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item text-info">
                                        <b>รอการรักษา</b> <a class="float-right"><span
                                                class="badge badge-info right">3</span></a>
                                    </li>
                                

                                </ul> --}}

                            </div>
                        </div>
                    </td>

                </tr>

            </tbody>
        </table>
        </div>
        <!-- /.tab-pane -->

       
      </div>
      <!-- /.tab-content -->
    </div><!-- /.card-body -->
  </div>




<div class="card">
    <div class="card-header">
        <h5  class="card-title">ข้อมูลหมอเข้าเวร</h5>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
           
          </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
              <div class="info-box">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>
        
                <div class="info-box-content">
                  <span class="info-box-text">หมอเข้าเวร</span>
                  <span class="info-box-number">
                    4
                    <small>คน</small>
                  </span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
              <div class="info-box mb-3">
                <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-up"></i></span>
        
                <div class="info-box-content">
                  <span class="info-box-text">งานปรับ</span>
                  <span class="info-box-number">4 <small>Case</small></span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
        
            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>
        
            <div class="col-12 col-sm-6 col-md-3">
              <div class="info-box mb-3">
                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>
        
                <div class="info-box-content">
                  <span class="info-box-text">งานขูดหินปูน</span>
                  <span class="info-box-number">4 <small>Case</small></span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
              <div class="info-box mb-3">
                <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
        
                <div class="info-box-content">
                  <span class="info-box-text">งานถอนฟัน</span>
                  <span class="info-box-number">2 <small>Case</small></span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </div>
          <div class="row">
            <div class="col-md-3">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                      <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/user5-128x128.jpg')}}" alt="User profile picture">
                      </div>
                
                      <h3 class="profile-username text-center">หมอนุ่น</h3>
                
                      <p class="text-muted text-center"></p>
                
                      <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                          <b>ปรับ</b> <a class="float-right">2 case</a>
                        </li>
                        <li class="list-group-item">
                          <b>ขูดหินปูน</b> <a class="float-right">1 case</a>
                        </li>
                        <li class="list-group-item">
                          <b>ถอนฟัน</b> <a class="float-right">1 case</a>
                        </li>
                      </ul>
                
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>
            <div class="col-md-3">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                      <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="User profile picture">
                      </div>
                
                      <h3 class="profile-username text-center">หมอแทน</h3>
                
                
                      <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>ปรับ</b> <a class="float-right"> - </a>
                          </li>
                          <li class="list-group-item">
                            <b>ขูดหินปูน</b> <a class="float-right">1 case</a>
                          </li>
                          <li class="list-group-item">
                            <b>ถอนฟัน</b> <a class="float-right">1 case</a>
                          </li>
                      </ul>
                
                   
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>
            <div class="col-md-3">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                      <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/user3-128x128.jpg')}}" alt="User profile picture">
                      </div>
                
                      <h3 class="profile-username text-center">หมอน๊อต</h3>
                
                
                      <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>ปรับ</b> <a class="float-right">2 case</a>
                          </li>
                          <li class="list-group-item">
                            <b>ขูดหินปูน</b> <a class="float-right">2 case</a>
                          </li>
                          <li class="list-group-item">
                            <b>ถอนฟัน</b> <a class="float-right">- </a>
                          </li>
                      </ul>
                
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>
            <div class="col-md-3">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                      <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" alt="User profile picture">
                      </div>
                
                      <h3 class="profile-username text-center">หมอเอ</h3>
                
                
                      <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>ปรับ</b> <a class="float-right"> - </a>
                          </li>
                          <li class="list-group-item">
                            <b>ขูดหินปูน</b> <a class="float-right">- </a>
                          </li>
                          <li class="list-group-item">
                            <b>ถอนฟัน</b> <a class="float-right">1 case</a>
                          </li>
                      </ul>
                
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>
        </div>
    </div>
</div>