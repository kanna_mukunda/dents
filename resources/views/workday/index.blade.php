@extends('layouts.app')
@section('title')
{{-- วันที่ {{$dutyTimeofDentists->thaiDate}} --}}
@endsection
@section('a_appointment_index')
    active
@endsection
@section('navigate')
    <a href="{{url('appointment/index')}}">ข้อมูลคลีนิคประจำวัน</a>
@endsection
@section('style')
<style>
    .table td {
        padding: 0.1rem !important
    }

    .list-group-item {
        position: relative;
        display: block;
        padding: .2rem 1.25rem;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .125);
    }

    .profile-user-img,
    .profile-user-img-success,
    .profile-user-img-warning,
    .profile-user-img-cancel,
    .profile-user-img-nextday,
    .profile-user-img-empty,
    .profile-user-img-info {
        border: 3px solid #adb5bd;
        margin: 0 auto;
        padding: 3px;
        width: 80px;
    }

    .profile-user-img-success {
        border: 4px solid green;
    }

    .profile-user-img-info {
        border: 4px solid lightskyblue;
    }

    .profile-user-img-warning {
        border: 4px solid yellow;
    }

    .profile-user-img-cancel {
        border: 4px solid black;
    }

    .profile-user-img-nextday {
        border: 4px solid lightcoral;
    }

    .profile-user-img-empty {
        border: 4px solid #01ff70;
    }

    .room {
        text-align: center;
        /* width: 1%; */
    }
    .table td {
    padding: 0.3rem !important;
}
div.scrollmenu {
  /* background-color: #333; */
  overflow: auto;
  white-space: nowrap;
}

div.scrollmenu .col-md-3 {
  display: inline-block;
}

div.scrollmenu .col-md-3:hover {
  background-color: #007bff;
}
.card-title{
    font-size: 20px;
    font-weight: bold
}

#slotdiv .col-md-4, #slotdiv .col-md-12{
    position: relative;
    width: 100%;
    padding-right: 0px;
    padding-left: 0px;
}
#slotdiv .col-md-4 .btn-sm {
    padding: .25rem .5rem;
    font-size: .875rem;
    line-height: 1.5;
    border-radius: 0;
}

</style>
@endsection


@section('content')

{{-- จำนวน​ Case --}}
<div class="card card-primary card-outline mb-4 collapsed-card">
    <div class="card-header">
        <div class="card-title">
            <span class="info-box-icon bg-primary rounded elevation-1 p-2"><i class="fas fa-tooth"></i></span>

            จำนวน Case
        </div>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            @foreach ($treatments as $item) 
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-cog"></i></span>

                        <div class="info-box-content">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                  <a href="#" class="nav-link h5">
                                    {{$item->treatment_name}} <span class="float-right  h5">{{$item->count}}</span>
                                  </a>
                                </li>
                              </ul> 
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            @endforeach
           
            <!-- /.col -->
        </div>
    </div>
</div>
{{-- ทัตแพทย์เวร --}}
<div class="card card-primary card-outline mb-4 collapsed-card">
    <div class="card-header">
        <div class="card-title">
            <span class="info-box-icon bg-primary rounded elevation-1 p-2"><i class="fas fa-user-md"></i></span>
             ทันตแพทย์เวร
        </div>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div><!-- /.card-header -->
    <div class="card-body">
        <div class="tab-content">
            <div class="row">
                <div class="col-md-3 offset-md-9 col-sm-6 col-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-info"><i class="fas fa-user-md"></i></span>
      
                    <div class="info-box-content">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                              <a href="#" class="nav-link h6">
                                หมอเข้าเวร <span class="float-right">{{$dutyTimeofDentists->count()}} คน</span>
                              </a>
                            </li>
                        </ul> 
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->
              </div>
           
            <div class="scrollmenu">
                {{-- {{dd($dutyTimeofDentists[0]->dentist->dent_name)}} --}}
                @foreach ($dutyTimeofDentists as $item)
                    {{-- {{dd($item)}} --}}
                    <div class="col-md-3">
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <img class="profile-user-img img-fluid img-circle"
                                        src="{{asset('adminlte/dist/img/user5-128x128.jpg')}}" alt="User profile picture">
                                </div>
    
                                <h3 class="profile-username text-center">{{$item->dentist->dent_name}}</h3>
    
                                <p class="text-muted text-center"></p>
    
                                <ul class="list-group list-group-unbordered mb-3">
                                    @foreach ($item->job_grouped as $job) 
                                        <li class="list-group-item">
                                            <b>{{$job[0]->treatment->treatment_name}}</b> <a class="float-right">{{$job['count']}} case</a>
                                        </li>
                                  
                                    @endforeach
                                   
                                </ul>
    
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                @endforeach               
            </div>
        </div>
    </div>
</div>
<div class="card card-primary card-outline">
    <div class="card-header">
        <div class="card-title">
            <span class="info-box-icon bg-primary rounded  elevation-1 p-2"><i class="fas fa-calendar-alt"></i></span>
            ข้อมูลคนไข้ที่นัดวันนี้
        </div>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" href="#appointment-table" data-toggle="tab">สรุปตารางนัดคนไข้</a></li>

            <li class="nav-item"><a class="nav-link" href="#summary" data-toggle="tab">สรุปข้อมูลการนัดแยกตามห้องตรวจ</a></li>
            @for ($i = 0; $i < $dutyTimeofDentists->count(); $i++)
                <li class="nav-item">
                    <a class="nav-link" href="#room{{$i}}" data-toggle="tab">
                        {{$dutyTimeofDentists[$i]->room->room_name}}
                    </a>
                </li>
            @endfor
        </ul>
        <br>
        <div class="tab-content">
            <div class="active tab-pane" id="appointment-table">
            <div class="card card-primary card-outline">
    <div class="card-header">
        <div class="card-title">
            <span class="info-box-icon bg-primary rounded  elevation-1 p-2"><i class="fab fa-bitcoin"></i></span>
            ตารางคำนวณค่ารักษา
        </div>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive p-0">
            <table class="table table-hover text-nowrap datatable">
              <thead>
                <tr>
                  <th>#</th>
                  {{-- <th>Invoice No.</th> --}}
                  <th>วันที่</th>
                  <th>HN</th>
                  <th>ขื่อ - สกุล คนไข้</th>
                  <th>การรักษา</th>
                  <th>ชื่อ - สกุลหมอ</th>
                  {{-- <th>เวลา</th> --}}
                  <th>สถานะ</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                  <?php $c = 1; ?>
                  @foreach ($jobs as $item)
                  <tr>
                    <td>{{$c++}}</td>
                    {{-- <td>{{'inv-'.$item->date.$item->patient_id.$item->treatment_id}}</td> --}}
                    <td>{{$item->date}}</td>
                    <td>{{$item->patient->hn}}</td>
                    <td>{{$item->patient->f_name}}  {{$item->patient->l_name}}</td>
                    <td>{{$item->treatment->treatment_name}}</td>
                    <td>{{$item->dentist->dent_name}}</td>
                    {{-- <td>{{$item->convertSlotToTime($item->slot_start, $item->slot_end, $item->period_type)}}</td> --}}
                    <td class="text-center">
                        <span class="btn {{$item->status == 'complete' ? 'btn-outline-success' :'btn-outline-danger'}} ">
                                {{$item->changeJobStatus($item->status)}}
                        </span>
                    </td>
                    <td>
                
                            <a href="{{url('appointment/view/'.$item->patient->hn)}}" class="btn btn-warning ">ดูข้อมูล</a>
                       
                    </td>
                    </tr>
                   
                  @endforeach
                
              </tbody>
            </table>
          </div>
        </table>
    </div>
  </div>
            </div>
            <div class="tab-pane" id="summary">
                <div class="row">
                    @foreach ($dutyTimeofDentists as $dent)
                    <div class="col-md-4">
                
                        <div class="info-box bg-warning">
                            <span class="info-box-icon bg-warning"><i class="far fa-copy"></i></span>
              
                            <div class="info-box-content">
                              <h3>ห้องตรวจ {{$dent['room']->room_name}}</h3>
                            </div>
                          </div>
                
                
                        <div class="card collapsed-card">
                            <div class="card-header">
                                <h2 class="card-title">ช่วงเช้า 09:00 - 12:00</h2>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse"
                                        data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:40%">เวลา</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                
                                        @foreach ($dent['morningSlotChunk_3'] as $morning)
                                        <tr>
                                            <td style="word-spacing :.1em;">
                                                <?php 
                                                        $lastHour = $morning[2]['min_l_slot']/60 == 1 ? $morning[2]['hour'] +1 : $morning[2]['hour'];
                                                        $lastMin  = $morning[2]['min_l_slot']/60 == 1 ?  "00" : $morning[2]['min_l_slot'];
                                                        $firstHour = $morning[0]['hour'] < 10 ? '0'.$morning[0]['hour'] : $morning[0]['hour'];
                                                        $firstMin = $morning[0]['min_f_slot'] < 10 ? '0'.$morning[0]['min_f_slot'] : $morning[0]['min_f_slot'];
                                                      ?>
                                                {{$firstHour}}:{{$firstMin}}
                                                -
                                                {{$lastHour}}:{{$lastMin }}
                                            </td>
                                            <td>
                
                                                <div class="row" id="slotdiv">
                                                    @foreach ($morning as $item)
                                                    <?php 
                                                        $rowSpanCount = collect($morning)->filter(function($val, $key){
                                                            if($key == 'chunk'){
                                                                return $val == [];
                                                            }
                                                           
                                                        });
                                                    ?>
                                                    @if($rowSpanCount->count() > 0)
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default btn-sm btn-block" href="javascript:void(0)">
                                                            <b>-</b>
                                                            <div>ว่าง</div>
                                                        </a>
                                                    </div>
                                                    <?php break; ?>
                                                    @else
                
                                                    @if (isset($item['index']))
                                                    <div class="col-md-4">
                                                        @if ($item['attribute']['treatment_name'] != '')
                                                            <a class="btn {{$item['attribute']['btn_type']}} btn-sm btn-block" href="script:void(0)"
                                                                style="cursor:not-allowed">
                                                                <b>{{$item['attribute']['patient_name']}}</b>
                                                                <div>{{$item['attribute']['treatment_name']}}</div>
                                                            </a>
                                                        @else
                                                            <a class="btn {{$item['attribute']['btn_type']}} btn-sm btn-block" href="#">
                                                                <b>-</b>
                                                                <div>ว่าง</div>
                                                            </a>
                                                        @endif
                                                    </div>
                                                    @endif
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                
                                    </tbody>
                                </table>
                            </div>
                
                        </div>
                
                        {{-- //////////////////////   ช่วงบ่าย  ///////////////////////// --}}
                        <div class="card collapsed-card">
                            <div class="card-header">
                                <h2 class="card-title">ช่วงบ่าย 13:00 - 16:00</h2>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse"
                                        data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:40%">เวลา</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                
                                        @foreach ($dent['eveningSlotChunk_3'] as $morning)
                                        <tr>
                                            <td style="word-spacing :.1em;">
                                                <?php 
                                                            $lastHour = $morning[2]['min_l_slot']/60 == 1 ? $morning[2]['hour'] +1 : $morning[2]['hour'];
                                                            $lastMin  = $morning[2]['min_l_slot']/60 == 1 ?  "00" : $morning[2]['min_l_slot'];
                                                            $firstHour = $morning[0]['hour'] < 10 ? '0'.$morning[0]['hour'] : $morning[0]['hour'];
                                                            $firstMin = $morning[0]['min_f_slot'] < 10 ? '0'.$morning[0]['min_f_slot'] : $morning[0]['min_f_slot'];
                                                          ?>
                                                {{$firstHour}}:{{$firstMin}}
                                                -
                                                {{$lastHour}}:{{$lastMin }}
                                            </td>
                                            <td>
                
                                                <div class="row" id="slotdiv">
                                                    @foreach ($morning as $item)
                                                    <?php 
                                                            $rowSpanCount = collect($morning)->filter(function($val, $key){
                                                                if($key == 'chunk'){
                                                                    return $val == [];
                                                                }
                                                               
                                                            });
                                                        ?>
                                                    @if($rowSpanCount->count() > 0)
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default btn-sm btn-block" href="javascript:void(0)"
                                                            style="cursor:not-allowed">
                                                            <b>-</b>
                                                            <div>ว่าง</div>
                                                        </a>
                                                    </div>
                                                    <?php break; ?>
                                                    @else
                
                                                    @if (isset($item['index']))
                                                    <div class="col-md-4">
                                                        <a class="btn {{$item['attribute']['btn_type']}} btn-sm btn-block" href="#">
                                                            <b>{{$item['attribute']['patient_name']}}</b>
                                                            <div>{{$item['attribute']['treatment_name']}}</div>
                                                        </a>
                                                    </div>
                                                    @endif
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                
                                    </tbody>
                                </table>
                            </div>
                
                        </div>
                
                        {{-- //////////////////////   ช่วงค่ำ ///////////////////////// --}}
                        <div class="card collapsed-card">
                            <div class="card-header">
                                <h2 class="card-title">ช่วงค่ำ 1700 - 20:00</h2>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse"
                                        data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:40%">เวลา</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                
                                        @foreach ($dent['nightSlotChunk_3'] as $morning)
                                        <tr>
                                            <td style="word-spacing :.1em;">
                                                <?php 
                                                            $lastHour = $morning[2]['min_l_slot']/60 == 1 ? $morning[2]['hour'] +1 : $morning[2]['hour'];
                                                            $lastMin  = $morning[2]['min_l_slot']/60 == 1 ?  "00" : $morning[2]['min_l_slot'];
                                                            $firstHour = $morning[0]['hour'] < 10 ? '0'.$morning[0]['hour'] : $morning[0]['hour'];
                                                            $firstMin = $morning[0]['min_f_slot'] < 10 ? '0'.$morning[0]['min_f_slot'] : $morning[0]['min_f_slot'];
                                                          ?>
                                                {{$firstHour}}:{{$firstMin}}
                                                -
                                                {{$lastHour}}:{{$lastMin }}
                                            </td>
                                            <td>
                
                                                <div class="row" id="slotdiv">
                                                    @foreach ($morning as $item)
                                                    <?php 
                                                            $rowSpanCount = collect($morning)->filter(function($val, $key){
                                                                if($key == 'chunk'){
                                                                    return $val == [];
                                                                }
                                                               
                                                            });
                                                        ?>
                                                    @if($rowSpanCount->count() > 0)
                                                    <div class="col-md-12">
                                                        <a class="btn btn-default btn-sm btn-block" href="javascript:void(0)"
                                                            style="cursor:not-allowed">
                                                            <b>-</b>
                                                            <div>ว่าง</div>
                                                        </a>
                                                    </div>
                                                    <?php break; ?>
                                                    @else
                
                                                    @if (isset($item['index']))
                                                    <div class="col-md-4">
                                                        <a class="btn {{$item['attribute']['btn_type']}} btn-sm btn-block" href="#">
                                                            <b>{{$item['attribute']['patient_name']}}</b>
                                                            <div>{{$item['attribute']['treatment_name']}}</div>
                                                        </a>
                                                    </div>
                                                    @endif
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                
                                    </tbody>
                                </table>
                            </div>
                
                        </div>
                    </div>
                    @endforeach
                
                </div>
                {{-- <table class="table table-striped projects">
                    <thead>
                        <tr>
                            <th style="width: 1%">
                                #
                            </th>
                            <th style="width: 10%">
        
                            </th>
                            <th class="text-center" style="width:89%" colspan="4">
                                ห้องตรวจ
                            </th>
        
                            <th>
        
                            </th>
                        </tr>
                        <tr>
                            <th style="width: 1%">
        
                            </th>
                            <th style="width: 10%">
                                เวลา
                            </th>
                            @for($i = 0; $i < $dutyTimeofDentists->count(); $i++)
                                <th class="room">ห้อง {{$dutyTimeofDentists[$i]->room->room_name}}</th>
                            @endfor
                          
                        </tr>
                    </thead>
                    <tbody>

                
                       
                        @for ($i = 0; $i < 36; $i++) 
                            <?php 
                                $btn_type_0= $dutyTimeofDentists[0]['morningSlot'][$i]['attribute']['btn_type'];
                                $btn_type_1= $dutyTimeofDentists[1]['morningSlot'][$i]['attribute']['btn_type'];
                                $patient_name_0 = "-";
                                $patient_name_1 = "-";
                                $treatment_name_0 = "ว่าง";
                                $treatment_name_1 = "ว่าง";

                                if($dutyTimeofDentists[0]['morningSlot'][$i]['attribute']['treatment_name'] != ""){
                                    $attribute = $dutyTimeofDentists[0]['morningSlot'][$i]['attribute'];
                                    $patient_name_0 = $dutyTimeofDentists[0]->morningSlot[$i]['attribute']['patient_name'];
                                    $treatment_name_0   = $attribute['treatment_name'];
                                    $btn_type_0         = $attribute['btn_type'];
                        
                                }
                                if($dutyTimeofDentists[1]['morningSlot'][$i]['attribute']['treatment_name'] != ""){
                                    $attribute = $dutyTimeofDentists[1]['morningSlot'][$i]['attribute'];
                                    $patient_name_1 = $dutyTimeofDentists[1]->morningSlot[$i]['attribute']['patient_name'];
                                    $treatment_name_1   = $attribute['treatment_name'];
                                    $btn_type_1         = $attribute['btn_type'];
                        
                                }

                            
                            ?>
                            <tr>
                                <td>{{ $i }}</td>
                                <td class="text-center">{{$dutyTimeofDentists[0]->morningSlot[$i]['hour'].":".$dutyTimeofDentists[0]->morningSlot[$i]['min']}}</td>
                                <td class="text-center">
                                    <a class="btn {{$btn_type_0}} btn-sm btn-block" href="#">
                                        <b>{{$patient_name_0}}</b>
                                        <div>{{$treatment_name_0}}</div>
                                    </a>
                                    
                                </td>
                                <td class="text-center">
                                    <a class="btn {{$btn_type_1}} btn-sm btn-block" href="#">
                                        <b>{{$patient_name_1}}</b>
                                        <div>{{$treatment_name_1}}</div>
                                    </a>
                                    
                                </td>
                            </tr>

                        @endfor
                    </tbody>
                </table> --}}
            </div>

            {{-- rooom1 --}}
            @foreach ($dutyTimeofDentists as $key => $item)
            <div class="tab-pane" id="room{{$key}}">
                {{-- <h3>ข้อมูลตารางนัดคนไข้ ห้องที่ 1</h3> --}}
                <div class="card card-warning card-outline mb-4">
                    <div class="card-header">
                        <h3 class="card-title">
                            หมอเข้าเวรประจำห้อง {{$item->room->room_name}}
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="col-md-3">
                            <div class="card card-primary card-outline">
                                <div class="card-body box-profile">
                                    <div class="text-center">
                                        <img class="profile-user-img img-fluid img-circle"
                                            src="{{asset('adminlte/dist/img/user5-128x128.jpg')}}" alt="User profile picture">
                                    </div>
        
                                    <h3 class="profile-username text-center">{{$item->dentist->dent_name}}</h3>
        
                                    <p class="text-muted text-center"></p>
        
                                    <ul class="list-group list-group-unbordered mb-3">
                                        @foreach ($item->job_grouped as $job) 
                                            <li class="list-group-item">
                                                <b>{{$job[0]->treatment->treatment_name}}</b> <a class="float-right">{{$job['count']}} case</a>
                                            </li>
                                      
                                        @endforeach
                                       
                                    </ul>
        
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                      
                    </div>
                    <!-- /.card-body -->
                </div>
                <div class="card card-outline card-warning">
                    <div class="card-header">
                        <div class="card-title">คนไข้ที่ทำการรักษา</div>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped projects">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 1%">
                                        #
                                    </th>
                                    <th class="text-center" style="width: 10%">เวลา</th>
                                    <th class="text-center">ชื่อคนไข้</th>
                                    <th class="text-center">เบอร์ติดต่อ</th>
                                    <th class="text-center">อาการ</th>
                                    <th class="text-center">ผู้รับผิดชอบ</th>
                                    <th class="text-center">วันที่ทำรายการ</th>
                                    <th class="text-center">สถานะ</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $c = 1; ?>
                                @foreach ($item->infoByRoom as  $item)
                                    
                                <tr>
                                    
                                    <td class="text-center" style="width: 1%">
                                        {{$c++}}
                                    </td>
                                    <td class="text-center" style="width: 10%">{{$item['firstHourMin'] ."-". $item['lastHourMin']}}</td>
                                    <td class="text-center">{{$item['patient_name']}}</td>
                                    <td class="text-center">{{$item['patient_phone']}}</td>
                                    <td class="text-center">{{$item['treatment']}}</td>
                                    <td class="text-center">{{$item['dent_name']}}</td>
                                    <td class="text-center">{{$item['created_at']}}</td>
                                    <td class="text-center">{{$item['status']}}</td>
                                    <td>
                                        <a href="{{url('appointment/edit/'.$item['job_id'])}}" class="btn btn-warning">แก้ไข</a>
                                        <a href="{{url('appointment/destroy/'.$item['job_id'])}}" class="btn btn-danger">ลบ</a>
                                    </td>
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endforeach
                
          
        </div><!--tab-content-->
        
    </div>

</div>




@endsection
