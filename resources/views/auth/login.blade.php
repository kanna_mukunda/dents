<!doctype html>
<html lang="en">

<head>



    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css2?family=Prompt:wght@100;200;300&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('fonts/icomoon/style.css')}}">

    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <!-- Style -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <title>เข้าสู่ระบบ2 กลางเมืองทันตกรรม</title>
</head>
<style>
div,
body,
h1,
h2,
h3,
h4,
h5 h6 {
    font-family: 'Prompt', sans-serif !important;
}

.card-title .p-2 {
    padding: .5rem .7rem !important;
}
</style>

<body>


    <div class="d-lg-flex half">
        <div class="bg order-1 order-md-2" style="background-image: url('{{asset('images/clinic8.jpg')}}');width: 100%;
    height: 100%;
    max-height: 100%;
    max-width: 100%;
    /* background-size: cover; */
    /* position: absolute; */
    top: 50%;
    left: 50%;
    /* transform: translate(-50%, -100%); */
    /* filter: blur(5px); */">
        </div>

        <div class="contents order-2 order-md-2">

            <div class="container">

                <div class="row align-items-center justify-content-center">
                    <div class="col-md-7">
                        <h3>ล็อกอินเข้าสู่ <strong><br>กลางเมืองทันตกรรม</strong></h3>
                        <!-- <p class="mb-4">Lorem ipsum dolor sit amet elit. Sapiente sit aut eos consectetur adipisicing.
                        </p> -->
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group first">
                                <!-- <label for="username">Username</label> -->
                                <label for="email" class="col-md-4 col-form-label ">{{ __('อีเมล') }}</label>
                                <!-- <input type="text" class="form-control" placeholder="your-email@gmail.com" id="username"> -->
                                <input id="email" type="email" placeholder="ใส่อีเมลของคุณ" id="username"
                                    class="form-control @error('email') is-invalid @enderror" name="email"
                                    value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group last mb-3">
                                <!-- <label for="password">Password</label> -->
                                <label for="password" class="col-md-6 col-form-label ">{{ __('รหัสผ่าน') }}</label>

                                <!-- <input type="password" class="form-control" placeholder="Your Password" id="password"> -->
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror"
                                    placeholder="ใส่รหัสผ่านของคุณ" id="password" name="password" required
                                    autocomplete="current-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="d-flex mb-5 align-items-center">
                                {{-- <label class="control control--checkbox mb-0"><span class="caption">จดจำฉัน</span>
                                    <input type="checkbox" checked="checked" />
                                    <div class="control__indicator"></div>
                                </label> --}}
                                <span class="ml-auto"><a href="#" class="forgot-pass">ลืมรหัสผ่าน</a></span>
                            </div>

                            <input type="submit" value="เข้าสู่ระบบ" class="btn btn-block btn-primary">
                            <br>
                            <div class="d-flex mb-5 align-items-center">
                                <br>
                                <!-- <span class="ml-auto"><a href="#" class="register">ลงทะเบียน</a></span> -->
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>



</body>

</html>