@extends('layouts.app')
@section('title')
 ข้อมูลหมอ
@endsection
@section('dentist')
active
@endsection

@section('navigate')
<a href="{{url('dentist/index')}}">ข้อมูลหมอ</a>
@endsection
@section('content')
<div class="row">
  <div class="col-12">
    <a href="{{url('dentist/create')}}" class="btn btn-primary col-md-2 mb-3">เพิ่มข้อมูลของหมอ</a>

    <div class="card">
      <div class="card-header">
        <h3 class="card-title">ตารางข้อมูลของหมอ</h3>

        <div class="card-tools">
          <form method="GET" action="{{ url('/dentist/index') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
            <div class="input-group">
              <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
              <span class="input-group-append">
                <button class="btn btn-secondary" type="submit">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap">
          <thead>
            <tr>
              <th>#</th>
              <th>ชื่อ - สกุลหมอ</th>
              <th>เบอร์โทรศัพท์</th>
              <th>อีเมลล์</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php $c = 1; ?>
            @foreach ($dentists as $item)
            <tr>
              <td>{{$c++}}</td>
              <td>{{$item->dent_name}}</td>
              <td>{{$item->phone}}</td>
              <td>{{$item->email}}</td>
              <td>
                <a href="{{url('dentist/show/' . $item->id) }}" title="View Dentist"><button class="btn btn-info btn-sm">ดู</button></a>
                <a href="{{url('dentist/edit/'.$item->id)}}" class="btn btn-warning btn-sm">แก้ไข</a>
                <a href="{{url('dentist/destroy/'.$item->id)}}" class="btn btn-danger btn-sm">ลบ</a>
              </td>
            </tr>

            @endforeach

          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
</div>
@endsection