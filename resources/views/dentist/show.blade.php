
@extends('layouts.app')
@section('title')
 ดูข้อมูลหมอ
@endsection
@section('dentist')
active
@endsection
@section('navigate')
<a href="{{url('dentist/index')}}">ข้อมูลหมอ</a>
@endsection
@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header"> {{$dentist[0]->dent_name}}</div>
                <div class="card-body">

                    {{-- <a href="{{ url('/treament_type/treatment-type') }}" title="Back"><button
                        class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                        Back</button></a> --}}
                    <a href="{{url('dentist/edit/'.$dentist[0]->id)}}"
                        title="Edit TreatmentType"><button class="btn btn-warning btn-sm"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i>แก้ไข</button></a>

                    {{-- <form method="POST" action="{{ url('treament_type/treatmenttype' . '/' . $treatmenttype->id) }}"
                    accept-charset="UTF-8" style="display:inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger btn-sm" title="Delete TreatmentType"
                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                            aria-hidden="true"></i> Delete</button>
                    </form> --}}
                    <br />
                    <br />

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>ชื่อหมอ</th>
                                    <td>{{$dentist[0]->dent_name}}</td>
                                </tr>
                                <tr>
                                    <th> เบอร์โทรศัพท์ </th>
                                    <td> {{$dentist[0]->phone}} </td>
                                </tr>
                                <tr>
                                    <th> อีเมล</th>
                                    <td> {{$dentist[0]->email}} </td>
                                </tr>
                            </tbody>
                        </table>
                        
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover text-nowrap">
                            <thead>
                              <tr class="text-center">
                                <th>ชื่อการรักษา</th>
                                <th>อัตราส่วนเวลาต่อการรักษา (1x5นาที)</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($treatment_skill_ratios as $item)
                              <tr>
                                <td  class="text-center">{{collect($item->treat)->isEmpty() ? '' : $item->treat->treatment_name }}</td>
                                <td class="text-center">{{$item->ratio }}</td>
                              </tr>
                  
                              @endforeach
                  
                            </tbody>
                          </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection



