@extends('layouts.app')
@section('title')
 เพิ่มข้อมูลหมอ
@endsection
@section('dentist')
active
@endsection
@section('navigate')
<a href="{{url('dentist/index')}}">ข้อมูลหมอ</a>
@endsection
@section('content')

<div class="card">
    <div class="card-header">
      <h3 class="card-title">เพิ่มข้อมูลของหมอ</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form role="form" action="{{ url('dentist/store')}}" method="post">
        @csrf
        <div class="row">
          <div class="col-sm-12">
            <!-- text input -->
            <div class="form-group">
              <label>ชื่อ - สกุลหมอ {!! $errors->first('dent_name', '<span class="help-block text-red ml-3">(:message)</span>') !!}
              </label>
              <input type="text" class="form-control"  name="dent_name" id="dent_name">
            </div>

          </div>
       
          <div class="col-sm-12">
            <div class="form-group">
              <label>เบอร์โทรศัพท์</label>
              <input type="text" class="form-control"  name="phone" id="phone">
            </div>
          </div>

          <div class="col-sm-12">
            <div class="form-group">
              <label>อีเมลล์</label>
              <input type="text" class="form-control"  name="email" id="email">
            </div>
          </div>

          <button type="submit"class="btn btn-success col-md-2 col-xs-12 col-lg-2">บันทึก</button>

      </form>
    </div>
    <!-- /.card-body -->
  </div>
@endsection