@extends('layouts.print')

@section('content')
<div class="invoice p-3 mb-3">
    <!-- title row -->
    <div class="row">
        <div class="col-12">
            <h4>
                <i class="fas fa-globe"></i> AdminLTE, Inc.
                <small class="float-right">วันที่: {{$invoices[0]->date}}</small>
            </h4>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            From
            <address>
                <strong>Admin, Inc.</strong><br>
                795 Folsom Ave, Suite 600<br>
                San Francisco, CA 94107<br>
                Phone: (804) 123-5432<br>
                Email: info@almasaeedstudio.com
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            To
            <address>
                <strong>John Doe</strong><br>
                795 Folsom Ave, Suite 600<br>
                San Francisco, CA 94107<br>
                Phone: (555) 539-1037<br>
                Email: john.doe@example.com
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <b>Invoice #{{ $invoices[0]->bill }}</b><br>
            <br>
            <b>Order ID:</b> 4F3S8J<br>
            <b>Payment Due:</b>{{$invoices[0]->date}} <br>
            <b>Account:</b> 968-34567
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>การรักษา</th>
                        <th>ราคา</th>
                        <th>จำนวน(หน่วย)</th>
                        <th>รายละเอียด</th>
                        <th>เป็นเงิน(บาท)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $total = 0;
                        $n= 1;
                    ?>
                    @foreach($invoices as $key => $invoice)
                        <tr>
                            <td>{{$n++}}</td>
                            <td>{{$invoice->treatment->treatment_name}}</td>
                            <td>{{number_format($invoice->cost, 2)}}</td>
                            <td>{{$invoice->t_amount}}</td>
                            <td>{{$invoice->detail}}</td>
                            <td>{{number_format($invoice->cost,2)}}</td>
                            <?php $total += $total + $invoice->cost; ?>
                        </tr>
                    @endforeach
                      
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- accepted payments column -->
        <div class="col-6"> </div>
        <!-- /.col -->
        <div class="col-6">
            {{-- <p class="lead">Amount Due {{$invoices[0]->date}}</p> --}}

            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        
                        <tr>
                            <th>รวม:</th>
                            <td>{{number_format($total, 2)}} บาท</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-12">
            <a href="invoice-print.html" class="btn btn-default"><i class="fas fa-print"></i> Print</a>
            {{-- <button type="button" class="btn btn-success float-right"><i class="far fa-credit-card"></i> Submit
                Payment
            </button>
            <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                <i class="fas fa-download"></i> Generate PDF
            </button> --}}
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript"> 
  window.addEventListener("load", window.print());
  window.onafterprint = function(){
    window.close()
}
</script>
@endsection
