@extends('layouts.app')

@section('title')
ประจำเดือน พฤษภาคม 2563
@endsection
@section('navigate')
<a href="{{ url('dutytime/index') }}">จัดการเวรหมอ</a>
@endsection

@section('style')
<!-- fullCalendar -->
<meta name="csrf-token" id="csrf_token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="http://cdn.jsdelivr.net/npm/fullcalendar@5.4.0/main.min.css">

<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/core/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/daygrid/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/timegrid/main.min.css') }}">
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
    integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
    crossorigin="anonymous" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

<style>
    .list-group-item {
        position: relative;
        display: block;
        padding: .3rem 1.25rem;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .125);
    }

    .hidden {
        display: none
    }

    .show {
        display: block;
    }

    .bgred {
        background-color: gray;
    }

    .disabled {
        cursor: not-allowed;

    }
    table{
        width:100%;
        table-layout: auto;

    }
    td.hour {
        min-width: 100px;
        max-width: 100px;
        overflow: hidden;
     }

     #clinic_day_off_lists_div{
         /*min-height:210px*/
     }

     .center_div {
        margin-top:1%;
    }

</style>

@endsection


@section('content')
<div class="card">
    {{-- <div class="card-header"></div> --}}
    <div class="card-body">
        <div class="row">

            <div class="col-12">
                <div id='calendar'></div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">เพิ่มตารางเวรของหมอ</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body  ">
            <div class="card card-info">
                <form role="form" action="{{ url('dutytime/store')}}" method="post">
                    @csrf
                    <!-- /.card-header -->
                    <div class="card-body">
                        @csrf

                        <div class="row">
                          <div class="col-md-3">
                              <!-- text input -->
                              <div class="form-group">
                                  <label class="label-control">วันที่</label>
                                  <input id="inputdatepicker" name="date" class="dateselected form-control">
                              </div>
                          </div>
                          <div class="col-sm-6 ">
                              <div class="form-group">
                                  <label class="label-control">ชื่อ - สกุลหมอ</label>
                                  <select class="form-control" name="dent_id" id="dent_id">
                                      <option value="">เลือก..</option>
                                      @foreach ($dents as $dent)
                                      <option value="{{$dent->id}}">{{$dent->dent_name}}</option>
                                      @endforeach
                                  </select>
                              </div>
                          </div>
                          <div class="col-sm-3">
                              <div class="form-group">
                                <label class="label-control">ประจำหัองตรวจ</label>
                                <select class="form-control" name="room_id" id="room_id">
                                    <option value="">เลือก..</option>
                                    @foreach ($rooms as $item)
                                    <option value="{{$item->id}}">{{$item->room_name}}</option>
                                    @endforeach
                                </select>
                              </div>
                          </div>
                        </div> <!--row -->

                        <div class="row">
                            <div class="col-md-2 col-xs-12 col-lg-2 text-center">
                                <label for="date_book" class="control-label">{{ 'ทั้งวัน' }}</label>
                                <input class="form-control"  name="allday" type="checkbox" id="allday" >
                            </div>
                            <div class="col-md-3 col-xs-12 col-lg-3">
                                <label for="date_book" class="control-label">{{ 'จาก' }}</label>
                                <input class="form-control timepicker"  name="start_time" type="text" id="start_time" value="">
                            </div>
                            <div class="col-md-3 col-xs-12 col-lg-3">
                                <label for="date_book" class="control-label">{{ 'ถึง' }}</label>
                                <input class="form-control timepicker"  name="end_time" type="text" id="end_time" value="">
                            </div>
                            <div class="col-md-2">
                                <label for="date_book" class="control-label">{{ 'รับคนไข้' }}</label>
                                <input type="text" class="form-control" name="patient_no_am" value="100">
                              </div>
                           
                           
                            <div class="col-md-1 col-xs-12 col-lg-2 pt-2">
                                <br>
                                <a href="" class="btn btn-info"><i class="fa fa-plus"></i></a>
                            </div>
                        </div>

                        <div class="row mt-3">
                          <button type="submit" class="btn btn-success btn-block offset-md-9 col-md-3">บันทึก</button>
                        </div>
                    
            
                </form>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script src="http://cdn.jsdelivr.net/npm/fullcalendar@5.4.0/main.min.js"></script>
{{-- <script src="{{ asset('fullcalendar/core@4.4.0/main.min.js') }}"></script> --}}
<script src="{{ asset('fullcalendar/core@4.4.0/locales-all.min.js') }}"></script>
<script src="{{ asset('fullcalendar/interaction@4.4.0/main.min.js') }}"></script>
<script src="{{ asset('fullcalendar/daygrid@4.4.0/main.min.js') }}"></script>
<script src="{{ asset('fullcalendar/daygrid@4.4.0/main.min.js') }}"></script>
<script src="{{ asset('fullcalendar/interaction@4.4.0/main.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
    integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.th.min.js"
    integrity="sha512-cp+S0Bkyv7xKBSbmjJR0K7va0cor7vHYhETzm2Jy//ZTQDUvugH/byC4eWuTii9o5HN9msulx2zqhEXWau20Dg=="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.th.min.js" integrity="sha512-cp+S0Bkyv7xKBSbmjJR0K7va0cor7vHYhETzm2Jy//ZTQDUvugH/byC4eWuTii9o5HN9msulx2zqhEXWau20Dg==" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: true,
            language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
            thaiyear: true //Set เป็นปี พ.ศ.
        }).datepicker("setDate", "0"); //กำหนดเป็นวันปัจุบัน

        $('.clinic_day_off_datepicker').datepicker({
            format: 'dd-mm-yyyy',
            todayBtn: true,
            thaiyear: true ,
            language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        }).datepicker("setDate", "0"); //กำหนดเป็นวันปัจุบัน


        var dateNow = new Date();

        $('.timepicker').datetimepicker({
            format: 'HH:mm',
            stepping: 30,
            enabledHours: [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
            //defaultDate:moment(dateNow).hours(0).minutes(0).seconds(0).milliseconds(0)

        })
    });

</script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
    var Calendar = FullCalendar.Calendar;

    var calendarEl = document.getElementById('calendar');

    var calendar = new Calendar(calendarEl, {
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth'
    },
    dateClick: function (info) {
        let date = info.dateStr.split('-')
        $('.dateselected').val(`${date[2]}-${date[1]}-${date[0]} `)
        $('#modal').modal()

        console.log(info.dateStr)

        // window.location.href = '../appointment/find_dutytime_dents/' + info.dateStr

    },
    eventClick: function(info) {
        console.log('Event: ' + info.event.extendedProps.id);

        // change the border color just for fun
        info.el.style.borderColor = 'red';
    },
    locale: 'th',
    editable: true,
    droppable: true, // this allows things to be dropped onto the calendar
    drop: function(info) {
      // is the "remove after drop" checkbox checked?
      if (checkbox.checked) {
        // if so, remove the element from the "Draggable Events" list
        info.draggedEl.parentNode.removeChild(info.draggedEl);
      }
    },
    events:[
        {
        id: 1,
        title: 'วันหยุดคลินิก',
        color: '#378006',
        start: '2020-11-01',
        },
        {
        id: 2,
        title: 'วันหยุดคลินิก',
        color: '#378006',
        start: '2020-11-08',
        },
        {
        id: 3,
        title: 'วันหยุดคลินิก',
        color: '#378006',
        start: '2020-11-15',
        },
        {
        id: 4,
        title: 'วันหยุดคลินิก',
        color: '#378006',
        start: '2020-11-19',
        },
        {
        id: 5,
        title: 'วันหยุดคลินิก',
        color: '#378006',
        start: '2020-11-20',
        },
        {
        id: 6,
        title: 'วันหยุดคลินิก',
        color: '#378006',
        start: '2020-11-22',
        },
        {
        id: 7,
        title: 'วันหยุดคลินิก',
        start: '2020-11-29',
        color: '#378006'
        },
        {
            title:'หมอโย',
            start: '2020-11-02',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-02',
        },
        {
            title:'หมอเอ',
            start: '2020-11-02',
        },
        {
            title:'หมอแทน',
            start: '2020-11-02',
        },
        {
            title:'หมอโย',
            start: '2020-11-03',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-03',
        },
        {
            title:'หมอเอ',
            start: '2020-11-03',
        },
        {
            title:'หมอแทน',
            start: '2020-11-03',
        },
        {
            title:'หมอโย',
            start: '2020-11-04',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-04',
        },
        {
            title:'หมอเอ',
            start: '2020-11-04',
        },
        {
            title:'หมอแทน',
            start: '2020-11-04',
        },
        {
            title:'หมอโย',
            start: '2020-11-05',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-05',
        },
        {
            title:'หมอเอ',
            start: '2020-11-05',
        },
        {
            title:'หมอโย',
            start: '2020-11-06',
        },
        {
            title:'หมอเอ',
            start: '2020-11-06',
        },               
        {
            title:'หมอโย',
            start: '2020-11-07',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-07',
        },
        {
            title:'หมอเอ',
            start: '2020-11-07',
        },
        {
            title:'หมอแทน',
            start: '2020-11-07',
        }, 
        {
            title:'หมอโย',
            start: '2020-11-09',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-09',
        },
        {
            title:'หมอเอ',
            start: '2020-11-09',
        },
        {
            title:'หมอแทน',
            start: '2020-11-09',
        },
        {
            title:'หมอโย',
            start: '2020-11-10',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-10',
        },
        {
            title:'หมอเอ',
            start: '2020-11-10',
        },
        {
            title:'หมอแทน',
            start: '2020-11-10',
        },
        {
            title:'หมอโย',
            start: '2020-11-11',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-11',
        },
        {
            title:'หมอเอ',
            start: '2020-11-11',
        },
        {
            title:'หมอแทน',
            start: '2020-11-11',
        },
        {
            title:'หมอโย',
            start: '2020-11-12',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-12',
        },
        {
            title:'หมอเอ',
            start: '2020-11-12',
        },
        {
            title:'หมอแทน',
            start: '2020-11-12',
        },
        {
            title:'หมอโย',
            start: '2020-11-13',
        },
        {
            title:'หมอเอ',
            start: '2020-11-13',
        },
        {
            title:'หมอแทน',
            start: '2020-11-13',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-14',
        },
        {
            title:'หมอเอ',
            start: '2020-11-14',
        },
        {
            title:'หมอแทน',
            start: '2020-11-14',
        },
        {
            title:'หมอโย',
            start: '2020-11-16',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-16',
        },
        {
            title:'หมอเอ',
            start: '2020-11-16',
        },
        {
            title:'หมอแทน',
            start: '2020-11-16',
        },
        {
            title:'หมอโย',
            start: '2020-11-17',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-17',
        },
        {
            title:'หมอเอ',
            start: '2020-11-17',
        },
        {
            title:'หมอแทน',
            start: '2020-11-17',
        },
        {
            title:'หมอโย',
            start: '2020-11-18',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-18',
        },
        {
            title:'หมอเอ',
            start: '2020-11-18',
        },
        {
            title:'หมอแทน',
            start: '2020-11-18',
        },
        {
            title:'หมอโย',
            start: '2020-11-21',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-21',
        },
        {
            title:'หมอเอ',
            start: '2020-11-21',
        },
        {
            title:'หมอแทน',
            start: '2020-11-21',
        },
        {
            title:'หมอโย',
            start: '2020-11-23',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-23',
        },
        {
            title:'หมอเอ',
            start: '2020-11-23',
        },
        {
            title:'หมอแทน',
            start: '2020-11-23',
        },
        {
            title:'หมอโย',
            start: '2020-11-24',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-24',
        },
        {
            title:'หมอเอ',
            start: '2020-11-24',
        },
        {
            title:'หมอแทน',
            start: '2020-11-24',
        },
        {
            title:'หมอโย',
            start: '2020-11-25',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-25',
        },
        {
            title:'หมอเอ',
            start: '2020-11-25',
        },
        {
            title:'หมอแทน',
            start: '2020-11-25',
        },
        {
            title:'หมอโย',
            start: '2020-11-26',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-26',
        },
        {
            title:'หมอเอ',
            start: '2020-11-26',
        },
        {
            title:'หมอแทน',
            start: '2020-11-26',
        },
        {
            title:'หมอโย',
            start: '2020-11-27',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-27',
        },
        {
            title:'หมอเอ',
            start: '2020-11-27',
        },
        {
            title:'หมอแทน',
            start: '2020-11-27',
        },
        {
            title:'หมอโย',
            start: '2020-11-28',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-28',
        },
        {
            title:'หมอเอ',
            start: '2020-11-28',
        },
        {
            title:'หมอแทน',
            start: '2020-11-28',
        },
        {
            title:'หมอโย',
            start: '2020-11-30',
        },
        {
            title:'หมอม่อน',
            start: '2020-11-30',
        },
        {
            title:'หมอเอ',
            start: '2020-11-30',
        },
        {
            title:'หมอแทน',
            start: '2020-11-30',
        },
    ],
    eventClick: function(info) {
        window.location.href = '/calendar/dutytime_table'
    }
  });

  calendar.render();
});
    
$('#allday').change(function(){
        if(this.checked){
            $('#start_time').val('09:00')
            $('#end_time').val('20:00')
        }else{
            $('#start_time').val('')
            $('#end_time').val('')
        }
    });

    

    $('#addEventBtn').click(function () {
        showHiddenModal();
    });

    function showHiddenModal() {
        if ($('#myModal').hasClass('hidden')) {
            $('#myModal').removeClass('hidden')
            $('#myModal').addClass('show')
        } else {
            $('#myModal').removeClass('show')
            $('#myModal').addClass('hidden')
        }
    }

    $('.close').click(function () {
        showHiddenModal()
    })
        let i=0;

    $('#clinic_day_off_add_btn').click(function(){
        //เพิ่มรายการ วันหยุดคลินิค
        let day_off_name = $('#day_off_name').val();
        let day_off_to_date = $('#day_off_to_date').val();
        let day_off_from_date = $('#day_off_from_date').val();
        let day_off_to_time = $('#day_off_to_time').val();
        let day_off_from_time = $('#day_off_from_time').val();
        let day_off_dent_id = $('#day_off_dent_id option:selected').val()
        let day_off_dent_name = $('#day_off_dent_id option:selected').text()
        let text = `
            <tr>
                <td style='width:30%'>
                    <input type="text" class="form-control" name="day_off[${i}]['name']" value="${day_off_name}">
                </td>
                <td>
                    <input type="text" class="form-control" name="day_off[${i}]['dent_name']" value="${day_off_dent_name}">
                    <input type="hidden" class="form-control" name="day_off[${i}]['dent_id']" value="${day_off_dent_id}">
                </td>
                <td>
                    <div class="row">
                    <input type="text" class="form-control col-md-7" name="day_off[${i}]['from_date']" value="${day_off_from_date}">

                    <input type="text" class="form-control col-md-5" name="day_off[${i}]['from_time']" value="${day_off_from_time}">
                    </div>
                </td>
                <td>
                    <div class="row">
                    <input type="text" class="form-control col-md-7" name="day_off[${i}]['to_date']" value="${day_off_to_date}">

                    <input type="text" class="form-control col-md-5" name="day_off[${i}]['to_time']" value="${day_off_to_time}">
                    </div>
                </td>

                <td><a href="javascript:void(0)" class="btn btn-danger btn-sm clinic_day_off_lists_del_btn"> ลบ </a></td>

            </tr>

        `;
        $('#clinic_day_off_lists').append(text)
        i++;
    });

    $('body').delegate('.clinic_day_off_lists_del_btn', 'click', function(){
        $(this).parent().parent().remove();
    });

$(function() {

  $('input[name="datefilter"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
  });

  $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
  });

  $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });




});


        $(document).ready(function(){
           $('.fc-dayGridMonth-button').text('เดือน')
           $('.fc-today-button').text('วันนี้')
           $('.fc-toolbar-title').text(' พฤษภาคม 2563')
        });
  

</script>
@endsection

