@extends('layouts.app')

@section('title')
ปฏิทินประจำเดือน
@endsection
@section('navigate')
<a href="{{ url('dutytime/index') }}">กำหนดปฏิทินประจำเดือน</a>
@endsection

@section('style')
<!-- fullCalendar -->
<meta name="csrf-token" id="csrf_token" content="{{ csrf_token() }}" />

<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/core/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/daygrid/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/timegrid/main.min.css') }}">
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
    integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
    crossorigin="anonymous" />
<style>
    .list-group-item {
        position: relative;
        display: block;
        padding: .3rem 1.25rem;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .125);
    }

    .hidden {
        display: none
    }

    .show {
        display: block;
    }

    .bgred {
        background-color: gray;
    }

    .disabled {
        cursor: not-allowed;

    }
    table{
        width:100%;
        table-layout: auto;

    }
    td.hour {
        min-width: 100px;
        max-width: 100px;
        overflow: hidden;
     }

</style>

@endsection


@section('content')

<div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title">Latest Orders</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">            
                <div class="table-responsive ">
                  <table class="table table-bordered m-0">
                    <thead>
                    <tr>
                        <th>วันที่</th>
                        @foreach($dutytimeEachDate[0]['dutytimeOfDent'] as $key => $hr)
                            <th class="text-center">  
                                {{$hr['hr'] < 10 ? '0'.$hr['hr'] : $hr['hr']}}:00
                            </th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($dutytimeEachDate as $key => $date)
                            <tr>
                                <td class="bg-warning">
                                    {{$date['date']}}
                                </td>

                                @foreach($date['dutytimeOfDent'] as $key => $hour)
                                    @if(collect($hour['dents'])->count() > 0)
                                        <td class="hour"> 
                                            @foreach($hour['dents'] as $key => $dent)
                                                <a href="#" class="btn btn-block btn-xs btn-outline-info">{{$dent['Dentist']->dent_name}}</a>
                                            @endforeach
                                        </td>
                                    @else
                                        <td onclick="alert()" class="bg-default"></td>
                                    @endif
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              
            </div>

<div class="card">
    <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
    </div>
    <!-- /.card-header -->
    <div class="card-body pt-0">
        <div class="text-center">
            <a href="javascript:void(0)" id="addEventBtn" class="col-md-2 btn btn-success">Add new event</a>
        </div>
        <div class="row">
            <div class=" col-12">
                <div id="calendar" style="width: 100%"></div>
            </div>
            {{-- <div class="col-6"></div> --}}
        </div>
    </div><!-- /.card-body -->
</div>
{{-- Modal --}}

<div class="modal hidden" id="myModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">กำหนดปฏิทินประจำเดือน</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="{{ url('calendar/store') }}" method="post">
                    @csrf
                    <div class="form-grop  row mb-1">
                        <label class="col-sm-5 form-label">title</label>
                        <input type="text" class="col-sm-7 form-control" value="" name="title" id="title">
                    </div>

                    <div class="form-grop  row mb-1">
                        <label class="col-sm-2 form-label">จากวันที่</label>
                        <input id="inputdatepicker" name="start_date" class="datepicker col-sm-4 form-control"
                            data-date-format="mm/dd/yyyy">
                        <label class="col-sm-2 form-label">เวลา</label>
                        <input id="inputdatepicker" name="start_time" class="datepicker col-sm-4 form-control"
                            data-date-format="mm/dd/yyyy">

                    </div>
                    <div class="form-grop  row mb-1">
                        <label class="col-sm-2 form-label">ถึงวันที่</label>
                        <input id="inputdatepicker" name="end_date" class="datepicker col-sm-4 form-control"
                            data-date-format="mm/dd/yyyy">
                        <label class="col-sm-2 form-label">เวลา</label>
                        <input id="inputdatepicker" name="end_time" class="timepicker col-sm-4 form-control"
                            data-date-format="mm/dd/yyyy">

                    </div>
                    <div class="form-grop  row mb-1">
                        <label class="col-sm-5 form-label">สถานะ</label>
                        <select class="form-control col-sm-7" name="status">
                            <option>เลือก..</option>
                            <option value="active">Active</option>
                            <option value="inactive">InActive</option>
                        </select>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success confirm_btn">บันทึก</button>
            </div>
            </form>

        </div>
        <!--modal-content-->
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('fullcalendar/core@4.4.0/main.min.js') }}"></script>
<script src="{{ asset('fullcalendar/core@4.4.0/locales-all.min.js') }}"></script>
<script src="{{ asset('fullcalendar/interaction@4.4.0/main.min.js') }}"></script>
<script src="{{ asset('fullcalendar/daygrid@4.4.0/main.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
    integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.th.min.js"
    integrity="sha512-cp+S0Bkyv7xKBSbmjJR0K7va0cor7vHYhETzm2Jy//ZTQDUvugH/byC4eWuTii9o5HN9msulx2zqhEXWau20Dg=="
    crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: true,
            language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
            thaiyear: true //Set เป็นปี พ.ศ.
        }).datepicker("setDate", "0"); //กำหนดเป็นวันปัจุบัน
    });

</script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        let addEvents;
        var calendarEl = document.getElementById('calendar');
        $.get('/calendar/addEvents')
            .done(function (data) {
                addEvents = data
                // console.log(addEvents)

                var calendar = new FullCalendar.Calendar(calendarEl, {
                    dateClick: function (info) {


                    },
                    plugins: ["interaction", "dayGrid", "timeGrid", "resourceTimeline"],
                    initialView: 'dayGridMonth',
                    selectable: true,
                    editable: true,
                    unselectAuto: true,
                    dropable: true,
                    eventResizableFromStart: true,
                    //events: addEvents,
                    events: [{
                            end: "2020-10-11",
                            id: 4,
                            start: "2020-10-11",
                            title: "หมอโอ 10:00 - 12:00",
                            color: '#000000',
                            textColor: '#ffffff',
                            type: 'workday'
                        },
                        {
                            end: "2020-10-23",
                            id: 4,
                            start: "2020-10-23",
                            title: "วันปิยะมหาราช",
                            color: 'blue',
                            textColor: '#ffffff',
                            type: 'holiday'
                        }
                    ],

                    locale: 'th',
                });

                calendar.render()

            });


        // console.log(add)
        // window.location = '/settings/patient_per_day/index' 
    });

    $('#addEventBtn').click(function () {
        showHiddenModal();
    });

    function showHiddenModal() {
        if ($('#myModal').hasClass('hidden')) {
            $('#myModal').removeClass('hidden')
            $('#myModal').addClass('show')
        } else {
            $('#myModal').removeClass('show')
            $('#myModal').addClass('hidden')
        }
    }

    $('.close').click(function () {
        showHiddenModal()
    })




    //   var calendar = new FullCalendar.Calendar(calendarEl, {
    //     dateClick: function(info) {
    //         var params= {
    //             id:1
    //         }
    //         $.get('/calendar/addEvents/', params)
    //                 .done(function (data) {
    //                     console.log('data',data)
    //                     addEvents = data
    //                     // window.location = '/settings/patient_per_day/index' 
    //                 })

    //     },
    //     plugins: ["interaction", "dayGrid", "timeGrid", "resourceTimeline"],
    //     initialView: 'dayGridMonth',
    //     selectable: true,
    //     editable: true,
    //     unselectAuto: true,
    //     dropable: true,
    //     eventResizableFromStart:true,
    //     events: addEvents,


    //   });

    //   calendar.render();
    // });
    //   var date = new Date();
    // var d;
    // var m;
    // var y;
    //   var calendarEl = document.getElementById("calendar");
    //   var calendar = new FullCalendar.Calendar(calendarEl, {

    //     plugins: ["interaction", "dayGrid", "timeGrid", "resourceTimeline"],
    //     header: {
    //       left: "prev,next today",
    //       center: "title",
    //       right: ""//"dayGridMonth,timeGridWeek,timeGridDay"
    //     },
    //     editable: true,
    //     navLinks: true,
    //     selectable: true,
    //     navLinkDayClick: function(date, jsEvent) {

    //         jsEvent.preventDefault();
    //         d = date.getDate();
    //         m = date.getMonth();
    //         y = date.getFullYear();
    //         var thDate = date.toLocaleDateString('th-TH', { timeZone: 'UTC' })
    //         $('#date_selected').val(thDate)
    //         console.log(date.toLocaleDateString('th-TH', { timeZone: 'UTC' }))
    //         showHiddenModal()
    //     //    window.location = '/appointment/index/'+day
    //     //     console.log('day', date);
    //     //     console.log('coords', jsEvent.pageX, jsEvent.pageY);
    //     },
    //     locale: 'th',

    //   });


    //   calendar.render();


    //   $('.confirm_btn').click(function (event) {
    //         event.preventDefault();
    //         showHiddenModal()
    //         let _token = $('#csrf_token').attr('content')

    //         let params = {
    //             d: d,
    //             m: m,
    //             y: y,
    //             from: $('#fromtime').val(),
    //             to: $('#totime').val(),
    //             patient_no: $('#patient_no').val(),
    //             _token: _token

    //         };
    //         console.log('params',params)
    //         $.post('/settings/patient_per_day/store', params)
    //             .done(function (data) {
    //                 console.log('data',data)
    //                 window.location = '/settings/patient_per_day/index' 
    //             })
    //     })


    //   function showHiddenModal() {
    //         if ($('#myModal').hasClass('hidden')) {
    //             $('#myModal').removeClass('hidden')
    //             $('#myModal').addClass('show')
    //         } else {
    //             $('#myModal').removeClass('show')
    //             $('#myModal').addClass('hidden')
    //         }
    //     }

    //     $('.close').click(function () {
    //         showHiddenModal()
    //     })

    //     $('#fromtime').change(function(){
    //         var from = $(this).val()
    //         console.log(from)
    //         if(from == 'all'){
    //             $('#totime').val('all')
    //         }
    //     })

</script>
@endsection
