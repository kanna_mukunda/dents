@extends('layouts.app')

@section('title')
patial
@endsection
@section('navigate')
<a href="{{ url('dutytime/index') }}">กำหนดปฏิทินประจำเดือน</a>
@endsection

@section('style')
<!-- fullCalendar -->
<meta name="csrf-token" id="csrf_token" content="{{ csrf_token() }}" />

<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/core/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/daygrid/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/timegrid/main.min.css') }}">
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
    integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
    crossorigin="anonymous" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

<style>
    .list-group-item {
        position: relative;
        display: block;
        padding: .3rem 1.25rem;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .125);
    }

    .hidden {
        display: none
    }

    .show {
        display: block;
    }

    .bgred {
        background-color: gray;
    }

    .disabled {
        cursor: not-allowed;

    }
    table{
        width:100%;
        table-layout: auto;

    }
    td.hour {
        min-width: 100px;
        max-width: 100px;
        overflow: hidden;
     }

     #clinic_day_off_lists_div{
         /*min-height:210px*/
     }

     .center_div {
        margin-top:1%;
    }

</style>

@endsection


@section('content')
<div class="card table-responsive">
    <div class="card card-body">
        <table class="table table-bordered  datatable">
            <thead>
                <tr>
                    <th>วันที่</th>
                    @for($i = 9; $i <= 20; $i++)
                        <th class="text-center">  
                            {{$i < 10 ? '0'.$i : $i}}:00
                        </th>
                    @endfor
                </tr>
            </thead>
            <tbody>
                @foreach($day_number_arr as $key => $day_number)
                
                    <tr>
                    {{dd($day_number['dayInfo']['dentInfo'])}}
                        <td>{{$day_number['day_number']}}</td>
                        @for($i = 9; $i <= 20; $i++)
                        
                            <td class="text-center {{$day_number['day_type'] == 'dayOff' ? 'bg-info' : ''}} ">  
                                @if($day_number['day_type'] == 'dayOff')
                                    {{-- วันหยุด --}}
                                    {{$day_number['day_name']}}
                                @elseif($day_number['day_type'] == 'dayWork')
                                    {{-- วันทำงาน กับวันลากิจของหมอ --}}
                                    {{-- 1.เอาข้อ มูลการเข้าเวรของหมอ มาแสดง --}}
                                    @foreach($day_number['dents']['work'] as $key => $dentWork)
                                        {{-- เชค ช่วงเช้า --}}
                                        @if($i >= $dentWork['dentInfo']['am_hour_start'] && $i <=  $dentWork['dentInfo']['am_hour_end'])
                                            <a  href="{{ url('appointment/index') }}" class="btn btn-block btn-outline-success btn-sm appointment_btn"
                                                data-hour="{{$i}}" 
                                            >
                                                w-{{ $dentWork['dentInfo']['Dentist']['dent_name'] }}
                                            </a>
                                        @endif

                                        {{-- เชค ช่วงเช้า --}}
                                        @if($i >= $dentWork['dentInfo']['ev_hour_start'] && $i <=  $dentWork['dentInfo']['ev_hour_end'])
                                            <a  href="{{ url('appointment/index') }}" class="btn btn-block btn-outline-success btn-sm appointment_btn"
                                                data-hour="{{$i}}" 
                                            >
                                                w-{{ $dentWork['dentInfo']['Dentist']['dent_name'] }}
                                            </a>
                                        @endif

                                        {{-- เชค ช่วงเช้า --}}
                                        @if($i >= $dentWork['dentInfo']['pm_hour_start'] && $i <=  $dentWork['dentInfo']['pm_hour_end'])
                                            <a  href="{{ url('appointment/index') }}" class="btn btn-block btn-outline-success btn-sm appointment_btn"
                                                data-hour="{{$i}}" 
                                            >
                                            w-{{ $dentWork['dentInfo']['Dentist']['dent_name'] }}
                                            </a>
                                        @endif
                                    @endforeach

                                    @foreach($day_number['dents']['vacation'] as $key => $dentDayOff)
                                    {{-- {{dd($dentWork['dentInfo']['Dentist']['dent_name'])}} --}}
                                        <div class="btn btn-block btn-secondary btn-sm">v-{{ $dentDayOff['dentInfo']['dent_name'] }}</div>
                                    @endforeach
                                @else
                                    {{-- วันยังว่าง --}}
                                    <div style="padding:18% 10%"  class="appointment_btn"
                                                data-hour="{{$i}}" 
                                    ></div>
                                @endif
                                
                            </td>
                        @endfor
                    </tr>
                @endforeach
                    
            </tbody>
        </table>
    </div>
</div>

@endsection



@section('script')
<script>

</script>

@endsection