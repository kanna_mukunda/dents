@extends('layouts.app')

@section('style')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.4.0/main.min.css">
@endsection

@section('title')
ประจำเดือน พฤษภาคม 2563
@endsection
@section('navigate')
<a href="{{ url('dutytime/index') }}">จัดการเวรหมอ</a>
@endsection

@section('content')
    <div id='calendar'></div>
@endsection

@section('script')
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.4.0/main.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
            timeZone: 'UTC',
            initialView: 'dayGridMonth',
            // events: 'https://fullcalendar.io/demo-events.json',
            editable: true,
            selectable: true,
            dateClick: function(info) {
            // alert('clicked ' + info.dateStr);
            },
            select: function(info) {
            // alert('selected ' + info.startStr + ' to ' + info.endStr);
                window.location.href = '/calendar/date_click';
            }
        });

        calendar.render();
        });
    </script>
@endsection