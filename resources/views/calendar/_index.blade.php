@extends('layouts.app')

@section('title')
ปฏิทินประจำเดือน
@endsection
@section('navigate')
<a href="{{ url('dutytime/index') }}">กำหนดปฏิทินประจำเดือน</a>
@endsection

@section('style')
<!-- fullCalendar -->
<meta name="csrf-token" id="csrf_token" content="{{ csrf_token() }}" />

<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/core/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/daygrid/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('fullcalendar/4.2.0/timegrid/main.min.css') }}">
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
    integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
    crossorigin="anonymous" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>

<style>
    .list-group-item {
        position: relative;
        display: block;
        padding: .3rem 1.25rem;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, .125);
    }

    .hidden {
        display: none
    }

    .show {
        display: block;
    }

    .bgred {
        background-color: gray;
    }

    .disabled {
        cursor: not-allowed;

    }
    table{
        width:100%;
        table-layout: auto;

    }
    td.hour {
        min-width: 100px;
        max-width: 100px;
        overflow: hidden;
     }

     #clinic_day_off_lists_div{
         /*min-height:210px*/
     }

     .center_div {
        margin-top:1%;
    }

</style>

@endsection


@section('content')


<div class="card card-primary card-tabs">
    <div class="card-header p-0 pt-1">
        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="calendar-main-tab" data-toggle="pill" href="#calendar-main"
                    role="tab" aria-controls="calendar-main" aria-selected="true">ปฏิทินประจำเดือน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " id="clinic-day-off-tab" data-toggle="pill" href="#clinic-day-off"
                    role="tab" aria-controls="clinic-day-off" aria-selected="false">เพิ่มวันหยุดคลีนิค / วันลาของหมอ</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="add_dent_dutytime-tab" data-toggle="pill"
                    href="#add_dent_dutytime" role="tab" aria-controls="add_dent_dutytime"
                    aria-selected="false">เพิ่มตารางเวรของหมอ</a>
            </li>
        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content" id="custom-tabs-one-tabContent">

            <div class="tab-pane fade  active show " id="calendar-main" role="tabpanel"
                aria-labelledby="calendar-main-tab">
                {{-- <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">ตารางเวร เดือน {{date('m')}}</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                    <div class="table-responsive ">
                        <table class="table table-bordered m-0">
                        <thead>
                        <tr>
                            <th>วันที่</th>
                            @foreach($dutytimeEachDate[0]['dutytimeOfDent'] as $key => $hr)
                                <th class="text-center">
                                    {{$hr['hr'] < 10 ? '0'.$hr['hr'] : $hr['hr']}}:00
                                </th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>

                            @foreach($dutytimeEachDate as $key => $date)
                                <tr>
                                    <td class="bg-warning">
                                        {{$date['date']}}
                                    </td>

                                    @foreach($date['dutytimeOfDent'] as $key => $hour)

                                        @if($hour['date_type'] != 'day_off')
                                            @if(collect($hour['dents'])->count() > 0)
                                                <td class="hour">
                                                    @foreach($hour['dents'] as $key => $dent)
                                                        <a href="#" class="btn btn-block btn-xs btn-outline-info">{{$dent['Dentist']->dent_name}}</a>
                                                    @endforeach
                                                </td>
                                            @else
                                                <td onclick="alert()" class="bg-default"></td>
                                            @endif
                                        @else
                                            <td>day off </td>
                                        @endif <!--hour['date_type'] != 'day_off' -->
                                    @endforeach
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    </div>
                    <!-- /.card-body -->

                </div> --}}
                <div class="row">
                    <div class=" col-12">
                        <div id="calendar" style="width: 100%"></div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="clinic-day-off" role="tabpanel"aria-labelledby="clinic-day-off-tab">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">รายการวันหยุด</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse"
                            data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>ชื่อวันหยุด</th>
                                <th>ชื่อหมอ</th>
                                <th>จากวันที่</th>
                                <th>ถึงวันที่</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i =1; ?>
                            @foreach($day_offs as $key => $day_off)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $day_off->title }}</td>
                                    <td>{{ $day_off->dent_id == 0 ? 'หมอทุกคน' : $day_off->dentist->dent_name }}
                                    </td>
                                    <td>{{ $day_off->start_date }}</td>
                                    <td>{{ $day_off->end_date }}</td>
                                    <td>
                                        <a href="#" class="btn btn-warning">แก้ไข</a>
                                        <a href="#" class="btn btn-danger">ลบ</a>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
            <hr>
                {{-- วันหยุดคลินิก --}}
                <div class="card card-info">
                    <form  action="{{url('calendar/store')}}" method="post">
                        @csrf
                        <div class="card-body bg-info" id="clinic_day_off_lists_div">
                            <table class="table table-stripped" >
                                <thead>
                                    <tr>
                                        <th class="text-center">ชื่อวันหยุด</th>
                                        <th class="text-center">ชื่อหมอ</td>
                                        <th class="text-center">จากวันที่</th>
                                        <th class="text-center">ถึงวันที่</th>
                                    </tr>
                                </thead>
                                <tbody id="clinic_day_off_lists">
                                </tbody>
                            </table>
                        </div>

                        <div class="card-footer text-right">
                            <button type="submit" class="btn  btn-success "><i class="fas fa-save"></i> บันทึก</button>
                        </div>
                    </form>
                </div>

                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">ตั้งค่าวันหยุด คลินิค</h3>
                    </div><!-- /.card-header -->
                    <div class="card-body"><!-- form start -->
                        <div class="row">
                            <div class="form-grop mb-2 col-md-3">
                                <label class=" form-label">ชื่อวันหยุด</label>
                                <input type="text" class="form-control" value="" name="day_off_name" id="day_off_name">
                            </div>

                            <div class="form-grop mb-2 col-md-2">
                                <label class=" form-label">หมอ</label>
                                <select class="form-control" name="day_off_dent_id" id="day_off_dent_id">
                                    <option>เลือก</option>
                                    <option value="0">หมอทุกคน</option>
                                    @foreach($dents as $key => $dent)
                                        <option value="{{$dent->id}}">{{$dent->dent_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-grop mb-2 col-md-2">
                                <label class="form-label">จากวันที่</label>
                                <input id="day_off_from_date" name="start_date" class="clinic_day_off_datepicker form-control">
                            </div>
                            <div class="form-group mb-2 col-md-1">
                                <label class="form-label">เวลา</label>
                                <input id="day_off_from_time" name="start_time" class="timepicker form-control" data-time-format="HH:mm">
                            </div>
                            <div class="form-grop mb-2 col-md-2">
                                <label class="form-label">ถึงวันที่</label>
                                <input id="day_off_to_date" name="end_date" class="clinic_day_off_datepicker form-control">
                            </div>
                            <div class="form-group mb-2 col-md-1">
                                <label class="form-label">เวลา</label>
                                <input id="day_off_to_time" name="start_time" class="timepicker form-control" data-time-format="HH:mm">
                            </div>

                            <div class="col-md-1 mb-2 center_div" style="font-size:50px; color:#f012be" id="clinic_day_off_add_btn">
                                <i class="fas fa-arrow-alt-circle-up"></i>
                            </div>
                        </div><!--row-->
                    </div>
                </div>


            </div><!-- tab calendar-main -->

            <div class="tab-pane fade" id="add_dent_dutytime" role="tabpanel" aria-labelledby="add_dent_dutytime-tab">
                {{-- จัดการตารางเวรหมอ --}}
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">ตารางเวรของหมอ</h3>

                                <div class="card-tools">
                                    <div class="input-group ">
                                        <a href="{{ url('dutytime/create') }}"
                                            class="btn btn-primary">เพิ่มข้อมูลตารางเวร</a>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover text-nowrap datatable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>วันที่</th>
                                            <th>ชื่อ - สกุลหมอ</th>
                                            <th>เริ่มเวลา</th>
                                            <th>สิ้นสุดเวลา</th>
                                            <th>จำนวนคนไข้</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $c = 1; ?>
                                        @foreach($dent_dutytimes as $item)
                                            {{-- {{dd($item) }} --}}
                                            <tr>
                                                <td>{{ $c++ }}</td>
                                                <td>{{ $item->date }}</td>
                                                <td>{{ $item->dentist->dent_name }}</td>
                                                <td>
                                                    <ul class="list-group list-group-unbordered mb-3">
                                                        @if($item->am_slot_start != [])
                                                            <li class="list-group-item">
                                                                <b>เช้า</b> <a
                                                                    class="float-right">{{ $item->am_slot_start }}</a>
                                                            </li>
                                                        @endif

                                                        @if($item->ev_slot_start != [])
                                                            <li class="list-group-item">
                                                                <b>บ่าย</b> <a
                                                                    class="float-right">{{ $item->ev_slot_start }}</a>
                                                            </li>
                                                        @endif

                                                        @if($item->pm_slot_start != [])
                                                            <li class="list-group-item">
                                                                <b>ค่ำ</b> <a
                                                                    class="float-right">{{ $item->pm_slot_start }}</a>
                                                            </li>
                                                        @endif

                                                    </ul>

                                                </td>
                                                <td>
                                                    <ul class="list-group list-group-unbordered mb-3">
                                                        @if($item->am_slot_end != [])
                                                            <li class="list-group-item">
                                                                <b>เช้า</b> <a
                                                                    class="float-right">{{ $item->am_slot_end }}</a>
                                                            </li>
                                                        @endif

                                                        @if($item->ev_slot_end != [])
                                                            <li class="list-group-item">
                                                                <b>บ่าย</b> <a
                                                                    class="float-right">{{ $item->ev_slot_end }}</a>
                                                            </li>
                                                        @endif

                                                        @if($item->pm_slot_end != [])
                                                            <li class="list-group-item">
                                                                <b>ค่ำ</b> <a
                                                                    class="float-right">{{ $item->pm_slot_end }}</a>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul class="list-group list-group-unbordered mb-3">
                                                        @if($item->am_slot_end != [])
                                                            <li class="list-group-item text-right">
                                                                {{ $item->patient_no_am }}
                                                            </li>
                                                        @endif

                                                        @if($item->ev_slot_end != [])
                                                            <li class="list-group-item text-right">
                                                                {{ $item->patient_no_ev }}
                                                            </li>
                                                        @endif

                                                        @if($item->pm_slot_end != [])
                                                            <li class="list-group-item text-right">
                                                                {{ $item->patient_no_pm }}
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </td>
                                                <td>
                                                    <a href="{{ url('dutytime/edit/'.$item->id) }}"
                                                        class="btn btn-warning">แก้ไข</a>
                                                    <a href="{{ url('dutytime/destroy/'.$item->id) }}"
                                                        class="btn btn-danger">ลบ</a>
                                                </td>
                                            </tr>

                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /.card -->
</div>



<div class="card">
    <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
    </div>
    <!-- /.card-header -->
    <div class="card-body pt-0">
        <div class="text-center">
            {{-- <a href="javascript:void(0)" id="addEventBtn" class="col-md-2 btn btn-success">Add new event</a> --}}
        </div>
        <div class="row">
            <div class=" col-12">
                {{-- <div id="calendar" style="width: 100%"></div> --}}
            </div>
            {{-- <div class="col-6"></div> --}}
        </div>
    </div><!-- /.card-body -->
</div>
{{-- Modal --}}

<div class="modal hidden" id="myModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">กำหนดปฏิทินประจำเดือน</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="{{ url('calendar/store') }}" method="post">
                    @csrf
                    <div class="form-grop  row mb-1">
                        <label class="col-sm-2 form-label">ชื่อวันหยุด</label>
                        <input type="text" class="col-sm-7 form-control" value="" name="title" id="title">
                    </div>

                    <div class="form-grop  row mb-1">
                        <label class="col-sm-2 form-label">จากวันที่</label>
                        <input id="inputdatepicker" name="start_date" class="datepicker col-sm-4 form-control">
                        <label class="col-sm-2 form-label">เวลา</label>
                        <input id="inputdatepicker" name="start_time" class="timepicker col-sm-4 form-control">

                    </div>
                    <div class="form-grop  row mb-1">
                        <label class="col-sm-2 form-label">ถึงวันที่</label>
                        <input id="inputdatepicker" name="end_date" class="datepicker col-sm-4 form-control">
                        <label class="col-sm-2 form-label">เวลา</label>
                        <input id="inputdatepicker" name="end_time" class="timepicker col-sm-4 form-control">

                    </div>
                    <div class="form-grop  row mb-1">
                        <label class="col-sm-5 form-label">สถานะ</label>
                        <select class="form-control col-sm-7" name="status">
                            <option>เลือก..</option>
                            <option value="active">Active</option>
                            <option value="inactive">InActive</option>
                        </select>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success confirm_btn">บันทึก</button>
            </div>
            </form>

        </div>
        <!--modal-content-->
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('fullcalendar/core@4.4.0/main.min.js') }}"></script>
<script src="{{ asset('fullcalendar/core@4.4.0/locales-all.min.js') }}"></script>
<script src="{{ asset('fullcalendar/interaction@4.4.0/main.min.js') }}"></script>
<script src="{{ asset('fullcalendar/daygrid@4.4.0/main.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
    integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.th.min.js"
    integrity="sha512-cp+S0Bkyv7xKBSbmjJR0K7va0cor7vHYhETzm2Jy//ZTQDUvugH/byC4eWuTii9o5HN9msulx2zqhEXWau20Dg=="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.th.min.js" integrity="sha512-cp+S0Bkyv7xKBSbmjJR0K7va0cor7vHYhETzm2Jy//ZTQDUvugH/byC4eWuTii9o5HN9msulx2zqhEXWau20Dg==" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: true,
            language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
            thaiyear: true //Set เป็นปี พ.ศ.
        }).datepicker("setDate", "0"); //กำหนดเป็นวันปัจุบัน

        $('.clinic_day_off_datepicker').datepicker({
            format: 'dd-mm-yyyy',
            todayBtn: true,
            thaiyear: true ,
            language: 'th', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        }).datepicker("setDate", "0"); //กำหนดเป็นวันปัจุบัน


        var dateNow = new Date();

        $('.timepicker').datetimepicker({
            format: 'HH:mm',
            stepping: 30,
            enabledHours: [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
            //defaultDate:moment(dateNow).hours(0).minutes(0).seconds(0).milliseconds(0)

        })
    });

</script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        let addEvents;
        var calendarEl = document.getElementById('calendar');
        $.get('./add_events')
            .done(function (data) {
                addEvents = data
                // console.log(addEvents)

                var calendar = new FullCalendar.Calendar(calendarEl, {
                    dateClick: function (info) {
                        console.log(info.dateStr)

                        // window.location.href = '../appointment/find_dutytime_dents/' + info.dateStr

                    },
                    eventClick: function(info) {
                        console.log('Event: ' + info.event.extendedProps.id);

                        // change the border color just for fun
                        info.el.style.borderColor = 'red';
                    },

                    plugins: ["interaction", "dayGrid", "timeGrid", "resourceTimeline"],
                    initialView: 'dayGridMonth',
                    selectable: true,
                    editable: true,
                    unselectAuto: true,
                    dropable: true,
                    eventResizableFromStart: true,
                    events: addEvents,
                    locale: 'th',
                });

                calendar.render()
                $('.fc-time').each(function(index){
                    $(this).text('- ')
                })

                $('.fc-title').css('color','white')
            });


        // console.log(add)
        // window.location = '/settings/patient_per_day/index'
    });

    $(document).ready(function(){
        $
    })

    $('#addEventBtn').click(function () {
        showHiddenModal();
    });

    function showHiddenModal() {
        if ($('#myModal').hasClass('hidden')) {
            $('#myModal').removeClass('hidden')
            $('#myModal').addClass('show')
        } else {
            $('#myModal').removeClass('show')
            $('#myModal').addClass('hidden')
        }
    }

    $('.close').click(function () {
        showHiddenModal()
    })
        let i=0;

    $('#clinic_day_off_add_btn').click(function(){
        //เพิ่มรายการ วันหยุดคลินิค
        let day_off_name = $('#day_off_name').val();
        let day_off_to_date = $('#day_off_to_date').val();
        let day_off_from_date = $('#day_off_from_date').val();
        let day_off_to_time = $('#day_off_to_time').val();
        let day_off_from_time = $('#day_off_from_time').val();
        let day_off_dent_id = $('#day_off_dent_id option:selected').val()
        let day_off_dent_name = $('#day_off_dent_id option:selected').text()
        let text = `
            <tr>
                <td style='width:30%'>
                    <input type="text" class="form-control" name="day_off[${i}]['name']" value="${day_off_name}">
                </td>
                <td>
                    <input type="text" class="form-control" name="day_off[${i}]['dent_name']" value="${day_off_dent_name}">
                    <input type="hidden" class="form-control" name="day_off[${i}]['dent_id']" value="${day_off_dent_id}">
                </td>
                <td>
                    <div class="row">
                    <input type="text" class="form-control col-md-7" name="day_off[${i}]['from_date']" value="${day_off_from_date}">

                    <input type="text" class="form-control col-md-5" name="day_off[${i}]['from_time']" value="${day_off_from_time}">
                    </div>
                </td>
                <td>
                    <div class="row">
                    <input type="text" class="form-control col-md-7" name="day_off[${i}]['to_date']" value="${day_off_to_date}">

                    <input type="text" class="form-control col-md-5" name="day_off[${i}]['to_time']" value="${day_off_to_time}">
                    </div>
                </td>

                <td><a href="javascript:void(0)" class="btn btn-danger btn-sm clinic_day_off_lists_del_btn"> ลบ </a></td>

            </tr>

        `;
        $('#clinic_day_off_lists').append(text)
        i++;
    });

    $('body').delegate('.clinic_day_off_lists_del_btn', 'click', function(){
        $(this).parent().parent().remove();
    });

$(function() {

  $('input[name="datefilter"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
  });

  $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
  });

  $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });




});





</script>
@endsection

